package com.selenium.commonfiles.base;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
//import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.selenium.commonfiles.util.ErrorInTestMethod;
import com.selenium.commonfiles.util.TestUtil;


public class CommonFunction_GTB extends TestBase{

	SimpleDateFormat df = new SimpleDateFormat();
	Properties CCD_Rater = new Properties();
	public Map<String,Double> Book_rate_Rater_output = new HashMap<>();
	public boolean isMTARewindFlow=false,isFPEntries=false,isMTARewindStarted=false, isNBRewindStarted = false, isNBRquoteStarted = false;
	public int actual_no_of_years=0,err_count=0,trans_error_val=0 , Can_returnP_Error=0;
	public List<String> CoversDetails_data_list = null, MD_Building_Occupancies_list=new ArrayList<>();
	public Map<String,Double> TC_Calculations = new HashMap<>();
	public boolean isIncludingHeatPresent = false,isExcludingHeatPresent = false,isIncludeHeat_10M=false;
	public double cent_work_including_heat = 0.0; 
	Date currentDate = new Date();
	public static int p_Index = 0;
	public static String Environment = null;
	public double JCT_TotalPremium = 0.0, OP_TotalPremium = 0.00, Ter_TotalPremium = 0.00; 
	public static String pas_NoOfEmp_AllOthers = null, pas_NoOfEmp_Clerical = null, pas_NoOfEmp_Drivers = null, LE_TotalWegroll = null;
	public static String Ter_BuildingContents_Sum = null;
	public double totalMD = 0.0, Ter_BI_Sum = 0.00;
	public boolean isGrossPremiumReferralCheckDone = false;
	
	public int VehiclesTotal = 0;
	public String riskFactor = "";
	
	public Map<String,Map<String,Double>> CAN_CCD_ReturnP_Values_Map = new HashMap<>();
	
	public void NewBusinessFlow(String code,String event) throws ErrorInTestMethod{
		String testName = (String)common.NB_excel_data_map.get("Automation Key");
		String navigationBy = CONFIG.getProperty("NavigationBy");
		common.currentRunningFlow = "NB";
		try{
			
			customAssert.assertTrue(common.StingrayLogin("PEN"),"Unable to login.");
			customAssert.assertTrue(common.checkClient(common.NB_excel_data_map,code,event),"Unable to check Client.");
			customAssert.assertTrue(common.createNewQuote(common.NB_excel_data_map,code,event), "Unable to create new quote.");
			customAssert.assertTrue(common.selectLatestQuote(common.NB_excel_data_map,code,event), "Unable to select quote from table.");
			customAssert.assertTrue(funcPolicyGeneral(common.NB_excel_data_map,code,event), "Policy Details function having issue .");
			customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Previous Claims"),"Issue while Navigating to Material Facts and Declarations . ");
			customAssert.assertTrue(funcPreviousClaims(common.NB_excel_data_map), "Previous claim function is having issue(S) .");
			
			customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Material Facts & Declarations"),"Issue while Navigating to Material Facts & Declarations . ");
			customAssert.assertTrue(MaterialFactsDeclerationPage(), "MaterialFactsDeclerationPage function is having issue(S) . ");
			customAssert.assertTrue(func_Referrals_MaterialFactsDeclerationPage(), "Referrals for MaterialFactsDeclerationPage function is having issue(S) . ");
			
			customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Goods in Transit"),"Issue while Navigating to Goods in Transit screen.");
			customAssert.assertTrue(GIT(common.NB_excel_data_map), "GIT function is having issue(S) . ");
			
			customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
			customAssert.assertTrue(common_HHAZ.funcPremiumSummary(common.NB_excel_data_map,code,event), "Premium Summary function is having issue(S) . ");
			
			//To cover Alternate Quote Scenario.
			if(((String)common.NB_excel_data_map.get("Q_AlternateQuote")).equalsIgnoreCase("Yes")){
				alternateQuote = true;
				common_HHAZ.PremiumFlag = false;
				customAssert.assertTrue(common_HHAZ.alternateQuote(common.NB_excel_data_map,"Quotes"), "Alternate Quote function is having issue(S) . ");
				customAssert.assertTrue(common_GTA.policyGeneralComparisionAlternateQuote(common.NB_excel_data_map), "Policy Details function having issue .");
				customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Goods in Transit"),"Issue while Navigating to Goods in Transit screen.");
				TestUtil.reportStatus("<b>Cover specific Premium table verification after alternate Quote.</b>", "Info", true);
				customAssert.assertTrue(GIT(common.NB_excel_data_map), "GIT function is having issue(S) . ");
				TestUtil.reportStatus("<b>------------------------ Alternate Quote Operation performed -----------------------------</b>", "Info", true);
				customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
				customAssert.assertTrue(common_HHAZ.funcPremiumSummary(common.NB_excel_data_map,code,event), "Premium Summary function is having issue(S) . ");
			}
			
			
			
			Assert.assertTrue(common_HHAZ.funcStatusHandling(common.NB_excel_data_map,code,"NB"));
		
			if(TestBase.businessEvent.equals("NB")){
				
				customAssert.assertEquals(err_count,0,"Errors in premium calculations . ");
				customAssert.assertEquals(trans_error_val,0,"Errors in Transaction premium calculations . ");
				customAssert.assertEquals(common.final_err_pdf_count,0,"Varification Errors in PDF Documents . ");
				customAssert.assertTrue(common.StingrayLogout(), "Unable to Logout.");
				alternateQuote = false;
				TestUtil.reportTestCasePassed(testName);
				
			}
			
		}catch(Throwable t){
			TestUtil.reportTestCaseFailed(testName, t);
			throw new ErrorInTestMethod(t.getMessage());
		}
		
	}
	
	// Cancellation starts here :

	public void CancellationFlow(String code,String event) throws ErrorInTestMethod{
		

		common_HHAZ.cancellationProcess(code,event);
	}
	
	public boolean funcPolicyGeneral(Map<Object, Object> map_data, String code, String event) {
		boolean retVal = true;
		
		try{
			
			customAssert.assertTrue(common.funcPageNavigation("Policy General", ""),"Policy General page not loaded");
			customAssert.assertTrue(k.Input("COB_PG_InsuredName", (String)map_data.get("PG_InsuredName")),	"Unable to enter value in Insured Name  field .");
			customAssert.assertTrue(!k.getAttributeIsEmpty("COB_PG_InsuredName", "value"),"Insured Name Field Should Contain Valid Name  .");
			
			customAssert.assertTrue(k.Input("COB_PG_TurnOver", (String)map_data.get("PG_TurnOver")),	"Unable to enter value in Turnover field .");
			
			String[] Prof_Bodies = ((String)map_data.get("PG_InsuredDetails_Q1")).split(",");
			List<WebElement> ul_ele = driver.findElements(By.xpath("//ul"));
			for(String pBodies_limit : Prof_Bodies){
				for(WebElement each_ul : ul_ele){
					customAssert.assertTrue(k.Click("GTB_PG_ProfBuddies"),"Error while Clicking Professional Bodies List object . ");
					k.waitTwoSeconds();
					if(each_ul.findElement(By.xpath("//li[text()='"+pBodies_limit+"']")).isDisplayed())
						each_ul.findElement(By.xpath("//li[text()='"+pBodies_limit+"']")).click();
					else
						continue;
					break;
				}
			}
			customAssert.assertTrue(k.Input("CCF_Address_CC_Address", (String) map_data.get("PG_Address")),"Unable to enter value in Address field. ");
			customAssert.assertTrue(!k.getAttributeIsEmpty("CCF_Address_CC_Address", "value"),"Address Field Should Contain Valid Address  .");
			customAssert.assertTrue(k.Input("CCF_Address_CC_line2", (String) map_data.get("PG_Line1")),"Unable to enter value in Address field line 1 . ");
			customAssert.assertTrue(k.Input("CCF_Address_CC_line3", (String) map_data.get("PG_Line2")),"Unable to enter value in Address field line 2 . ");
			customAssert.assertTrue(k.Input("CCF_Address_CC_Town", (String) map_data.get("PG_Town")),"Unable to enter value in Town field . ");
			customAssert.assertTrue(k.Input("CCF_Address_CC_County", (String) map_data.get("PG_County")),"Unable to enter value in County  . ");
			customAssert.assertTrue(k.Input("CCF_Address_CC_Postcode", (String)map_data.get("PG_Postcode")),"Unable to enter value in PostCode");
			customAssert.assertTrue(!k.getAttributeIsEmpty("CCF_Address_CC_Postcode", "value"),"PostCode Field Should Contain Valid Postcode  .");
			customAssert.assertTrue(common.validatePostCode((String)map_data.get("PG_Postcode")),"Post Code is not in Correct format .");
				
			customAssert.assertTrue(k.Input("COB_PG_BusDesc", (String)map_data.get("PG_BusDesc")),	"Unable to enter value in Provided Details field .");
			
			if(common.currentRunningFlow.equalsIgnoreCase("NB")){
				// Select Trade Code :
				
				String sValue = (String)map_data.get("PG_TCS_TradeCode_Button");
				if(sValue.contains("Yes")){
				//	customAssert.assertTrue(SelectTradeCode((String)map_data.get("PG_TCS_TradeCode") , "Policy Details" , 0,map_data),"Trade code selection function is having issue(S).");
				}
				
				customAssert.assertTrue(common.funcPageNavigation("Policy General", ""),"Policy General page not loaded");	
			}
			
			TestUtil.reportStatus("Entered all the details on Policy General page .", "Info", true);
			
			return retVal;
		
		}catch(Throwable t) {
	        String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
	        TestUtil.reportFunctionFailed("Failed in "+methodName+" function");
	        Assert.fail("Unable to to do operation on policy General page. \n", t);
	        return false;
		}
		
	}

	
	public boolean SelectTradeCode(String tradeCodeValue , String pageName , int currentPropertyIndex,Map<Object, Object> map_data) {
		
		try{
			
			customAssert.assertTrue(k.Click("GTD_SelectTradeCode"), "Unable to click on Select Trade Code button in Policy Details .");
			customAssert.assertTrue(common.funcPageNavigation("Trade Code Selection", ""), "Navigation problem to Trade Code Selection page .");
			
			String sVal = tradeCodeValue;
			
			customAssert.assertTrue(k.Input("GTD_TC_SearchField", sVal), "Unable to inputTrade Code value");
			k.waitFiveSeconds();
			customAssert.assertTrue(k.Click("GTD_TradeCode"), "Unable to click on Select Trade Code button in Policy Details .");
			
	 		return true;
	 		
		}catch(Throwable t){
			return false;
		}
	}
	
	public boolean MaterialFactsDeclerationPage(){
		boolean retValue = true;
		Map<Object, Object> data_map = new HashMap<>();
		switch(common.currentRunningFlow){
			case "NB":
				data_map = common.NB_excel_data_map;
				break;
			case "MTA":
				data_map = common.MTA_excel_data_map;
				break;
			case "Renewal":
				data_map = common.Renewal_excel_data_map;
				break;
			case "Rewind":
				data_map = common.Rewind_excel_data_map;
				break;
			case "Requote":
				data_map = common.Requote_excel_data_map;
				break;
		
		}
		try{
			 customAssert.assertTrue(common.funcPageNavigation("Material Facts & Declarations", ""),"Material Facts & Declarations page is having issue(S)");
			 k.ImplicitWaitOff();
			 String q_value = null;
			 
			List<WebElement> elements = driver.findElements(By.className("selectinput"));
			 Select sel = null;
			
			 for(int i = 0;i<elements.size();i++){
				 if(elements.get(i).isDisplayed()){
					 
					 sel = new Select(elements.get(i));
					 try{
						 q_value = (String)data_map.get("MFD_Q"+(i+1));
						 
						 sel.selectByVisibleText(q_value);
						 }
					 catch(Throwable t){
						 
						 try{
							 sel.selectByVisibleText("No");
						 }catch(Throwable t1){
							 
						 }
						 
					 } 
				 }		 
			 }
			 
							 
			 List<WebElement> txt_Elements = driver.findElements(By.className("write"));
			 q_value = (String)data_map.get("MFD_T");
			 WebElement ws = null;
			 
			 for(int i = 0;i<txt_Elements.size();i++){
				 ws = txt_Elements.get(i);
				 if(ws.isDisplayed()){
					 ws.sendKeys(q_value);
				 }			 
			 }
			 
			 customAssert.assertTrue(common.funcButtonSelection("Save"), "Unable to click on Save Button on Material Facts and Declarations Screen .");
			 
			 return retValue;
			 
		}catch(Throwable t){
			k.ImplicitWaitOn();
			String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
	        TestUtil.reportFunctionFailed("Failed in "+methodName+" function");     
	        return false;
			}
		finally{
			 k.ImplicitWaitOn();
		 }
		
	}
	
	public boolean funcClaimsExperience(Map<Object, Object> map_data){

	    boolean retvalue = true;
	    
	      try {    
	    	  	//customAssert.assertTrue(common.funcMenuSelection("Navigate", "Claims Experience"),"Unable To navigate to Claims Experience screen");
				customAssert.assertTrue(common.funcPageNavigation("Claims Experience",""), "Claims Experience Page Navigation issue . ");
				TestUtil.reportStatus("Verified Claims Experience page .", "Info", true);
				//customAssert.assertTrue(common.funcButtonSelection("Next"), "Unable to click on Next Button on Claims Experience .");
				return retvalue;
	             
	      } catch(Throwable t) {
	             String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
	          TestUtil.reportFunctionFailed("Failed in "+methodName+" function");     k.reportErr("Failed in "+methodName+" function", t);
	          Assert.fail("Unable to enter details in ClaimsExperience Page", t);
	          return false;
	      }


	}
	
	public boolean funcPreviousClaims(Map<Object, Object> map_data){

	    boolean retvalue = true;
	    
	      try {    
	    	  	//customAssert.assertTrue(common.funcMenuSelection("Navigate", "Previous Claims"),"Unable To navigate to Previous Claims scren");
				customAssert.assertTrue(common.funcPageNavigation("Previous Claims",""), "Previous Claims Page Navigation issue . ");
				TestUtil.reportStatus("Verified Previous Claims page .", "Info", true);
				//customAssert.assertTrue(common.funcButtonSelection("next"), "Unable to click on Next Button on Previous Claims .");
				return retvalue;
	             
	      } catch(Throwable t) {
	             String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
	          TestUtil.reportFunctionFailed("Failed in "+methodName+" function");     k.reportErr("Failed in "+methodName+" function", t);
	          Assert.fail("Unable to handle Previous Claims Page", t);
	          return false;
	      }


	}
	
	/**
	 * 
	 * @param map_data
	 * @return retValue
	 * Insert and verify data on GIT Screen
	 */
public boolean GIT(Map<Object, Object> map_data)
{
	boolean retValue = true;
	VehiclesTotal = 0;
	try
	{
		customAssert.assertTrue(common.funcPageNavigation("Goods in Transit", ""),"Goods in Transit page is having issue(S)");
				
		Map<String, List<Map<String, String>>> internal_data_map = new HashMap<>();
		
		switch (common.currentRunningFlow )
		{
			case "NB" :
				internal_data_map = common.NB_Structure_of_InnerPagesMaps;
				break;
			
			case "MTA" :
				internal_data_map = common.MTA_Structure_of_InnerPagesMaps;
				break;
				
			case "Rewind" :
				internal_data_map = common.Rewind_Structure_of_InnerPagesMaps;
				break;
				
			case "Requote" :
				internal_data_map = common.Requote_Structure_of_InnerPagesMaps;
				break;
			case "Renewal" :
				internal_data_map = common.Renewal_Structure_of_InnerPagesMaps;
				break;
		}		
		
		
		if(!common.currentRunningFlow.equalsIgnoreCase("NB")){

			JavascriptExecutor j_exe = (JavascriptExecutor) driver;
			j_exe.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[@id='gtb_tot']")));
			common_HHAZ.GTB_Premium = driver.findElement(By.xpath("//*[@id='gtb_tot']")).getAttribute("value").replaceAll(",", "");

			if(TestBase.businessEvent.equalsIgnoreCase("MTA") && ((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes")){

				try {	  
					switch((String)map_data.get("MTA_Operation")) {

					case "AP":
					case "RP":

						String _cover = common_EP.AP_RP_Cover_Key.split("-")[1];

						if(!_cover.contains("GoodsinTransitRSAROI")) {
							common.MTA_excel_data_map.put("PS_GoodsinTransitRSAROI_NetNetPremium", common_HHAZ.GTB_Premium);
							TestUtil.reportStatus("Business Interruption Net Net Premium captured successfully . ", "Info", true);
							return true;
						}
						break;

					case "Non-Financial":

						common.MTA_excel_data_map.put("PS_GoodsinTransitRSAROI_NetNetPremium", common_HHAZ.GTB_Premium);
						TestUtil.reportStatus("Due to Non-Financial Flow, Only Business Interruption Net Net Premium captured  . ", "Info", true);
						return true;

					}
				}catch(NullPointerException npe) {

				}
			}
			customAssert.assertTrue(common_HHAZ.deleteItems(),"Delete Items function is having issues.");
		}
		
		if(!alternateQuote){
			
			// Target goods :
			
			String[] target_Goods = ((String)map_data.get("GIT_TargetGoods")).split(";");
			List<WebElement> ul_elements = driver.findElements(By.xpath("//ul"));
			for(String t_Goods : target_Goods)
			{
				for(WebElement each_ul : ul_elements)
				{
					customAssert.assertTrue(k.Click("GIT_TargetGoods"),"Error while Clicking Target goods List object . ");
					k.waitTwoSeconds();
					String xpath_List = "//*[contains(@id,'"+t_Goods+"')]";
					if(each_ul.findElement(By.xpath(xpath_List)).isDisplayed())
						each_ul.findElement(By.xpath(xpath_List)).click();
					else
						continue;
					break;
				}
			}		
			
			customAssert.assertTrue(k.DropDownSelection("GTD_GIT_SubContractor", (String)map_data.get("GIT_SubContractor")), "Unable to enter value in GIT_SubContractor field");
			customAssert.assertTrue(k.DropDownSelection("GTA_PMovers", (String)map_data.get("GIT_PrimeMovers")), "Unable to enter value in GIT_PrimeMovers field");
			customAssert.assertTrue(k.DropDownSelection("GTA_DetCover", (String)map_data.get("GIT_CoverForDet")), "Unable to enter value in GIT_CoverForDet field");
			customAssert.assertTrue(k.DropDownSelection("GTA_DamageCover", (String)map_data.get("GIT_CoverForDamage")), "Unable to enter value in GIT_CoverForDamage field");
		
			
			//customAssert.assertTrue(k.DropDownSelection("GTA_GIT_AnyOneLimit", (String)map_data.get("GIT_AnyOneClaim")), "Unable to enter value in GIT_AnyOneClaim field");
			//carriage_terms
			
			String carItems_xpath = "//table[@id='gtb_carriage_terms_table1']";
			WebElement carItems_Table = driver.findElement(By.xpath(carItems_xpath));
			
			int cItem_tble_Rows = carItems_Table.findElements(By.tagName("tr")).size();
			
			String sectionName = null,colName="";
			double totalCarr = 0.00;
			
			
			for(int i  = 0; i < cItem_tble_Rows-1; i++)
			{
				WebElement item_Name = driver.findElement(By.xpath(carItems_xpath+"/tbody/tr["+(i+1)+"]/td["+1+"]"));
				sectionName = item_Name.getText();
				
				if(sectionName.contains("RHA Terms")){
					colName = "GIT_RHA";
				}else if(sectionName.contains("BIFA Terms")){
					colName = "GIT_BIFA";
				}else if(sectionName.contains("Your own specific terms")){
					colName = "GIT_YourOwn";
				}else if(sectionName.contains("FTA")){
					colName = "GIT_FTA";
				}else if(sectionName.contains("Full responsibility")){
					colName = "GIT_FR";
				}else if(sectionName.contains("CMR")){
					colName = "GIT_CMR";
				}else if(sectionName.contains("CMR - UK ONLY")){
					colName = "GIT_CMRUK";
				}
				
				if(!sectionName.contains("Total"))
				{
					driver.findElement(By.xpath(carItems_xpath+"/tbody/tr["+(i+1)+"]/td["+2+"]/input")).clear();
					driver.findElement(By.xpath(carItems_xpath+"/tbody/tr["+(i+1)+"]/td["+2+"]/input")).sendKeys((String)map_data.get(colName+"_AddInfo"));
					driver.findElement(By.xpath(carItems_xpath+"/tbody/tr["+(i+1)+"]/td["+3+"]/input")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
					driver.findElement(By.xpath(carItems_xpath+"/tbody/tr["+(i+1)+"]/td["+3+"]/input")).sendKeys((String)map_data.get(colName+"_Proportion"));
					totalCarr = totalCarr + Double.parseDouble((String)map_data.get(colName+"_Proportion"));
				}			
				
				if(sectionName.contains("Total"))
				{
					common.funcButtonSelection("Save");
					
					WebElement totalCar = driver.findElement(By.xpath(carItems_xpath+"/tbody/tr["+(i+1)+"]/td["+3+"]/input"));
					String sTotalCar = totalCar.getAttribute("value");
					 
					double diff = totalCarr - Double.parseDouble(sTotalCar);
					if(diff==0.00){
						//
					}
				}
			}
			
			//Terms of Carriage Details		
			String tcd = (String)map_data.get("GIT_RHACarriage");
			
			if(tcd.length() > 0)
			{
							
				String[] cargo_TCDs = tcd.split(";");
	            int count = cargo_TCDs.length;
	            

	    		String carDetails_xpath = "//table[@id='table_rha_details']";
	    		
	         	for(int l=0;l<count;l++)
	        	{        		
	        		k.scrollInViewByXpath(carDetails_xpath);
	        		
	        		if(l > 0)
	        		{
	        			driver.findElement(By.xpath(carDetails_xpath+"/tbody/tr["+(l+2)+"]/td[4]/a")).click();
	        		}
	        		
	        		k.scrollInViewByXpath(carDetails_xpath);
	        		
	        		driver.findElement(By.xpath(carDetails_xpath+"/tbody/tr["+(l+1)+"]/td["+1+"]/input")).sendKeys((String)internal_data_map.get("RHA").get(l).get("RHA_Conditions"));
	        		driver.findElement(By.xpath(carDetails_xpath+"/tbody/tr["+(l+1)+"]/td["+2+"]/input")).sendKeys((String)internal_data_map.get("RHA").get(l).get("RHA_AddInfo"));
	        		
	        		driver.findElement(By.xpath(carDetails_xpath+"/tbody/tr["+(l+1)+"]/td["+3+"]/input")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
	        		driver.findElement(By.xpath(carDetails_xpath+"/tbody/tr["+(l+1)+"]/td["+3+"]/input")).sendKeys((String)internal_data_map.get("RHA").get(l).get("RHA_Proportion"));
	        	}
				
			}
			       
			String car_AddV = (String)map_data.get("GIT_OwnVehicle");
			if(car_AddV.length() > 0)
			{
				
				String[] car_AddVehicle = car_AddV.split(";");
				
				int l_Count = car_AddVehicle.length;
				
				for(int l=0;l<l_Count;l++)
				{
					k.Click("GTA_AddVehicle");
					
					DecimalFormat formatter = new DecimalFormat("#,###");
					String sVal = formatter.format(Double.parseDouble((String)internal_data_map.get("OwnV").get(l).get("OwnV_ChooseLimit")));
					customAssert.assertTrue(k.DropDownSelection("GTA_ChooseLimit", sVal), "Unable to enter value in OwnV_ChooseLimit field");
					customAssert.assertTrue(k.Input("GTA_NoOfVehicles", (String)internal_data_map.get("OwnV").get(l).get("OwnV_NoOfV")), "Unable to enter Description for OwnV_NoOfV on GIT screen.");
					customAssert.assertTrue(k.DropDownSelection("GTA_Security", (String)internal_data_map.get("OwnV").get(l).get("OwnV_DeviceAcc")), "Unable to enter value in OwnV_DeviceAcc field");
					
					int NoOfVehicles = Integer.parseInt((String)internal_data_map.get("OwnV").get(l).get("OwnV_NoOfV"));
					
					k.clickInnerButton("GTC_inner_page_locator", "Save");				
					
					VehiclesTotal = VehiclesTotal + NoOfVehicles;
				}
			}
	        
	        //customAssert.assertTrue(k.Click("GTA_ApplyBookRate"), "Unable to click on ApplyBookRate button.");
			
		}else{
			JavascriptExecutor j_exe = (JavascriptExecutor) driver;
			j_exe.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[@id='gtb_tot']")));
			common_HHAZ.GTB_Premium = driver.findElement(By.xpath("//*[@id='gtb_tot']")).getAttribute("value").replaceAll(",", "");

			if(common_HHAZ.GTB_Premium.equalsIgnoreCase("0.00") || common_HHAZ.GTB_Premium.equalsIgnoreCase("0") || common_HHAZ.GTB_Premium.equalsIgnoreCase("00")){
				TestUtil.reportStatus("Premium <b>[ "+common_HHAZ.GTB_Premium+" ]</b> is verified successfully on GIT screen after alternate quote and before clicking on Save button. ", "Info", true);
			}else{
				TestUtil.reportStatus("<p style='color:red'> Premium <b>[ "+common_HHAZ.GTB_Premium+" ]</b> should be '0' after alternate quote and before clicking on Save button. ", "Info", true);
			}
		}
		
		
		customAssert.assertTrue(common.funcButtonSelection("Save"), "Unable to click on Save Button on Material Facts and Declarations Screen .");
		
        // Cover specific calculation :
        
        common_GTA.GTABRaterCalculation(map_data, VehiclesTotal, (String)map_data.get("GIT_CoverForDamage"));
        
        //String sTablePath = "//a[text()='Apply Book Rates']//following::table[@id='table0']";
        String sTablePath = "//*[text()='Any One Occurrence']//following::table[@id='rateTable_gtb']";
		WebElement s_table= driver.findElement(By.xpath(sTablePath));
		 
		k.scrollInViewByXpath(sTablePath);
		
		/**
		 * 
		 *
		 *  To manage AP(Additional Premium) or RP(Reduced Premium) for MTA flow.
		 * 
		 * 
		 */

		String AP_RP_Flag = "";
		String s_CoverName = "GoodsinTransitRSAROI";
		if(common.currentRunningFlow.equalsIgnoreCase("MTA")){

			String AP_RP_Key = (String)map_data.get("CD_AP_RP_CoverSpecific_Decision");

			if(!(AP_RP_Key.equalsIgnoreCase(""))){
				String[] AP_RP_Array = AP_RP_Key.split(",");

				for(String cover : AP_RP_Array){

					String[] splitCoverNameFormat = cover.split("-");

					if(splitCoverNameFormat[1].equalsIgnoreCase(s_CoverName.replaceAll(" ", ""))){
						if(splitCoverNameFormat[0].equalsIgnoreCase("AP")){
							AP_RP_Flag = "AP";
							TestUtil.reportStatus("<b>------ Additional Premium for Cover - "+s_CoverName+" -------------</b>", "Info", false);
							break;
						}else if(splitCoverNameFormat[0].equalsIgnoreCase("RP")){
							AP_RP_Flag = "RP";
							TestUtil.reportStatus("<b>------- Reduced Premium for Cover - "+s_CoverName+" -------------</b>", "Info", false);
							break;
						}
					}

				}
			}
		}
		
		int totalRows = s_table.findElements(By.tagName("tr")).size();
        
		// Input to the premium table :
		String s_NoOfVehicles = "";
		int iVal = 0;
		String pOverride = "";
		
		for(int i = 0; i< totalRows-2; i++)
		{
					
				String s_Description = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[1]");
				s_NoOfVehicles = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[2]");
					
				if(!s_Description.contains("Flat Premium"))
				{
					 if(!s_NoOfVehicles.equals("0") && !s_NoOfVehicles.equals(""))
					 {								
						 
						 /**
							 * 
							 * To manage AP / RP for MTA flow.
							 * 
							 */

							if(common.currentRunningFlow.equalsIgnoreCase("MTA")){
								if(!common_HHAZ.GTB_Premium.equalsIgnoreCase("0.00")){

									if(AP_RP_Flag.equalsIgnoreCase("AP")){

										map_data.put("GIT_Poverride",common_HHAZ.GTB_Premium);

									}else if(AP_RP_Flag.equalsIgnoreCase("RP")){

										String RP_PremiumOverride = Double.toString((Double.parseDouble(common_HHAZ.GTB_Premium) / (totalRows-1)));
										map_data.put("GIT_Poverride", RP_PremiumOverride);

									}else{
										pOverride = (String)map_data.get("GIT_Poverride");
										if(pOverride.equalsIgnoreCase("")){
											pOverride = "0";
										}
									}

								}else{
									pOverride = (String)map_data.get("GIT_Poverride");
									if(pOverride.equalsIgnoreCase("")){
										pOverride = "0";
									}
								}
							}else{
								pOverride = (String)map_data.get("GIT_Poverride");
								if(pOverride.equalsIgnoreCase("")){
									pOverride = "0";
								}
							}
							/////////////////-----------------****Input To Application*****----------------------//////////////////////////// 
							pOverride = (String)map_data.get("GIT_Poverride");
						 
							if(s_Description.contains("Own Goods Extension"))
							{
								driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
								driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input")).sendKeys((String)map_data.get("GIT_TechAdjust"));
								
								if(alternateQuote){
									
									double GIT_TechAdjust = Double.parseDouble((String)map_data.get("GIT_TechAdjust"))+Double.parseDouble((String)map_data.get("GIT_TechAdjust"));
									driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
									driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input")).sendKeys(Double.toString(GIT_TechAdjust));
									map_data.put("GIT_TechAdjust",Double.toString(GIT_TechAdjust));
								}
							}
							else
							{
								driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
								driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input")).sendKeys((String)internal_data_map.get("OwnV").get(iVal).get("OwnV_TechAdjust"));
								internal_data_map.get("OwnV").get(iVal).put("OwnV_Poverride",pOverride);
								
								if(alternateQuote){
									
									double OwnV_TechAdjust = Double.parseDouble((String)internal_data_map.get("OwnV").get(iVal).get("OwnV_TechAdjust"))+Double.parseDouble((String)internal_data_map.get("OwnV").get(iVal).get("OwnV_TechAdjust"));
									driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
									driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input")).sendKeys(Double.toString(OwnV_TechAdjust));
									internal_data_map.get("OwnV").get(iVal).put("OwnV_TechAdjust", Double.toString(OwnV_TechAdjust));

								}
								
								iVal = iVal+1;
							}
							
							driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input")).sendKeys(Keys.TAB);
							
							//if(!pOverride.equals("0"))
							{
								driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[9]/input")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
								driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[9]/input")).sendKeys(pOverride);
							}
							
							driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[9]/input")).sendKeys(Keys.TAB);
							
					 }else{
						 driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[9]/input")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
						 driver.findElement(By.xpath(sTablePath+"/tbody/tr["+(i+1)+"]/td[9]/input")).sendKeys("0");
					 }
				}
		}
				
		
		// Calculation :
		 int iVar = 0;
		 double s_TotalP = 0.00, c_TotalP = 0.00, c_Premium = 0.00;
		 
		for(int i = 0; i< totalRows-2; i++)
		{					 
			 //Read values from screen :
			 
			 s_NoOfVehicles = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[2]");
			 String s_Description = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[1]");
			 
			 if(!s_Description.contains("Flat Premium"))
			 {
				 if(!s_NoOfVehicles.equals("0") && !s_NoOfVehicles.equals(""))
				 {						
						String s_InitialRate = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[3]/input");
					 	String s_AutoAdjust = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[4]/input");
					 	String s_BookRate = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[5]/input");
					 	String s_BookP = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[6]/input");
					 	String s_TechAdjust = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input");
					 	String s_RevisedP = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[8]/input");
					 	String s_pOverride = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[9]/input");
					 	String s_Premium = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[10]/input");
					 	
					 	s_TotalP = s_TotalP + Double.parseDouble(s_Premium);
					 	
					 	//Calculation :				
					 	
					 	double c_InitialRate = 0.00, c_AutoAdjust = 0.00, c_BookRate = 0.00, c_BookP =0.00, c_POverride;
					 	double c_TechAdjust =0.00;
					 	if(s_Description.contains("Own Goods Extension"))
					 	{
					 		c_InitialRate = common_GTA.Book_rate_Rater_output.get("OGE_Initial Rate");
					 		c_AutoAdjust = common_GTA.Book_rate_Rater_output.get("OGE_Auto-Adjustment (%)");
					 		c_BookRate = c_InitialRate;
					 		c_BookP = c_BookRate;
					 		
					 		c_POverride = Double.parseDouble((String)map_data.get("GIT_Poverride"));
					 		if(TestBase.businessEvent.equalsIgnoreCase("MTA") && ((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes"))
							{
					 			if(pOverride!="0")
								{
									c_POverride = Double.parseDouble(pOverride);
								}
								else
								{
									c_POverride = Double.parseDouble(s_pOverride);
								}
							}
					 		
					 		
					 		c_TechAdjust = Double.parseDouble((String)map_data.get("GIT_TechAdjust"));
					 	}
					 	else
					 	{
					 		c_InitialRate = common_GTA.Book_rate_Rater_output.get("VL_"+s_Description);
					 		c_AutoAdjust = common_GTA.Book_rate_Rater_output.get("Auto-Adjustment (%)");
					 		c_BookRate = c_InitialRate + (c_InitialRate * (c_AutoAdjust/100));
					 		c_BookP = c_BookRate*(Double.parseDouble(s_NoOfVehicles));
					 		
					 		c_POverride = Double.parseDouble((String)internal_data_map.get("OwnV").get(iVar).get("OwnV_Poverride"));
					 		if(TestBase.businessEvent.equalsIgnoreCase("MTA") && ((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes"))
							{
					 			if(pOverride!="0")
								{
									c_POverride = Double.parseDouble(pOverride);
								}
								else
								{
									c_POverride = Double.parseDouble(s_pOverride);
								}
							}					 		
					 		
					 		c_TechAdjust = Double.parseDouble((String)internal_data_map.get("OwnV").get(iVar).get("OwnV_TechAdjust"));
					 		iVar = iVar+1;
					 	}					 						 	
					 			
					 	
					 	double c_RevisedP = c_BookP + (c_BookP*(c_TechAdjust/100));
					 	double c_TempP = c_RevisedP;
					 	
					 	if(c_POverride > 0.00)
					 	{
					 		c_Premium = c_POverride;
					 	}
					 	else
					 	{
					 		c_Premium = c_TempP;
					 	}
					 	
					 	c_TotalP = c_TotalP + c_Premium;
					 	
					 	//Compare values:					 						 	
					 	CommonFunction.compareValues(c_InitialRate ,  Double.parseDouble(s_InitialRate) ,"InitialRate for Limit  "+s_Description +" of GIT cover");
					 	CommonFunction.compareValues(c_AutoAdjust ,  Double.parseDouble(s_AutoAdjust) ,"AutoAdjust for Limit  "+s_Description +" of GIT cover");
					 	CommonFunction.compareValues(c_BookRate ,  Double.parseDouble(s_BookRate) ,"BookRate for Limit  "+s_Description  +" of GIT cover");
					 	CommonFunction.compareValues(c_BookP ,  Double.parseDouble(s_BookP) ,"BookP for Limit  "+s_Description  +" of GIT cover");
					 	CommonFunction.compareValues(c_TechAdjust ,  Double.parseDouble(s_TechAdjust) ,"TechAdjust for Limit  "+s_Description  +" of GIT cover");
					 	CommonFunction.compareValues(c_RevisedP ,  Double.parseDouble(s_RevisedP) ,"RevisedP for Limit  "+s_Description  +" of GIT cover");
					 	CommonFunction.compareValues(c_POverride ,  Double.parseDouble(s_pOverride) ,"POverride for Limit  "+s_Description  +" of GIT cover");
					 	CommonFunction.compareValues(c_Premium ,  Double.parseDouble(s_Premium) ,"Premium for Limit  "+s_Description  +" of GIT cover");
				} 
			 }
		}
				 
		CommonFunction.compareValues(c_TotalP , s_TotalP ,"Total Premium for activity of GIT cover");
		
		TestUtil.WriteDataToXl(CommonFunction.product+"_"+common.currentRunningFlow, "Premium Summary", (String)map_data.get("Automation Key"), "PS_GoodsinTransitRSAROI_NetNetPremium", String.valueOf(c_TotalP), map_data);
				 
        TestUtil.reportStatus("All details are added and verified successfully on GIT screen. ", "Info", true);
			
        return retValue;
		 
	}
	catch(Throwable t)
	{
		k.ImplicitWaitOn();
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
        TestUtil.reportFunctionFailed("Failed in "+methodName+" function");     
        return false;
	}
	finally
	{
		 k.ImplicitWaitOn();
	}
	
}

public void RewindFlow(String code,String event) throws ErrorInTestMethod
{
	String testName = (String)common.Rewind_excel_data_map.get("Automation Key");
	String navigationBy = CONFIG.getProperty("NavigationBy");
	try
	{		
		if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes")) 
		{
			customAssert.assertTrue(common_EP.ExistingPolicyAlgorithm(common.Rewind_excel_data_map,(String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type"), (String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Status")), "Existing Policy Algorithm function is having issues. ");
		}
		else 
		{
			CommonFunction_HHAZ.AdjustedTaxDetails.clear();
			if(!common.currentRunningFlow.equalsIgnoreCase("Renewal") && !common.currentRunningFlow.equalsIgnoreCase("MTA"))
			{
				NewBusinessFlow(code,"NB");
			}
				
			common_HHAZ.PremiumFlag = false;
		}
		
		common.currentRunningFlow="Rewind";
		common_VELA.isTaxSuccessful=false;
		
		navigationBy = CONFIG.getProperty("NavigationBy");
		common_HHAZ.PremiumFlag = false;
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common.funcRewind());
		
		TestUtil.reportStatus("<b> -----------------------Rewind flow started---------------------- </b>", "Info", false);
		
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
		if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes"))
		{
			customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
			customAssert.assertTrue(common.funcSearchPolicy(common.MTA_excel_data_map), "Policy Search function is having issue(S) . ");
			customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Endorsement Submitted (Rewind)"), "Verify Policy Status (Endorsement Submitted (Rewind)) function is having issue(S) . ");
			
		}
		else
		{
			customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
			customAssert.assertTrue(common.funcSearchPolicy(common.NB_excel_data_map), "Policy Search function is having issue(S) . ");
			if(TestBase.businessEvent.equalsIgnoreCase("MTA"))
			{
				customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Endorsement Submitted (Rewind)"), "Verify Policy Status (Endorsement Submitted (Rewind)) function is having issue(S) . ");
			}
			else
			{
				customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Submitted (Rewind)"), "Verify Policy Status (Submitted (Rewind)) function is having issue(S) . ");
			}
		}
				
		customAssert.assertTrue(funcPolicyGeneral(common.Rewind_excel_data_map,code,event), "Policy Details function having issue .");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Previous Claims"),"Issue while Navigating to Material Facts and Declarations . ");
		customAssert.assertTrue(funcPreviousClaims(common.Rewind_excel_data_map), "Previous claim function is having issue(S) .");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Material Facts & Declarations"),"Issue while Navigating to Material Facts & Declarations . ");
		customAssert.assertTrue(MaterialFactsDeclerationPage(), "MaterialFactsDeclerationPage function is having issue(S) . ");
		customAssert.assertTrue(func_Referrals_MaterialFactsDeclerationPage(), "Referrals for MaterialFactsDeclerationPage function is having issue(S) . ");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Goods in Transit"),"Issue while Navigating to Goods in Transit screen.");
		customAssert.assertTrue(GIT(common.Rewind_excel_data_map), "GIT function is having issue(S) . ");		

		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		
		if(TestBase.businessEvent.equalsIgnoreCase("MTA")){
			customAssert.assertTrue(common_HHAZ.funcPremiumSummary_MTA(common.Rewind_excel_data_map,code,event), "Rewind MTA Premium Summary in function is having issue(S) . ");
		}else if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes")){
			customAssert.assertTrue(common_HHAZ.funcPremiumSummary_MTA(common.Rewind_excel_data_map,code,event), "Rewind MTA Premium Summary in function is having issue(S) . ");
		}else{
			customAssert.assertTrue(common_HHAZ.funcPremiumSummary(common.Rewind_excel_data_map,code,event), "Premium Summary function is having issue(S) . ");
		}
			
		if(!TestBase.businessEvent.equalsIgnoreCase("Renewal") && !TestBase.businessEvent.equalsIgnoreCase("MTA"))
		{
			customAssert.assertTrue(common_HHAZ.funcStatusHandling(common.Rewind_excel_data_map,code,event));
		}
			
		if(TestBase.businessEvent.equals("Rewind"))
		{				
				customAssert.assertEquals(err_count,0,"Errors in premium calculations . ");
				customAssert.assertEquals(trans_error_val,0,"Errors in Transaction premium calculations . ");
				customAssert.assertEquals(common.final_err_pdf_count,0,"Varification Errors in PDF Documents . ");
				customAssert.assertTrue(common.StingrayLogout(), "Unable to Logout.");
				TestUtil.reportTestCasePassed(testName);
		}			
	}
	catch(Throwable t)
	{
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
}


public void RequoteFlow(String code,String event) throws ErrorInTestMethod{
	String testName = (String)common.Requote_excel_data_map.get("Automation Key");
	try{
		
		if(!common.currentRunningFlow.equalsIgnoreCase("Renewal")){
			NewBusinessFlow(code,"NB");
		}
		common.currentRunningFlow="Requote";
	
		String navigationBy = CONFIG.getProperty("NavigationBy");
		common_HHAZ.PremiumFlag = false;
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common.funcButtonSelection("Re-Quote"));
		
		TestUtil.reportStatus("<b> -----------------------Requote flow is started---------------------- </b>", "Info", false);
		
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
		customAssert.assertTrue(common.funcSearchPolicy(common.NB_excel_data_map), "Policy Search function is having issue(S) . ");
		
		customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Requote_excel_data_map,code,event,"Submitted"), "Verify Policy Status (Submitted (Rewind)) function is having issue(S) . ");
		
		customAssert.assertTrue(funcPolicyGeneral(common.Requote_excel_data_map,code,event), "Policy Details function having issue .");
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Previous Claims"),"Issue while Navigating to Material Facts and Declarations . ");
		customAssert.assertTrue(funcPreviousClaims(common.Requote_excel_data_map), "Previous claim function is having issue(S) .");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Material Facts & Declarations"),"Issue while Navigating to Material Facts & Declarations . ");
		customAssert.assertTrue(MaterialFactsDeclerationPage(), "MaterialFactsDeclerationPage function is having issue(S) . ");
		customAssert.assertTrue(func_Referrals_MaterialFactsDeclerationPage(), "Referrals for MaterialFactsDeclerationPage function is having issue(S) . ");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Goods in Transit"),"Issue while Navigating to Goods in Transit screen.");
		customAssert.assertTrue(GIT(common.Requote_excel_data_map), "Goods in Transit function is having issue(S) . ");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Endorsements"),"Issue while Navigating to Endorsements screen.");
		customAssert.assertTrue(common_HHAZ.funcEndorsementOperations(common.Requote_excel_data_map),"Endorsements is having issue(S).");
		
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common_HHAZ.funcPremiumSummary(common.Requote_excel_data_map,code,event), "Premium Summary function is having issue(S) . ");
		Assert.assertTrue(common_HHAZ.funcStatusHandling(common.Requote_excel_data_map,code,event));		
		
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
	
	
	
}


public void RenewalFlow(String code,String fileName) throws ErrorInTestMethod{
String testName = (String)common.Renewal_excel_data_map.get("Automation Key");
	
	try {
		
		common.currentRunningFlow="Renewal";
		String navigationBy = CONFIG.getProperty("NavigationBy");
		
		customAssert.assertTrue(common.StingrayLogin("PEN"),"Unable to login.");
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""),"");
		customAssert.assertTrue(common_CCJ.renewalSearchPolicyNEW(common.Renewal_excel_data_map), "Policy Search function is having issue(S) . ");
		customAssert.assertTrue(common.funcVerifyPolicyStatus_Renewal(common.Renewal_excel_data_map,CommonFunction.product,CommonFunction.businessEvent,"Renewal Pending"), "Verify Policy Status (Submitted (Rewind)) function is having issue(S) . ");
		
		if(!common_HHAZ.isAssignedToUW){ 
			customAssert.assertTrue(common.funcButtonSelection("Assign Underwriter"));
			customAssert.assertTrue(common_SPI.funcAssignPolicyToUW());
		}
		
		customAssert.assertTrue(common.funcButtonSelection("Send to Underwriter"));
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""),"");
	
		customAssert.assertTrue(common.funcSearchPolicy_Renewal(common.Renewal_excel_data_map), "Policy Search function is having issue(S) . ");
		customAssert.assertTrue(common.funcVerifyPolicyStatus_Renewal(common.Renewal_excel_data_map,CommonFunction.product,CommonFunction.businessEvent,"Renewal Submitted"), "Verify Policy Status (Renewal Submitted) function is having issue(S) . ");
		
		String agencyName = k.getText("CCF_AgencyName");
		int length = agencyName.length();
		int index = agencyName.indexOf("of");
		String agency = agencyName.substring(index+3, length);
		TestUtil.WriteDataToXl(CommonFunction.product+"_"+common.currentRunningFlow, "QuoteCreation",(String)common.Renewal_excel_data_map.get("Automation Key"), "QC_AgencyName", agency, common.Renewal_excel_data_map);
		
		//This will write Quote Number from screen to excel sheet.
		
		String QuoteNumber = k.getText("POF_QuoteNumber");
		TestUtil.WriteDataToXl(CommonFunction.product+"_"+common.currentRunningFlow, "Renewal", testName, "Renewal_QuoteNumber", QuoteNumber, common.Renewal_excel_data_map);
		
		customAssert.assertTrue(funcPolicyGeneral(common.Renewal_excel_data_map,code,TestBase.businessEvent), "Policy Details function having issue .");
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Previous Claims"),"Issue while Navigating to Material Facts and Declarations . ");
		customAssert.assertTrue(funcPreviousClaims(common.Renewal_excel_data_map), "Previous claim function is having issue(S) .");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Material Facts & Declarations"),"Issue while Navigating to Material Facts & Declarations . ");
		customAssert.assertTrue(MaterialFactsDeclerationPage(), "MaterialFactsDeclerationPage function is having issue(S) . ");
		customAssert.assertTrue(func_Referrals_MaterialFactsDeclerationPage(), "Referrals for MaterialFactsDeclerationPage function is having issue(S) . ");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Goods in Transit"),"Issue while Navigating to Goods in Transit screen.");
		customAssert.assertTrue(GIT(common.Renewal_excel_data_map), "GIT function is having issue(S) . ");
		
		/*customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Endorsements"),"Issue while Navigating to Endorsements screen.");
		customAssert.assertTrue(common_HHAZ.funcEndorsementOperations(common.Renewal_excel_data_map),"Endorsements is having issue(S).");
		*/
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common_HHAZ.funcPremiumSummary(common.Renewal_excel_data_map,code,TestBase.businessEvent), "Premium Summary function is having issue(S) . ");
		Assert.assertTrue(common_HHAZ.funcStatusHandling(common.Renewal_excel_data_map,code,"NB"));
		
	} catch (Exception e) {
		
	}
	
	TestUtil.reportStatus("Test Method of Renewal For - "+code, "Pass", true);
	
	
}


public void RenewalRewindFlow(String code,String event) throws ErrorInTestMethod{
	String testName = (String)common.Rewind_excel_data_map.get("Automation Key");
	try{
		
		CommonFunction_HHAZ.AdjustedTaxDetails.clear();
		common.currentRunningFlow="Rewind";
		//common_HHAZ.CoversDetails_data_list.clear();
		String navigationBy = CONFIG.getProperty("NavigationBy");
		common_HHAZ.PremiumFlag = false;
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common.funcRewind());
		
		TestUtil.reportStatus("<b> -----------------------Renewal Rewind flow started---------------------- </b>", "Info", false);
		
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
		customAssert.assertTrue(common.funcSearchPolicy(common.Renewal_excel_data_map), "Policy Search function is having issue(S) . ");
		customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Renewal Submitted (Rewind)"), "Verify Policy Status (Submitted (Rewind)) function is having issue(S) . ");
		
		customAssert.assertTrue(funcPolicyGeneral(common.Rewind_excel_data_map,code,event), "Policy Details function having issue .");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Previous Claims"),"Issue while Navigating to Material Facts and Declarations . ");
		customAssert.assertTrue(funcPreviousClaims(common.Rewind_excel_data_map), "Previous claim function is having issue(S) .");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Material Facts & Declarations"),"Issue while Navigating to Material Facts & Declarations . ");
		customAssert.assertTrue(MaterialFactsDeclerationPage(), "MaterialFactsDeclerationPage function is having issue(S) . ");
		customAssert.assertTrue(func_Referrals_MaterialFactsDeclerationPage(), "Referrals for MaterialFactsDeclerationPage function is having issue(S) . ");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Goods in Transit"),"Issue while Navigating to Goods in Transit screen.");
		customAssert.assertTrue(GIT(common.Rewind_excel_data_map), "GIT function is having issue(S) . ");		

		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common_HHAZ.funcPremiumSummary(common.Rewind_excel_data_map,code,event), "Premium Summary function is having issue(S) . ");
		
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
	
}

//Cancellation starts here :

	public double get_Book_Rate_from_Properties_GIT(String _Activity){
		
		double r_value=0.0;
		DecimalFormat formatter = new DecimalFormat("###.###");
		//Drain_Rodding_�_incl._repair
		try{
			CCD_Rater = OR.getORProperties();
			String t_activity = _Activity.replaceAll("�", "").replaceAll(" ", "_").replaceAll(",", "").replaceAll("__", "_");
			t_activity = t_activity + "_GIT_BR";
			
			//Maximum_any_one_load_GIT_BR
		
			r_value = Double.parseDouble(CCD_Rater.getProperty(t_activity));
			r_value = Double.valueOf(formatter.format(r_value));
		
		}catch(Throwable t ){
			System.out.println("Error while getting Book rate for activity > "+_Activity+" < "+t.getMessage());
		}
		return r_value;
			
	}
	
	public double get_GIT_Book_Rate(String git_Activity){
		
		double book_rate = 0.0;
			
		book_rate = get_Book_Rate_from_Properties_GIT(git_Activity);
		
		//System.out.println("Book Rate of "+git_Activity + " = "+book_rate);
		common_CCD.Book_rate_Rater_output.put("GIT"+"_"+git_Activity, book_rate);
		return book_rate;
			
	}
public void MTAFlow(String code,String fileName) throws ErrorInTestMethod{
	String testName = (String)common.MTA_excel_data_map.get("Automation Key");
	String navigationBy = CONFIG.getProperty("NavigationBy");
	try{
		
		if(((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes")) {
			common_EP.ExistingPolicyAlgorithm(common.MTA_excel_data_map ,(String)common.MTA_excel_data_map.get("MTA_ExistingPolicy_Type"), (String)common.MTA_excel_data_map.get("MTA_ExistingPolicy_Status"));
		}else {
			if(!common.currentRunningFlow.equalsIgnoreCase("Renewal")){
				NewBusinessFlow(code,"NB");
			}
			common_HHAZ.PremiumFlag = false;
		}
		
		
		common.currentRunningFlow="MTA";
		navigationBy = CONFIG.getProperty("NavigationBy");
		common_HHAZ.CoversDetails_data_list= new ArrayList<String>();
		common_HHAZ.CoversDetails_data_list.add("GoodsinTransitRSAROI");
		
		customAssert.assertTrue(common_CCD.funcCreateEndorsement(),"Error in Create Endorsement function . ");
		
		customAssert.assertTrue(funcPolicyGeneral(common.MTA_excel_data_map,code,TestBase.businessEvent), "Policy Details function having issue .");
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Previous Claims"),"Issue while Navigating to Material Facts and Declarations . ");
		customAssert.assertTrue(funcPreviousClaims(common.MTA_excel_data_map), "Previous claim function is having issue(S) .");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Material Facts & Declarations"),"Issue while Navigating to Material Facts & Declarations . ");
		customAssert.assertTrue(MaterialFactsDeclerationPage(), "MaterialFactsDeclerationPage function is having issue(S) . ");
		customAssert.assertTrue(func_Referrals_MaterialFactsDeclerationPage(), "Referrals for MaterialFactsDeclerationPage function is having issue(S) . ");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Goods in Transit"),"Issue while Navigating to Goods in Transit screen.");
		customAssert.assertTrue(GIT(common.MTA_excel_data_map), "GIT function is having issue(S) . ");
		
		//customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Endorsements"),"Issue while Navigating to Endorsements screen.");
		//customAssert.assertTrue(common_HHAZ.funcEndorsementOperations(common.MTA_excel_data_map),"Endorsements is having issue(S).");
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common_HHAZ.funcPremiumSummary_MTA(common.MTA_excel_data_map,code,TestBase.businessEvent), "Premium Summary function is having issue(S) . ");
		
		if(!TestBase.businessEvent.equals("Renewal")){
			Assert.assertTrue(common_HHAZ.funcStatusHandling(common.MTA_excel_data_map,code,TestBase.businessEvent));
			
			customAssert.assertEquals(err_count,0,"Errors in premium calculations . ");
			customAssert.assertEquals(trans_error_val,0,"Errors in Transaction premium calculations . ");
			customAssert.assertEquals(common.final_err_pdf_count,0,"Varification Errors in PDF Documents . ");
			customAssert.assertTrue(common.StingrayLogout(), "Unable to Logout.");
			TestUtil.reportTestCasePassed(testName);
			
		}		
		
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
	
}



/**
 * 
 * This method gives MF&D pages referrals texts."
 * 
 *
 */
public boolean func_Referrals_MaterialFactsDeclerationPage(){
	
	boolean retValue = true;
	
	Map<Object,Object> map_data=null;
	Properties CCD_referrals = OR.getORProperties();
	
	switch(common.currentRunningFlow){
		case "NB":
			map_data = common.NB_excel_data_map;
			break;
		case "MTA":
			map_data = common.MTA_excel_data_map;
			break;
		case "Renewal":
			map_data = common.Renewal_excel_data_map;
			break;
	}
	
	try{
		 customAssert.assertTrue(common.funcPageNavigation("Material Facts & Declarations", ""),"Material Facts & Declarations page is having issue(S)");
		 k.ImplicitWaitOff();
		 String mfd_q_value = null,mf_key="RM_MaterialFactsandDeclarations_";
		 
		 try{
		 //Have there been any previous claims  in the last 5 years?
		 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_previous_claims");
		 if(mfd_q_value.equalsIgnoreCase("Yes"))
			 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"previousClaims"));
		 }catch(Throwable t){
			 }
		 
		 try{
		 //Has any proposer, director or partner of the Trade or Business or its Subsidiary Companies ever, either personally or in any business capacity, had a proposal refused or declined or claim repudiated or ever had an insurance cancelled, renewal refused or had special terms imposed?
		 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_special_terms_imposed");
		 if(mfd_q_value.equalsIgnoreCase("Yes"))
			 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"specialTermsImposed"));
		 }catch(Throwable t){
		 }
		 
		 try{
		 //Has any proposer, director or partner of the Trade or Business or its Subsidiary Companies ever, either personally or in any business capacity had any convictions, criminal offences or prosecutions pending other than motor offences?
		 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_motor_offences");
		 if(mfd_q_value.equalsIgnoreCase("Yes"))
			 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"motorOffences"));
		 }catch(Throwable t){
		 }
		 
		 try{
		 //Has any proposer, director or partner of the Trade or Business or its Subsidiary Companies ever, either personally or in any business capacity been declared bankrupt or insolvent or been the subject of bankruptcy proceedings or receivership/ insolvency proceedings?
		 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_bankrupt_or_insolvent");
		 if(mfd_q_value.equalsIgnoreCase("Yes"))
			 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"bankruptOrInsolvent"));
		 }catch(Throwable t){
		 }
		 
		 try{
		 //Involved in another company within 6 months before receivership/insolvency?
		 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_6_months_receivership_insolvency");
		 if(mfd_q_value.equalsIgnoreCase("Yes"))
			 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"6monthsReceivershipInsolvency"));
		 }catch(Throwable t){
		 }
			
		 try{
		 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_administrative_receivership_proceedings");
		 if(mfd_q_value.equalsIgnoreCase("Yes"))
			 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"administrativeReceivershipProceedings"));
		 }catch(Throwable t){
		 }
		
		 try{
		 //Has any proposer, director or partner of the Trade or Business or its Subsidiary Companies ever, either personally or in any business capacity been the owner or director of, or partner in, any business, company or partnership had a county court judgement awarded against them?
		 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_awarded_against_them");
		 if(mfd_q_value.equalsIgnoreCase("Yes"))
			 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"awardedAgainstThem"));
		 }catch(Throwable t){
		 }
		
		 try{
		 //Do you use high pressure water jetting equipment?
		 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_jetting_equipment");
		 if(mfd_q_value.equalsIgnoreCase("Yes"))
			 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"jettingEquipment"));
		 }catch(Throwable t){
		 }
		
		 try{
		 //Does a senior person have overall reponsibility for health and safety
		 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_senior_person");
		 if(mfd_q_value.equalsIgnoreCase("Yes"))
			 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"seniorPerson"));
		 }catch(Throwable t){
		 }
		 
		 try{
		 //Have you appointed a competent person to advise you on health and safety matters?
		 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_health_safety_matters");
		 if(mfd_q_value.equalsIgnoreCase("Yes"))
			 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"healthSafetyMatters"));
		 }catch(Throwable t){
		 }
		 
		 try{
			
			 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_representative_company");
			 if(mfd_q_value.equalsIgnoreCase("Yes"))
				 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"representativeCompany"));
			 }catch(Throwable t){
			 }
		 
		 try{
			
			 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_Risk_Assessments");
			 if(mfd_q_value.equalsIgnoreCase("Yes"))
				 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"RiskAssessments"));
			 }catch(Throwable t){
			 }
		 
		 try{
			 
			 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_safe_working_procedures");
			 if(mfd_q_value.equalsIgnoreCase("Yes"))
				 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"safeWorkingProcedures"));
			 }catch(Throwable t){
			 }
		 
		 try{
			 
			 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_employees_undertake");
			 if(mfd_q_value.equalsIgnoreCase("Yes"))
				 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"employeesUndertake"));
			 }catch(Throwable t){
			 }
		 
		 try{
			
			 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_hazardous_to_health");
			 if(mfd_q_value.equalsIgnoreCase("Yes"))
				 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"hazardousToHealth"));
			 }catch(Throwable t){
			 }
		 
		 try{

			 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_Noise_Assessment");
			 if(mfd_q_value.equalsIgnoreCase("Yes"))
				 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"NoiseAssessment"));
			 }catch(Throwable t){
			 }
		 
		 try{
			 
			 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_third_party_ladders");
			 if(mfd_q_value.equalsIgnoreCase("Yes"))
				 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"thirdPartyLadders"));
			 }catch(Throwable t){
			 }
		 
		 try{
			 
			 mfd_q_value = k.GetDropDownSelectedValueIgnoreError("CCD_MFD_confined_space_work");
			 if(mfd_q_value.equalsIgnoreCase("Yes"))
				 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"confinedSpaceWork"));
			 }catch(Throwable t){
			 }
		 
		 
		 //Do you knowingly - Multi-select list
		 try{
			 List<WebElement> _dyknow_MF = driver.findElements(By.xpath("//*[text()='Do you knowingly']//following::ul[1]//li"));
			 String know_value =null,exp_val=null;
			 for(WebElement know_:_dyknow_MF){
				 try{
					 know_value = know_.getAttribute("title");
				 }catch(Throwable t){
					 know_value="None";
					 
					 }
				 exp_val = CCD_referrals.getProperty("CCD_MFD_DoYouKnow_title");
				 if(know_value.equalsIgnoreCase(exp_val)){
				 	 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"DoYouKnowingly"));
				 }
				 
			 }
		 }catch(Throwable t){
			 
		 }
		
		 
		 // Do you operate? - Multi Select list
		 try{
			 List<WebElement> _Operate = driver.findElements(By.xpath("//*[text()='Do you operate?']//following::ul[1]//li"));
			 String _operate_value =null,exp_val=null;
			 for(WebElement prd_elm:_Operate){
				 try{
					 _operate_value = prd_elm.getAttribute("title");
				 }catch(Throwable t){
					 _operate_value="None";
					 
					 }
				 
				 exp_val = CCD_referrals.getProperty("CCD_MFD_DoYouOperate_title");
				 if(_operate_value.equalsIgnoreCase(exp_val)){
				 	 common_HHAZ.referrals_list.add((String)map_data.get(mf_key+"DoYouOperate"));
				 }
				 
			 }
			
		 }catch(Throwable t){
			 
		 }
		  
		 
		 
		 return retValue;
		 
	}catch(Throwable t){
		k.ImplicitWaitOn();
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
        TestUtil.reportFunctionFailed("Failed in "+methodName+" function");     
        return false;
		}
	finally{
		 k.ImplicitWaitOn();
	 }
	
	}

}
