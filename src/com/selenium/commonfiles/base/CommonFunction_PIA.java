package com.selenium.commonfiles.base;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.selenium.commonfiles.util.ErrorInTestMethod;
import com.selenium.commonfiles.util.ObjectMap;
import com.selenium.commonfiles.util.TestUtil;

public class CommonFunction_PIA extends TestBase{

	
Properties PIA_Rater = new Properties();
DateFormat df = null;
public int actual_no_of_years=0,err_count=0,trans_error_val=0, Can_returnP_Error=0;
public double spi_GrossFee = 0.0;

public static double TotalFeesIncome_Estimate = 0.00, TotalFeesIncome_2017 = 0.00, TotalFeesIncome_2016 = 0.00, TotalFeesIncome_2015 = 0.00, TotalFeesIncome_2014 = 0.00, Total_Average = 0.00;
public static double AvgReagon_1 = 0.00, AvgReagon_2 = 0.00, AvgReagon_3 = 0.00, AvgReagon_4 = 0.00, AvgTotal = 0.00;
public static double burnRate = 0.00, dAdminFee_CoverSPI = 0.00, aAdminFee_CoverSE = 0.00;
public static int duration_SoPI;
public String currentFlow = null;

Date currentDate = new Date();

public static int global_PeriodMonths =0;
public static double global_FeesIncome = 0.00, global_IndemnityLimit = 0.00, global_SOI_BookRate = 0.00, global_SOI_InitialP = 0.00, gloabal_SOI_BookP = 0.00, global_SOI_CommAdjust = 0.00, global_SOI_RevisedP = 0.00, global_SOI_sPremium = 0.00;
public static double global_SOI_GrossP = 0.00, global_SOI_GrossFees = 0.00, global_SOI_TotalPremium = 0.00, global_SOI_AnnualP = 0.00, global_SOI_TecgAdjust = 0.00, global_SOI_NetNetP = 0.00 ;
public static double Refer_PenComm = 0.00, Refer_BrokerComm = 0.00, Refer_InsTax = 0.00;
public static double PS_PenComm = 0.00, PS_BrokerComm = 0.00, PS_InsTaxRate = 0.00;
public double PI_pdf_InsuranceTax = 0.0, PI_pdf_GrossPremium = 0.0,SEL_pdf_InsuranceTax = 0.0, SEL_pdf_GrossPremium = 0.0;
public String Refer_Policy_SDate, Refer_Policy_EDate, isRewindSelected = "No",PI_Premium = "0.0",SEL_Premium = "0.0";
public boolean isMTARewindFlow = false, isRenewalRewindFlow=false;
public boolean PI_FP= false , SEL_FP = false;

public double global_PreviousPremium  = 0.00;

public static double global_SEL_AdminFees  = 0.00, global_SEL_GrossP = 0.00, global_SEL_PenComm = 0.00, global_SEL_BrokerComm = 0.00, global_SEL_GrossComm = 0.00, global_SEL_BookP =0.00;


public Map<String,Double> PIA_Rater_output = new HashMap<>();

public String Endorsement_date = null, dateDiff_Endorse;
public int stale_count = 0;

/**
 * 
 * This method handles all New Business Cases for SPI product.
 * 
 */
public void NewBusinessFlow(String code,String event) throws ErrorInTestMethod{
	
	String testName = (String)common.NB_excel_data_map.get("Automation Key");
	common_PIA.currentFlow="NB";
	common.currentRunningFlow="NB";
	
	try{
		customAssert.assertTrue(common.StingrayLogin("IRS"),"Unable to login.");
		customAssert.assertTrue(common.checkClient(common.NB_excel_data_map,code,event),"Unable to check Client.");
		customAssert.assertTrue(common.createNewQuote(common.NB_excel_data_map,code,event), "Unable to create new quote.");
		customAssert.assertTrue(common.selectLatestQuote(common.NB_excel_data_map,code,event), "Unable to select quote from table.");
		customAssert.assertTrue(funcPolicyGeneral(common.NB_excel_data_map,code,event), "Policy General function having issue .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(common.funcCovers(common.NB_excel_data_map), "Select covers function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Material Facts & Declarations"),"Issue while Navigating to Material Facts & Declarations  . ");
		customAssert.assertTrue(MaterialFactsDeclerationPage(), "MaterialFactsDeclerationPage function is having issue(S) . ");
	
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Claims History"),"Issue while Navigating to Specified Perils  . ");
		customAssert.assertTrue(funcClaimsHistory(common.NB_excel_data_map), "Claims History function having issue .");
		
		if(((String)common.NB_excel_data_map.get("CD_SolicitorsPI")).equals("Yes")){
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Solicitors PI"),"Issue while Navigating to Solicitors PI. ");
			customAssert.assertTrue(Solicitors_PI(common.NB_excel_data_map), "Solicitors PI function is having issue(S) . ");
		}
		
		if(((String)common.NB_excel_data_map.get("CD_Solicitorsexcesslayer")).equals("Yes")){
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Solicitors Excess Layer"),"Issue while Navigating to Solicitors Excess Layer. ");
			customAssert.assertTrue(func_Solicitors_ExcessLayer(common.NB_excel_data_map),"Issue while Navigating to Solicitors Excess Layer page  . ");
		}
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common_HHAZ.funcPremiumSummary(common.NB_excel_data_map,code,event), "Premium Summary function is having issue(S) . ");
		
		Assert.assertTrue(common_HHAZ.funcStatusHandling(common.NB_excel_data_map,code,"NB"));
	
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		
		if(TestBase.businessEvent.equals("NB")){
			customAssert.assertEquals(err_count,0,"Errors in premium calculations . ");
			customAssert.assertEquals(trans_error_val,0,"Errors in Transaction premium calculations . ");
			customAssert.assertEquals(common.final_err_pdf_count,0,"Varification Errors in PDF Documents . ");
		
			customAssert.assertTrue(common.StingrayLogout(), "Unable to Logout.");
			TestUtil.reportTestCasePassed(testName);
		}
		
		
		
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
	
	
}

/**
 * MTA Flow Method
 */
public void MTAFlow(String code,String fileName) throws ErrorInTestMethod{
	
	String testName = (String)common.MTA_excel_data_map.get("Automation Key");
	try{
		if(((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes")) {
			customAssert.assertTrue(common_EP.ExistingPolicyAlgorithm(common.MTA_excel_data_map , (String)common.MTA_excel_data_map.get("MTA_ExistingPolicy_Type"), (String)common.MTA_excel_data_map.get("MTA_ExistingPolicy_Status")), "Existing Policy Algorithm function is having issues. ");
		}else {
			if(!common.currentRunningFlow.equalsIgnoreCase("Renewal")){
				NewBusinessFlow(code,"NB");
			}
			//common_HHAZ.CoversDetails_data_list.clear();
			common_HHAZ.PremiumFlag = false;
		}
		common_PIA.currentFlow="MTA";
		common.currentRunningFlow="MTA";
		customAssert.assertTrue(common_CCD.funcCreateEndorsement(),"Error while creating Endorsement . ");
		
		customAssert.assertTrue(funcPolicyGeneral_MTA(common.MTA_excel_data_map,code,"MTA"), "MTA Policy General function having issue .");
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(common_HHAZ.funcCovers(common.MTA_excel_data_map),"Error in selecting cover for MTA.");
			
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Claims History"),"Issue while Navigating to Specified Perils  . ");
		customAssert.assertTrue(funcClaimsHistory(common.MTA_excel_data_map), "MTA Claims History function having issue .");
	
		
		if(((String)common.MTA_excel_data_map.get("CD_SolicitorsPI")).equals("Yes")){
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Solicitors PI"),"Issue while Navigating to Solicitors PI. ");
			customAssert.assertTrue(Solicitors_PI_MTA(common.MTA_excel_data_map), "Solicitors PI function is having issue(S) . ");
		}
		
		if(((String)common.MTA_excel_data_map.get("CD_Solicitorsexcesslayer")).equals("Yes")){
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Solicitors Excess Layer"),"Issue while Navigating to Solicitors Excess Layer. ");
			customAssert.assertTrue(func_Solicitors_ExcessLayer(common.MTA_excel_data_map),"Issue while Navigating to Solicitors Excess Layer page  . ");
		}
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common_HHAZ.funcPremiumSummary_MTA(common.MTA_excel_data_map,code,"MTA"), "Premium Summary function is having issue(S) . ");
		
		if(!TestBase.businessEvent.equalsIgnoreCase("Renewal")){
			Assert.assertTrue(common_HHAZ.funcStatusHandling(common.MTA_excel_data_map,code,"MTA"));
			customAssert.assertEquals(err_count,0,"Errors in premium calculations . ");
			customAssert.assertEquals(trans_error_val,0,"Errors in Transaction premium calculations . ");
			customAssert.assertEquals(common.final_err_pdf_count,0,"Varification Errors in PDF Documents . ");
			
			customAssert.assertTrue(common.StingrayLogout(), "Unable to Logout.");
		
			TestUtil.reportTestCasePassed(testName);
		}
		
	
	}catch (ErrorInTestMethod e) {
		System.out.println("Error in New Business test method for MTA > "+testName);
		throw e;
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
	
	
}

//Renewal Flow

public void RenewalFlow(String code,String fileName) throws Throwable{
	
	String testName = (String)common.Renewal_excel_data_map.get("Automation Key");
	try {
		common.currentRunningFlow = "Renewal";
		String navigationBy = CONFIG.getProperty("NavigationBy");
		
		customAssert.assertTrue(common.StingrayLogin("IRS"),"Unable to login.");
		
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""),"");
		customAssert.assertTrue(common_CCJ.renewalSearchPolicyNEW(common.Renewal_excel_data_map), "Policy Search function is having issue(S) . ");
		
		customAssert.assertTrue(common.funcVerifyPolicyStatus_Renewal(common.Renewal_excel_data_map,CommonFunction.product,CommonFunction.businessEvent,"Renewal Pending"), "Verify Policy Status (Submitted (Rewind)) function is having issue(S) . ");
		if(!common_HHAZ.isAssignedToUW){ // This variable is initialized in common_CCJ.renewalSearchPolicyNEW function
			customAssert.assertTrue(common.funcButtonSelection("Assign Underwriter"));
			customAssert.assertTrue(common_SPI.funcAssignPolicyToUW());
		}
		
		customAssert.assertTrue(common.funcButtonSelection("Send to Underwriter"));
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""),"");
		customAssert.assertTrue(common.funcSearchPolicy_Renewal(common.Renewal_excel_data_map), "Policy Search function is having issue(S) . ");
		customAssert.assertTrue(common.funcVerifyPolicyStatus_Renewal(common.Renewal_excel_data_map,CommonFunction.product,CommonFunction.businessEvent,"Renewal Submitted"), "Verify Policy Status (Renewal Submitted) function is having issue(S) . ");
			
		customAssert.assertTrue(funcPolicyGeneral_MTA(common.Renewal_excel_data_map,code,"Renewal"), "Renewal Policy General function having issue .");
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(funcUpdateCoverDetails_MTA(common.Renewal_excel_data_map),"Error in selecting cover for MTA.");
			
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Claims History"),"Issue while Navigating to Specified Perils  . ");
		customAssert.assertTrue(funcClaimsHistory(common.Renewal_excel_data_map), "MTA Claims History function having issue .");
	
		
		if(((String)common.Renewal_excel_data_map.get("CD_SolicitorsPI")).equals("Yes")){
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Solicitors PI"),"Issue while Navigating to Solicitors PI. ");
			customAssert.assertTrue(Solicitors_PI_MTA(common.Renewal_excel_data_map), "Solicitors PI function is having issue(S) . ");
		}
		
		if(((String)common.Renewal_excel_data_map.get("CD_Solicitorsexcesslayer")).equals("Yes")){
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Solicitors Excess Layer"),"Issue while Navigating to Solicitors Excess Layer. ");
			customAssert.assertTrue(func_Solicitors_ExcessLayer(common.Renewal_excel_data_map),"Issue while Navigating to Solicitors Excess Layer page  . ");
		}
		
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common_HHAZ.funcPremiumSummary(common.Renewal_excel_data_map,code,"Renewal"), "Premium Summary function is having issue(S) . ");
		Assert.assertTrue(common_HHAZ.funcStatusHandling(common.Renewal_excel_data_map,code,"Renewal"));
		
		TestUtil.reportTestCasePassed(testName);
		
	}catch (ErrorInTestMethod e) {
		System.out.println("Error in Renewal test method -  > "+testName);
		throw e;
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
	
}

public void RenewalRewindFlow(String code,String event) throws ErrorInTestMethod{
	String testName = (String)common.Rewind_excel_data_map.get("Automation Key");
	try{
		
		CommonFunction_HHAZ.AdjustedTaxDetails.clear();
		common.currentRunningFlow="Rewind";
		//common_HHAZ.CoversDetails_data_list.clear();
		String navigationBy = CONFIG.getProperty("NavigationBy");
		common_HHAZ.PremiumFlag = false;
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common.funcRewind());
		
		TestUtil.reportStatus("<b> -----------------------Renewal Rewind flow started---------------------- </b>", "Info", false);
		
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
		customAssert.assertTrue(common.funcSearchPolicy(common.Renewal_excel_data_map), "Policy Search function is having issue(S) . ");
		customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Renewal Submitted (Rewind)"), "Verify Policy Status (Submitted (Rewind)) function is having issue(S) . ");
		
		customAssert.assertTrue(funcPolicyGeneral_MTA(common.Rewind_excel_data_map,code,"MTA"), "MTA Policy General function having issue .");
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(funcUpdateCoverDetails_MTA(common.Rewind_excel_data_map),"Error in selecting cover for MTA.");
			
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Claims History"),"Issue while Navigating to Specified Perils  . ");
		customAssert.assertTrue(funcClaimsHistory(common.Rewind_excel_data_map), "Rewind Claims History function having issue .");
	
		if(((String)common.Rewind_excel_data_map.get("CD_SolicitorsPI")).equals("Yes")){
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Solicitors PI"),"Issue while Navigating to Solicitors PI. ");
			customAssert.assertTrue(Solicitors_PI_MTA(common.Rewind_excel_data_map), "Solicitors PI function is having issue(S) . ");
		}
		
		if(((String)common.Rewind_excel_data_map.get("CD_Solicitorsexcesslayer")).equals("Yes")){
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Solicitors Excess Layer"),"Issue while Navigating to Solicitors Excess Layer. ");
			customAssert.assertTrue(func_Solicitors_ExcessLayer(common.Rewind_excel_data_map),"Issue while Navigating to Solicitors Excess Layer page  . ");
		}
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Issue while Navigating to Premium Summary. ");
		customAssert.assertTrue(common_HHAZ.funcPremiumSummary(common.Rewind_excel_data_map,code,event), "Premium Summary function is having issue(S) . ");
		
		
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
	
	
	
}

public void RewindFlow(String code,String event) throws ErrorInTestMethod{
	String testName = (String)common.Rewind_excel_data_map.get("Automation Key");
	try{
		
		if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes")) {
			customAssert.assertTrue(common_EP.ExistingPolicyAlgorithm(common.Rewind_excel_data_map,(String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type"), (String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Status")), "Existing Policy Algorithm function is having issues. ");
		}else {
			CommonFunction_HHAZ.AdjustedTaxDetails.clear();
			if(!common.currentRunningFlow.equalsIgnoreCase("Renewal") && !common.currentRunningFlow.equalsIgnoreCase("MTA")){
				NewBusinessFlow(code,"NB");
			}
			//common_HHAZ.CoversDetails_data_list.clear();
			//common_CCD.MD_Building_Occupancies_list.clear();
			common_HHAZ.PremiumFlag = false;
		}
		
		common.currentRunningFlow="Rewind";
		String navigationBy = CONFIG.getProperty("NavigationBy");
		//common_HHAZ.PremiumFlag = false;
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common.funcRewind());
		
		TestUtil.reportStatus("<b> -----------------------Rewind flow is started---------------------- </b>", "Info", false);
		
		//Existing Policy search condition for Rewind on MTA or Rewind on NB.
		if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes")){
			customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
			customAssert.assertTrue(common.funcSearchPolicy(common.MTA_excel_data_map), "Policy Search function is having issue(S) . ");
			customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Endorsement Submitted (Rewind)"), "Verify Policy Status (Endorsement Submitted (Rewind)) function is having issue(S) . ");
					
		}else{
			customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
			customAssert.assertTrue(common.funcSearchPolicy(common.NB_excel_data_map), "Policy Search function is having issue(S) . ");
			if(TestBase.businessEvent.equalsIgnoreCase("MTA")){
				customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Endorsement Submitted (Rewind)"), "Verify Policy Status (Endorsement Submitted (Rewind)) function is having issue(S) . ");
			}else{
				customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Submitted (Rewind)"), "Verify Policy Status (Submitted (Rewind)) function is having issue(S) . ");
			}
		}
		
		customAssert.assertTrue(funcPolicyGeneral_MTA(common.Rewind_excel_data_map,code,"MTA"), "MTA Policy General function having issue .");
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(funcUpdateCoverDetails_MTA(common.Rewind_excel_data_map),"Error in selecting cover for MTA.");
			
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Claims History"),"Issue while Navigating to Specified Perils  . ");
		customAssert.assertTrue(funcClaimsHistory(common.Rewind_excel_data_map), "Rewind Claims History function having issue .");
	
		
		if(((String)common.Rewind_excel_data_map.get("CD_SolicitorsPI")).equals("Yes")){
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Solicitors PI"),"Issue while Navigating to Solicitors PI. ");
			customAssert.assertTrue(Solicitors_PI_MTA(common.Rewind_excel_data_map), "Solicitors PI function is having issue(S) . ");
		}
		
		if(((String)common.Rewind_excel_data_map.get("CD_Solicitorsexcesslayer")).equals("Yes")){
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Solicitors Excess Layer"),"Issue while Navigating to Solicitors Excess Layer. ");
			customAssert.assertTrue(func_Solicitors_ExcessLayer(common.Rewind_excel_data_map),"Issue while Navigating to Solicitors Excess Layer page  . ");
		}
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Issue while Navigating to Premium Summary. ");
		
		if(TestBase.businessEvent.equalsIgnoreCase("MTA")){
			customAssert.assertTrue(common_HHAZ.funcPremiumSummary_MTA(common.Rewind_excel_data_map,code,event), "Rewind MTA Premium Summary in function is having issue(S) . ");
		}else if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes")){
			customAssert.assertTrue(common_HHAZ.funcPremiumSummary_MTA(common.Rewind_excel_data_map,code,event), "Rewind MTA Premium Summary in function is having issue(S) . ");
		}else{
			customAssert.assertTrue(common_HHAZ.funcPremiumSummary(common.Rewind_excel_data_map,code,event), "Premium Summary function is having issue(S) . ");
		}
		
		if(!TestBase.businessEvent.equalsIgnoreCase("Renewal") && !TestBase.businessEvent.equalsIgnoreCase("MTA")){
			customAssert.assertTrue(common_HHAZ.funcStatusHandling(common.Rewind_excel_data_map,code,event));
		}
		
		if(TestBase.businessEvent.equals("Rewind")){
			customAssert.assertEquals(err_count,0,"Errors in premium calculations . ");
			customAssert.assertEquals(trans_error_val,0,"Errors in Transaction premium calculations . ");
			customAssert.assertEquals(common.final_err_pdf_count,0,"Varification Errors in PDF Documents . ");
		
			customAssert.assertTrue(common.StingrayLogout(), "Unable to Logout.");
			TestUtil.reportTestCasePassed(testName);
		}
		
		
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
	
	
	
}

public void CancellationFlow(String code,String fileName) throws ErrorInTestMethod{
	
	common_HHAZ.cancellationProcess(code,"CAN");
	
	
}
///SPI Screens Scripting --///

/**
 * 
 * This method handles SPI Policy General screens scripting.
 * 
 */
public boolean funcPolicyGeneral(Map<Object, Object> map_data,String code ,String event){
	
	try{
		customAssert.assertTrue(common.funcPageNavigation("Policy General", ""), "Navigation problem to Policy General page .");
		
		customAssert.assertTrue(k.Input("SPI_PG_DateEstablished", (String)map_data.get("PG_DateEstablishment")),	"Unable to enter value in Date Established field .");
		customAssert.assertTrue(k.DropDownSelection("SPI_PG_IsYourPracticeLLP",(String)common.NB_excel_data_map.get("PG_IsYourPracticeLLP")), "Unable to select value from Is your practice an LLP dropdown .");
		customAssert.assertTrue(k.DropDownSelection("SPI_PG_QuoteValidity",(String)common.NB_excel_data_map.get("PG_QuoteValidity")), "Unable to select value from Quote Validity dropdown .");
		
		driver.findElement(By.xpath("html/body/div[3]/form/div/table[1]/tbody/tr[10]/td[2]/span/span[1]/span/ul/li[1]/span")).click();
		
		String[] geographical_Limits = ((String)common.NB_excel_data_map.get("PG_Geographical_limit")).split(",");
		List<WebElement> ul_elements = driver.findElements(By.xpath("//ul"));
		for(String geo_limit : geographical_Limits){
			for(WebElement each_ul : ul_elements){
				k.waitTwoSeconds();
				customAssert.assertTrue(k.Click("SPI_PG_Geographic_Limit"),"Error while Clicking Geographic Limit List object . ");
				k.waitTwoSeconds();
				if(each_ul.findElement(By.xpath("//li[text()='"+geo_limit+"']")).isDisplayed())
					each_ul.findElement(By.xpath("//li[text()='"+geo_limit+"']")).click();
				else
					continue;
				break;
			}
		}
		
		customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Policy General .");
		TestUtil.reportStatus("Entered all the details on Policy General page .", "Info", true);
		
		stale_count = 0;
		return true;
	}catch(StaleElementReferenceException stale_ex) {
       if(stale_count++ < 3)
    	   funcPolicyGeneral(map_data,code ,event);
       System.out.println("StaleElementReferenceException in Policy General function after >"+stale_count+"< attempts - "+stale_ex);
 	}catch(Throwable t) {
        String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
        TestUtil.reportFunctionFailed("Failed in "+methodName+" function");
        Assert.fail("Unable to to do operation on policy General page. \n", t);
 	}
	 stale_count = 0;
	 return false;
}

/**
 * 
 * This method handles SPI Policy General screens scripting.
 * 
 */
public boolean funcPolicyGeneral_MTA(Map<Object, Object> map_data,String code ,String event){
	
	try{
		customAssert.assertTrue(common.funcPageNavigation("Policy General", ""), "Navigation problem to Policy General page .");
		
		customAssert.assertTrue(k.Input("SPI_PG_DateEstablished", (String)map_data.get("PG_DateEstablishment")),	"Unable to enter value in Date Established field .");
		customAssert.assertTrue(k.DropDownSelection("SPI_PG_IsYourPracticeLLP",(String)map_data.get("PG_IsYourPracticeLLP")), "Unable to select value from Is your practice an LLP dropdown .");
		customAssert.assertTrue(k.DropDownSelection("SPI_PG_QuoteValidity",(String)map_data.get("PG_QuoteValidity")), "Unable to select value from Quote Validity dropdown .");
		
		k.ImplicitWaitOff();
		for(int i=1;i<=4;i++){
			try{
				driver.findElement(By.xpath("html/body/div[3]/form/div/table[1]/tbody/tr[10]/td[2]/span/span[1]/span/ul/li["+1+"]/span")).click();
			}catch(Throwable t){
				continue;
			}
		}
		k.ImplicitWaitOn();
		
		String[] geographical_Limits = ((String)map_data.get("PG_Geographical_limit")).split(",");
		List<WebElement> ul_elements = driver.findElements(By.xpath("//ul"));
		for(String geo_limit : geographical_Limits){
			for(WebElement each_ul : ul_elements){
				k.waitTwoSeconds();
				customAssert.assertTrue(k.Click("SPI_PG_Geographic_Limit"),"Error while Clicking Geographic Limit List object . ");
				k.waitTwoSeconds();
				if(each_ul.findElement(By.xpath("//li[text()='"+geo_limit+"']")).isDisplayed())
					each_ul.findElement(By.xpath("//li[text()='"+geo_limit+"']")).click();
				else
					continue;
				break;
			}
		}
		
		customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Policy General .");
		TestUtil.reportStatus("Entered all the details on Policy General page .", "Info", true);
		
		stale_count = 0;
		return true;
	}catch(StaleElementReferenceException stale_ex) {
       if(stale_count++ < 3)
    	   funcPolicyGeneral_MTA(map_data,code ,event);
       System.out.println("StaleElementReferenceException in Policy General function after >"+stale_count+"< attempts - "+stale_ex);
 	}catch(Throwable t) {
        String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
        TestUtil.reportFunctionFailed("Failed in "+methodName+" function");
        Assert.fail("Unable to to do operation on policy General page. \n", t);
 	}
	 stale_count = 0;
	 return false;
}

/**
 * 
 * This method handles Claims History screens scripting.
 * 
 */
public boolean funcClaimsHistory(Map<Object, Object> map_data){
	
	boolean r_value= true;
		
		customAssert.assertTrue(common.funcPageNavigation("Claims History", ""),"Claims History page navigations issue(S)");
		
		try {
			//Add Claims History Data
			addInput_ClaimsHistoryData("this_year", 0);
			addInput_ClaimsHistoryData("this_year", 1);
			addInput_ClaimsHistoryData("this_year", 2);
			addInput_ClaimsHistoryData("this_year", 3);
			addInput_ClaimsHistoryData("this_year", 4);
			addInput_ClaimsHistoryData("this_year", 5);
		
			customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Claims History .");
			
			TestUtil.reportStatus("Claims History details are filled sucessfully . ", "Info", true);
			stale_count = 0;
			return r_value;
		}catch(StaleElementReferenceException stale_e) {
			if(stale_count++ < 3)
				funcClaimsHistory(map_data);
			System.out.println("StaleElementReferenceException in funcClaimsHistory function after >"+stale_count+"< attempts - "+stale_e);
		}catch(Throwable t){
			
		}
		stale_count = 0;
		return false;
	}

public boolean addInput_ClaimsHistoryData(String year_keyword,int year){
	
	boolean r_value = true;
	Map<Object,Object> data_map = new HashMap<>();
	switch(common.currentRunningFlow){
	case "NB":
		data_map = common.NB_excel_data_map;
		break;
	case "MTA":
		data_map = common.MTA_excel_data_map;
		break;
	case "Renewal":
		data_map = common.Renewal_excel_data_map;
		break;
	case "Rewind":
		data_map = common.Rewind_excel_data_map;
		break;
	}
	
	try{
	
	String _openClaim_xpath = "//*[contains(@id,'open_claims_"+year_keyword+"_"+year+"')]";
	customAssert.assertTrue(k.DynamicXpathWebElement_driver(_openClaim_xpath,(String)data_map.get("CH_open_claims_"+year_keyword+"_"+year),"Input"),"Error while adding "+year_keyword+"_"+year+" Open Claims data . ");
	//CH_open_claims_this_year_0
	String _closedClaim_xpath = "//*[contains(@id,'closed_claims_"+year_keyword+"_"+year+"')]";
	customAssert.assertTrue(k.DynamicXpathWebElement_driver(_closedClaim_xpath,(String)data_map.get("CH_closed_claims_"+year_keyword+"_"+year),"Input"),"Error while adding "+year_keyword+"_"+year+" Closed Claims data . ");
	
	String _reserve_xpath = "//*[contains(@id,'reserve_"+year_keyword+"_"+year+"')]";
	customAssert.assertTrue(k.DynamicXpathWebElement_driver(_reserve_xpath,(String)data_map.get("CH_reserve_"+year_keyword+"_"+year),"Input"),"Error while adding "+year_keyword+"_"+year+" reserve data . ");
	
	String _paid_xpath = "//*[contains(@id,'paid_"+year_keyword+"_"+year+"')]";
	customAssert.assertTrue(k.DynamicXpathWebElement_driver(_paid_xpath,(String)data_map.get("CH_paid_"+year_keyword+"_"+year),"Input"),"Error while adding "+year_keyword+"_"+year+" paid data . ");
		
	}catch(Throwable t){
		System.out.println("Error while adding Claims History Data - "+t);
	}
	
	return r_value;
	
	
}

public boolean calculate_Total(List<String> coverList,String code,Map<Object,Object> data_map,String testName,String total_Key){
	boolean r_value=true;
	double _val_total=0.0;String val_total=null;
	try{
		for(String cover:coverList){
			_val_total = _val_total + Double.parseDouble((String)data_map.get("PS_"+cover+"_"+code));
	
		}
		val_total=Double.toString(_val_total);
		customAssert.assertTrue(TestUtil.WriteDataToXl(TestBase.product+"_"+common.currentRunningFlow, "Premium Summary", testName,"PS_"+total_Key,val_total,data_map),"Error while writing Total NN Premium to excel .");
	}catch(Throwable t){
		r_value=false;
	}

	return r_value;

}


//-----PIA Rater Calculation Functions ----////
/**
 * This method Calculates Rater sheet's B7 Value
 */
public int calculate_ActualNo_OfYears(){
	
Map<Object,Object> data_map = null;
	
	switch(common.currentRunningFlow){
	
		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;
	
	}
	
	String policyStartDate = null;
	try{
		
		if(common.currentRunningFlow.equalsIgnoreCase("NB")) //NB
			policyStartDate = k.getAttribute("Policy_Start_Date", "value");
		else if(common.currentRunningFlow.equalsIgnoreCase("Rewind") && !TestBase.businessEvent.equals("MTA")) { //NB Rewind{
			if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes")){
				policyStartDate = driver.findElement(By.xpath("//*[contains(@id,'start_date') and @class='read']")).getText();
			}else {
				policyStartDate = k.getAttribute("Policy_Start_Date", "value");
			}
		}else if(common.currentRunningFlow.equalsIgnoreCase("MTA") || common.currentRunningFlow.equalsIgnoreCase("Rewind")) //MTA | MTA Rewind
			policyStartDate = driver.findElement(By.xpath("//*[contains(@id,'start_date') and @class='read']")).getText();
		else if(common.currentRunningFlow.equalsIgnoreCase("Renewal"))
			policyStartDate = k.getAttribute("Policy_Start_Date", "value");
		
		//String policyStartDate = "27/05/2017";
		int month_of_policyStartDate = Integer.parseInt(policyStartDate.split("/")[1]);
		int year_of_PolicyStartDate = Integer.parseInt(policyStartDate.split("/")[2]); 
		if(month_of_policyStartDate < 10){
			actual_no_of_years = year_of_PolicyStartDate-1-Integer.parseInt((String)data_map.get("PG_DateEstablishment"));
		}else{
			actual_no_of_years = year_of_PolicyStartDate-Integer.parseInt((String)data_map.get("PG_DateEstablishment"));
		}
	}catch(Throwable t){
		System.out.println("Error while Calculating Actual No Of Years - "+t);
		return 0;
	}
	return actual_no_of_years;
	
}
/**
 * This method Calculates Rater sheet's B8 Value
 */
public int calculate_No_OfYears(){
	
	PIA_Rater = OR.getORProperties();
	actual_no_of_years=calculate_ActualNo_OfYears();
	
	int no_of_years=0;
	try{
		if(actual_no_of_years < 1){
			no_of_years = 1;
		}else{
			if(actual_no_of_years > Integer.parseInt(PIA_Rater.getProperty("PIA_No_years_claims_to_look_at"))){
				no_of_years = Integer.parseInt(PIA_Rater.getProperty("PIA_No_years_claims_to_look_at"));
			}else{
				no_of_years = actual_no_of_years;
			}
		}
	}catch(Throwable t){
		System.out.println("Error while Calculating No Of Years - "+t);
	}
	return no_of_years;
	
}

/**
 * This method Calculates Premium_Rates sheet's A14(Base Rate) Value
 */
public double calculate_BaseRate(){
	
	double baseRate = 0.0;
	
Map<Object,Object> data_map = null;
	
	switch(common.currentRunningFlow){
	
		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;
	
	}
	PIA_Rater = OR.getORProperties();
	
	try{
		if(((String)data_map.get("PG_IsYourPracticeLLP")).equals("Yes")){
			baseRate = Double.parseDouble(PIA_Rater.getProperty("Base_Rate_LLP"));
		}else{
			baseRate = Double.parseDouble(PIA_Rater.getProperty("Base_Rate_Non_LLP"));
		}
	}catch(Throwable t){
		System.out.println("Error while Calculating No Of Years - "+t);
	}
	return baseRate;
	
}

/**
 * This method Calculates Premium_Rates sheet's B14(RDI Rate) Value
 */
public double calculate_RDIRate(){
	
	double rdiRate = 0.0;
	df = new SimpleDateFormat("dd/MM/yyyy");
	PIA_Rater = OR.getORProperties();
	
Map<Object,Object> data_map = null;
	
	switch(common.currentRunningFlow){
	
		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;
	
	}
	//Policy_End_Date.compareTo(todaysDate)>0
	try{
		/*if(common.currentRunningFlow.equalsIgnoreCase("NB")) //NB
			riskStartDate = (Date)df.parse(k.getAttribute("Policy_Start_Date", "value"));
		else if(common.currentRunningFlow.equalsIgnoreCase("Rewind") && !TestBase.businessEvent.equals("MTA")) { //NB Rewind{
			if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes")){
				riskStartDate = (Date)df.parse(driver.findElement(By.xpath("//*[contains(@id,'start_date') and @class='read']")).getText());
			}else {
				riskStartDate = (Date)df.parse(k.getAttribute("Policy_Start_Date", "value"));
			}
		}else if(common.currentRunningFlow.equalsIgnoreCase("MTA") || common.currentRunningFlow.equalsIgnoreCase("Rewind")) //MTA | MTA Rewind
			riskStartDate = (Date)df.parse(driver.findElement(By.xpath("//*[contains(@id,'start_date') and @class='read']")).getText());
		else if(common.currentRunningFlow.equalsIgnoreCase("Renewal"))
			riskStartDate = (Date)df.parse(k.getAttribute("Policy_Start_Date", "value"));*/
		//riskStartDate = (Date)df.parse("27/05/2017");
		
		/*//Right Side lookup
		Date r_first_Date = (Date)df.parse("01/10/2016");
		Date r_last_Date = (Date)df.parse("30/09/2017");
		
		if((riskStartDate.compareTo(r_first_Date) > 0 ) && (riskStartDate.compareTo(r_last_Date) < 0 )){
			
			switch(Integer.parseInt((String)data_map.get("PG_DateEstablishment"))){
			
				case 2014:
					rdiRate = Double.parseDouble(PIA_Rater.getProperty("RDI_Risk_Start_01_10_2016_to_30_09_2017_DOE_2014"));
					return rdiRate;
				case 2015:
					rdiRate = Double.parseDouble(PIA_Rater.getProperty("RDI_Risk_Start_01_10_2016_to_30_09_2017_DOE_2015"));
					return rdiRate;
				case 2016:
					rdiRate = Double.parseDouble(PIA_Rater.getProperty("RDI_Risk_Start_01_10_2016_to_30_09_2017_DOE_2016"));
					return rdiRate;
				case 2017:
					rdiRate = Double.parseDouble(PIA_Rater.getProperty("RDI_Risk_Start_01_10_2016_to_30_09_2017_DOE_2017"));
					return rdiRate;
				default:
					
					return 1;
			
			}
		}
		
			//Left Side lookup
			Date l_first_Date = (Date)df.parse("01/10/2017");
			Date l_last_Date = (Date)df.parse("30/09/2018");
				
			if((riskStartDate.compareTo(l_first_Date) > 0 ) && (riskStartDate.compareTo(l_last_Date) < 0 )){
					
				switch(Integer.parseInt((String)data_map.get("PG_DateEstablishment"))){
					
					case 2014:
						rdiRate = Double.parseDouble(PIA_Rater.getProperty("RDI_Risk_Start_01_10_2017_to_30_09_2018_DOE_2014"));
						return rdiRate;
					case 2015:
						rdiRate = Double.parseDouble(PIA_Rater.getProperty("RDI_Risk_Start_01_10_2017_to_30_09_2018_DOE_2015"));
						return rdiRate;
					case 2016:
						rdiRate = Double.parseDouble(PIA_Rater.getProperty("RDI_Risk_Start_01_10_2017_to_30_09_2018_DOE_2016"));
						return rdiRate;
					case 2017:
						rdiRate = Double.parseDouble(PIA_Rater.getProperty("RDI_Risk_Start_01_10_2017_to_30_09_2018_DOE_2017"));
						return rdiRate;
					default:
						return 1;
					
				}
			}
			
			//CR 274- SPI2 change
			//Left Side second lookup
			Date l2_first_Date = (Date)df.parse("01/10/2018");
			Date l2_last_Date = (Date)df.parse("30/09/2019");
				
			if((riskStartDate.compareTo(l2_first_Date) > 0 ) && (riskStartDate.compareTo(l2_last_Date) < 0 )){
					
				switch(Integer.parseInt((String)data_map.get("PG_DateEstablishment"))){
					
					case 2016:
						rdiRate = Double.parseDouble(PIA_Rater.getProperty("PIA_RDI_Risk_Start_01_10_2018_to_30_09_2019_DOE_2016"));
						return rdiRate;
					case 2017:
						rdiRate = Double.parseDouble(PIA_Rater.getProperty("PIA_RDI_Risk_Start_01_10_2018_to_30_09_2019_DOE_2017"));
						return rdiRate;
					case 2018:
						rdiRate = Double.parseDouble(PIA_Rater.getProperty("PIA_RDI_Risk_Start_01_10_2018_to_30_09_2019_DOE_2018"));
						return rdiRate;
					default:
						return 1;
					
				}
			}*/
			
			//As Per SUP - 1765
			TimeZone uk_timezone = TimeZone.getTimeZone("Europe/London");
			int runningYear = Calendar.getInstance(uk_timezone).get(Calendar.YEAR); 
			
			if(Integer.parseInt((String)data_map.get("PG_DateEstablishment")) == runningYear) {
				rdiRate = Double.parseDouble(PIA_Rater.getProperty("PIA_RDI_DOE_RunningYear"));
			}else if(runningYear - Integer.parseInt((String)data_map.get("PG_DateEstablishment")) == 1) {
				rdiRate = Double.parseDouble(PIA_Rater.getProperty("PIA_RDI_DOE_RunningYearMinusOne"));
			}else if(runningYear - Integer.parseInt((String)data_map.get("PG_DateEstablishment")) == 2) {
				rdiRate = Double.parseDouble(PIA_Rater.getProperty("PIA_RDI_DOE_RunningYearMinusTwo"));
			}else {
				rdiRate = 1;
			}
				
		
	}catch(Throwable t){
		System.out.println("Error while Calculating RDI rate - "+t);
	}
	return rdiRate;
	
}

/**
 * This method Calculates Premium_Rates sheet's C14(Size Rate) Value
 */
public double calculate_SizeRate(){
	
	double sizeRate = 0.0;
	
Map<Object,Object> data_map = null;
	
	switch(common.currentRunningFlow){
	
		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;
	
	}
	PIA_Rater = OR.getORProperties();
	
	try{
		
		//Changed As per SUP-1765
		
		//MIN(C10,MAX(C9,C7*POWER(Rater!B5,C8)))
		//c10 = Max Load
		//c9 = Max Discount
		//c7 = multiplier
		//c8 = power
		//Rater!B5 = Gross Fee
		
		double MULTIPLIER = Double.parseDouble(PIA_Rater.getProperty("PIA_MULTIPLIER"));
		double POWER = Double.parseDouble(PIA_Rater.getProperty("PIA_POWER"));
		spi_GrossFee = Double.parseDouble((String)data_map.get("SP_GrossFee"));
		
		//Find Max
		double max_part = 0.0;
		if((MULTIPLIER*Math.pow(spi_GrossFee, POWER)) > Double.parseDouble(PIA_Rater.getProperty("PIA_Max_Discount"))) {
			max_part = (MULTIPLIER*Math.pow(spi_GrossFee, POWER));
		}else {
			max_part = Double.parseDouble(PIA_Rater.getProperty("PIA_Max_Discount"));
		}
		
		//Find Min
		double min_part = 0.0;
		if(max_part > Double.parseDouble(PIA_Rater.getProperty("PIA_Max_Load"))) {
			min_part = Double.parseDouble(PIA_Rater.getProperty("PIA_Max_Load"));
		}else {
			min_part = max_part;
		}
				
		sizeRate = min_part;
		
		/*//int MULTIPLIER = Integer.parseInt(PIA_Rater.getProperty("MULTIPLIER"));
		//CR 274
		double MULTIPLIER = Double.parseDouble(PIA_Rater.getProperty("PIA_MULTIPLIER"));
		
		double POWER = Double.parseDouble(PIA_Rater.getProperty("PIA_POWER"));
		spi_GrossFee = Double.parseDouble((String)data_map.get("SP_GrossFee"));
		
		if((MULTIPLIER*Math.pow(spi_GrossFee, POWER)) > 1){
			sizeRate=1;
		}else{			sizeRate = MULTIPLIER*Math.pow(spi_GrossFee, POWER);
		}*/
		
		
	}catch(Throwable t){
		System.out.println("Error while Calculating Size Rate - "+t);
	}
	return sizeRate;
	
}

/**
 * This method Calculates Premium_Rates sheet's AS3(Ops Rate) Value
 */
public double calculate_OPsRate(){
	
	double final_OPsRate = 0.0;
	PIA_Rater = OR.getORProperties();
	int count_AOP = common.no_of_inner_data_sets.get("Area of Practice");
	String areaOfPractice=null;
	int AOP_Cent_Value = 0;
	int total_AOP_Cent_Value = 0;
	double AOP_Rate = 0.0;
	double AOP_final_value=0.0;
	String[] aop_ =null;
	
Map<Object,Object> data_map = null;
Map<String, List<Map<String, String>>> data_Structure_of_InnerPagesMaps = null;
	
	switch(common.currentRunningFlow){
	
		case "NB":
			data_map = common.NB_excel_data_map;
			data_Structure_of_InnerPagesMaps = common.NB_Structure_of_InnerPagesMaps;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			data_Structure_of_InnerPagesMaps = common.MTA_Structure_of_InnerPagesMaps;
			aop_ = ((String)data_map.get("SP_AreaofPractice")).split(";");
			count_AOP = aop_.length;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			data_Structure_of_InnerPagesMaps = common.Renewal_Structure_of_InnerPagesMaps;
			aop_ = ((String)data_map.get("SP_AreaofPractice")).split(";");
			count_AOP = aop_.length;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			data_Structure_of_InnerPagesMaps = common.Rewind_Structure_of_InnerPagesMaps;
			aop_ = ((String)data_map.get("SP_AreaofPractice")).split(";");
			count_AOP = aop_.length;
			break;
	
	}
	
	try{
		
		String WorkSplit = (String)data_map.get("SP_WorkSplittoUse");
		
		switch(WorkSplit){
		
		case "Last Year":
			
			for(int count=0;count<count_AOP;count++){
				
				areaOfPractice = data_Structure_of_InnerPagesMaps.get("Area of Practice").get(count).get("PIA_AOP_AreaOfPracticeValue");
				AOP_Cent_Value = Integer.parseInt(data_Structure_of_InnerPagesMaps.get("Area of Practice").get(count).get("PIA_AOP_LastYear"));
				total_AOP_Cent_Value = total_AOP_Cent_Value + AOP_Cent_Value;
				AOP_Rate = get_AreaOfPractice_Rate(areaOfPractice);
				AOP_final_value = AOP_Cent_Value*AOP_Rate;
				if(total_AOP_Cent_Value > 100){
					final_OPsRate = 0;
					System.out.println("** Total Area Of Practice 'Last Year %' is greater than 100 **");
				}else{
					final_OPsRate = final_OPsRate + AOP_final_value;
				}
			}
			
			if(total_AOP_Cent_Value < 100){
				final_OPsRate = 0;
				System.out.println("** Total Area Of Practice 'Last Year %' is less than 100 **");
			}
			
			break;
			
		case "Prior Year":
			
			for(int count=0;count<count_AOP;count++){
				
				areaOfPractice = data_Structure_of_InnerPagesMaps.get("Area of Practice").get(count).get("PIA_AOP_AreaOfPracticeValue");
				AOP_Cent_Value = Integer.parseInt(data_Structure_of_InnerPagesMaps.get("Area of Practice").get(count).get("PIA_AOP_PriorYear"));
				total_AOP_Cent_Value = total_AOP_Cent_Value + AOP_Cent_Value;
				AOP_Rate = get_AreaOfPractice_Rate(areaOfPractice);
				AOP_final_value = AOP_Cent_Value*AOP_Rate;
				if(total_AOP_Cent_Value > 100){
					final_OPsRate = 0;
					System.out.println("** Total Area Of Practice 'Prior Year %' is greater than 100 **");
				}else{
					final_OPsRate = final_OPsRate + AOP_final_value;
				}
			}
			if(total_AOP_Cent_Value < 100){
				final_OPsRate = 0;
				System.out.println("** Total Area Of Practice 'Prior Year %' is less than 100 **");
			}
			
			break;
			
		case "Prior Year 2":
			
			for(int count=0;count<count_AOP;count++){
				
				areaOfPractice = data_Structure_of_InnerPagesMaps.get("Area of Practice").get(count).get("PIA_AOP_AreaOfPracticeValue");
				AOP_Cent_Value = Integer.parseInt(data_Structure_of_InnerPagesMaps.get("Area of Practice").get(count).get("PIA_AOP_PriorYear2"));
				total_AOP_Cent_Value = total_AOP_Cent_Value + AOP_Cent_Value;
				AOP_Rate = get_AreaOfPractice_Rate(areaOfPractice);
				AOP_final_value = AOP_Cent_Value*AOP_Rate;
				if(total_AOP_Cent_Value > 100){
					final_OPsRate = 0;
					System.out.println("** Total Area Of Practice 'Prior Year %' is greater than 100 **");
				}else{
					final_OPsRate = final_OPsRate + AOP_final_value;
				}
			}
			if(total_AOP_Cent_Value < 100){
				final_OPsRate = 0;
				System.out.println("** Total Area Of Practice 'Prior Year %' is less than 100 **");
			}
			
			break;
			
			
		}
			
	}catch(Throwable t){
		System.out.println("Error while Calculating OPs Rate - "+t);
	}
	final_OPsRate = final_OPsRate/100;
	return final_OPsRate;
	
}

/**
 * This method Calculates Premium_Rates sheet's D14(Area Rate) Value
 */
public double calculate_AreaRate(){
	
	double final_AreaRate = 1; //Need to derive formula
	
	
Map<Object,Object> data_map = null;
Map<String, List<Map<String, String>>> data_Structure_of_InnerPagesMaps = null;
	
	switch(common.currentRunningFlow){
	
		case "NB":
			data_map = common.NB_excel_data_map;
			data_Structure_of_InnerPagesMaps = common.NB_Structure_of_InnerPagesMaps;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			data_Structure_of_InnerPagesMaps = common.MTA_Structure_of_InnerPagesMaps;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			data_Structure_of_InnerPagesMaps = common.Renewal_Structure_of_InnerPagesMaps;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			data_Structure_of_InnerPagesMaps = common.Rewind_Structure_of_InnerPagesMaps;
			break;
	
	}
	
	try{
		
		
	}catch(Throwable t){
		System.out.println("Error while Calculating Area Rate - "+t);
	}
	
	return final_AreaRate;
	
}

/**
 * This method Calculates Rater sheet's J20(PracticeDiversity) Value
 */
public double calculate_PracticeDiversity(){

	double practiceDiversity = 0.0;
	int count_AOP = common.no_of_inner_data_sets.get("Area of Practice");
	int AOP_Cent_Value = 0;
	int total_AOP_Cent_Value = 0;
	String[] aop_ =null;

	Map<Object,Object> data_map = null;
	Map<String, List<Map<String, String>>> data_Structure_of_InnerPagesMaps = null;

	switch(common.currentRunningFlow){

	case "NB":
		data_map = common.NB_excel_data_map;
		data_Structure_of_InnerPagesMaps = common.NB_Structure_of_InnerPagesMaps;
		break;
	case "MTA":
		data_map = common.MTA_excel_data_map;
		data_Structure_of_InnerPagesMaps = common.MTA_Structure_of_InnerPagesMaps;
		aop_ = ((String)data_map.get("SP_AreaofPractice")).split(";");
		count_AOP = aop_.length;
		break;
	case "Renewal":
		data_map = common.Renewal_excel_data_map;
		data_Structure_of_InnerPagesMaps = common.Renewal_Structure_of_InnerPagesMaps;
		aop_ = ((String)data_map.get("SP_AreaofPractice")).split(";");
		count_AOP = aop_.length;
		break;
	case "Rewind":
		data_map = common.Rewind_excel_data_map;
		data_Structure_of_InnerPagesMaps = common.Rewind_Structure_of_InnerPagesMaps;
		aop_ = ((String)data_map.get("SP_AreaofPractice")).split(";");
		count_AOP = aop_.length;
		break;

	}


	PIA_Rater = OR.getORProperties();

	try{

		//=IF(MAX(E2:E16,H2:H16,K2:K9)/K10<0.25,1.1,1)
		int max_aop = 0;
		String WorkSplit = (String)data_map.get("SP_WorkSplittoUse");
		String AOP_string = "";
		int[] aop_centArray = new int[count_AOP]; 
		switch(WorkSplit){

		case "Last Year":
			AOP_string = "PIA_AOP_LastYear";
			break;
		case "Prior Year":
			AOP_string = "PIA_AOP_PriorYear";
			break;
		case "Prior Year 2":
			AOP_string = "PIA_AOP_PriorYear2";
			break;
		
		}

		for(int count=0;count<count_AOP;count++){
			AOP_Cent_Value = Integer.parseInt(data_Structure_of_InnerPagesMaps.get("Area of Practice").get(count).get(AOP_string));
			total_AOP_Cent_Value = total_AOP_Cent_Value + AOP_Cent_Value;
			aop_centArray[count] = AOP_Cent_Value;
		}
		
		max_aop = maxAOPCentValue(aop_centArray);
		
		double first_value = Double.parseDouble(Integer.toString(max_aop))/Double.parseDouble(Integer.toString(total_AOP_Cent_Value));
		
		practiceDiversity = first_value < 0.25?1.1:1;
	
	}catch(Throwable t){
		System.out.println("Error while Calculating practiceDiversity - "+t);
	}
	return practiceDiversity;

}
	
public int maxAOPCentValue(int[] AOPCentValues) {
	
	int max = AOPCentValues[0];
	
	for(int i=0;i<AOPCentValues.length;i++) {
		for(int j=i;j<AOPCentValues.length;j++) {
			if(AOPCentValues[i] > AOPCentValues[j])
				max = AOPCentValues[i];
		}
	}
	
	return max;
	
}


	/**
	 * This method reads Area Of Practices property values
	 */
	public double get_AreaOfPractice_Rate(String area_Of_Practice){

		double OPsRate = 0.0;
		PIA_Rater = OR.getORProperties();

		try{

			switch(area_Of_Practice){

			case "Acting as an Arbitrator, Adjudicator and Mediator":
				return Double.parseDouble(PIA_Rater.getProperty("Acting_as_an_Arbitrator_Adjudicator_and_Mediator"));
			case "Administering oaths, taking affidavits and Notary Public":
				return Double.parseDouble(PIA_Rater.getProperty("Administering_oaths_taking_affidavits_and_Notary_Public"));
			case "Agency Advocacy":
				return Double.parseDouble(PIA_Rater.getProperty("Agency_Advocacy"));
			case "Children, Mental Health Tribunal and Welfare":
				return Double.parseDouble(PIA_Rater.getProperty("Children_Mental_Health_Tribunal_and_Welfare"));
			case "Conveyancing - Commercial":
				return Double.parseDouble(PIA_Rater.getProperty("PIA_Conveyancing_Commercial")); // Changed as Per SUP - 1765
			case "Conveyancing - Residential":
				return Double.parseDouble(PIA_Rater.getProperty("PIA_Conveyancing_Residential"));
			case "Corporate/Commercial work, including public companies":
				return Double.parseDouble(PIA_Rater.getProperty("Corporate_Commercial_work_including_public_companies"));
			case "Corporate/Commercial, (excluding work related to public companies)":
				return Double.parseDouble(PIA_Rater.getProperty("Corporate_Commercial_excluding_work_related_to_public_companies"));
			case "Criminal Law ":
				return Double.parseDouble(PIA_Rater.getProperty("Criminal_Law"));
			case "Debt Collection":
				return Double.parseDouble(PIA_Rater.getProperty("Debt_Collection"));
			case "Defendant litigious work for Insurers":
				return Double.parseDouble(PIA_Rater.getProperty("Defendant_litigious_work_for_Insurers"));
			case "EC Competition Law and Human Rights Law":
				return Double.parseDouble(PIA_Rater.getProperty("EC_Competition_Law_and_Human_Rights_Law"));
			case "Employment":
				return Double.parseDouble(PIA_Rater.getProperty("Employment"));
			case "Financial Advice and Services regulated by the Solicitors Regulation Authority":
				return Double.parseDouble(PIA_Rater.getProperty("Financial_Advice_and_Services_regulated_by_the_Solicitors_Regulation_Authority"));
			case "Financial Advice and Services where you opted into regulation by the FCA/ FSA":
				return Double.parseDouble(PIA_Rater.getProperty("Financial_Advice_and_Services_where_you_opted_into_regulation_by_the_FCA_FSA"));
			case "Immigration":
				return Double.parseDouble(PIA_Rater.getProperty("Immigration"));
			case "Intellectual Property including Patent, Trademark and Copyright":
				return Double.parseDouble(PIA_Rater.getProperty("Intellectual_Property_including_Patent_Trademark_and_Copyright"));
			case "Landlord and Tenant":
				return Double.parseDouble(PIA_Rater.getProperty("Landlord_and_Tenant"));
			case "Lecturing and related activities and expert witness work":
				return Double.parseDouble(PIA_Rater.getProperty("Lecturing_and_related_activities_and_expert_witness_work"));
			case "Litigation (Commercial) ":
				return Double.parseDouble(PIA_Rater.getProperty("Litigation_Commercial"));
			case "Litigious work other than included in any other category.":
				return Double.parseDouble(PIA_Rater.getProperty("Litigious_work_other_than_included_in_any_other_category"));
			case "Marine Litigation":
				return Double.parseDouble(PIA_Rater.getProperty("Marine_Litigation"));
			case "Matrimonial / Family ":
				return Double.parseDouble(PIA_Rater.getProperty("Matrimonial_Family"));
			case "Mergers & Acquisitions including Management":
				return Double.parseDouble(PIA_Rater.getProperty("Mergers_&_Acquisitions_including_Management"));
			case "Non-Litigious work other than included in any other category.":
				return Double.parseDouble(PIA_Rater.getProperty("PIA_NonLitigious_work_other_than_included_in_any_other_category"));
			case "Offices and Appointments ":
				return Double.parseDouble(PIA_Rater.getProperty("Offices_and_Appointments"));
			case "Other Low Risk Work":
				return Double.parseDouble(PIA_Rater.getProperty("Other_Low_Risk_Work"));
			case "Parliamentary Agency ":
				return Double.parseDouble(PIA_Rater.getProperty("Parliamentary_Agency"));
			case "Pension Trustee":
				return Double.parseDouble(PIA_Rater.getProperty("Pension_Trustee"));
			case "Personal Injury (Claimant) - Fast Track ":
				return Double.parseDouble(PIA_Rater.getProperty("Personal_Injury_Claimant_Fast_Track"));
			case "Personal Injury (Claimant) - Other":
				return Double.parseDouble(PIA_Rater.getProperty("Personal_Injury_Claimant_Other"));
			case "Personal Injury (Defendant)":
				return Double.parseDouble(PIA_Rater.getProperty("Personal_Injury_Defendant"));
			case "Probate and Estate Administration":
				return Double.parseDouble(PIA_Rater.getProperty("Probate_and_Estate_Administration"));
			case "Property Selling / Valuations and Property Management":
				return Double.parseDouble(PIA_Rater.getProperty("Property_Selling_Valuations_and_Property_Management"));
			case "Tax Planning/Mitigation":
				return Double.parseDouble(PIA_Rater.getProperty("Tax_PlanningMitigation"));
			case "Town & Country Planning":
				return Double.parseDouble(PIA_Rater.getProperty("Town_&_Country_Planning"));
			case "Trusts":
				return Double.parseDouble(PIA_Rater.getProperty("Trusts"));
			case "Wills":
				return Double.parseDouble(PIA_Rater.getProperty("Wills"));

			default:
				break;



			}



		}catch(Throwable t){
			System.out.println("Error while Calculating OPs Rate - "+t);
		}
		return OPsRate;

	}

	/**
	 * This method Calculates Premium_Rates sheet's A14/B57(Notional Rate) Value
	 */
	public double calculate_NotionalRate(){

		double notionalRate = 0.0;
		PIA_Rater = OR.getORProperties();

		try{
			double base_rate = calculate_BaseRate();
			double RDI_rate = calculate_RDIRate();
			double size_rate = calculate_SizeRate();
			double OPs_rate = calculate_OPsRate();

			//AS per SUP - 1765
			double Area_rate = calculate_AreaRate();
			double PracticeDiversity = calculate_PracticeDiversity();
			
			notionalRate = base_rate*RDI_rate*size_rate*OPs_rate*Area_rate*PracticeDiversity;

		}catch(Throwable t){
			System.out.println("Error while Calculating Notional Rate - "+t);
		}
		return notionalRate;

	}

	/**
	 * This method Calculates Premium_Rates sheet's B14/B58(Notional Premium) Value
	 */
	public double calculate_NotionalPremium(){

		double notionalPremium = 0.0;

		Map<Object,Object> data_map = null;

		switch(common.currentRunningFlow){

		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;

		}

		PIA_Rater = OR.getORProperties();
		spi_GrossFee = Double.parseDouble((String)data_map.get("SP_GrossFee"));
		try{
			notionalPremium = calculate_NotionalRate()*spi_GrossFee;
			//Two Decimal
		}catch(Throwable t){
			System.out.println("Error while Calculating Notional Rate - "+t);
		}
		//notionalPremium = Double.parseDouble(common.roundedOff(Double.toString(notionalPremium)));
		return notionalPremium;

	}

	/**
	 * This method Calculates Premium_Rates sheet's C17(Established 4 or more years?) Value
	 */
	public String Established_4_or_more_years(){

		String r_value = null;
		r_value = calculate_No_OfYears()>4?"Yes":"No";  
		return r_value;

	}


	/**
	 * This method Calculates Premium_Rates sheet's B19(ClaimIncurred flags) Value
	 */
	public int get_ClaimIncurred(int claim_year_seq,String year_to_consider){

		int claimIncurred = 0;
		String year=null;

		Map<Object,Object> data_map = null;

		switch(common.currentRunningFlow){

		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;

		}
		try{

			/*switch(year_to_consider){

		case "This Year":
			year="TY";
			break;
		case "Last Year":
			year="LY";
			break;
		}*/

			switch(year_to_consider){

			case "This Year":
				year="this_year";
				break;
			case "Last Year":
				year="last_year";
				break;
			}

			//CH_2011_Open_Claims_TY
			double _paid = Double.parseDouble((String)data_map.get("CH_paid_"+year+"_"+claim_year_seq));
			double _reserve = Double.parseDouble((String)data_map.get("CH_reserve_"+year+"_"+claim_year_seq));
			if((_paid > 0) || (_reserve > 0)){
				claimIncurred=1;
			}else{
				claimIncurred=0;
			} 


		}catch(Throwable t){
			System.out.println("Error while Calculating Claim Incurred - "+t);
		}
		return claimIncurred;

	}
	/**
	 * This method Calculates Premium_Rates sheet's B25(Total Years with Claims incurred) Value
	 */
	public int calculate_Total_Years_with_Claims_incurred(){

		int total_yrs = 0;
		PIA_Rater = OR.getORProperties();
		try{
			String[] years_for_Claims = PIA_Rater.getProperty("PIA_Years_to_consider_For_Claims").split(",");
			int[] year_index = {0,1,2,3,4};
			for(int i=0;i<years_for_Claims.length;i++){
				total_yrs = total_yrs + get_ClaimIncurred(year_index[i],"This Year");
			}

		}catch(Throwable t){
			System.out.println("Error while Calculating Total Yesrs Claim Incurred - "+t);
		}
		return total_yrs;

	}

	/**
	 * This method Calculates Premium_Rates sheet's B26(Eligible for claims discount? (>3 years history + Claims in 1 year only?)) Value
	 */
	public String Eligible_for_claims_discount_3_years_only(){

		String isEligibleForClaimDiscount = "";

		try{

			switch(Established_4_or_more_years()){

			case "Yes":
				if(calculate_Total_Years_with_Claims_incurred()==1){
					isEligibleForClaimDiscount = "Yes";
				}else{
					isEligibleForClaimDiscount = "No";
				}
				break;

			case "No":
				isEligibleForClaimDiscount = "No";
				break;
			default:
				System.out.println("** Invalid Value - Established_4_or_more_years ***");
				break;
			}



		}catch(Throwable t){
			System.out.println("Error while Calculating Eligible_for_claims_discount_3_years_only - "+t);
		}
		return isEligibleForClaimDiscount;

	}

	/**
	 * This method Calculates Premium_Rates sheet's B27(Year in which claim(s) incurred) Value
	 */
	public String Year_in_which_claims_incurred(){

		try{

			if(calculate_Total_Years_with_Claims_incurred()==0){
				return "None";
			}else{
				if(calculate_Total_Years_with_Claims_incurred()>1){
					return "Multiple";
				}else{
					String[] years_for_Claims = PIA_Rater.getProperty("PIA_Years_to_consider_For_Claims").split(",");
					for(String claim_year : years_for_Claims){
						if(get_ClaimIncurred(Integer.parseInt(claim_year),"This Year")==1)
							return claim_year;
						else
							continue;
					}

				}
			}
		}catch(Throwable t){
			System.out.println("Error while Calculating Year in which claim(s) incurred - "+t);
			return "";
		}
		return null;

	}

	/**
	 * This method Calculates Premium_Rates sheet's B29(No. claims reported) Value
	 */
	public int No_of_claims_reported(){

		int total_claims_reported = 0;
		PIA_Rater = OR.getORProperties();
		try{
			String[] years_for_Claims = PIA_Rater.getProperty("PIA_Years_to_consider_For_Claims").split(",");
			for(String claim_year : years_for_Claims){
				total_claims_reported = total_claims_reported + get_ClaimReported(Integer.parseInt(claim_year),"This Year");
			}



		}catch(Throwable t){
			System.out.println("Error while Calculating No_of_claims_reported - "+t);
			return 0;
		}
		return total_claims_reported;

	}

	/**
	 * This method Calculates Open Closed Claims Reported Value
	 */
	public int get_ClaimReported(int claim_year,String year_to_consider){

		int claimReported = 0;
		String year=null;

		Map<Object,Object> data_map = null;

		switch(common.currentRunningFlow){

		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;

		}
		try{

			switch(year_to_consider){

			case "This Year":
				year="TY";
				break;
			case "Last Year":
				year="LY";
				break;
			}

			//CH_2011_Open_Claims_TY
			int  _open_claims = Integer.parseInt((String)data_map.get("CH_"+claim_year+"_Open_Claims_"+year));
			int _closed_claims = Integer.parseInt((String)data_map.get("CH_"+claim_year+"_Closed_Claims_"+year));
			claimReported = _open_claims + _closed_claims;

		}catch(Throwable t){
			System.out.println("Error while Calculating Claim Incurred - "+t);
			return 0;
		}
		return claimReported;

	}

	/**
	 * This method Calculates Premium_Rates sheet's B30(Eligible for single loss discount (exactly 1 claim reported?)) Value
	 */
	public String Eligible_for_single_loss_discount_exactly_1_Claim_reported(){

		try{

			if(Eligible_for_claims_discount_3_years_only().equals("No")){
				return "No";
			}else{
				if(No_of_claims_reported()==1){
					return "Yes";
				}else{
					return "No";
				}
			}
		}catch(Throwable t){
			System.out.println("Error while Calculating Eligible_for_single_loss_discount_exactly_1_Claim_reported - "+t);
			return "";
		}

	}

	/**
	 * This method Calculates Premium_Rates sheet's B31(Claims History Adjustment) Value
	 */
	public double calculate_Claims_History_Adjustment(){

		double claimHistoryAdjust = 0.0;
		try{
			if(Eligible_for_claims_discount_3_years_only().equals("No")){
				claimHistoryAdjust=1;
			}else{
				if(calculate_No_OfYears()==3){ //CR 274 changes
					claimHistoryAdjust=0.75;
				}else if(calculate_No_OfYears()==4){ //CR 274 changes
					claimHistoryAdjust=0.625;
				}else if(calculate_No_OfYears()>=5){ //CR 274 changes
					claimHistoryAdjust=0.5;
				}
			}

		}catch(Throwable t){
			System.out.println("Error while Calculating Claims_History_Adjustment - "+t);
			return 0;
		}
		return claimHistoryAdjust;

	}


	/**
	 * This method Calculates Premium_Rates sheet's B32(Single Loss Adjustment) Value
	 */
	public double calculate_Single_Loss_Adjustment(){

		double single_Loss_Adjustment = 0.0;
		try{
			if(Eligible_for_claims_discount_3_years_only().equals("No")){
				single_Loss_Adjustment=1;
			}else{
				if(Eligible_for_single_loss_discount_exactly_1_Claim_reported().equals("Yes")){
					single_Loss_Adjustment=0.5;
				}else if(calculate_No_OfYears()==5){
					single_Loss_Adjustment=1;

				}
			}		
		}catch(Throwable t){
			System.out.println("Error while Calculating Single Loss Adjustment - "+t);
			return 0;
		}
		return single_Loss_Adjustment;

	}

	/**
	 * This method Calculates Premium_Rates sheet's B33(Total Claims Adjustment) Value
	 */
	public double calculate_Total_Claims_Adjustment(){

		double total_Claims_Adjustment = 0.0;
		try{
			total_Claims_Adjustment = calculate_Claims_History_Adjustment()*calculate_Single_Loss_Adjustment();
		}catch(Throwable t){
			System.out.println("Error while Calculating Total Claims Adjustment - "+t);
			return 0;
		}
		return total_Claims_Adjustment;

	}

	/**
	 * This method is to Calculates Premium_Rates sheet's B54(Adjusted Claims Incurred) Value
	 */
	public double get_AdjustedClaimsIncurred(int claim_year,String year_to_consider,int year_index,double total_claims_adj){

		double claimIncurred = 0;
		String year=null;

		Map<Object,Object> data_map = null;

		switch(common.currentRunningFlow){

		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;

		}

		try{

			switch(year_to_consider){

			case "This Year":
				year="this_year";
				break;
			case "Last Year":
				year="last_year";
				break;
			}

			//CR 274 change


			//CH_2011_Open_Claims_TY
			double _paid = total_claims_adj * Double.parseDouble((String)data_map.get("CH_paid_"+year+"_"+year_index));
			double _reserve = total_claims_adj * Double.parseDouble((String)data_map.get("CH_reserve_"+year+"_"+year_index));
			claimIncurred = _paid + _reserve;


		}catch(Throwable t){
			System.out.println("Error while Calculating Claim Incurred - "+t);
			return 0.0;
		}
		return claimIncurred;

	}
	/**
	 * This method Calculates Premium_Rates sheet's B54(Adjusted Claims Incurred) Value
	 */
	public double calculate_Total_Adjusted_Claims_incurred(){

		double total_Adjusted_Claims = 0;
		PIA_Rater = OR.getORProperties();
		try{
			String[] years_for_Claims = PIA_Rater.getProperty("PIA_Years_to_consider_For_Claims").split(",");
			double total_claims_adj = calculate_Total_Claims_Adjustment(); //CR 274
			//int[] year_index = {0,1,2,3,4,5,6};
			//CR 274
			int[] year_index = {0,1,2,3,4};
			for(int i=0;i< years_for_Claims.length;i++){
				total_Adjusted_Claims = total_Adjusted_Claims + get_AdjustedClaimsIncurred(Integer.parseInt(years_for_Claims[i]),"This Year",year_index[i],total_claims_adj);
			}

		}catch(Throwable t){
			System.out.println("Error while Calculating Adjusted Claims Incurred - "+t);
			return 0.0;
		}
		return total_Adjusted_Claims;

	}

	/**
	 * This method is to Calculates Premium_Rates sheet's B55(Adjusted Loaded Claims) Value
	 */
	public double get_Adjusted_Loaded_Claims(int claim_year,int claim_year_seq,String year_to_consider,int year_index,double total_claims_adj){

		double adjusted_Loaded_Claims = 0;
		String year=null;

		Map<Object,Object> data_map = null;

		switch(common.currentRunningFlow){

		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;

		}


		try{

			switch(year_to_consider){

			case "This Year":
				year="this_year";
				break;
			case "Last Year":
				year="last_year";
				break;
			}

			//CH_2011_Open_Claims_TY
			//CH_paid_this_year_0

			double _paid = total_claims_adj *Double.parseDouble((String)data_map.get("CH_paid_"+year+"_"+year_index));
			double _reserve = total_claims_adj * Double.parseDouble((String)data_map.get("CH_reserve_"+year+"_"+year_index));
			//Paid_Claims_Multiplier_previous_yr5
			if(claim_year_seq!=0){
				adjusted_Loaded_Claims = _paid*Double.parseDouble(PIA_Rater.getProperty("PIA_Paid_Claims_Multiplier_previous_yr"+claim_year_seq)) 
						+ 	_reserve*Double.parseDouble(PIA_Rater.getProperty("PIA_Reserved_Claims_Multiplier_previous_yr"+claim_year_seq)) ;
			}else{
				adjusted_Loaded_Claims = _paid*Double.parseDouble(PIA_Rater.getProperty("PIA_Paid_Claims_Multiplier_last_year"))
						+ 	_reserve*Double.parseDouble(PIA_Rater.getProperty("PIA_Reserved_Claims_Multiplier_last_year")) ;

			}


		}catch(Throwable t){
			System.out.println("Error while Calculating Adjusted Loaded Claims - "+t);
			return 0.0;
		}
		return adjusted_Loaded_Claims;

	}
	/**
	 * This method Calculates Premium_Rates sheet's B55(Adjusted Loaded Claims) Value
	 */
	public double calculate_Adjusted_Loaded_Claims(){

		double total_Adjusted_Loaded_Claims = 0.0;
		PIA_Rater = OR.getORProperties();
		try{
			String[] years_for_Claims = PIA_Rater.getProperty("PIA_Years_to_consider_For_Claims").split(",");
			String[] years_for_Claims_seq = PIA_Rater.getProperty("PIA_Years_to_consider_For_Claims_Sequence").split(",");
			double total_claims_adj = calculate_Total_Claims_Adjustment(); //CR 274
			//int[] year_index = {0,1,2,3,4,5,6};
			//CR 274
			int[] year_index = {1,2,3,4,5};
			for(int i=0;i<years_for_Claims.length;i++){
				total_Adjusted_Loaded_Claims = total_Adjusted_Loaded_Claims + get_Adjusted_Loaded_Claims(Integer.parseInt(years_for_Claims[i]),Integer.parseInt(years_for_Claims_seq[i]),"This Year",year_index[i],total_claims_adj);
			}

		}catch(Throwable t){
			System.out.println("Error while Calculating Adjusted Loaded Claims - "+t);
			return 0.0;
		}
		return total_Adjusted_Loaded_Claims;

	}

	/**
	 * This method Calculates Premium_Rates sheet's B55(Annual Burn) Value
	 */
	public double calculate_Annual_Burn(){

		double total_Annual_Burn = 0.0;
		try{

			total_Annual_Burn = calculate_Adjusted_Loaded_Claims()/calculate_No_OfYears();

		}catch(Throwable t){
			System.out.println("Error while Calculating Annual Burn - "+t);
			return 0.0;
		}
		return total_Annual_Burn;

	}

	/**
	 * This method Calculates Premium_Rates sheet's B59(Burn Rate) Value
	 */
	public double calculate_Annual_Burn_Rate(){

		double total_Annual_Burn_Rate = 0.0,raw_burn_rate=0.0;
		DecimalFormat df = new DecimalFormat("00.0000");
		try{

			total_Annual_Burn_Rate = calculate_Annual_Burn()/calculate_NotionalPremium();
			raw_burn_rate = total_Annual_Burn_Rate;
			if(Double.isInfinite(total_Annual_Burn_Rate) || Double.isNaN(total_Annual_Burn_Rate)){
				//TestUtil.reportStatus("SPI Rater | Annual Burn Rate = <b>"+0.0+" %</b>", "Info", false);
				common_PIA.PIA_Rater_output.put("Burn Rate", 0.0);
				total_Annual_Burn_Rate = 0.0;
			}else{
				total_Annual_Burn_Rate = Double.parseDouble(df.format(total_Annual_Burn_Rate));
				total_Annual_Burn_Rate = total_Annual_Burn_Rate*100;//Two Decimal
				//TestUtil.reportStatus("SPI Rater | Annual Burn Rate = <b>"+common.roundedOff(Double.toString(total_Annual_Burn_Rate))+" %</b>", "Info", false);
				common_PIA.PIA_Rater_output.put("Burn Rate", Double.parseDouble(common.roundedOff(Double.toString(total_Annual_Burn_Rate))));
			}

		}catch(Throwable t){

			System.out.println("Error while Calculating Annual Burn Rate - "+t);
			return 0.0;
		}
		//System.out.println("Annula Burn Rate B59 = "+total_Annual_Burn_Rate);
		return raw_burn_rate;

	}

	/**
	 * This method Calculates Premium_Rates sheet's B60(Is Burn between 40%-50%?) Value
	 */
	/*public boolean Is_Burn_between_40_50_Cent(){

	double annual_burn_rate = calculate_Annual_Burn_Rate();
	try{
		if(annual_burn_rate>=40 && annual_burn_rate <=50){
			return true;
		}else{

			return false;
		}

	}catch(Throwable t){
		System.out.println("Error while Calculating Is Burn between 40%-50%? - "+t);
		return false;
	}

}*/

	/**
	 * This method Calculates Premium_Rates sheet's B60(Is Burn between 51%-80%?) Value //CR 274
	 */
	public boolean Is_Burn_between_51_80_Cent(){

		double annual_burn_rate = calculate_Annual_Burn_Rate();
		annual_burn_rate *= 100;
		try{
			if((annual_burn_rate)>=50 && (annual_burn_rate) <=80){
				return true;
			}else{

				return false;
			}

		}catch(Throwable t){
			System.out.println("Error while Calculating Is Burn between 50%-80%? - "+t);
			return false;
		}

	}

	/**
	 * This method Calculates Premium_Rates sheet's B61(Claims Adjustments) Value
	 */
	public double calculate_Claims_Adjustments(){

		double total_Claims_Adjustments = 0.0;
		double annula_Burn_Rate = calculate_Annual_Burn_Rate();
		try{
			//if(annula_Burn_Rate > 0.4){
			//CR 274
			//annula_Burn_Rate=Double.parseDouble(common.roundedOff(Double.toString(annula_Burn_Rate)));
			if((annula_Burn_Rate) > 0.5){
				double f_part = (annula_Burn_Rate)-0.8;
				total_Claims_Adjustments = (f_part*1.3)+1;
			}else{
				int no_of_years_Claims = Integer.parseInt(PIA_Rater.getProperty("PIA_No_years_claims_to_look_at"));
				total_Claims_Adjustments = 1-(((1-(((0.5/0.4)*(annula_Burn_Rate))+0.5))*calculate_No_OfYears())/no_of_years_Claims);
			}
			/*	if(annula_Burn_Rate > 0.4){
				double f_part = (annula_Burn_Rate/100)-0.5;
				total_Claims_Adjustments = (f_part*1.32)+1;
			}else{
				int no_of_years_Claims = Integer.parseInt(PIA_Rater.getProperty("No_years_claims_to_look_at"));
				total_Claims_Adjustments = 1-(((1-(((0.5/0.4)*(annula_Burn_Rate/100))+0.5))*calculate_No_OfYears())/no_of_years_Claims);
			}*/		


		}catch(Throwable t){
			System.out.println("Error while Calculating Claims Adjustments - "+t);
			return 0.0;
		}
		//System.out.println("Claims Adjustments B61 = "+total_Claims_Adjustments);
		return total_Claims_Adjustments;

	}

	/**
	 * This method Calculates Premium_Rates sheet's B62(Rate) Value
	 */
	public double calculate_Rate(){

		double claim_adjustments = calculate_Claims_Adjustments();
		try{
			if(Is_Burn_between_51_80_Cent()){
				return 1;
			}else{
				return claim_adjustments;
			}

		}catch(Throwable t){
			System.out.println("Error while Calculating Rate - "+t);
			return 0;
		}

	}

	/**
	 * This method Calculates Premium_Rates sheet's B63(Capped Rate) Value
	 */
	public double calculate_Capped_Rate(){

		double capped_Rate = calculate_Rate();
		try{
			if(calculate_Rate() > 3){
				return 3;
			}else{
				return capped_Rate;
			}

		}catch(Throwable t){
			System.out.println("Error while Calculating Capped Rate - "+t);
			return 0;
		}

	}

	/**
	 * This method Calculates Premium_Rates sheet's B63(Rate Post Claims/Book Rate)/B20(Rater sheet) Value
	 */
	public double calculate_Rate_Post_Claims(){

		double rate_Post_Claims = 0.0;
		DecimalFormat formatter = new DecimalFormat("00.00000000"); //CR 274- added more decimals
		try{
			rate_Post_Claims = calculate_NotionalRate()*calculate_Rate();//6 Decimals
			if(!Double.isInfinite(rate_Post_Claims) && spi_GrossFee!=0.0 ){
				double f_book_rate = Double.parseDouble(formatter.format(((rate_Post_Claims*100))));
				common_PIA.PIA_Rater_output.put("Book_Rate_Cent", f_book_rate);
				TestUtil.reportStatus("Book Rate % = <b>"+f_book_rate+"</b>", "Info", false);
			}else{
				rate_Post_Claims = 0.0;
				common_PIA.PIA_Rater_output.put("Book_Rate_Cent", 0.0);
				TestUtil.reportStatus("Book Rate % = <b>"+0.0+"</b>", "Info", false);

			}
		}catch(Throwable t){
			System.out.println("Error while Calculating Rate Post Claims- "+t);
			return 0;
		}
		//System.out.println("Rate Post Claims Premium_Rates sheet's B63 = "+rate_Post_Claims);
		return rate_Post_Claims;
	}

	/**
	 * This method Calculates Rater sheet's B25(Technical Premium Capped) Value //CR 274
	 */
	public double Calculate_Technical_Premium_Capped(){
		Map<Object,Object> data_map = null;

		switch(common.currentRunningFlow){

		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;

		}



		double tech_Premium_cap = 0.0;
		double initial_Premium = 0.0;
		spi_GrossFee = Double.parseDouble((String)data_map.get("SP_GrossFee"));
		double OPsRate = calculate_OPsRate();
		try{
			initial_Premium = calculate_Rate_Post_Claims()*spi_GrossFee;// 2 Decimal
			//Minimum Premium Check
			if(OPsRate<0.4){
				if(initial_Premium<2150){
					tech_Premium_cap=2150;
				}else{
					common_PIA.PIA_Rater_output.put("Technical_Premium_Capped", initial_Premium);
					return initial_Premium;
				}
			}else{
				if(initial_Premium<3400){
					tech_Premium_cap=3400;
				}else{
					common_PIA.PIA_Rater_output.put("Technical_Premium_Capped", initial_Premium);
					return initial_Premium;
				}
			}

		}catch(Throwable t){
			System.out.println("Error while Calculating Technical_Premium_Capped - "+t);
			return 0;
		}
		System.out.println("Technical_Premium_Capped Rater sheet's B25 =  "+tech_Premium_cap);
		common_PIA.PIA_Rater_output.put("Technical_Premium_Capped", tech_Premium_cap);
		return tech_Premium_cap;
	}


	/**
	 * This method Calculates Rater sheet's B21/B25(Initial Premium) Value
	 */
	public double calculate_Initial_Premium(){

		Map<Object,Object> data_map = null;

		switch(common.currentRunningFlow){

		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;

		}



		double initial_Premium = 0.0;
		spi_GrossFee = Double.parseDouble((String)data_map.get("SP_GrossFee"));
		double OPsRate = calculate_OPsRate();
		try{
			initial_Premium = calculate_Rate_Post_Claims()*spi_GrossFee;// 2 Decimal
			//Minimum Premium Check
			if(OPsRate<0.4){
				if(initial_Premium<2150){
					initial_Premium=2150;
				}else{
					common_PIA.PIA_Rater_output.put("Initial_Premium", initial_Premium);
					common_PIA.PIA_Rater_output.put("Technical_Premium_Capped", initial_Premium); //CR274
					return initial_Premium;
				}
			}else{
				if(initial_Premium<3400){
					initial_Premium=3400;
				}else{
					common_PIA.PIA_Rater_output.put("Initial_Premium", initial_Premium);
					common_PIA.PIA_Rater_output.put("Technical_Premium_Capped", initial_Premium); //CR 274
					return initial_Premium;
				}
			}

		}catch(Throwable t){
			System.out.println("Error while Calculating Initial Premium - "+t);
			return 0;
		}
		//System.out.println("initial_Premium Rater sheet's B21 =  "+initial_Premium);
		common_PIA.PIA_Rater_output.put("Initial_Premium", initial_Premium);
		common_PIA.PIA_Rater_output.put("Technical_Premium_Capped", initial_Premium); //CR 274
		return initial_Premium;
	}

	/**
	 * This method Calculates Rater sheet's B22/B23/B24(Book Premium) Value
	 */
	public boolean calculate_SPI_Book_Premium(){

		double book_Premium_12m = 0.0,book_Premium_18m = 0.0,book_Premium_24m = 0.0;
		double tech_Premium_cap = calculate_Initial_Premium();
		//double tech_Premium_cap = Calculate_Technical_Premium_Capped(); //CR 274
		try{

			book_Premium_12m = tech_Premium_cap*1.05;// 2-decimal
			book_Premium_12m = Double.parseDouble(common.roundedOff(Double.toString(book_Premium_12m)));
			TestUtil.reportStatus("SPI Rater Book Premium 12m = "+book_Premium_12m, "Info", false);
			//System.out.println("Book Premium 12 m = "+book_Premium_12m);

			book_Premium_18m = (tech_Premium_cap*1.025); //2-decimal 
			//book_Premium_18m = (tech_Premium_cap*1.025) * 1.5; //2-decimal //CR 274
			book_Premium_18m = Double.parseDouble(common.roundedOff(Double.toString(book_Premium_18m)));
			TestUtil.reportStatus("SPI Rater Book Premium 18m = "+book_Premium_18m, "Info", false);
			//System.out.println("Book Premium 18 m = "+book_Premium_18m);

			book_Premium_24m = tech_Premium_cap; 
			//book_Premium_24m = tech_Premium_cap * 2; //CR274
			book_Premium_24m = Double.parseDouble(common.roundedOff(Double.toString(book_Premium_24m)));
			TestUtil.reportStatus("SPI Rater Book Premium 24m = "+book_Premium_24m, "Info", false);
			//System.out.println("Book Premium 24 m = "+book_Premium_24m);

			common_PIA.PIA_Rater_output.put("Book_Premium_12_months", book_Premium_12m);
			common_PIA.PIA_Rater_output.put("Book_Premium_18_months", book_Premium_18m);
			common_PIA.PIA_Rater_output.put("Book_Premium_24_months", book_Premium_24m);

			//System.out.println(common_PIA.PIA_Rater_output);

		}catch(Throwable t){
			System.out.println("Error while Calculating Book Premium Values - "+t);
			return false;

		}
		return true;
	}

	/**
	 * This method Calculates Rater sheet's B29/B30/B31(Gross Premium annualised) Value
	 */
	public void calculate_Gross_Premium(){

		//double gross_Premium_12m = 0.0,gross_Premium_18m = 0.0,gross_Premium_24m = 0.0;

		try{

			/*gross_Premium_24m = calculate_Initial_Premium();
		gross_Premium_12m = calculate_Initial_Premium()*1.05;
		gross_Premium_18m = calculate_Initial_Premium()*1.025;


		PIA_Rater_output.put("gross_Premium_12_months", gross_Premium_12m);
		PIA_Rater_output.put("gross_Premium_18_months", gross_Premium_18m);
		PIA_Rater_output.put("gross_Premium_24_months", gross_Premium_24m);*/


		}catch(Throwable t){
			System.out.println("Error while Calculating Gross Premium - "+t);

		}
	}


	public boolean Solicitors_PI( Map<Object, Object> map_data){
		boolean retVal = true;

		try{
			customAssert.assertTrue(common.funcPageNavigation("Solicitors PI", ""), "Navigation problem to Solicitors PI page .");

			// Area of Practice Table section :

			customAssert.assertTrue(SPI_AreaOfPractice(NB_Structure_of_InnerPagesMaps),"Unable to handle table 'Area of practice'");

			// Select Work split to value :
			customAssert.assertTrue(k.DropDownSelection("SPI_SP_WorkSplittoUse", (String)map_data.get("SP_WorkSplittoUse")), "Unable to enter 'Work Split to use' value");

			// fees/Turnover Table section :

			customAssert.assertTrue(SPI_Fees_Turnover(map_data),"Unable to handle table 'Fees/Turnover'");

			// Select 'Rate on' value :
			customAssert.assertTrue(k.DropDownSelection("SPI_SP_RateOn", (String)common.NB_excel_data_map.get("SP_RateOn")), "Unable to enter 'Rate on' value");

			String default_PI_policy_fee = k.getAttribute("PIA_SP_AdminFeeSP", "value");
			customAssert.assertEquals(default_PI_policy_fee, (String)map_data.get("SP_DefaultAdminFeeSP"),"PI Default Admin fee is In-correct. Expected: "+(String)map_data.get("SP_DefaultAdminFeeSP")+" and Actual: "+default_PI_policy_fee);

			// Excess Section :
			customAssert.assertTrue(k.Input("PIA_SP_AdminFeeSP", (String)map_data.get("SP_AdminFeeSP")),	"Unable to enter value in Admin fees field .");
			customAssert.assertTrue(k.DropDownSelection("SPI_SP_Aggegate", (String)common.NB_excel_data_map.get("SP_Aggegate")), "Unable to enter 'Aggregate' value");
			customAssert.assertTrue(calculate_SPI_Book_Premium(), "SPI Book Premium Calculation is function having issue .");
			TestUtil.reportStatus("SPI Rater Lookup values are calculated sucessfully . ", "Info", true);
			// Burn Rate:
			burnRate = common_PIA.PIA_Rater_output.get("Burn Rate");

			//Policy start and end date :
			Refer_Policy_SDate = k.getText("SPI_SP_PolicyStartDate");
			Refer_Policy_EDate = k.getText("SPI_SP_PolicyEndDate");
			dAdminFee_CoverSPI=  Double.parseDouble((String)map_data.get("SP_AdminFeeSP"));

			//Period Rating Table Section : 
			customAssert.assertTrue(coverSPI_PeriodRatingTable(map_data, ""),"Error in Period Rating Table function . ");

			TestUtil.reportStatus("Solicitors PI Cover details are filled and Verified sucessfully . ", "Info", true);

			stale_count = 0;
			return retVal;

		}catch(StaleElementReferenceException stale_ex) {
			if(stale_count++ < 3)
				Solicitors_PI(map_data);
			stale_count = 0;
			System.out.println("StaleElementReferenceException in Solicitors_PI function after >"+stale_count+"< attempts - "+stale_ex);
			return false;
		}catch(Throwable t){
			System.out.println("Error in Solicitors_PI function - "+t);
			stale_count = 0;
			return false;
		}

	}

	public boolean SPI_AreaOfPractice( Map<String, List<Map<String, String>>> mdata){
		boolean retVal = true;
		int totalCols, sTotal = 0;

		try{

			String sUniqueCol ="Area of practice";
			int tableId = 0;
			int noOfActivities =0;

			String sTablePath = "html/body/div[3]/form/div/table";

			tableId = k.getTableIndex(sUniqueCol,"xpath",sTablePath);

			sTablePath = "html/body/div[3]/form/div/table["+ tableId +"]";
			WebElement table= driver.findElement(By.xpath(sTablePath));
			totalCols = table.findElements(By.tagName("th")).size();
			try{

				noOfActivities = common.no_of_inner_data_sets.get("Area of Practice");

			}catch(Throwable t){
				noOfActivities = 0;
			}

			// Enter values to the table :

			for(int i = 0; i < noOfActivities; i++ ){

				//click on 'Add Row' link :
				driver.findElement(By.xpath(sTablePath + "/tbody/tr["+ (i+2) +"]/td[5]/a")).click();

				//Enter Data :

				customAssert.assertTrue(k.DropDownSelection_DynamicXpath(sTablePath + "/tbody/tr["+(i+1)+"]/td[1]/select", common.NB_Structure_of_InnerPagesMaps.get("Area of Practice").get(i).get("PIA_AOP_AreaOfPracticeValue")),"Unable to enter activity in Area of practice table");

				customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(i+1)+"]/td[2]/input", common.NB_Structure_of_InnerPagesMaps, "Area of Practice", i, "PIA_AOP_", "LastYear", "Last year (%)", "Input"),"Unable to enter Last year (%) in Area of practice table");
				customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(i+1)+"]/td[3]/input", common.NB_Structure_of_InnerPagesMaps, "Area of Practice", i, "PIA_AOP_", "PriorYear", "PriorYear", "Input"),"Unable to enter prior year (%) in Area of practice table");
				customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(i+1)+"]/td[4]/input", common.NB_Structure_of_InnerPagesMaps, "Area of Practice", i, "PIA_AOP_", "PriorYear2", "PriorYear", "Input"),"Unable to enter prior year 2 (%) in Area of practice table");		

			}

			k.Click("SPI_Save_SOI");
			// Read total, verify and adjust if not equals to 100% :

			for(int j = 0; j < totalCols-2; j++){

				sTotal =  Integer.parseInt(k.GetText_DynamicXpathWebDriver(driver, sTablePath + "/tbody/tr["+(noOfActivities+1)+"]/td["+(j+2)+"]"));

				if(sTotal != 100){
					TestUtil.reportStatus("Total does not equal to 100% for AreaOfPractice table. Entered value is : <b>[  "+ sTotal +"  ]</b>. ", "Info", false);
					retVal = false;
				}else{
					TestUtil.reportStatus("Total is verified AreaOfPractice table. Entered value is : <b>[  "+ sTotal +"  ]</b>.", "Info", true);
				}
			}		

			stale_count = 0;
			return retVal;

		}catch(StaleElementReferenceException stale_ex) {
			if(stale_count++ < 3)
				SPI_AreaOfPractice(mdata);
			stale_count = 0;
			System.out.println("StaleElementReferenceException in SPI_AreaOfPractice function after >"+stale_count+"< attempts - "+stale_ex);
			return false;
		}catch(Throwable t){
			System.out.println("Error in SPI_AreaOfPractice - "+t);
			stale_count = 0;
			return false;
		}


	}

	public boolean SPI_Fees_Turnover( Map<Object, Object> map_data){
		boolean retVal = true;
		int totalCols;
		String sFinal ="", sVal = "";

		try{

			if(common_PIA.isRewindSelected.contains("Yes") || common_PIA.isRenewalRewindFlow){
				sVal = "Rewind";
			}

			String sUniqueCol ="Year";
			int tableId = 0;

			String sTablePath = "html/body/div[3]/form/div/table";

			tableId = k.getTableIndex(sUniqueCol,"xpath",sTablePath);

			sTablePath = "html/body/div[3]/form/div/table["+ tableId +"]";
			WebElement table= driver.findElement(By.xpath(sTablePath));

			totalCols = table.findElements(By.tagName("th")).size();
	
			switch (sVal){
			case "Rewind" :
				sFinal = (String)map_data.get("SP_RateOn_Rewind");
				break;

			default :

				// Enter values to the table :
				sFinal = (String)map_data.get("SP_RateOn");
				for(int i = 0; i < totalCols-3; i++ ){

					//Get Column name :

					String Reagon = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/thead/tr/th["+(i+3)+"]");

					if(Reagon.length() > 0){

						if(Reagon.contains("Rest of the World")){
							Reagon = "ROW";
						}else if(Reagon.contains("USA/ Canada")){
							Reagon = "USACanada";
						}

						//Enter Data :

						customAssert.assertTrue(k.DynamicXpathWebDriver(driver, sTablePath + "/tbody/tr[1]/td[" + (i+3) +"]/input", map_data, "Solicitors PI", i, "SP_", "Estimate"+Reagon, Reagon, "Input"),"Unable to enter Estimate value for reagon - "+Reagon);				
						customAssert.assertTrue(k.DynamicXpathWebDriver(driver, sTablePath + "/tbody/tr[2]/td[" + (i+3) +"]/input", map_data, "Solicitors PI", i, "SP_", "2017"+Reagon, Reagon, "Input"),"Unable to enter 2017 value for reagon - "+Reagon);
						customAssert.assertTrue(k.DynamicXpathWebDriver(driver, sTablePath + "/tbody/tr[3]/td[" + (i+3) +"]/input", map_data, "Solicitors PI", i, "SP_", "2016"+Reagon, Reagon, "Input"),"Unable to enter 2016 value for reagon - "+Reagon);
						customAssert.assertTrue(k.DynamicXpathWebDriver(driver, sTablePath + "/tbody/tr[4]/td[" + (i+3) +"]/input", map_data, "Solicitors PI", i, "SP_", "2015"+Reagon, Reagon, "Input"),"Unable to enter 2015 value for reagon - "+Reagon);
						customAssert.assertTrue(k.DynamicXpathWebDriver(driver, sTablePath + "/tbody/tr[5]/td[" + (i+3) +"]/input", map_data, "Solicitors PI", i, "SP_", "2014"+Reagon, Reagon, "Input"),"Unable to enter 2014 value for reagon - "+Reagon);
					}			
				}

				break;			

			}

			// Read FeesIncome Total :

			TotalFeesIncome_Estimate = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[1]/td[" +totalCols+ "]/input").replaceAll(",", ""));
			TotalFeesIncome_2017 = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[2]/td[" +totalCols+ "]/input").replaceAll(",", ""));
			TotalFeesIncome_2016 = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[3]/td[" +totalCols+ "]/input").replaceAll(",", ""));
			TotalFeesIncome_2015 = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[4]/td[" +totalCols+ "]/input").replaceAll(",", ""));
			TotalFeesIncome_2014 = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[5]/td[" +totalCols+ "]/input").replaceAll(",", ""));
			Total_Average = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[6]/td[" +totalCols+ "]/input").replaceAll(",", ""));

			if(sFinal.contains("Estimate")){
				global_SOI_GrossFees = TotalFeesIncome_Estimate; 
			}else if(sFinal.contains("Last Year")){
				global_SOI_GrossFees = TotalFeesIncome_2017;
			}else if(sFinal.equalsIgnoreCase("Prior Year")){
				global_SOI_GrossFees = TotalFeesIncome_2016;
			}else if(sFinal.equalsIgnoreCase("Prior Year 2")){
				global_SOI_GrossFees = TotalFeesIncome_2015;
			}else if(sFinal.equalsIgnoreCase("Prior Year 3")){
				global_SOI_GrossFees = TotalFeesIncome_2014;
			}else{
				global_SOI_GrossFees = Total_Average;
			}

			TestUtil.WriteDataToXl(CommonFunction.product+"_"+common.currentRunningFlow, "Solicitors PI",(String)map_data.get("Automation Key"), "SP_GrossFee", String.valueOf(global_SOI_GrossFees), map_data);

			if(common_PIA.isRewindSelected.contains("Yes") || common_PIA.isRenewalRewindFlow){
				map_data.put("SP_GrossFee",  String.valueOf(global_SOI_GrossFees));
			}

			stale_count = 0;
			return retVal;


		}catch(StaleElementReferenceException stale_ex) {
			if(stale_count++ < 3)
				SPI_Fees_Turnover(map_data);
			stale_count = 0;
			System.out.println("StaleElementReferenceException in coverSPI_PeriodRatingTable function after >"+stale_count+"< attempts - "+stale_ex);
			return false;
		}catch(Throwable t){
			System.out.println("Error in SPI_Fees_Turnover - "+t);
			stale_count = 0;
			return false;
		}

	}

	public boolean coverSPI_PeriodRatingTable( Map<Object, Object> map_data, String sCase){
		boolean retVal = true;
		double s_SOI_BookRate = 0.00, s_SOI_InitialP = 0.00, s_SOI_BookP = 0.00, s_SOI_TechA = 0.00, s_SOI_RevisedP = 0.00, s_SOI_AnnualP = 0.00;
		double s_SOI_NetNetP = 0.00, s_SOI_Grossp = 0.00, s_SOI_TotalP = 0.00, s_SOI_IndLimit = 0.00;

		double s_SOI_PenComm = 0.00, s_SOI_BrokerComm = 0.00, s_SOI_InsTaxR = 0.00;
		double s_Val_PenAmt = 0.00, s_Val_BrokerAmt = 0.00, s_Val = 0.00;
		int xlWRite_Index = 0;

		String SummaryTable_UniqueCol, SummaryTable_Path;

		String sPeriodMonths;

		Map<String, List<Map<String, String>>> data_Structure_of_InnerPagesMaps = null;

		switch(common.currentRunningFlow){

		case "NB":
			data_Structure_of_InnerPagesMaps = common.NB_Structure_of_InnerPagesMaps;
			break;
		case "MTA":
			data_Structure_of_InnerPagesMaps = common.MTA_Structure_of_InnerPagesMaps;
			break;
		case "Renewal":
			data_Structure_of_InnerPagesMaps = common.Renewal_Structure_of_InnerPagesMaps;
			break;	

		}


		try{

			//Identify Period Rating Table :

			String sUniqueCol ="OPTION";
			int tableId = 0;

			String sTablePath = "html/body/div[3]/form/div/table";

			tableId = k.getTableIndex(sUniqueCol,"xpath",sTablePath);

			//Changed XPATH due to tableID change while D-rows changes from .41 server
			//sTablePath = "html/body/div[3]/form/div/table["+ tableId +"]";
			sTablePath = "//*[contains(text(),'Period Rating Table')]//following::table[1]";

			String sPeriodRatings = (String)map_data.get("SP_PeriodRatingTable");
			String sValue[] = sPeriodRatings.split(";");
			int noOfPR = sValue.length;

			// Identify Summary Table  :

			//SummaryTable_UniqueCol ="Description";		
			//tableId = k.getTableIndex(SummaryTable_UniqueCol,"xpath","html/body/div[3]/form/div/table");	

			//Changed XPATH due to tableID change while D-rows changes from .41 server
			//SummaryTable_Path = "html/body/div[3]/form/div/table["+ tableId +"]";
			SummaryTable_Path = "//*[contains(@id,\"rateTable_\")]";
			if(TestBase.businessEvent.equals("Rewind")) {
				if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement")){
					String period_path = "//*[text()='Professional Indemnity']//following::td[1]";
					WebElement p_elm = driver.findElement(By.xpath(period_path));
					String period = p_elm.getText();
					data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(0).put("PRT_PeriodofInsurance",period);
				}
			}



			switch (sCase){

			case "" :

				// Enter Data in Period Rating Table :

				if(common_PIA.isRewindSelected.contains("Yes") || common_PIA.isRenewalRewindFlow){
					burnRate = common_PIA.PIA_Rater_output.get("Burn Rate");
					// Do Nothing
				}else{
					for(int i = 0; i < noOfPR ; i++ ){

						//click on 'Add Row' link :

						String Add_row_link = "//*[text()='Period Rating Table']//following::a[text()='Add Row']";
						WebElement Add_row_link_element = driver.findElement(By.xpath(Add_row_link));
						k.ScrollInVewWebElement(Add_row_link_element);
						k.waitTwoSeconds();
						Add_row_link_element.click();
						k.waitFiveSeconds();
						// Enter Data :

						customAssert.assertTrue(k.DropDownSelection_DynamicXpath(sTablePath + "/tbody/tr["+(i+1)+"]/td[2]/select", data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_PeriodofInsurance")),"Unable to enter PeriodofInsurance in Period rating table");
						customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(i+1)+"]/td[3]/input", data_Structure_of_InnerPagesMaps, "Period Rating Table", i, "PRT_", "ExcessAmount", "ExcessAmount", "Input"),"Unable to enter ExcessAmount in Period rating table");
						customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(i+1)+"]/td[4]/input", data_Structure_of_InnerPagesMaps, "Period Rating Table", i, "PRT_", "TechAdjust", "TechAdjust", "Input"),"Unable to enter TechAdjust in Period rating table");

					}

					// Enter Comm Adjust in Summary Table if required :
					customAssert.assertTrue(k.DynamicXpathWebDriver(driver, SummaryTable_Path + "/tbody/tr[1]/td[10]/input", map_data, "Solicitors PI", 0, "SP_", "PICommAdjustment", "Comm Adjust", "Input"),"Unable to enter Comm Adjust value in summary table");
					global_SOI_CommAdjust = Double.parseDouble((String)map_data.get("SP_PICommAdjustment"));
				}

				if(common_PIA.isRenewalRewindFlow){
					customAssert.assertTrue(k.DynamicXpathWebDriver(driver, SummaryTable_Path + "/tbody/tr[1]/td[10]/input", map_data, "Solicitors PI", 0, "SP_", "PICommAdjustment", "Comm Adjust", "Input"),"Unable to enter Comm Adjust value in summary table");
					global_SOI_CommAdjust = Double.parseDouble((String)map_data.get("SP_PICommAdjustment"));

				}

				// Read Values of Period Rating Table from screen and calculations :

				for(int i = 0; i < noOfPR ; i++ ){	

					if(data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_IsSelected").contains("Yes")){

						xlWRite_Index = i ;

						if(i>0){
							driver.findElement(By.xpath(sTablePath + "/tbody/tr["+ (i+1) +"]/td[1]/input[2]")).click();
						}								

						k.Click("SPI_ApplyBookRate");


						//Duration :
						duration_SoPI = Integer.parseInt(k.getText("SPI_SP_Duration"));

						//Read Values from Screen -  Summary Table :

						global_PeriodMonths = Integer.parseInt(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[2]"));
						global_FeesIncome = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[3]").replaceAll(",", ""));
						global_IndemnityLimit = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[4]").replaceAll(",", ""));
						global_SOI_BookRate = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[5]/input").replaceAll(",", ""));
						global_SOI_InitialP = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[6]").replaceAll(",", "")));
						gloabal_SOI_BookP = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[7]/input").replaceAll(",", ""));
						global_SOI_TecgAdjust = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[8]/input").replaceAll(",", ""));
						global_SOI_RevisedP = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[9]/input").replaceAll(",", ""));
						global_SOI_sPremium = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[12]/input").replaceAll(",", ""));							

						// Read values from Period Rating Table :

						global_SOI_AnnualP = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[5]/input").replaceAll(",", "")));
						global_SOI_NetNetP = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[6]/input").replaceAll(",", "")));
						global_SOI_GrossP = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[9]/input").replaceAll(",", "")));
						global_SOI_TotalPremium = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[11]/input").replaceAll(",", "")));

						// Read Default Values Which are referred on Premium Summary Screen :

						Refer_PenComm = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input").replaceAll(",", "")));
						Refer_BrokerComm = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[8]/input").replaceAll(",", "")));
						Refer_InsTax = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[10]/input").replaceAll(",", "")));

						//Calculated Values :

						sPeriodMonths = data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_PeriodofInsurance");

						String sVal = (String)map_data.get("PG_IsYourPracticeLLP");
						if(sVal.contains("Yes")){
							s_SOI_IndLimit = 3000000.00;
						}else{
							s_SOI_IndLimit = 2000000.00;
						}

						s_SOI_BookRate = common_PIA.PIA_Rater_output.get("Book_Rate_Cent");
						//s_SOI_InitialP = common_PIA.PIA_Rater_output.get("Book_Premium_24_months");
						//CR 274
						s_SOI_InitialP = common_PIA.PIA_Rater_output.get("Technical_Premium_Capped");

						if(sPeriodMonths.contains("12")){
							s_SOI_BookP = common_PIA.PIA_Rater_output.get("Book_Premium_12_months");
						}else if(sPeriodMonths.contains("18")){
							s_SOI_BookP = common_PIA.PIA_Rater_output.get("Book_Premium_18_months");
						}else{
							s_SOI_BookP = common_PIA.PIA_Rater_output.get("Book_Premium_24_months");
						}

						s_SOI_TechA = Double.parseDouble(common.roundedOff(data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_TechAdjust")));
						s_SOI_RevisedP = Double.parseDouble(common.roundedOff(Double.toString(((s_SOI_BookP*s_SOI_TechA)/100) + s_SOI_BookP)));
						s_SOI_AnnualP = Double.parseDouble(common.roundedOff(Double.toString(((s_SOI_RevisedP * global_SOI_CommAdjust)/100) + s_SOI_RevisedP)));

						s_SOI_NetNetP = Double.parseDouble(common.roundedOff(Double.toString(((s_SOI_AnnualP/365)*duration_SoPI))));

						double grossCommPerc = Refer_PenComm + Refer_BrokerComm;

						s_Val_PenAmt = Math.abs(Double.parseDouble(common.roundedOff(Double.toString((s_SOI_NetNetP/((100-grossCommPerc)/100))*(Refer_PenComm/100)))));
						s_Val_BrokerAmt  = Math.abs(Double.parseDouble(common.roundedOff(Double.toString((s_SOI_NetNetP/((100-grossCommPerc)/100))*(Refer_BrokerComm/100)))));
						s_Val  = s_SOI_NetNetP + s_Val_PenAmt;

						s_SOI_Grossp = s_Val + s_Val_BrokerAmt;
						//CR 274 change


						s_SOI_TotalP = Double.parseDouble(common.roundedOff(Double.toString(s_SOI_Grossp + ((s_SOI_Grossp*Refer_InsTax)/100))));

						double s_BurnRate = Double.parseDouble(common.roundedOff(k.getText("SPI_PI_BurnRate").replaceAll(",", "")));

						// Comparisons :


						CommonFunction.compareValues(burnRate, s_BurnRate,"BurnRate");
						CommonFunction.compareValues(Double.parseDouble(sPeriodMonths), Double.parseDouble(String.valueOf(global_PeriodMonths)),"Period of insurance month ");
						CommonFunction.compareValues(global_SOI_GrossFees, global_FeesIncome,"Fees Income");
						CommonFunction.compareValues(s_SOI_IndLimit, global_IndemnityLimit,"Indemnity Limit");
						CommonFunction.compareValues(s_SOI_BookRate, global_SOI_BookRate,"SOI BookRate");
						CommonFunction.compareValues(s_SOI_InitialP, global_SOI_InitialP,"SOI Initial Premium");
						CommonFunction.compareValues(s_SOI_BookP, gloabal_SOI_BookP,"SOI Book Premium");
						CommonFunction.compareValues(s_SOI_TechA, global_SOI_TecgAdjust,"SOI TechAdjust");
						CommonFunction.compareValues(s_SOI_RevisedP, global_SOI_RevisedP,"SOI Revised Premium");
						CommonFunction.compareValues(s_SOI_AnnualP, global_SOI_sPremium,"SOI Annual Premium From Summary Table");
						CommonFunction.compareValues(s_SOI_AnnualP, global_SOI_AnnualP,"SOI Annual Premium From PR Table");
						CommonFunction.compareValues(s_SOI_NetNetP, global_SOI_NetNetP,"SOI Net Net Premium");
						CommonFunction.compareValues(s_SOI_Grossp, global_SOI_GrossP,"SOI Gross Premium");
						CommonFunction.compareValues(s_SOI_TotalP, global_SOI_TotalPremium,"SOI Total Premium");
					}						
				}

				break;

			case "Edit_PS" :

				// Enter Comm Adjust in Summary Table if required :
				customAssert.assertTrue(k.DynamicXpathWebDriver(driver, SummaryTable_Path + "/tbody/tr[1]/td[10]/input", map_data, "Solicitors PI", 0, "SP_", "'SP_PICommAdjustment", "Comm Adjust", "Input"),"Unable to enter Comm Adjust value in summary table");
				global_SOI_CommAdjust = Double.parseDouble((String)map_data.get("SP_PICommAdjustment"));

				// Enter data in PR Table :
				customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(noOfPR-1)+"]/td[2]/input", data_Structure_of_InnerPagesMaps, "Period Rating Table", (noOfPR-3), "PRT_", "ExcessAmount", "ExcessAmount", "Input"),"Unable to enter ExcessAmount in Period rating table");
				customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(noOfPR-1)+"]/td[3]/input", data_Structure_of_InnerPagesMaps, "Period Rating Table", (noOfPR-3), "PRT_", "TechAdjust", "TechAdjust", "Input"),"Unable to enter TechAdjust in Period rating table");

				k.Click("SPI_ApplyBookRate");

				//Duration :
				duration_SoPI = Integer.parseInt(k.getText("SPI_SP_Duration"));

				// Read reflected values from Premium summary screen :

				s_SOI_PenComm = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(noOfPR-1)+"]/td[7]/input").replaceAll(",", ""));
				s_SOI_BrokerComm = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(noOfPR-1)+"]/td[8]/input").replaceAll(",", ""));
				s_SOI_InsTaxR = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(noOfPR-1)+"]/td[10]/input").replaceAll(",", ""));


			}


			// Write data to excel :


			TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+common.currentRunningFlow, "Period Rating Table", data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_AnnualizedPremium", String.valueOf(s_SOI_AnnualP),data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
			TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+common.currentRunningFlow, "Period Rating Table", data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_NetNetPremium", String.valueOf(s_SOI_NetNetP),data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));

			if(sCase.equals("")){
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+common.currentRunningFlow, "Period Rating Table", data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_PenCommission", String.valueOf(Refer_PenComm),data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+common.currentRunningFlow, "Period Rating Table", data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_BrokerCommission", String.valueOf(Refer_BrokerComm),data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+common.currentRunningFlow, "Period Rating Table", data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_InsuranceTax", String.valueOf(Refer_InsTax),data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
			}else{
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+common.currentRunningFlow, "Period Rating Table", data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_PenCommission", String.valueOf(s_SOI_PenComm),data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+common.currentRunningFlow, "Period Rating Table", data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_BrokerCommission", String.valueOf(s_SOI_BrokerComm),data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+common.currentRunningFlow, "Period Rating Table", data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_InsuranceTax", String.valueOf(s_SOI_InsTaxR),data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
			}

			TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+common.currentRunningFlow, "Period Rating Table", data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_GrossPremium", String.valueOf(s_SOI_Grossp),data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
			TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+common.currentRunningFlow, "Period Rating Table", data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_TotalPremium", String.valueOf(s_SOI_TotalP),data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
			TestUtil.WriteDataToXl(CommonFunction.product+"_"+common.currentRunningFlow, "Premium Summary", (String)map_data.get("Automation Key"), "PS_SolicitorsPI_NetNetPremium", String.valueOf(s_SOI_AnnualP), map_data);	

			stale_count = 0;
			return retVal;


		}catch(StaleElementReferenceException stale_ex) {
			if(stale_count++ < 3)
				coverSPI_PeriodRatingTable(map_data,sCase);
			stale_count = 0;
			TestUtil.reportStatus("StaleElementReferenceException in coverSPI_PeriodRatingTable function after >"+stale_count+"< attempts - "+stale_ex, "Fail", true);
			return false;
		}catch(Throwable t){
			System.out.println("Error in coverSPI_PeriodRatingTable - "+t);
			stale_count = 0;
			return false;
		}


	}

	public boolean SPI_RenewalEndorse_PeriodRatingTable( Map<Object, Object> map_data, String sCase){
		boolean retVal = true;

		String SummaryTable_UniqueCol, SummaryTable_Path;


		try{


			//Identify Period Rating Table :

			String sUniqueCol ="OPTION";
			int tableId = 0;

			String sTablePath = "html/body/div[3]/form/div/table";

			tableId = k.getTableIndex(sUniqueCol,"xpath",sTablePath);

			sTablePath = "html/body/div[3]/form/div/table["+ tableId +"]";
	
			global_PreviousPremium = Double.parseDouble(driver.findElement(By.xpath("html/body/div[3]/form/div/table["+ tableId +"]/tbody/tr[1]/td[5]/input")).getAttribute("value").replaceAll(",", ""));

			// Identify Summary Table  :

			SummaryTable_UniqueCol ="Description";		
			tableId = k.getTableIndex(SummaryTable_UniqueCol,"xpath","html/body/div[3]/form/div/table");		
			SummaryTable_Path = "html/body/div[3]/form/div/table["+ tableId +"]";
	
			// Enter Data in Period Rating Table :

			customAssert.assertTrue(k.DynamicXpathWebDriver(driver, SummaryTable_Path + "/tbody/tr[1]/td[10]/input", map_data, "Solicitors PI", 0, "SP_", "RenewalPICommAdjustment", "Comm Adjust", "Input"),"Unable to enter Comm Adjust value in summary table for renewal");
			global_SOI_CommAdjust = Double.parseDouble((String)map_data.get("SP_RenewalPICommAdjustment"));

			// Read Values of Period Rating Table from screen and calculations :

			k.Click("SPI_ApplyBookRate");
			global_SOI_sPremium = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[12]/input").replaceAll(",", ""));

			// Write data to excel :

			TestUtil.WriteDataToXl(CommonFunction.product+"_"+common.currentRunningFlow, "Premium Summary", (String)map_data.get("Automation Key"), "PS_PI_NetNetPremium", String.valueOf(global_SOI_sPremium), map_data);	

			return retVal;

		}catch(Throwable t){
			System.out.println("Error in coverSPI_PeriodRatingTable - "+t);
			return false;
		}


	}


	public boolean func_Solicitors_ExcessLayer( Map<Object, Object> map_data){
		boolean retVal = true;
		double 	s_SEL_GrossP = 0.00, s_SEL_PenComm = 0.00, s_SEL_BrokerComm = 0.00, s_SEL_BookP = 0.00;
		double SEL_PremiumOverride = 0.0;
		try{
			customAssert.assertTrue(common.funcPageNavigation("Solicitors Excess Layer", ""), "Navigation problem to Solicitors Excess Layer page .");

			if(!common.currentRunningFlow.equalsIgnoreCase("NB")){

				JavascriptExecutor j_exe = (JavascriptExecutor) driver;
				j_exe.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[@id='pia_el_tot']")));
				common_PIA.SEL_Premium = driver.findElement(By.xpath("//*[@id='pia_el_tot']")).getAttribute("value");

				if(TestBase.businessEvent.equalsIgnoreCase("MTA") && ((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes")){

					try {	
						switch((String)map_data.get("MTA_Operation")) {

						case "AP":
						case "RP":

							String _cover = common_EP.AP_RP_Cover_Key.split("-")[1];

							if(!_cover.contains("SolicitorsExcessLayer")) {
								common.MTA_excel_data_map.put("PS_Solicitorsexcesslayer_NetNetPremium", common_PIA.SEL_Premium);
								TestUtil.reportStatus("Solicitors Excess Layer Net Net Premium captured successfully . ", "Info", true);
								return true;
							}
							break;

						case "Policy-level":

							break;

						case "Non-Financial":

							common.MTA_excel_data_map.put("PS_Solicitorsexcesslayer_NetNetPremium", common_PIA.SEL_Premium);
							TestUtil.reportStatus("Due to Non-Financial Flow, Only Solicitors Excess Layer Net Net Premium captured  . ", "Info", true);
							return true;

						}
					}catch(NullPointerException npe) {

					}
				}
			}



			// Enter Mandatory Values : 

			customAssert.assertTrue(k.Input("SPI_SEL_UICompany", (String)map_data.get("SEL_UICompany")),"Unable to enter value in UICompany field .");
			customAssert.assertTrue(k.Input("SPI_SEL_UILimitxpath", (String)map_data.get("SEL_UILimit")),	"Unable to enter value in UILimit field .");
			customAssert.assertTrue(k.Input("SPI_SEL_UIPolicyNumbe", (String)map_data.get("SEL_UIPolicyNumbe")),	"Unable to enter value in SEL_PolicyNumbe field .");

			customAssert.assertTrue(k.Input("PIA_SEL_BrokerComm", (String)map_data.get("SEL_BrokerComm")),	"Unable to enter value in BrokerComm field .");
			customAssert.assertTrue(k.Input("PIA_SEL_PenComm", (String)map_data.get("SEL_PenComm")),	"Unable to enter value in PenComm field .");

			if(common_EP.AP_RP_Cover_Key.split("-")[1].equalsIgnoreCase("SolicitorsExcessLayer")){
				switch (common_EP.AP_RP_Cover_Key.split("-")[0]){

				case "AP" :
					TestUtil.reportStatus("<b>------ Additional Premium for Cover - Solicitors Excess Layer -------------</b>", "Info", false);
					SEL_PremiumOverride = Double.parseDouble(common_PIA.SEL_Premium) * 2;
					//customAssert.assertTrue(k.DynamicXpathWebDriver(driver, SummaryTable_Path + "/tbody/tr[1]/td[11]/input", map_data, "Solicitors PI", 0, "SP_", "PIPremiumOverride", "Premium Override", "Input"),"Unable to enter Premium Override value in SP summary table");
					break;
				case "RP" :
					TestUtil.reportStatus("<b>------- Reduced Premium for Cover - Solicitors Excess Layer -------------</b>", "Info", false);
					SEL_PremiumOverride = Double.parseDouble(common_PIA.SEL_Premium) - (0.1 *Double.parseDouble(common_PIA.SEL_Premium));
					break;
				}

				map_data.put("SEL_GrossPremium",SEL_PremiumOverride);
				customAssert.assertTrue(k.Input("SPI_SEL_GrossPremium", (String)map_data.get("SEL_GrossPremium")),	"Unable to enter value in GrossPremium field .");
			}else {
				customAssert.assertTrue(k.Input("SPI_SEL_GrossPremium", (String)map_data.get("SEL_GrossPremium")),	"Unable to enter value in GrossPremium field .");
			}
			customAssert.assertTrue(k.Input("SPI_SEL_AdminFeeSEL", (String)map_data.get("SEL_AdminFeeSEL")),	"Unable to enter value in AdminFeeSEL field .");

			k.Click("SPI_Save_SOI");
			k.Click("SPI_ApplyBookRate");

			// Identify Table :

			String sUniqueCol ="Description";
			int tableId = 0;

			String sTablePath = "html/body/div[3]/form/div/table";

			tableId = k.getTableIndex(sUniqueCol,"xpath",sTablePath);

			sTablePath = "html/body/div[3]/form/div/table["+ tableId +"]";

			// Calculation :

			global_SEL_GrossP =  Double.parseDouble((String)map_data.get("SEL_GrossPremium"));
			global_SEL_PenComm = Double.parseDouble((String)map_data.get("SEL_PenComm"));
			global_SEL_BrokerComm = Double.parseDouble((String)map_data.get("SEL_BrokerComm"));
			global_SEL_GrossComm = (global_SEL_PenComm + global_SEL_BrokerComm);
			global_SEL_BookP = (global_SEL_GrossP *(1 - (global_SEL_GrossComm/100)))/((1 - (global_SEL_GrossComm/100)) + (global_SEL_PenComm/100) + (global_SEL_BrokerComm/100));

			// Read Values from screen :

			global_SEL_AdminFees =  Double.parseDouble(k.getAttribute("SPI_SEL_AdminFeeSEL", "value"));

			s_SEL_GrossP =  Double.parseDouble(k.getAttribute("SPI_SEL_GrossPremium", "value").replaceAll(",",""));
			s_SEL_PenComm =  Double.parseDouble(k.getAttribute("PIA_SEL_PenComm", "value").replaceAll(",",""));
			s_SEL_BrokerComm =  Double.parseDouble(k.getAttribute("PIA_SEL_BrokerComm", "value").replaceAll(",",""));

			s_SEL_BookP = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[1]/td[2]/input").replaceAll(",", ""));
	
			//Compare Values :

			CommonFunction.compareValues(global_SEL_GrossP, s_SEL_GrossP,"Gross Premium SEL");
			CommonFunction.compareValues(global_SEL_PenComm, s_SEL_PenComm,"Pen Comm SEL");
			CommonFunction.compareValues(global_SEL_BrokerComm, s_SEL_BrokerComm,"Broker Comm SEL");
			CommonFunction.compareValues(global_SEL_BookP, s_SEL_BookP,"Book Premium SEL");

			map_data.put("PS_Solicitorsexcesslayer_NetNetPremium", String.valueOf(s_SEL_BookP));
			//TestUtil.WriteDataToXl(CommonFunction.product+"_"+common.currentRunningFlow, "Premium Summary", (String)map_data.get("Automation Key"), "PS_Solicitorsexcesslayer_NetNetPremium", String.valueOf(s_SEL_BookP), map_data);

			TestUtil.reportStatus("Solicitors Excess Layer Cover details are filled and Verified sucessfully . ", "Info", true);


			return retVal;

		}catch(Throwable t){
			System.out.println("Error in func_Solicitors_ExcessLayer - "+t);
			return false;
		}


	}

	public boolean func_Confirm_Policy(){

		try{
			customAssert.assertTrue(common.funcButtonSelection("Confirm"), "Unable to click on Confirm button .");
			customAssert.assertTrue(common.funcPageNavigation("Confirm Policy", ""),"Confirm Policy page is having issue(S)");
			customAssert.assertTrue(common.funcButtonSelection("Confirm"), "Unable to click on Confirm button .");
			TestUtil.reportStatus("Entered all the details on Confirm Policy page .", "Info", true);

		}catch(Throwable t){
			return false;
		}

		return true;
	}

	public boolean funcAssignPolicyToUW(){

		boolean retvalue=true;

		try {
			customAssert.assertTrue(common.funcPageNavigation("Assign Underwriter",""), "Unable to navigate through Assign Underwriter Page.");

			List<WebElement> assign_Btns = driver.findElements(By.xpath("//a[text()='Assign']"));
			for(WebElement assign_btn:assign_Btns){
				k.ScrollInVewWebElement(assign_btn);
				if(assign_btn.isDisplayed()){
					assign_btn.click();
					break;
				}else
					continue;
			}
			// String tableXpath = "html/body/div[3]/form/div/table/tbody/tr[1]/td[4]/a";
			// driver.findElement(By.xpath(tableXpath)).click(); 
			customAssert.assertTrue(k.getText("Page_Header").equalsIgnoreCase("Premium Summary"), "Premium Summary Page is not loaded after policy assigned to UW . ");
			TestUtil.reportStatus("Policy Assigned to Underwriter successfully", "Info", true);

			return retvalue;

		}catch(Throwable t) {
			String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
			TestUtil.reportFunctionFailed("Failed in "+methodName+" function");     k.reportErr("Failed in "+methodName+" function", t);
			Assert.fail("Unable to Assign Policy to underwriter \n", t);
			return false;
		}

	}


	public boolean MaterialFactsDeclerationPage(){
		boolean retValue = true;

		try{
			customAssert.assertTrue(common.funcPageNavigation("Material Facts & Declarations", ""),"Material Facts & Declarations page is having issue(S)");
			k.ImplicitWaitOff();
			List<WebElement> elements = driver.findElements(By.className("selectinput"));
			Select sel = null;
			String q_value = null;
			for(int i = 0;i<elements.size();i++){
				sel = new Select(elements.get(i));
				try{
					q_value = (String)common.NB_excel_data_map.get("MF_Q"+(i+1));
					sel.selectByVisibleText(q_value);}
				catch(Throwable t){
					sel.selectByVisibleText("No");
					//System.out.println("Error while selecting Material Facts questions .");
				}

			}
			customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Material Facts & Declarations Screen .");
			stale_count = 0;
			return retValue;

		}catch(StaleElementReferenceException stale_ex) {
			if(stale_count++ < 3)
				MaterialFactsDeclerationPage();
			stale_count = 0;
			System.out.println("StaleElementReferenceException in MaterialFactsDeclerationPage function after >"+stale_count+"< attempts - "+stale_ex);
			return false;
		}catch(Throwable t){
			k.ImplicitWaitOn();
			String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
			TestUtil.reportFunctionFailed("Failed in "+methodName+" function");    
			stale_count = 0;
			return false;
		}
		finally{
			k.ImplicitWaitOn();
		}

	}



	public boolean func_Populate_CoverPIA_Rater_Map(Map<Object, Object> map_data, String sCase){
		boolean retVal = true;
		double s_SOI_BookRate = 0.00, s_SOI_InitialP = 0.00, s_SOI_BookP = 0.00, s_SOI_TechA = 0.00, s_SOI_RevisedP = 0.00, s_SOI_AnnualP = 0.00;
		double s_SOI_NetNetP = 0.00, s_SOI_Grossp = 0.00, s_SOI_TotalP = 0.00, s_SOI_IndLimit = 0.00;

		double s_SOI_PenComm = 0.00, s_SOI_BrokerComm = 0.00, s_SOI_InsTaxR = 0.00;
		double s_Val_PenAmt = 0.00, s_Val_BrokerAmt = 0.00, s_Val = 0.00;
		int xlWRite_Index = 0;

		String SummaryTable_UniqueCol, SummaryTable_Path;

		String sPeriodMonths;

		try{

			//Identify Period Rating Table :

			String sUniqueCol ="OPTION";
			int tableId = 0;

			String sTablePath = "html/body/div[3]/form/div/table";

			tableId = k.getTableIndex(sUniqueCol,"xpath",sTablePath);

			sTablePath = "html/body/div[3]/form/div/table["+ tableId +"]";
	
			String sPeriodRatings = (String)map_data.get("SP_PeriodRatingTable");
			String sValue[] = sPeriodRatings.split(";");
			int noOfPR = sValue.length;

			// Identify Summary Table  :

			SummaryTable_UniqueCol ="Description";		
			tableId = k.getTableIndex(SummaryTable_UniqueCol,"xpath","html/body/div[3]/form/div/table");		
			SummaryTable_Path = "html/body/div[3]/form/div/table["+ tableId +"]";
		
			switch (sCase){

			case "" :

				/*// Enter Data in Period Rating Table :

						if(isRewindSelected.contains("Yes")){
							burnRate = PIA_Rater_output.get("Burn Rate");
							// Do Nothing
						}else{
							for(int i = 0; i < noOfPR ; i++ ){

								String sVal = common.NB_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_IsSelected");

									//click on 'Add Row' link :

									if(i == 0){
										driver.findElement(By.xpath(sTablePath + "/tbody/tr/td[12]/a")).click();
									}else{
										driver.findElement(By.xpath(sTablePath + "/tbody/tr["+ (i+1) +"]/td[12]/a")).click();
									}							

									// Enter Data :

									customAssert.assertTrue(k.DropDownSelection_DynamicXpath(sTablePath + "/tbody/tr["+(i+1)+"]/td[2]/select", common.NB_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_PeriodofInsurance")),"Unable to enter PeriodofInsurance in Period rating table");
									customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(i+1)+"]/td[3]/input", common.NB_Structure_of_InnerPagesMaps, "Period Rating Table", i, "PRT_", "ExcessAmount", "ExcessAmount", "Input"),"Unable to enter ExcessAmount in Period rating table");
									customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(i+1)+"]/td[4]/input", common.NB_Structure_of_InnerPagesMaps, "Period Rating Table", i, "PRT_", "TechAdjust", "TechAdjust", "Input"),"Unable to enter TechAdjust in Period rating table");

							}
				 */
				// Enter Comm Adjust in Summary Table if required :
				customAssert.assertTrue(k.DynamicXpathWebDriver(driver, SummaryTable_Path + "/tbody/tr[1]/td[10]/input", map_data, "Solicitors PI", 0, "SP_", "RenewalPICommAdjustment", "Comm Adjust", "Input"),"Unable to enter Comm Adjust value in summary table for MTA Rewind ");
				global_SOI_CommAdjust = Double.parseDouble((String)map_data.get("SP_RenewalPICommAdjustment"));
				//}

				// Read Values of Period Rating Table from screen and calculations :

				for(int i = 0; i < noOfPR ; i++ ){	

					if(common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_IsSelected").contains("Yes")){

						xlWRite_Index = i ;

						if(i>0){
							driver.findElement(By.xpath(sTablePath + "/tbody/tr["+ (i+1) +"]/td[1]/input[2]")).click();
						}								

						k.Click("SPI_ApplyBookRate");


						//Duration :
						duration_SoPI = Integer.parseInt(k.getText("SPI_SP_Duration"));

						//Read Values from Screen -  Summary Table :

						global_PeriodMonths = Integer.parseInt(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[2]"));
						global_FeesIncome = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[3]").replaceAll(",", ""));
						global_IndemnityLimit = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[4]").replaceAll(",", ""));
						global_SOI_BookRate = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[5]/input").replaceAll(",", ""));
						common_PIA.PIA_Rater_output.put("Book_Rate_Cent",global_SOI_BookRate);
						global_SOI_InitialP = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[6]").replaceAll(",", "")));
						//common_PIA.PIA_Rater_output.("Book_Premium_24_months",global_SOI_InitialP);
						gloabal_SOI_BookP = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[7]/input").replaceAll(",", ""));
						global_SOI_TecgAdjust = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[8]/input").replaceAll(",", ""));
						global_SOI_RevisedP = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[9]/input").replaceAll(",", ""));
						global_SOI_sPremium = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[12]/input").replaceAll(",", ""));							

						// Read values from Period Rating Table :

						global_SOI_AnnualP = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[5]/input").replaceAll(",", "")));
						global_SOI_NetNetP = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[6]/input").replaceAll(",", "")));
						global_SOI_GrossP = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[9]/input").replaceAll(",", "")));
						global_SOI_TotalPremium = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[11]/input").replaceAll(",", "")));

						// Read Default Values Which are referred on Premium Summary Screen :

						Refer_PenComm = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input").replaceAll(",", "")));
						Refer_BrokerComm = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[8]/input").replaceAll(",", "")));
						Refer_InsTax = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[10]/input").replaceAll(",", "")));

						//Calculated Values :

						sPeriodMonths = common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_PeriodofInsurance");

						String sVal = (String)map_data.get("PG_IsYourPracticeLLP");
						if(sVal.contains("Yes")){
							s_SOI_IndLimit = 3000000.00;
						}else{
							s_SOI_IndLimit = 2000000.00;
						}

						s_SOI_BookRate = common_PIA.PIA_Rater_output.get("Book_Rate_Cent");
						s_SOI_InitialP = common_PIA.PIA_Rater_output.get("Book_Premium_24_months");

						if(sPeriodMonths.contains("12")){
							s_SOI_BookP = common_PIA.PIA_Rater_output.get("Book_Premium_12_months");
						}else if(sPeriodMonths.contains("18")){
							s_SOI_BookP = common_PIA.PIA_Rater_output.get("Book_Premium_18_months");
						}else{
							s_SOI_BookP = common_PIA.PIA_Rater_output.get("Book_Premium_24_months");
						}

						s_SOI_TechA = Double.parseDouble(common.roundedOff(common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_TechAdjust")));
						s_SOI_RevisedP = Double.parseDouble(common.roundedOff(Double.toString(((s_SOI_BookP*s_SOI_TechA)/100) + s_SOI_BookP)));
						s_SOI_AnnualP = Double.parseDouble(common.roundedOff(Double.toString(((s_SOI_RevisedP * global_SOI_CommAdjust)/100) + s_SOI_RevisedP)));

						s_SOI_NetNetP = Double.parseDouble(common.roundedOff(Double.toString(((s_SOI_AnnualP/365)*duration_SoPI))));
						/*double NB_PI_NNP = Double.parseDouble((String)common.Renewal_excel_data_map.get("PS_PI_NetNetPremium"));
								s_SOI_NetNetP = ((s_SOI_AnnualP - NB_PI_NNP)/Integer.parseInt((String)common.Renewal_excel_data_map.get("PS_Duration")))*
										((Integer.parseInt((String)common.Renewal_excel_data_map.get("PS_Duration")) - Integer.parseInt((String)common.Renewal_excel_data_map.get("MTA_EndorsementPeriod"))));
						 */
						double grossCommPerc = Refer_PenComm + Refer_BrokerComm;

						s_Val_PenAmt = Math.abs(Double.parseDouble(common.roundedOff(Double.toString((s_SOI_NetNetP/((100-grossCommPerc)/100))*(Refer_PenComm/100)))));
						s_Val_BrokerAmt  = Math.abs(Double.parseDouble(common.roundedOff(Double.toString((s_SOI_NetNetP/((100-grossCommPerc)/100))*(Refer_BrokerComm/100)))));
						s_Val  = s_SOI_NetNetP + s_Val_PenAmt;

						s_SOI_Grossp = s_Val + s_Val_BrokerAmt;
						s_SOI_TotalP = Double.parseDouble(common.roundedOff(Double.toString(s_SOI_Grossp + ((s_SOI_Grossp*Refer_InsTax)/100))));

						double s_BurnRate = Double.parseDouble(common.roundedOff(k.getText("SPI_PI_BurnRate").replaceAll(",", "")));
						common_PIA.PIA_Rater_output.put("Burn Rate", s_BurnRate);

						// Comparisons :


						CommonFunction.compareValues(burnRate, s_BurnRate,"BurnRate");
						CommonFunction.compareValues(Double.parseDouble(sPeriodMonths), Double.parseDouble(String.valueOf(global_PeriodMonths)),"Period of insurance month ");
						CommonFunction.compareValues(global_SOI_GrossFees, global_FeesIncome,"Fees Income");
						CommonFunction.compareValues(s_SOI_IndLimit, global_IndemnityLimit,"Indemnity Limit");
						CommonFunction.compareValues(s_SOI_BookRate, global_SOI_BookRate,"SOI BookRate");
						CommonFunction.compareValues(s_SOI_InitialP, global_SOI_InitialP,"SOI Initial Premium");
						CommonFunction.compareValues(s_SOI_BookP, gloabal_SOI_BookP,"SOI Book Premium");
						CommonFunction.compareValues(s_SOI_TechA, global_SOI_TecgAdjust,"SOI TechAdjust");
						CommonFunction.compareValues(s_SOI_RevisedP, global_SOI_RevisedP,"SOI Revised Premium");
						CommonFunction.compareValues(s_SOI_AnnualP, global_SOI_sPremium,"SOI Annual Premium From Summary Table");
						CommonFunction.compareValues(s_SOI_AnnualP, global_SOI_AnnualP,"SOI Annual Premium From PR Table");
						CommonFunction.compareValues(s_SOI_NetNetP, global_SOI_NetNetP,"SOI Net Net Premium");
						CommonFunction.compareValues(s_SOI_Grossp, global_SOI_GrossP,"SOI Gross Premium");
						CommonFunction.compareValues(s_SOI_TotalP, global_SOI_TotalPremium,"SOI Total Premium");
					}						
				}

				break;
			}

			// Write data to excel :


			TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+CommonFunction.businessEvent, "Period Rating Table", common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_AnnualizedPremium", String.valueOf(s_SOI_AnnualP),common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
			TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+CommonFunction.businessEvent, "Period Rating Table", common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_NetNetPremium", String.valueOf(s_SOI_NetNetP),common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));

			if(sCase.equals("")){
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+CommonFunction.businessEvent, "Period Rating Table", common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_PenCommission", String.valueOf(Refer_PenComm),common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+CommonFunction.businessEvent, "Period Rating Table", common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_BrokerCommission", String.valueOf(Refer_BrokerComm),common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+CommonFunction.businessEvent, "Period Rating Table", common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_InsuranceTax", String.valueOf(Refer_InsTax),common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
			}else{
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+CommonFunction.businessEvent, "Period Rating Table", common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_PenCommission", String.valueOf(s_SOI_PenComm),common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+CommonFunction.businessEvent, "Period Rating Table", common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_BrokerCommission", String.valueOf(s_SOI_BrokerComm),common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+CommonFunction.businessEvent, "Period Rating Table", common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_InsuranceTax", String.valueOf(s_SOI_InsTaxR),common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
			}

			TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+CommonFunction.businessEvent, "Period Rating Table", common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_GrossPremium", String.valueOf(s_SOI_Grossp),common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));
			TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+CommonFunction.businessEvent, "Period Rating Table", common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index).get("Automation Key"), "PRT_TotalPremium", String.valueOf(s_SOI_TotalP),common.Renewal_Structure_of_InnerPagesMaps.get("Period Rating Table").get(xlWRite_Index));

			TestUtil.WriteDataToXl(CommonFunction.product+"_"+CommonFunction.businessEvent, "Premium Summary", (String)common.Renewal_excel_data_map.get("Automation Key"), "PS_PI_NetNetPremium", String.valueOf(s_SOI_AnnualP), common.Renewal_excel_data_map);	

			return retVal;


		}catch(Throwable t){
			System.out.println("Error in coverSPI_PeriodRatingTable in Renewal Submitted chanegs - "+t);
			return false;
		}

	}

	public boolean funcUpdateCoverDetails_MTA(Map<Object, Object> map_data){

		try {
			customAssert.assertTrue(common.funcPageNavigation("Covers", ""),"Cover page is having issue(S)");
			String coverName = null;
			String c_locator = null;
			k.pressDownKeyonPage();
			String all_cover = ObjectMap.properties.getProperty(CommonFunction.product+"_CD_AllCovers");
			String[] split_all_covers = all_cover.split(",");
			for(String coverWithLocator : split_all_covers){
				String coverWithoutLocator = coverWithLocator.split("_")[0];
				try{
					//CoversDetails_data_list.add(coverWithoutLocator);
					coverName = coverWithLocator.split("_")[0];	
					c_locator = coverWithLocator.split("_")[1];
					k.waitTwoSeconds();
					if(c_locator.equals("md")){


						if (!driver.findElement(By.xpath("//*[contains(@name,'"+c_locator+"_selected')]")).isSelected()){
							if(((String) map_data.get("CD_"+coverName)).equalsIgnoreCase("No"))
								continue;
							else {
								customAssert.assertTrue(common.selectCover(coverWithLocator,map_data), "Select covers function is having issue(S) . ");
							}
						}else{
							if(((String) map_data.get("CD_"+coverName)).equalsIgnoreCase("Yes")) {
								continue;
							}else
								customAssert.assertTrue(common.deSelectCovers(coverWithLocator,map_data), "Select covers function is having issue(S) . ");
						}

					}else if(c_locator.equals("PEL")){

					}else{
						if (!driver.findElement(By.xpath("//*[contains(@name,'"+c_locator+"_selected')]")).isSelected()){
							JavascriptExecutor j_exe = (JavascriptExecutor) driver;
							j_exe.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[contains(@name,'"+c_locator+"_selected')]")));

							if(((String) map_data.get("CD_"+coverName)).equalsIgnoreCase("No"))
								continue;
							else {
								customAssert.assertTrue(common.selectCover(coverWithLocator,map_data), "Select covers function is having issue(S) . ");
								common_HHAZ.CoversDetails_data_list.add(coverWithoutLocator);
							}

						}else{
							if(((String) map_data.get("CD_"+coverName)).equalsIgnoreCase("Yes")) {
								common_HHAZ.CoversDetails_data_list.add(coverWithoutLocator);
								continue;
							}else
								customAssert.assertTrue(common.deSelectCovers(coverWithLocator,map_data), "Select covers function is having issue(S) . ");
						}

					}	

				}catch(StaleElementReferenceException stale_ex) {
					if(stale_count++ < 3)
						funcUpdateCoverDetails_MTA(map_data);
					stale_count = 0;
					System.out.println("StaleElementReferenceException in funcUpdateCoverDetails_MTA function after >"+stale_count+"< attempts - "+stale_ex);
					break;
				}catch(Throwable tt){
					System.out.println("Error while Updating Cover data for MTA - "+coverWithoutLocator);
					break;
				}
			}

			stale_count = 0;
			return true;

		}catch(StaleElementReferenceException stale_ex) {
			if(stale_count++ < 3)
				funcUpdateCoverDetails_MTA(map_data);
			stale_count = 0;
			System.out.println("StaleElementReferenceException in funcUpdateCoverDetails_MTA function after >"+stale_count+"< attempts - "+stale_ex);
			return false;
		} catch (Exception e) {
			stale_count = 0;
			return false;
		}

	}


	public boolean Solicitors_PI_MTA( Map<Object, Object> map_data){
		boolean retVal = true;

		Map<String, List<Map<String, String>>> data_map = null;

		switch(common.currentRunningFlow){
		case "MTA":
			data_map = common.MTA_Structure_of_InnerPagesMaps;
			break;
		case "NB":
			data_map = common.NB_Structure_of_InnerPagesMaps;
			break;
		case "Rewind":
			data_map = common.Rewind_Structure_of_InnerPagesMaps;
			break;
		case "Requote":
			data_map = common.Requote_Structure_of_InnerPagesMaps;
			break;
		}

		try{
			customAssert.assertTrue(common.funcPageNavigation("Solicitors PI", ""), "Navigation problem to Solicitors PI page .");

			if(!common.currentRunningFlow.equalsIgnoreCase("NB")){

				JavascriptExecutor j_exe = (JavascriptExecutor) driver;
				j_exe.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[@id='pia_pi_tot']")));
				common_PIA.PI_Premium = driver.findElement(By.xpath("//*[@id='pia_pi_tot']")).getAttribute("value");

				if(TestBase.businessEvent.equalsIgnoreCase("MTA") && ((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes")){

					try {	
						switch((String)map_data.get("MTA_Operation")) {

						case "AP":
						case "RP":

							String _cover = common_EP.AP_RP_Cover_Key.split("-")[1];

							if(!_cover.contains("SolicitorsPI")) {
								common.MTA_excel_data_map.put("PS_SolicitorsPI_NetNetPremium", common_PIA.PI_Premium);
								TestUtil.reportStatus("Solicitors PI Net Net Premium captured successfully . ", "Info", true);
								return true;
							}
							break;

						case "Policy-level":

							break;

						case "Non-Financial":

							common.MTA_excel_data_map.put("PS_SolicitorsPI_NetNetPremium", common_PIA.PI_Premium);
							customAssert.assertTrue(k.Input("PIA_SP_AdminFeeSP", (String)map_data.get("SP_AdminFeeSP")),	"Unable to enter value in Admin fees field .");
							customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Excesses page .");  
							TestUtil.reportStatus("Due to Non-Financial Flow, Only Solicitors PI Net Net Premium captured  . ", "Info", true);
							return true;

						}
					}catch(NullPointerException npe) {

					}
				}
			}

			if(common_EP.AP_RP_Cover_Key.contains("SolicitorsPI")) {
				customAssert.assertTrue(k.Input("PIA_SP_AdminFeeSP", (String)map_data.get("SP_AdminFeeSP")),	"Unable to enter value in Admin fees field .");
				customAssert.assertTrue(coverSPI_PeriodRatingTable_MTA_EP(map_data, (String)map_data.get("MTA_Operation")),"Error in coverSPI_PeriodRatingTable_MTA_EP function. ");
			}else {
				// Area of Practice Table section :

				customAssert.assertTrue(SPI_AreaOfPractice_MTA(data_map),"Unable to handle table 'Area of practice'");

				// Select Work split to value :
				customAssert.assertTrue(k.DropDownSelection("SPI_SP_WorkSplittoUse", (String)map_data.get("SP_WorkSplittoUse")), "Unable to enter 'Work Split to use' value");

				// fees/Turnover Table section :

				customAssert.assertTrue(SPI_Fees_Turnover_MTA(map_data),"Unable to handle table 'Fees/Turnover'");

				// Select 'Rate on' value :
				if(common_PIA.isMTARewindFlow){
					customAssert.assertTrue(k.DropDownSelection("SPI_SP_RateOn", (String)map_data.get("SP_MTARewind_RateOn")), "Unable to enter 'Rate on' value");
				}else{
					customAssert.assertTrue(k.DropDownSelection("SPI_SP_RateOn", (String)map_data.get("SP_RateOn")), "Unable to enter 'Rate on' value");
				}

				if(!common.currentRunningFlow.equalsIgnoreCase("Renewal") && !common.currentRunningFlow.equalsIgnoreCase("Rewind")){
					String default_PI_policy_fee = k.getAttribute("PIA_SP_AdminFeeSP", "value");
					customAssert.assertEquals(default_PI_policy_fee, (String)map_data.get("SP_DefaultAdminFeeSP"),"PI Default Admin fee is In-correct for Endorsement. Expected: "+(String)map_data.get("SP_DefaultAdminFeeSP")+" and Actual: "+default_PI_policy_fee);
				}

				// Excess Section :
				customAssert.assertTrue(k.Input("PIA_SP_AdminFeeSP", (String)map_data.get("SP_AdminFeeSP")),	"Unable to enter value in Admin fees field .");
				customAssert.assertTrue(k.DropDownSelection("SPI_SP_Aggegate", (String)map_data.get("SP_Aggegate")), "Unable to enter 'Aggregate' value");
				customAssert.assertTrue(calculate_SPI_Book_Premium(), "SPI Book Premium Calculation is function having issue .");
				TestUtil.reportStatus("SPI Rater Lookup values are calculated sucessfully after Endorsement . ", "Info", true);
				// Burn Rate:
				burnRate = common_PIA.PIA_Rater_output.get("Burn Rate");

				//Policy start and end date :
				//Refer_Policy_SDate = k.getText("SPI_SP_PolicyStartDate");
				Refer_Policy_EDate = k.getText("SPI_SP_PolicyEndDate");
				dAdminFee_CoverSPI=  Double.parseDouble((String)map_data.get("SP_AdminFeeSP"));

				//Period Rating Table Section :
				customAssert.assertTrue(coverSPI_PeriodRatingTable_MTA(map_data, ""),"Error in coverSPI_PeriodRatingTable_MTA function. ");
			}


			TestUtil.reportStatus("Solicitors PI Cover details are filled and Verified sucessfully after Endorsement . ", "Info", true);

			stale_count = 0;
			return retVal;

		}catch(StaleElementReferenceException stale_ex) {
			if(stale_count++ < 3)
				Solicitors_PI_MTA(map_data);
			stale_count = 0;
			System.out.println("StaleElementReferenceException in Solicitors_PI function after >"+stale_count+"< attempts - "+stale_ex);
			return false;
		}catch(Throwable t){
			System.out.println("Error in Solicitors_PI function - "+t);
			stale_count = 0;
			return false;
		}


	}

	public boolean SPI_AreaOfPractice_MTA( Map<String, List<Map<String, String>>> mdata){
		boolean retVal = true;
		int totalCols, sTotal = 0;
		Map<Object,Object> data_map = null;
		switch(common.currentRunningFlow){
		case "MTA":
			data_map=common.MTA_excel_data_map;
			mdata = common.MTA_Structure_of_InnerPagesMaps;
			break;
		case "Renewal":
			data_map=common.Renewal_excel_data_map;
			mdata = common.Renewal_Structure_of_InnerPagesMaps;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			mdata = common.Rewind_Structure_of_InnerPagesMaps;
			break;
		}

		try{

			String sUniqueCol ="Area of practice";
			int tableId = 0;
	
			String sTablePath = "html/body/div[3]/form/div/table";

			tableId = k.getTableIndex(sUniqueCol,"xpath",sTablePath);

			sTablePath = "html/body/div[3]/form/div/table["+ tableId +"]";
			WebElement table= driver.findElement(By.xpath(sTablePath));
			totalCols = table.findElements(By.tagName("th")).size();
			int noOfAOP =0;
			String sAreaOfPractice = null;
			try{

				//if(common_PIA.isMTARewindFlow){
				//sAreaOfPractice = (String)common.MTA_excel_data_map.get("SP_MTARewind_AreaofPractice");
				//}else{
				sAreaOfPractice = (String)data_map.get("SP_AreaofPractice");
				//}


				String sValue[] = sAreaOfPractice.split(";");
				noOfAOP = sValue.length;

			}catch(Throwable t){
				noOfAOP = 0;
			}
			//mdata = common.MTA_Structure_of_InnerPagesMaps;
			//Click Delete Button for Fresh MTA Data
			boolean isNotStale=true;

			while(isNotStale){
				try{
					List<WebElement> delete_Btns = driver.findElements(By.xpath("//td[text()='Area of Practice']//following::table[@id='p3_spi_table1']//a[text()='Delete']"));
					for(WebElement element: delete_Btns){
						if(element.isDisplayed())
							element.click();
						else
							continue;
					}
					stale_count = 0;
					isNotStale=false;
				}catch(StaleElementReferenceException stale_ex) {
					if(stale_count++ < 10)
						continue;
					System.out.println("StaleElementReferenceException in Delete Area of Practice after >"+stale_count+"< attempts - "+stale_ex);
					stale_count = 0;
					return false;
				}catch(Throwable t){
					continue;
				}
			}



			// Enter values to the table :

			for(int i = 0; i < noOfAOP; i++ ){
				int row = i;
				//click on 'Add Row' link :
				WebElement Add_row_btn = driver.findElement(By.xpath("//td[text()='Area of Practice']//following::table[@id='p3_spi_table1']//a[text()='Add Row']"));
				Add_row_btn.click();
				//driver.findElement(By.xpath(sTablePath + "/tbody/tr["+ (i+2) +"]/td[5]/a")).click();

				//Enter Data :
				/*if(common_PIA.isMTARewindFlow){
				i = 1;
			}*/

				customAssert.assertTrue(k.DropDownSelection_DynamicXpath(sTablePath + "/tbody/tr["+(row+1)+"]/td[1]/select", mdata.get("Area of Practice").get(i).get("PIA_AOP_AreaOfPracticeValue")),"Unable to enter activity in Area of practice table");

				customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(row+1)+"]/td[2]/input", mdata, "Area of Practice", i, "PIA_AOP_", "LastYear", "Last year (%)", "Input"),"Unable to enter Last year (%) in Area of practice table");
				customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(row+1)+"]/td[3]/input", mdata, "Area of Practice", i, "PIA_AOP_", "PriorYear", "PriorYear", "Input"),"Unable to enter prior year (%) in Area of practice table");
				customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(row+1)+"]/td[4]/input", mdata, "Area of Practice", i, "PIA_AOP_", "PriorYear2", "PriorYear", "Input"),"Unable to enter prior year 2 (%) in Area of practice table");		

			}

			k.Click("SPI_Save_SOI");
			// Read total, verify and adjust if not equals to 100% :

			for(int j = 0; j < totalCols-2; j++){

				sTotal =  Integer.parseInt(k.GetText_DynamicXpathWebDriver(driver, sTablePath + "/tbody/tr["+(noOfAOP+1)+"]/td["+(j+2)+"]"));

				if(sTotal != 100){
					TestUtil.reportStatus("Total does not equal to 100% for AreaOfPractice table in Endorsement. Entered value is : <b>[  "+ sTotal +"  ]</b>. ", "Info", false);
					retVal = false;
				}else{
					TestUtil.reportStatus("Total Percent is verified AreaOfPractice table after Endorsement. Entered value is : <b>[  "+ sTotal +"  ]</b>.", "Info", true);
				}
			}		

			stale_count = 0;
			return retVal;

		}catch(StaleElementReferenceException stale_ex) {
			if(stale_count++ < 3)
				SPI_AreaOfPractice_MTA(mdata);
			System.out.println("StaleElementReferenceException in SPI_AreaOfPractice_MTA function after >"+stale_count+"< attempts - "+stale_ex);
			stale_count = 0;
			return false;
		}catch(Throwable t){
			System.out.println("Error in SPI_AreaOfPractice - "+t);
			stale_count = 0;
			return false;
		}


	}

	public boolean SPI_Fees_Turnover_MTA( Map<Object, Object> map_data){
		boolean retVal = true;
		int totalCols;
		String sFinal ="", sVal = "";

		try{

			if(common_PIA.isRewindSelected.contains("Yes") || common_PIA.isRenewalRewindFlow){
				sVal = "Rewind";
			}

			String sUniqueCol ="Year";
			int tableId = 0;

			String sTablePath = "html/body/div[3]/form/div/table";

			tableId = k.getTableIndex(sUniqueCol,"xpath",sTablePath);

			sTablePath = "html/body/div[3]/form/div/table["+ tableId +"]";
			WebElement table= driver.findElement(By.xpath(sTablePath));

			totalCols = table.findElements(By.tagName("th")).size();
	
			switch (sVal){
			case "Rewind" :
				sFinal = (String)map_data.get("SP_RateOn_Rewind");
				break;

			default :

				// Enter values to the table :
				if(common_PIA.isMTARewindFlow){
					sFinal = (String)map_data.get("SP_MTARewind_RateOn");
				}else{
					sFinal = (String)map_data.get("SP_RateOn");
				}

				for(int i = 0; i < totalCols-3; i++ ){

					//Get Column name :

					String Reagon = k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/thead/tr/th["+(i+3)+"]");

					if(Reagon.length() > 0){

						if(Reagon.contains("Rest of the World")){
							Reagon = "ROW";
						}else if(Reagon.contains("USA/ Canada")){
							Reagon = "USACanada";
						}

						//Enter Data :

						customAssert.assertTrue(k.DynamicXpathWebDriver(driver, sTablePath + "/tbody/tr[1]/td[" + (i+3) +"]/input", map_data, "Solicitors PI", i, "SP_", "Estimate"+Reagon, Reagon, "Input"),"Unable to enter Estimate value for reagon - "+Reagon);				
						customAssert.assertTrue(k.DynamicXpathWebDriver(driver, sTablePath + "/tbody/tr[2]/td[" + (i+3) +"]/input", map_data, "Solicitors PI", i, "SP_", "2017"+Reagon, Reagon, "Input"),"Unable to enter 2017 value for reagon - "+Reagon);
						customAssert.assertTrue(k.DynamicXpathWebDriver(driver, sTablePath + "/tbody/tr[3]/td[" + (i+3) +"]/input", map_data, "Solicitors PI", i, "SP_", "2016"+Reagon, Reagon, "Input"),"Unable to enter 2016 value for reagon - "+Reagon);
						customAssert.assertTrue(k.DynamicXpathWebDriver(driver, sTablePath + "/tbody/tr[4]/td[" + (i+3) +"]/input", map_data, "Solicitors PI", i, "SP_", "2015"+Reagon, Reagon, "Input"),"Unable to enter 2015 value for reagon - "+Reagon);
						customAssert.assertTrue(k.DynamicXpathWebDriver(driver, sTablePath + "/tbody/tr[5]/td[" + (i+3) +"]/input", map_data, "Solicitors PI", i, "SP_", "2014"+Reagon, Reagon, "Input"),"Unable to enter 2014 value for reagon - "+Reagon);
					}			
				}

				break;			

			}

			// Read FeesIncome Total :

			TotalFeesIncome_Estimate = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[1]/td[" +totalCols+ "]/input").replaceAll(",", ""));
			TotalFeesIncome_2017 = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[2]/td[" +totalCols+ "]/input").replaceAll(",", ""));
			TotalFeesIncome_2016 = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[3]/td[" +totalCols+ "]/input").replaceAll(",", ""));
			TotalFeesIncome_2015 = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[4]/td[" +totalCols+ "]/input").replaceAll(",", ""));
			TotalFeesIncome_2014 = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[5]/td[" +totalCols+ "]/input").replaceAll(",", ""));
			Total_Average = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr[6]/td[" +totalCols+ "]/input").replaceAll(",", ""));

			if(sFinal.contains("Estimate")){
				global_SOI_GrossFees = TotalFeesIncome_Estimate; 
			}else if(sFinal.contains("Last Year")){
				global_SOI_GrossFees = TotalFeesIncome_2017;
			}else if(sFinal.equalsIgnoreCase("Prior Year")){
				global_SOI_GrossFees = TotalFeesIncome_2016;
			}else if(sFinal.equalsIgnoreCase("Prior Year 2")){
				global_SOI_GrossFees = TotalFeesIncome_2015;
			}else if(sFinal.equalsIgnoreCase("Prior Year 3")){
				global_SOI_GrossFees = TotalFeesIncome_2014;
			}else{
				global_SOI_GrossFees = Total_Average;
			}

			TestUtil.WriteDataToXl(CommonFunction.product+"_"+common.currentRunningFlow, "Solicitors PI",(String)map_data.get("Automation Key"), "SP_GrossFee", String.valueOf(global_SOI_GrossFees), map_data);

			if(common_PIA.isRewindSelected.contains("Yes") || common_PIA.isRenewalRewindFlow){
				map_data.put("SP_GrossFee",  String.valueOf(global_SOI_GrossFees));
			}

			stale_count = 0;
			return retVal;

		}catch(StaleElementReferenceException stale_ex) {
			if(stale_count++ < 3)
				SPI_Fees_Turnover_MTA(map_data);
			stale_count = 0;
			System.out.println("StaleElementReferenceException in SPI_Fees_Turnover_MTA function after >"+stale_count+"< attempts - "+stale_ex);
			return false;
		}catch(Throwable t){
			System.out.println("Error in SPI_Fees_Turnover - "+t);
			stale_count = 0;
			return false;
		}

	}

	public boolean coverSPI_PeriodRatingTable_MTA( Map<Object, Object> map_data, String sCase){
		boolean retVal = true;
		double s_SOI_BookRate = 0.00, s_SOI_InitialP = 0.00, s_SOI_BookP = 0.00, s_SOI_TechA = 0.00, s_SOI_RevisedP = 0.00, s_SOI_AnnualP = 0.00;
		double s_SOI_IndLimit = 0.00;

		String SummaryTable_UniqueCol, SummaryTable_Path;

		String sPeriodMonths;

		Map<String, List<Map<String, String>>> data_Structure_of_InnerPagesMaps = null;

		switch(common.currentRunningFlow){

		case "NB":
			data_Structure_of_InnerPagesMaps = common.NB_Structure_of_InnerPagesMaps;
			break;
		case "MTA":
			data_Structure_of_InnerPagesMaps = common.MTA_Structure_of_InnerPagesMaps;
			break;
		case "Renewal":
			data_Structure_of_InnerPagesMaps = common.Renewal_Structure_of_InnerPagesMaps;
			break;	
		case "Rewind":
			data_Structure_of_InnerPagesMaps = common.Rewind_Structure_of_InnerPagesMaps;
			break;	

		}


		try{

			//Identify Period Rating Table :

			String sUniqueCol ="OPTION";
			int tableId = 0;

			String sTablePath = "html/body/div[3]/form/div/table";

			tableId = k.getTableIndex(sUniqueCol,"xpath",sTablePath);

			sTablePath = "html/body/div[3]/form/div/table["+ tableId +"]";
	
			String sPeriodRatings = (String)map_data.get("SP_PeriodRatingTable");
			String sValue[] = sPeriodRatings.split(";");
			int noOfPR = sValue.length;

			// Identify Summary Table  :

			SummaryTable_UniqueCol ="Description";		
			tableId = k.getTableIndex(SummaryTable_UniqueCol,"xpath","html/body/div[3]/form/div/table");		
			SummaryTable_Path = "html/body/div[3]/form/div/table["+ tableId +"]";
		
			switch (sCase){

			case "" :

				// Enter Data in Period Rating Table :

				if(common_PIA.isRewindSelected.contains("Yes") || common_PIA.isRenewalRewindFlow){
					burnRate = common_PIA.PIA_Rater_output.get("Burn Rate");
					// Do Nothing
				}else{
					//customAssert.assertTrue(deleteItems_PR_Items(),"Delete Period Rating Table Items function is having issues.");

					//if(!((String)map_data.get("Automation Key")).contains("MTA_07")){
					for(int i = 0; i < noOfPR ; i++ ){

						//click on 'Add Row' link :

						WebElement Add_row_btn = driver.findElement(By.xpath("//td[text()='Period Rating Table']//following::a[text()='Add Row']"));
						k.ScrollInVewWebElement(Add_row_btn);
						Add_row_btn.click();

						// Enter Data :

						customAssert.assertTrue(k.DropDownSelection_DynamicXpath(sTablePath + "/tbody/tr["+(i+1)+"]/td[2]/select", data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_PeriodofInsurance")),"Unable to enter PeriodofInsurance in Period rating table");
						customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(i+1)+"]/td[3]/input", data_Structure_of_InnerPagesMaps, "Period Rating Table", i, "PRT_", "ExcessAmount", "ExcessAmount", "Input"),"Unable to enter ExcessAmount in Period rating table");
						customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, sTablePath + "/tbody/tr["+(i+1)+"]/td[4]/input", data_Structure_of_InnerPagesMaps, "Period Rating Table", i, "PRT_", "TechAdjust", "TechAdjust", "Input"),"Unable to enter TechAdjust in Period rating table");

					}
					//	}

					// Enter Comm Adjust in Summary Table if required :
					customAssert.assertTrue(k.DynamicXpathWebDriver(driver, SummaryTable_Path + "/tbody/tr[1]/td[10]/input", map_data, "Solicitors PI", 0, "SP_", "PICommAdjustment", "Comm Adjust", "Input"),"Unable to enter Comm Adjust value in summary table");
					global_SOI_CommAdjust = Double.parseDouble((String)map_data.get("SP_PICommAdjustment"));
				}

				if(common_PIA.isRenewalRewindFlow){
					customAssert.assertTrue(k.DynamicXpathWebDriver(driver, SummaryTable_Path + "/tbody/tr[1]/td[10]/input", map_data, "Solicitors PI", 0, "SP_", "PICommAdjustment", "Comm Adjust", "Input"),"Unable to enter Comm Adjust value in summary table");
					global_SOI_CommAdjust = Double.parseDouble((String)map_data.get("SP_PICommAdjustment"));

				}

				// Read Values of Period Rating Table from screen and calculations :

				for(int i = 0; i < noOfPR ; i++ ){	

					if(data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_IsSelected").contains("Yes")){

						if(i>0){
							driver.findElement(By.xpath(sTablePath + "/tbody/tr["+ (i+1) +"]/td[1]/input[2]")).click();
						}								

						k.Click("SPI_ApplyBookRate");


						//Duration :
						duration_SoPI = Integer.parseInt(k.getText("SPI_SP_Duration"));

						//Read Values from Screen -  Summary Table :

						global_PeriodMonths = Integer.parseInt(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[2]"));
						global_FeesIncome = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[3]").replaceAll(",", ""));
						global_IndemnityLimit = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[4]").replaceAll(",", ""));
						global_SOI_BookRate = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[5]/input").replaceAll(",", ""));
						global_SOI_InitialP = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[6]").replaceAll(",", "")));
						gloabal_SOI_BookP = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[7]/input").replaceAll(",", ""));
						global_SOI_TecgAdjust = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[8]/input").replaceAll(",", ""));
						global_SOI_RevisedP = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[9]/input").replaceAll(",", ""));
						global_SOI_sPremium = Double.parseDouble(k.GetText_DynamicXpathWebDriver(driver, SummaryTable_Path+"/tbody/tr[1]/td[12]/input").replaceAll(",", ""));							

						// Read values from Period Rating Table :

						global_SOI_AnnualP = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[5]/input").replaceAll(",", "")));
						global_SOI_NetNetP = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[6]/input").replaceAll(",", "")));
						global_SOI_GrossP = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[9]/input").replaceAll(",", "")));
						global_SOI_TotalPremium = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[11]/input").replaceAll(",", "")));

						// Read Default Values Which are referred on Premium Summary Screen :

						Refer_PenComm = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[7]/input").replaceAll(",", "")));
						Refer_BrokerComm = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[8]/input").replaceAll(",", "")));
						Refer_InsTax = Double.parseDouble(common.roundedOff(k.GetText_DynamicXpathWebDriver(driver, sTablePath+"/tbody/tr["+(i+1)+"]/td[10]/input").replaceAll(",", "")));

						//Calculated Values :

						sPeriodMonths = data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_PeriodofInsurance");

						String sVal = (String)map_data.get("PG_IsYourPracticeLLP");
						if(sVal.contains("Yes")){
							s_SOI_IndLimit = 3000000.00;
						}else{
							s_SOI_IndLimit = 2000000.00;
						}

						s_SOI_BookRate = common_PIA.PIA_Rater_output.get("Book_Rate_Cent");
						s_SOI_InitialP = common_PIA.PIA_Rater_output.get("Book_Premium_24_months");

						if(sPeriodMonths.contains("12")){
							s_SOI_BookP = common_PIA.PIA_Rater_output.get("Book_Premium_12_months");
						}else if(sPeriodMonths.contains("18")){
							s_SOI_BookP = common_PIA.PIA_Rater_output.get("Book_Premium_18_months");
						}else{
							s_SOI_BookP = common_PIA.PIA_Rater_output.get("Book_Premium_24_months");
						}

						s_SOI_TechA = Double.parseDouble(common.roundedOff(data_Structure_of_InnerPagesMaps.get("Period Rating Table").get(i).get("PRT_TechAdjust")));
						s_SOI_RevisedP = Double.parseDouble(common.roundedOff(Double.toString(((s_SOI_BookP*s_SOI_TechA)/100) + s_SOI_BookP)));
						s_SOI_AnnualP = Double.parseDouble(common.roundedOff(Double.toString(((s_SOI_RevisedP * global_SOI_CommAdjust)/100) + s_SOI_RevisedP)));

						double s_BurnRate = Double.parseDouble(common.roundedOff(k.getText("SPI_PI_BurnRate").replaceAll(",", "")));

						// Comparisons :

						CommonFunction.compareValues(burnRate, s_BurnRate,"BurnRate");
						CommonFunction.compareValues(Double.parseDouble(sPeriodMonths), Double.parseDouble(String.valueOf(global_PeriodMonths)),"Period of insurance month ");
						CommonFunction.compareValues(global_SOI_GrossFees, global_FeesIncome,"Fees Income");
						CommonFunction.compareValues(s_SOI_IndLimit, global_IndemnityLimit,"Indemnity Limit");
						CommonFunction.compareValues(s_SOI_BookRate, global_SOI_BookRate,"SOI BookRate");
						CommonFunction.compareValues(s_SOI_InitialP, global_SOI_InitialP,"SOI Initial Premium");
						CommonFunction.compareValues(s_SOI_BookP, gloabal_SOI_BookP,"SOI Book Premium");
						CommonFunction.compareValues(s_SOI_TechA, global_SOI_TecgAdjust,"SOI TechAdjust");
						CommonFunction.compareValues(s_SOI_RevisedP, global_SOI_RevisedP,"SOI Revised Premium");
						CommonFunction.compareValues(s_SOI_AnnualP, global_SOI_sPremium,"SOI Annual Premium From Summary Table");
						CommonFunction.compareValues(s_SOI_AnnualP, global_SOI_AnnualP,"SOI Annual Premium From PR Table");
					}						
				}

				break;

			}


			// Write data to excel :
			TestUtil.WriteDataToXl(CommonFunction.product+"_"+common.currentRunningFlow, "Premium Summary", (String)map_data.get("Automation Key"), "PS_SolicitorsPI_NetNetPremium", String.valueOf(global_SOI_sPremium), map_data);	

			stale_count = 0;
			return retVal;

		}catch(StaleElementReferenceException stale_ex) {
			if(stale_count++ < 3)
				coverSPI_PeriodRatingTable_MTA(map_data,sCase);
			stale_count = 0;
			System.out.println("StaleElementReferenceException in coverSPI_PeriodRatingTable_MTA function after >"+stale_count+"< attempts - "+stale_ex);
			return false;
		}catch(Throwable t){
			System.out.println("Error in coverSPI_PeriodRatingTable - "+t);
			return false;
		}


	}
	/**
	 * @param map_data
	 * @param sCase
	 * @return
	 */
	public boolean coverSPI_PeriodRatingTable_MTA_EP( Map<Object, Object> map_data, String MTA_Operation){
		boolean retVal = true;

		String SummaryTable_UniqueCol, SummaryTable_Path, total_SP_Premium;

		double SP_PremiumOverride = 0.0;

		try{

			int tableId = 0;

			// Identify Summary Table  :

			SummaryTable_UniqueCol ="Description";		
			tableId = k.getTableIndex(SummaryTable_UniqueCol,"xpath","html/body/div[3]/form/div/table");		
			SummaryTable_Path = "html/body/div[3]/form/div/table["+ tableId +"]";
			WebElement Summary_table= driver.findElement(By.xpath("//*[text()='Apply Book Rates']//following::table[@id='table0']"));
			k.ScrollInVewWebElement(Summary_table);

			switch (MTA_Operation){

			case "AP" :
				TestUtil.reportStatus("<b>------ Additional Premium for Cover - Solicitors PI -------------</b>", "Info", false);
				SP_PremiumOverride = Double.parseDouble(common_PIA.PI_Premium) * 2;
				driver.findElement(By.xpath(SummaryTable_Path + "/tbody/tr[1]/td[11]/input")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				driver.findElement(By.xpath(SummaryTable_Path + "/tbody/tr[1]/td[11]/input")).sendKeys(Double.toString(SP_PremiumOverride));
				//customAssert.assertTrue(k.DynamicXpathWebDriver(driver, SummaryTable_Path + "/tbody/tr[1]/td[11]/input", map_data, "Solicitors PI", 0, "SP_", "PIPremiumOverride", "Premium Override", "Input"),"Unable to enter Premium Override value in SP summary table");
				break;
			case "RP" :
				TestUtil.reportStatus("<b>------- Reduced Premium for Cover - Solicitors PI -------------</b>", "Info", false);
				SP_PremiumOverride = Double.parseDouble(common_PIA.PI_Premium) - (0.1 *Double.parseDouble(common_PIA.PI_Premium));
				driver.findElement(By.xpath(SummaryTable_Path + "/tbody/tr[1]/td[11]/input")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				driver.findElement(By.xpath(SummaryTable_Path + "/tbody/tr[1]/td[11]/input")).sendKeys(Double.toString(SP_PremiumOverride));
				break;
			}

			customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Solicitors PI screen .");

			k.Click("SPI_ApplyBookRate");

			JavascriptExecutor j_exe = (JavascriptExecutor) driver;
			j_exe.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[@id='pia_pi_tot']")));
			total_SP_Premium = driver.findElement(By.xpath("//*[@id='pia_pi_tot']")).getAttribute("value");

			map_data.put("PS_SolicitorsPI_NetNetPremium", String.valueOf(total_SP_Premium));

			stale_count = 0;
			return retVal;

		}catch(StaleElementReferenceException stale_ex) {
			if(stale_count++ < 3)
				coverSPI_PeriodRatingTable_MTA_EP(map_data,MTA_Operation);
			stale_count = 0;
			System.out.println("StaleElementReferenceException in coverSPI_PeriodRatingTable_MTA_EP function after >"+stale_count+"< attempts - "+stale_ex);
			return false;
		}catch(Throwable t){
			System.out.println("Error in coverSPI_PeriodRatingTable_MTA_EP - "+t);
			stale_count = 0;
			return false;
		}


	}

	public boolean update_MTA_EffectiveDays() {

		try {

			String transactionDetailsMsg_xpath = "//p[text()=' Transaction Details ']//following-sibling::p";
			WebElement transactionDetails_Msg = driver.findElement(By.xpath(transactionDetailsMsg_xpath));
			k.ScrollInVewWebElement(transactionDetails_Msg);

			String text = transactionDetails_Msg.getText();

			String days[] = text.split(",");

			common.MTA_excel_data_map.put("MTA_EffectiveDays",days[1].substring(days[1].indexOf(":")+2, days[1].indexOf("days")));

			return true;
		}catch(Throwable t) {
			System.out.println("Error while getting MTA effective days . ");
			return false;
		}




	}

	public boolean deleteItems_PR_Items(){

		boolean isNotStale=true;
		k.ImplicitWaitOff();
		while(isNotStale){
			try{


				List<WebElement> delete_Btns = driver.findElements(By.xpath("//*[text()='Period Rating Table']//following::a[text()='Delete']"));

				for(WebElement element: delete_Btns){
					if(element.isDisplayed()){
						// This is just to check if Bonafied value on Public Liabiliy screen for CCC and CCD product is "Yes" or not.
						// Sometimes If Bonafied contains hiddedn "Delete" button then it is not appearing on page and it is failing. So to handle this case below code will help.
						try{
							String headerName = driver.findElement(By.xpath("html/body/div[3]/form/p")).getText();

							if(headerName.equalsIgnoreCase("Public Liability")){
								WebElement bonafiedValue = driver.findElement(By.xpath("//*[contains(@name,'bona_fide_subcontractors')]"));
								JavascriptExecutor j_exe = (JavascriptExecutor) driver;
								j_exe.executeScript("arguments[0].scrollIntoView(true);", bonafiedValue);
								Select mySelect = new Select(bonafiedValue);
								mySelect.selectByValue("Yes");
							}
						}catch(Throwable t){

						}

						element = driver.findElement(By.xpath("//*[text()='Period Rating Table']//following::a[text()='Delete']"));
						JavascriptExecutor j_exe = (JavascriptExecutor) driver;
						j_exe.executeScript("arguments[0].scrollIntoView(true);", element);
						element.click();
						WebDriverWait wait = new WebDriverWait(driver, 3);
						if(wait.until(ExpectedConditions.alertIsPresent())!=null){
							k.AcceptPopup();
						}

					}else{
						continue;
					}
				}
				isNotStale=false;
			}catch(Throwable t){
				continue;
			}
		}
		k.ImplicitWaitOn();
		return true;

	}

	// End oF CommonFunction_SPI

}
