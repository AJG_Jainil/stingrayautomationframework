package com.selenium.commonfiles.base;

import java.util.List;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement; 
import org.testng.Assert;


import com.selenium.commonfiles.util.ErrorInTestMethod;
import com.selenium.commonfiles.util.TestUtil;

public class CommonFunction_POI extends TestBase{
	public int err_count = 0 , trans_error_val = 0;
	public static DecimalFormat f = new DecimalFormat("00.00");
	public double TotalPremiumWithAdminDocAct = 0.00, TotalPremiumWithAdminDocExp = 0.00, PremiumExcTerrDocAct = 0.00,  PremiumExcTerrDocExp = 0.00, TerPremDocAct = 0.00, TerPremDocExp = 0.00, InsTaxDocAct = 0.00, InsTaxDocExp = 0.00;
	public double AdditionalPWithAdminDocAct = 0.00, AdditionalExcTerrDocAct = 0.00,  AdditionalTerPDocAct = 0.00, AdditionalInsTaxDocAct = 0.00;
	public double InsTaxTerrDoc = 0.00, tpTotal = 0.00, AddTaxTerrDoc = 0.00;
	public double rewindMTADoc_Premium = 0.00, rewindMTADoc_TerP = 0.00, rewindMTADoc_InsPTax = 0.00, rewindMTADoc_TotalP = 0.00;
	public double rewindDoc_Premium = 0.00, rewindDoc_TerP = 0.00, rewindDoc_InsPTax = 0.00, rewindDoc_TotalP = 0.00, rewindDoc_InsTaxTer = 0.00;
	public double rewindMTADoc_AddTaxTer = 0.00;
	public String FP_Covers = null;
	public void NewBusinessFlow(String code,String event){
	String testName = (String)common.NB_excel_data_map.get("Automation Key");
	try{
		
		customAssert.assertTrue(common.StingrayLogin("PEN"),"Unable to login.");
		customAssert.assertTrue(common.checkClient(common.NB_excel_data_map,code,event),"Unable to check Client.");
		customAssert.assertTrue(common.createNewQuote(common.NB_excel_data_map,code,event), "Unable to create new quote.");
		customAssert.assertTrue(common.selectLatestQuote(common.NB_excel_data_map,code,event), "Unable to select quote from table.");
		customAssert.assertTrue(common_POG.funcPolicyDetails(common.NB_excel_data_map), "Policy Details function having issue .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Previous Claims"),"Issue while Navigating to Previous Claims  . ");
		customAssert.assertTrue(common_CCF.funcPreviousClaims(common.NB_excel_data_map), "Previous claim function is having issue(S) .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(common.funcCovers(common.NB_excel_data_map), "Select covers function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Specified Perils"),"Issue while Navigating to Specified Perils  . ");
		customAssert.assertTrue(common.funcSpecifiedPerils(common.NB_excel_data_map), "Select covers function is having issue(S) . ");
		
		if(((String)common.NB_excel_data_map.get("CD_MaterialDamage")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Insured Properties"),"Issue while Navigating to Insured Properties  . ");
			customAssert.assertTrue(common_POG.funcInsuredProperties(common.NB_excel_data_map), "Insured Property function is having issue(S) . ");
		}
		
		if(((String)common.NB_excel_data_map.get("CD_Liability")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Employers Liability"),"Issue while Navigating to Employers Liability  . ");
			customAssert.assertTrue(common_CCF.funcEmployersLiability(common.NB_excel_data_map), "Employers Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","ELD Information"),"Issue while Navigating to ELD Information  . ");
			customAssert.assertTrue(common_CCF.funcELDInformation(common.NB_excel_data_map), "ELD Information function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability"),"Issue while Navigating to Property Owners Liability  . ");
			customAssert.assertTrue(common_POG.funcPropertyOwnersLiability(common.NB_excel_data_map), "Property Oweners Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability Information"),"Issue while Navigating to Products Liability  . ");
			customAssert.assertTrue(common_POG.funcLiabilityInformation(common.NB_excel_data_map), "Liability Information function is having issue(S) . ");
			}
		if(((String)common.NB_excel_data_map.get("CD_Terrorism")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Terrorism"),"Issue while Navigating to Terrorism screen . ");
			customAssert.assertTrue(common_CCF.funcTerrorism(common.NB_excel_data_map), "Terrorism function is having issue(S) . ");
			}
		if(((String)common.NB_excel_data_map.get("CD_LegalExpenses")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Legal Expenses"),"Issue while Navigating to Legal Expenses screen . ");
			customAssert.assertTrue(common_CCF.funcLegalExpenses(common.NB_excel_data_map,code,event), "Legal Expenses function is having issue(S) . ");
			}
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		Assert.assertTrue(common_VELA.funcPremiumSummary(common.NB_excel_data_map,code,event));
		Assert.assertTrue(common_PEN.funcStatusHandling(common.NB_excel_data_map,code,event));
		
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
	}
	
		
}

public void RewindFlow(String code,String event) throws ErrorInTestMethod{
	String testName = (String)common.Rewind_excel_data_map.get("Automation Key");
	try{
		
		if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes")) 
		{
			customAssert.assertTrue(common_EP.ExistingPolicyAlgorithm(common.Rewind_excel_data_map,(String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type"), (String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Status")), "Existing Policy Algorithm function is having issues. ");
		}
		else 
		{
			CommonFunction_HHAZ.AdjustedTaxDetails.clear();
			if(!common.currentRunningFlow.equalsIgnoreCase("Renewal") && !common.currentRunningFlow.equalsIgnoreCase("MTA"))
			{
				NewBusinessFlow(code,"NB");
			}
			common_PEN.PremiumFlag = false;
		}
		
	
		common.currentRunningFlow="Rewind";
		String navigationBy = CONFIG.getProperty("NavigationBy");
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common.funcRewind());
		
		TestUtil.reportStatus("<b> -----------------------Rewind flow started---------------------- </b>", "Info", false);
		
		if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes"))
		{
			customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
			customAssert.assertTrue(common.funcSearchPolicy(common.MTA_excel_data_map), "Policy Search function is having issue(S) . ");
			customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Endorsement Submitted (Rewind)"), "Verify Policy Status (Endorsement Submitted (Rewind)) function is having issue(s).");
		}
		else
		{
			customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
			customAssert.assertTrue(common.funcSearchPolicy(common.NB_excel_data_map), "Policy Search function is having issue(S) . ");
			if(TestBase.businessEvent.equalsIgnoreCase("MTA"))
			{
				customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Endorsement Submitted (Rewind)"), "Verify Policy Status (Endorsement Submitted (Rewind)) function is having issue(s).");
			}
			else if(TestBase.businessEvent.equalsIgnoreCase("Renewal"))
			{
				customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Renewal Submitted (Rewind)"), "Verify Policy Status (Renewal Submitted (Rewind)) function is having issue(s).");
			}
			else
			{
				customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Submitted (Rewind)"), "Verify Policy Status (Submitted (Rewind)) function is having issue(s).");
			}
		}
		customAssert.assertTrue(common_POG.funcPolicyDetails(common.Rewind_excel_data_map), "Policy Details function having issue .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Previous Claims"),"Issue while Navigating to Previous Claims  . ");
		customAssert.assertTrue(common_CCF.funcPreviousClaims(common.Rewind_excel_data_map), "Previous claim function is having issue(S) .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(common.funcCovers(common.Rewind_excel_data_map), "Select covers function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Specified Perils"),"Issue while Navigating to Specified Perils  . ");
		customAssert.assertTrue(common.funcSpecifiedPerils(common.Rewind_excel_data_map), "Select covers function is having issue(S) . ");
		if(((String)common.Rewind_excel_data_map.get("CD_MaterialDamage")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Insured Properties"),"Issue while Navigating to Insured Properties  . ");
			customAssert.assertTrue(common_POG.funcInsuredProperties(common.Rewind_excel_data_map), "Insured Property function is having issue(S) . ");
		}
		
		if(((String)common.Rewind_excel_data_map.get("CD_Liability")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Employers Liability"),"Issue while Navigating to Employers Liability  . ");
			customAssert.assertTrue(common_CCF.funcEmployersLiability(common.Rewind_excel_data_map), "Employers Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","ELD Information"),"Issue while Navigating to ELD Information  . ");
			customAssert.assertTrue(common_CCF.funcELDInformation(common.Rewind_excel_data_map), "ELD Information function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability"),"Issue while Navigating to Property Owners Liability  . ");
			customAssert.assertTrue(common_POG.funcPropertyOwnersLiability(common.Rewind_excel_data_map), "Public Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability Information"),"Issue while Navigating to Products Liability  . ");
			customAssert.assertTrue(common_POG.funcLiabilityInformation(common.Rewind_excel_data_map), "Liability Information function is having issue(S) . ");
			}
		
		if(((String)common.Rewind_excel_data_map.get("CD_Terrorism")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Terrorism"),"Issue while Navigating to Terrorism screen . ");
			customAssert.assertTrue(common_CCF.funcTerrorism(common.Rewind_excel_data_map), "Terrorism function is having issue(S) . ");
			}
		
		if(((String)common.Rewind_excel_data_map.get("CD_LegalExpenses")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Legal Expenses"),"Issue while Navigating to Legal Expenses screen . ");
			customAssert.assertTrue(common_CCF.funcLegalExpenses(common.Rewind_excel_data_map,code,event), "Legal Expenses function is having issue(S) . ");
			}
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		
		if(TestBase.businessEvent.equalsIgnoreCase("MTA")){
			customAssert.assertTrue(common_VELA.funcPremiumSummary_MTA(common.Rewind_excel_data_map, code, event), "Rewind MTA Premium Summary in function is having issue(S) . ");
		}else if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes")){
			customAssert.assertTrue(common_PEN.funcPremiumSummary_MTA(common.Rewind_excel_data_map, code, event), "Rewind MTA Premium Summary in function is having issue(S) . ");
		}else{
			customAssert.assertTrue(common_VELA.funcPremiumSummary(common.Rewind_excel_data_map,code,event), "Premium Summary function is having issue(S) . ");
		}
		if(!TestBase.businessEvent.equalsIgnoreCase("MTA") && !TestBase.businessEvent.equalsIgnoreCase("Renewal")){ 
			Assert.assertTrue(common_PEN.funcStatusHandling(common.Renewal_excel_data_map,code,event));
			
		}
			
		if(TestBase.businessEvent.equals("Rewind")){
			customAssert.assertEquals(err_count,0,"Errors in premium calculations . ");
			customAssert.assertEquals(trans_error_val,0,"Errors in Transaction premium calculations . ");
			customAssert.assertEquals(common.final_err_pdf_count,0,"Varification Errors in PDF Documents . ");
			customAssert.assertTrue(common.StingrayLogout(), "Unable to Logout.");
			TestUtil.reportTestCasePassed(testName);
			
		} 
		
		
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}	
}

public void MTAFlow(String code,String event) throws ErrorInTestMethod{
	
	String NavigationBy = CONFIG.getProperty("NavigationBy");
	String testName = (String)common.MTA_excel_data_map.get("Automation Key");
	common_PEN.AdjustedTaxDetails.clear();
	try{
	
	if(!TestBase.businessEvent.equalsIgnoreCase("Renewal")){
		NewBusinessFlow(code,"NB");
	}
	common.currentRunningFlow="MTA";
	
	common_HHAZ.CoversDetails_data_list.clear();
	customAssert.assertTrue(common.funcNextNavigateDecesionMaker(NavigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen.");
	customAssert.assertTrue(common_POG.funcCreateEndorsement(),"Error in Create Endorsement function .");
	
    customAssert.assertTrue(common_POG.funcPolicyDetails(common.MTA_excel_data_map),"Policy Details function having issue");
    customAssert.assertTrue(common.funcMenuSelection("Navigate","Previous Claims"), "Issue while Navigating to Previous Claims");
    customAssert.assertTrue(common_CCF.funcPreviousClaims(common.MTA_excel_data_map), "Previous claim function is having issue(S) .");
	String MTA_Method = (String)common.MTA_excel_data_map.get("MTA_TCDescription");
	customAssert.assertTrue(common.funcNextNavigateDecesionMaker(NavigationBy,"Covers"),"Issue while Navigating to Covers screen.");
	customAssert.assertTrue(common.funcCovers(common.MTA_excel_data_map), "Select covers function is having issue(S) . ");
//	customAssert.assertTrue(funcUpdateCoverDetails_MTA(common.MTA_excel_data_map),"Error in selecting cover for MTA.");
	customAssert.assertTrue(common.funcMenuSelection("Navigate","Specified Perils"),"Issue while Navigating to Specified Perils  . ");
	customAssert.assertTrue(common.funcSpecifiedPerils(common.MTA_excel_data_map), "Select covers function is having issue(S) . ");
	

	if(((String)common.MTA_excel_data_map.get("CD_MaterialDamage")).equals("Yes")){		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Insured Properties"),"Issue while Navigating to Insured Properties  . ");
		customAssert.assertTrue(common_POG.funcInsuredProperties(common.MTA_excel_data_map), "Insured Property function is having issue(S) . ");
	}
	
	if(((String)common.MTA_excel_data_map.get("CD_Liability")).equals("Yes")){
		
	//customAssert.assertTrue(common.funcMenuSelection("Navigate","Employers Liability"), "Issue while Navigating to Employers Liability .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Employers Liability"),"Issue while Navigating to Employers Liability  . ");
		customAssert.assertTrue(common_CCF.funcEmployersLiability(common.MTA_excel_data_map), "Employers Liability function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","ELD Information"),"Issue while Navigating to ELD Information  . ");
		customAssert.assertTrue(common_CCF.funcELDInformation(common.MTA_excel_data_map), "ELD Information function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability"),"Issue while Navigating to Property Owners Liability  . ");
		customAssert.assertTrue(common_POG.funcPropertyOwnersLiability(common.MTA_excel_data_map), "Public Liability function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability Information"),"Issue while Navigating to Products Liability  . ");
		customAssert.assertTrue(common_POG.funcLiabilityInformation(common.MTA_excel_data_map), "Liability Information function is having issue(S) . ");
		
	}
	
	if(((String)common.MTA_excel_data_map.get("CD_Terrorism")).equals("Yes")){		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Terrorism"),"Issue while Navigating to Terrorism screen . ");
		customAssert.assertTrue(common_CCF.funcTerrorism(common.MTA_excel_data_map), "Terrorism function is having issue(S) . ");
		}
	
	if(((String)common.MTA_excel_data_map.get("CD_LegalExpenses")).equals("Yes")){		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Legal Expenses"),"Issue while Navigating to Legal Expenses screen . ");
		customAssert.assertTrue(common_CCF.funcLegalExpenses(common.MTA_excel_data_map,code,event), "Legal Expenses function is having issue(S) . ");
		}
	
	customAssert.assertTrue(common.funcNextNavigateDecesionMaker(NavigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
	Assert.assertTrue(common_VELA.funcPremiumSummary_MTA(common.MTA_excel_data_map,code,event));
	if(!TestBase.businessEvent.equalsIgnoreCase("Renewal")){
		Assert.assertTrue(common_PEN.funcStatusHandling(common.MTA_excel_data_map,code,event));
	}
	
	TestUtil.reportStatus("Test Method of MTA For - "+code, "Pass", true);
	
	}catch(Throwable t){
		
 		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
}

public void CancellationFlow(String code,String event) throws ErrorInTestMethod{
	
	String testName = (String)common.CAN_excel_data_map.get("Automation Key");
	try{
		common_VELA.cancellationProcess(code,event);	
		
	}catch (ErrorInTestMethod e) {
		System.out.println("Error in New Business test method for Cancellation > "+testName);
		throw e;
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
}

public void RenewalFlow(String code,String event){
	String testName = (String)common.Renewal_excel_data_map.get("Automation Key");
	common.currentRunningFlow = "Renewal";
	common_CCD.isMTARewindStarted = true;
	try{
		
		customAssert.assertTrue(common.StingrayLogin("PEN"),"Unable to login.");
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""),"");
		customAssert.assertTrue(common_CCJ.renewalSearchPolicyNEW(common.Renewal_excel_data_map), "Policy Search function is having issue(S) . ");
		
		if(!common_HHAZ.isAssignedToUW){ // This variable is initialized in common_CCJ.renewalSearchPolicyNEW function
			customAssert.assertTrue(common.funcButtonSelection("Assign Underwriter"));
			customAssert.assertTrue(common_SPI.funcAssignPolicyToUW());
		}
		
		customAssert.assertTrue(common.funcButtonSelection("Send to Underwriter"));
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""),"");
		customAssert.assertTrue(common.funcSearchPolicy_Renewal(common.Renewal_excel_data_map), "Policy Search function is having issue(S) . ");
		customAssert.assertTrue(common.funcVerifyPolicyStatus_Renewal(common.Renewal_excel_data_map,CommonFunction.product,CommonFunction.businessEvent,"Renewal Submitted"), "Verify Policy Status (Renewal Submitted) function is having issue(S) . ");
		
		customAssert.assertTrue(common_POG.funcPolicyDetails(common.Renewal_excel_data_map), "Policy Details function having issue .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Previous Claims"),"Issue while Navigating to Previous Claims  . ");
		customAssert.assertTrue(common_CCF.funcPreviousClaims(common.Renewal_excel_data_map), "Previous claim function is having issue(S) .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(common.funcCovers(common.Renewal_excel_data_map), "Select covers function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Specified Perils"),"Issue while Navigating to Specified Perils  . ");
		customAssert.assertTrue(common.funcSpecifiedPerils(common.Renewal_excel_data_map), "Select covers function is having issue(S) . ");
		
		if(((String)common.Renewal_excel_data_map.get("CD_MaterialDamage")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Insured Properties"),"Issue while Navigating to Insured Properties  . ");
			customAssert.assertTrue(common_POG.funcInsuredProperties(common.Renewal_excel_data_map), "Insured Property function is having issue(S) . ");
		}
		
		if(((String)common.Renewal_excel_data_map.get("CD_Liability")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Employers Liability"),"Issue while Navigating to Employers Liability  . ");
			customAssert.assertTrue(common_CCF.funcEmployersLiability(common.Renewal_excel_data_map), "Employers Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","ELD Information"),"Issue while Navigating to ELD Information  . ");
			customAssert.assertTrue(common_CCF.funcELDInformation(common.Renewal_excel_data_map), "ELD Information function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability"),"Issue while Navigating to Property Owners Liability  . ");
			customAssert.assertTrue(common_POG.funcPropertyOwnersLiability(common.Renewal_excel_data_map), "Public Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability Information"),"Issue while Navigating to Products Liability  . ");
			customAssert.assertTrue(common_POG.funcLiabilityInformation(common.Renewal_excel_data_map), "Liability Information function is having issue(S) . ");
			}
		if(((String)common.Renewal_excel_data_map.get("CD_Terrorism")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Terrorism"),"Issue while Navigating to Terrorism screen . ");
			customAssert.assertTrue(common_CCF.funcTerrorism(common.Renewal_excel_data_map), "Terrorism function is having issue(S) . ");
			}
		if(((String)common.Renewal_excel_data_map.get("CD_LegalExpenses")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Legal Expenses"),"Issue while Navigating to Legal Expenses screen . ");
			customAssert.assertTrue(common_CCF.funcLegalExpenses(common.Renewal_excel_data_map,code,event), "Legal Expenses function is having issue(S) . ");
		}
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common_VELA.funcPremiumSummary(common.Renewal_excel_data_map,code,event), "Premium Summary function is having issue(S) . ");
		
		customAssert.assertTrue(common_PEN.funcStatusHandling(common.Renewal_excel_data_map,code,event));
		
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
	}
	
}

@SuppressWarnings("unused")
public boolean transactionSummary(String fileName,String testName,String event,String code){
	Boolean retvalue = true;  
	try{
		Map<Object,Object> data_map = null;

		switch (common.currentRunningFlow) {
		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Requote":
			data_map = common.Requote_excel_data_map;
			break;
		case "CAN":
			data_map = common.CAN_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;
		}
		customAssert.assertTrue(common.funcMenuSelection("Navigate", "Transaction Summary"), "Navigation problem to Transaction Summary page .");

		Assert.assertEquals(k.getText("Page_Header"),"Transaction Summary", "Not on Transaction Summary Page.");

		String tableXpath= "//*[@id='table0']/tbody";
		String Recipient = null, covername= null, exit = "";
		int columnNumber=0, count =0;
		String ActualDueDate , ExpecteTransactionDate , ActualTransationDate;

		WebElement table = driver.findElement(By.xpath(tableXpath));
		List<WebElement> list = table.findElements(By.tagName("tr"));

		outer:
			for(int i=1;i<list.size();i++){
				String trasacSummaryType = driver.findElement(By.xpath(tableXpath+"/tr["+i+"]/td[1]")).getText();
				double Total =0.00;
				String ExpecteDueDate = "";
				switch (trasacSummaryType) {
				case "New Business" :

					TestUtil.reportStatus("Verification Started on Transaction Summary page "+trasacSummaryType+" . ", "PASS", false);
					ActualDueDate = driver.findElement(By.xpath(tableXpath+"/tr["+i+"]/td[4]")).getText();
					ExpecteDueDate = common.getLastDayOfMonth((String)data_map.get("QuoteDate"), 1);
					// Verification of Due Date
					if(ActualDueDate.equalsIgnoreCase(ExpecteDueDate)){
						String tMsg="Actual Due Date : <b>[  "+ActualDueDate+"  ]</b> has been matched with Expected Due Date : <b>[  "+ExpecteDueDate+"  ]</b>";
						TestUtil.reportStatus(tMsg, "Pass", false);
					}
					else{
						String tMsg="Actual Due Date : <b>[  "+ActualDueDate+"  ]</b> does not matche with Expected Due Date : <b>[   "+ExpecteDueDate+"  ]</b>";
						TestUtil.reportStatus(tMsg, "Fail", false);
					}

					//Verification of Transaction Date
					ActualTransationDate = driver.findElement(By.xpath(tableXpath+"/tr["+i+"]/td[3]")).getText();
					ExpecteTransactionDate = (String)data_map.get("QuoteDate");
					if(ActualTransationDate.equalsIgnoreCase(ExpecteTransactionDate)){
						String tMsg="Actual Transaction Date : <b>[  "+ActualTransationDate+"  ]</b> has been matched with Expected Transaction Date : <b>[  "+ExpecteTransactionDate+"  ]</b>";
						TestUtil.reportStatus(tMsg, "Pass", false);
					}
					else{
						String tMsg="Actual Transaction Date : <b>[  "+ActualTransationDate+"  ]</b> does not matche with Expected Transaction Date : <b>[  "+ExpecteTransactionDate+"  ]</b>";
						TestUtil.reportStatus(tMsg, "Fail", false);
					}
					break;
				case "Endorsement":
					TestUtil.reportStatus("Verification Started on Transaction Summary page "+trasacSummaryType+" . ", "PASS", false);
					ActualDueDate = driver.findElement(By.xpath(tableXpath+"/tr["+i+"]/td[4]")).getText();

					ExpecteTransactionDate = "";

					if(((String)data_map.get("PS_PaymentWarrantyRules")).equals("Yes")){
						ExpecteDueDate = (String)data_map.get("PS_PaymentWarrantyDueDate");
					}else{

						switch(TestBase.businessEvent) {
						case "MTA":

							if(((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes") && ((String)common.MTA_excel_data_map.get("MTA_Status")).equalsIgnoreCase("Endorsement Rewind")) {
								ExpecteDueDate = common.getLastDayOfMonth((String)common.NB_excel_data_map.get("EffectiveDate"), 1);
								ExpecteTransactionDate = (String)common.Rewind_excel_data_map.get("QuoteDate");
							}else {
								ExpecteDueDate = common.getLastDayOfMonth((String)common.MTA_excel_data_map.get("MTA_EffectiveDate"), 1);
								ExpecteTransactionDate = (String)data_map.get("QuoteDate");
							}
							break;
						case "Rewind":
							if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes")) {
								ExpecteDueDate = common.getLastDayOfMonth((String)common.NB_excel_data_map.get("EffectiveDate"), 1);
							}
							ExpecteTransactionDate = (String)common.Rewind_excel_data_map.get("QuoteDate");
							break;
						case "Renewal":
							if(common.currentRunningFlow.equalsIgnoreCase("MTA")){
								ExpecteDueDate = common.getLastDayOfMonth((String)common.MTA_excel_data_map.get("MTA_EffectiveDate"), 1);
								ExpecteTransactionDate = (String)data_map.get("QuoteDate");
							}else{
								ExpecteDueDate = common.getLastDayOfMonth((String)data_map.get("QuoteDate"), 1);
								ExpecteTransactionDate = (String)data_map.get("QuoteDate");
							}
							
							break;
						}   						

					}   					
					if(ActualDueDate.equalsIgnoreCase(ExpecteDueDate)){
						String tMsg="Actual Due Date : <b>[  "+ActualDueDate+"  ]</b> has been matched with Expected Due Date : <b>[  "+ExpecteDueDate+"  ]</b>";
						TestUtil.reportStatus(tMsg, "Pass", false);
					}
					else{
						String tMsg="Actual Due Date : <b>[  "+ActualDueDate+"  ]</b> does not matche with Expected Due Date : <b>[   "+ExpecteDueDate+"  ]</b>";
						TestUtil.reportStatus(tMsg, "Fail", false);
					}

					ActualTransationDate = driver.findElement(By.xpath(tableXpath+"/tr["+i+"]/td[3]")).getText();

					if(ActualTransationDate.equalsIgnoreCase(ExpecteTransactionDate)){
						String tMsg="Actual Transaction Date : <b>[  "+ActualTransationDate+"  ]</b> has been matched with Expected Transaction Date : <b>[  "+ExpecteTransactionDate+"  ]</b>";
						TestUtil.reportStatus(tMsg, "Pass", false);
					}
					else{
						String tMsg="Actual Transaction Date : <b>[  "+ActualTransationDate+"  ]</b> does not matche with Expected Transaction Date : <b>[  "+ExpecteTransactionDate+"  ]</b>";
						TestUtil.reportStatus(tMsg, "Fail", false);
					}
					break;
				case "Cancel" : //MTA

					TestUtil.reportStatus("Verification Started on Transaction Summary page for Cancellation . ", "PASS", false);
					ActualDueDate = driver.findElement(By.xpath(tableXpath+"/tr["+i+"]/td[4]")).getText();

					if(((String)common.NB_excel_data_map.get("PS_PaymentWarrantyRules")).equals("Yes") && !common.currentRunningFlow.equalsIgnoreCase("MTA")){
						ExpecteDueDate = (String)common.NB_excel_data_map.get("PS_PaymentWarrantyDueDate");
					}else{
						ExpecteDueDate = common.getLastDayOfMonth((String)common.CAN_excel_data_map.get("CP_CancellationDate"), 1);
					}

					if(ActualDueDate.equalsIgnoreCase(ExpecteDueDate)){
						String tMsg="Actual Due Date : <b> [ "+ActualDueDate+" ] </b> has been matched with Expected Due Date : <b> [ "+ExpecteDueDate+" ] </b>";
						TestUtil.reportStatus(tMsg, "Pass", false);
					}
					else{
						String tMsg="Actual Due Date : <b> [ "+ActualDueDate+" ] </b> does not matche with Expected Due Date : <b> [ "+ExpecteDueDate+" ] </b>";
						TestUtil.reportStatus(tMsg, "Fail", false);
					}
					ActualTransationDate = driver.findElement(By.xpath(tableXpath+"/tr["+i+"]/td[3]")).getText();
					ExpecteTransactionDate = (String)common.NB_excel_data_map.get("QuoteDate");
					if(ActualTransationDate.equalsIgnoreCase(ExpecteTransactionDate)){
						String tMsg="Actual Transaction Date : <b> [ "+ActualTransationDate+" ] </b> has been matched with Expected Transaction Date : <b> [ "+ExpecteTransactionDate+" ] </b>";
						TestUtil.reportStatus(tMsg, "Pass", false);
					}
					else{
						String tMsg="Actual Transaction Date : <b> [ "+ActualTransationDate+" ] </b> does not matche with Expected Transaction Date : <b> [ "+ExpecteTransactionDate+" ] </b>";
						TestUtil.reportStatus(tMsg, "Fail", false);
					}
					break;
				}
				
				
				for(int rowNumber=i;!exit.equalsIgnoreCase("Total");rowNumber++){
					String transactSumVal = driver.findElement(By.xpath(tableXpath+"/tr["+rowNumber+"]/td[1]")).getText();
					exit = driver.findElement(By.xpath(tableXpath+"/tr["+rowNumber+"]/td[2]")).getText();

					if(exit.equalsIgnoreCase("Total")){
						i=rowNumber;
						String actualTotal = driver.findElement(By.xpath(tableXpath+"/tr["+rowNumber+"]/td[4]")).getText();  
						CommonFunction.compareValues(Double.parseDouble(actualTotal), Double.parseDouble(common.roundedOff(Double.toString(Total))), "Transaction Summary Total");
						break outer;
					}

					if(transactSumVal.equalsIgnoreCase("")){
						Recipient= driver.findElement(By.xpath(tableXpath+"/tr["+rowNumber+"]/td[3]")).getText();
						covername = driver.findElement(By.xpath(tableXpath+"/tr["+rowNumber+"]/td[6]")).getText();
						columnNumber=8;
					}else{
						Recipient= driver.findElement(By.xpath(tableXpath+"/tr["+rowNumber+"]/td[6]")).getText();
						covername = driver.findElement(By.xpath(tableXpath+"/tr["+rowNumber+"]/td[9]")).getText();
						columnNumber=11;
					}
				 if(covername.equalsIgnoreCase("Terrorism")){
						double Terrorism = TerrorismTransSummary(data_map,rowNumber,columnNumber);
						count++;
						if(count==3 && !TestBase.product.equals("POH") && !TestBase.product.equals("CCL")){
							//isFlatTerrorism= true;
						}else if(count==3){
							//isFlatTerrorism= true;
						}
						Total = Total + Terrorism;
					}

				else if(covername.isEmpty()){
						double general = OtherCalculationTransSummary(data_map,rowNumber,columnNumber);	
						Total = Total + general;
					}	

				}
			}
	}catch(Throwable t) {
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		TestUtil.reportFunctionFailed("Failed in "+methodName+" function");     k.reportErr("Failed in "+methodName+" function", t);
		Assert.fail("Failed in Transaction Summary \n", t);
		return false;
	}

	TestUtil.reportStatus("Verification Completed successful on Transaction Summary page . ", "Info", false);

	return retvalue;

}

public double TerrorismTransSummary(Map<Object, Object> map_data, int rownum, int columnum){
	try{

		double terrorPremium=0.00, terrorIPT =0.00, terrorGrossPremium =0.00;
		double PPC = 0.00;
		double[] expectdvalue = null;
		String tabelXpath= "//*[@id='table0']/tbody";		
		String recipient = driver.findElement(By.xpath(tabelXpath+"/tr["+rownum+"]/td["+(columnum-5)+"]")).getText();
		double[] actualvalues = getActualValues(tabelXpath, rownum, columnum);
		boolean isFlatTerrorism = false;
		if(!isFlatTerrorism){
			expectdvalue = getPremiumsAndIPT("Terrorism");
		}else{
			expectdvalue = getPremiumsAndIPT("Terrorism_FP");
		}
		terrorPremium = expectdvalue[1];
		terrorIPT = expectdvalue[2];
		terrorGrossPremium = expectdvalue[0];
		if(TestBase.businessEvent.equalsIgnoreCase("Renewal")){
		  PPC = PenPropertyConsortiumCalculation(common.Renewal_excel_data_map, terrorGrossPremium);	
	    }else{
		  PPC = PenPropertyConsortiumCalculation(common.NB_excel_data_map, terrorGrossPremium);
		}
		double[] values = TerrorismCalculation(recipient,terrorPremium, terrorIPT, PPC);
		CommonFunction.compareValues(values[0],actualvalues[0], "Terrorism "+recipient+" Amount ");
		CommonFunction.compareValues(values[1], actualvalues[1], "Terrorism "+recipient+" IPT ");
		double terrorDue=values[0] + values[1];
		CommonFunction.compareValues(terrorDue, actualvalues[2], "Terrorism "+recipient+" Due ");
		return  terrorDue;

	}catch(Throwable t) {
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		TestUtil.reportFunctionFailed("Failed in "+methodName+" function");    
		k.reportErr("Failed in "+methodName+" function", t);
		t.printStackTrace();
		Assert.fail("Failed in Calculate Terrorisam ammount.  \n", t);
		return 0;
	}

}
public double PenPropertyConsortiumCalculation(Map<Object, Object> map_data, double grossPremiumAmt )
{
	try{
	double PPCsplitrate = Double.parseDouble(((String)map_data.get("TS_PenPropertyConsortiumSplitRate")))/100;
 
	double PPC = grossPremiumAmt * PPCsplitrate;

	return PPC;
	
}catch(Throwable t) {
	String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
	TestUtil.reportFunctionFailed("Failed in "+methodName+" function"); 
	t.printStackTrace();
	k.reportErr("Failed in "+methodName+" function", t);
	Assert.fail("Failed in Calculate Pen Property Consortium method.  \n", t);
	return 0.00;
}
	
}

public double[] TerrorismCalculation(String recipient,double premiumAmt, double ipt, double PPCAmt){
	try{

		double[] splitandCommissionRates= getSplitandCommissionRates(recipient);
		double Premium =  (premiumAmt - PPCAmt) * splitandCommissionRates[0];
		double IPT = ipt * splitandCommissionRates[0];
		return new double[] {Premium, IPT, PPCAmt};			
	}catch(Throwable t) {
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		t.printStackTrace();
		TestUtil.reportFunctionFailed("Failed in "+methodName+" function");    
		k.reportErr("Failed in "+methodName+" function", t);
		Assert.fail("Failed in Calculate Terrorisam ammount according to Split.  \n", t);
		return new double[] {0, 0};
	}
}
public double[] getSplitandCommissionRates(String recipient){
	try{
		Map<Object,Object> data_map = null;

		switch (common.currentRunningFlow) {
		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Requote":
			data_map = common.Requote_excel_data_map;
			break;
		case "CAN":
			data_map = common.CAN_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;
		}
		double splitRate = 0.00;
		if(recipient.contains("Legal & General")){recipient = "LegalandGeneral";}
		if(recipient.contains("Ergo")){recipient = "Ergo";}
		
		try{
			splitRate = Double.parseDouble((String)data_map.get("TS_"+recipient+"SplitRate"))/100;	
		}catch(NullPointerException npe){
			splitRate = 0;
		}
		return new double[] {splitRate};
	}catch(Throwable t){
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		TestUtil.reportFunctionFailed("Failed in "+methodName+" function");    
		t.printStackTrace();
		k.reportErr("Failed in "+methodName+" function", t);
		Assert.fail("Failed in Get Split Rate.  \n", t);
		return new double[] {0, 0};
	}
}
	public double[] getPremiumsAndIPT(String covername){
	try{
		Map<Object,Object> data_map = null;

		switch (common.currentRunningFlow) {
		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Requote":
			data_map = common.Requote_excel_data_map;
			break;
		case "CAN":
			data_map = common.CAN_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;
		}
		double NetNetPremium = 0.00, GrossPremium =0.00, PenComm = 0.00, NetPremium = 0.00, GrossTax =0.00, BrokerComm =0.00;
		switch(TestBase.businessEvent){
		case "NB":
			if(((String)data_map.get("PS_Duration")).equals("365")){
				NetNetPremium = Double.parseDouble((String)data_map.get("PS_"+covername+"_NetNetPremium"));
				GrossPremium =  Double.parseDouble((String)data_map.get("PS_"+covername+"_GP"));
				NetPremium =  Double.parseDouble((String)data_map.get("PS_"+covername+"_NetPremium"));
				GrossTax =  Double.parseDouble((String)data_map.get("PS_"+covername+"_GT"));
				PenComm = Double.parseDouble((String)data_map.get("PS_"+covername+"_PenComm"));
				BrokerComm = Double.parseDouble((String)data_map.get("PS_"+covername+"_BrokerComm"));
			}else{
				covername = common_PEN.getCoverName(covername);
				GrossPremium = common_VELA.transaction_Premium_Values.get(covername).get("Gross Premium");
				NetPremium = common_VELA.transaction_Premium_Values.get(covername).get("Net Premium");
				GrossTax = common_VELA.transaction_Premium_Values.get(covername).get("Insurance Tax");
				PenComm =  common_VELA.transaction_Premium_Values.get(covername).get("Pen Comm %");
				BrokerComm = common_VELA.transaction_Premium_Values.get(covername).get("Broker Comm %");
				NetNetPremium = common_VELA.transaction_Premium_Values.get(covername).get("Net Net Premium");
			}

			break;
		case "MTA":
			double GrossFP=0, NetFP =0;
			if(common.currentRunningFlow.equals("NB")){
				if(((String)data_map.get("PS_Duration")).equals("365")){
					NetNetPremium = Double.parseDouble((String)data_map.get("PS_"+covername+"_NetNetPremium"));
					GrossPremium =  Double.parseDouble((String)data_map.get("PS_"+covername+"_GP"));
					NetPremium =  Double.parseDouble((String)data_map.get("PS_"+covername+"_NetPremium"));
					GrossTax =  Double.parseDouble((String)data_map.get("PS_"+covername+"_GT"));
					PenComm = Double.parseDouble((String)data_map.get("PS_"+covername+"_PenComm"));
					BrokerComm = Double.parseDouble((String)data_map.get("PS_"+covername+"_BrokerComm"));
				}else{
					covername = common_PEN.getCoverName(covername);
					GrossPremium = common_VELA.transaction_Premium_Values.get(covername).get("Gross Premium");
					NetPremium = common_VELA.transaction_Premium_Values.get(covername).get("Net Premium");
					GrossTax = common_VELA.transaction_Premium_Values.get(covername).get("Insurance Tax");
					PenComm =  common_VELA.transaction_Premium_Values.get(covername).get("Pen Comm %");
					BrokerComm = common_VELA.transaction_Premium_Values.get(covername).get("Broker Comm %");
					NetNetPremium = common_VELA.transaction_Premium_Values.get(covername).get("Net Net Premium");
				}
			}else if(common.currentRunningFlow.equals("MTA") || common.currentRunningFlow.equals("Rewind")){
				covername = common_PEN.getCoverName(covername);
				try{
					/*GrossPremium =  common.transaction_Details_Premium_Values.get(covername).get("Gross Premium (GBP)");
					NetPremium =  common.transaction_Details_Premium_Values.get(covername).get("Net Premium (GBP)");
					GrossTax =  common.transaction_Details_Premium_Values.get(covername).get("Gross IPT (GBP)");
					PenComm = common.transaction_Details_Premium_Values.get(covername).get("Com. Rate (%)");*/
					GrossPremium =	common.transaction_Details_Premium_Values.get(covername).get("Gross Premium");
					NetPremium = common.transaction_Details_Premium_Values.get(covername).get("Net Premium");
					GrossTax = common.transaction_Details_Premium_Values.get(covername).get("Insurance Tax");
					PenComm =  common.transaction_Details_Premium_Values.get(covername).get("Pen Comm");
					BrokerComm = common.transaction_Details_Premium_Values.get(covername).get("Broker Commission");
					NetNetPremium = common.transaction_Details_Premium_Values.get(covername).get("Net Net Premium");
					try{
						GrossFP = common.transaction_Details_Premium_Values.get(covername+"_FP").get("Gross Premium");
						NetFP = common.transaction_Details_Premium_Values.get(covername+"_FP").get("Net Premium");
						GrossPremium = GrossPremium + GrossFP;
						NetPremium = NetPremium + NetFP;
					}catch(NullPointerException npe){
						GrossFP =0.00;
						NetFP=0;
					}
				}catch(NullPointerException npe){
					return new double[] {0, 0, 0, 0};
				}
			}
			break;
		case "CAN":
			if(common.currentRunningFlow.equals("NB")){
				NetNetPremium = Double.parseDouble((String)data_map.get("PS_"+covername+"_NetNetPremium"));
				GrossPremium =  Double.parseDouble((String)data_map.get("PS_"+covername+"_GP"));
				NetPremium =  Double.parseDouble((String)data_map.get("PS_"+covername+"_NetPremium"));
				GrossTax =  Double.parseDouble((String)data_map.get("PS_"+covername+"_GT"));
				PenComm = Double.parseDouble((String)data_map.get("PS_"+covername+"_PenComm"));
				BrokerComm = Double.parseDouble((String)data_map.get("PS_"+covername+"_BrokerComm"));
				
			}else if(common.currentRunningFlow.equals("CAN")){
				covername = common_PEN.getCoverName(covername);
				NetNetPremium = -common_COB.CAN_COB_ReturnP_Values_Map.get(covername).get("Net Net Premium");
				GrossPremium = -common_COB.CAN_COB_ReturnP_Values_Map.get(covername).get("Gross Premium");
				NetPremium = -common_COB.CAN_COB_ReturnP_Values_Map.get(covername).get("Net Premium");
				GrossTax = -common_COB.CAN_COB_ReturnP_Values_Map.get(covername).get("Insurance Tax");
				PenComm = -common_COB.CAN_COB_ReturnP_Values_Map.get(covername).get("Pen Comm");
				BrokerComm = -common_COB.CAN_COB_ReturnP_Values_Map.get(covername).get("Broker Commission");		
			}
			break;
		case "Rewind":
			NetNetPremium = Double.parseDouble((String)data_map.get("PS_"+covername+"_NetNetPremium"));
			GrossPremium =  Double.parseDouble((String)data_map.get("PS_"+covername+"_GP"));
			NetPremium =  Double.parseDouble((String)data_map.get("PS_"+covername+"_NetPremium"));
			GrossTax =  Double.parseDouble((String)data_map.get("PS_"+covername+"_GT"));
			PenComm = Double.parseDouble((String)data_map.get("PS_"+covername+"_PenComm"));
			BrokerComm = Double.parseDouble((String)data_map.get("PS_"+covername+"_BrokerComm"));
			break;
		case "Renewal":
			if(common.currentRunningFlow.equals("Renewal") || (TestUtil.businessEvent.equalsIgnoreCase("Renewal") && common.currentRunningFlow.equalsIgnoreCase("Rewind"))){
				NetNetPremium = Double.parseDouble((String)data_map.get("PS_"+covername+"_NetNetPremium"));
				GrossPremium =  Double.parseDouble((String)data_map.get("PS_"+covername+"_GP"));
				NetPremium =  Double.parseDouble((String)data_map.get("PS_"+covername+"_NetPremium"));
				GrossTax =  Double.parseDouble((String)data_map.get("PS_"+covername+"_GT"));
				PenComm = Double.parseDouble((String)data_map.get("PS_"+covername+"_PenComm"));
				BrokerComm = Double.parseDouble((String)data_map.get("PS_"+covername+"_BrokerComm"));
			}else if(common.currentRunningFlow.equals("MTA") || common.currentRunningFlow.equals("Rewind")){
				covername = common_PEN.getCoverName(covername);
				try{
					GrossPremium =  common.transaction_Details_Premium_Values.get(covername).get("Gross Premium (GBP)");
					NetPremium =  common.transaction_Details_Premium_Values.get(covername).get("Net Premium (GBP)");
					GrossTax =  common.transaction_Details_Premium_Values.get(covername).get("Gross IPT (GBP)");
					PenComm = common.transaction_Details_Premium_Values.get(covername).get("Com. Rate (%)");
				}catch(Exception e){
					GrossPremium =  0.0;
					NetPremium =  0.0;
					GrossTax =  0.0;
					PenComm = 0.0;
					BrokerComm = 0.0;
				}
				
			}
			break;
		}

		return new double[] {GrossPremium, NetNetPremium, GrossTax, PenComm, BrokerComm};


	}catch(Throwable t) {
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		TestUtil.reportFunctionFailed("Failed in "+methodName+" function");   
		t.printStackTrace();
		k.reportErr("Failed in "+methodName+" function", t);
		Assert.fail("Failed in Getting Premium and IPT value of Transaction Summary for "+covername+".  \n", t);
		return new double[] {0, 0, 0, 0};
	}
}

public double[] getActualValues(String tableXpath,int rowNum, int columnNum){
	try{
		double actualIPT =0;
		double actualPremium = Double.parseDouble(driver.findElement(By.xpath(tableXpath+"/tr["+rowNum+"]/td["+columnNum+"]")).getText());
		double actualDue = Double.parseDouble(driver.findElement(By.xpath(tableXpath+"/tr["+rowNum+"]/td["+(columnNum+2)+"]")).getText());
		try{
			actualIPT = Double.parseDouble(driver.findElement(By.xpath(tableXpath+"/tr["+rowNum+"]/td["+(columnNum+1)+"]")).getText());
		}
		catch(NumberFormatException npe){
			actualIPT = 0.00;
		}
		return new double[] {actualPremium, actualIPT, actualDue};	
	}catch(Throwable t) {
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		TestUtil.reportFunctionFailed("Failed in "+methodName+" function");   
		t.printStackTrace();
		k.reportErr("Failed in "+methodName+" function", t);
		Assert.fail("Failed in Getting Actual value of Transaction Summary for "+rowNum+" row.  \n", t);
		return new double[] {0, 0, 0};
	}

}
public double[] getSplitandCommissionRatesForPPC(String recipient){
	try{
		Map<Object,Object> data_map = null;

		switch (common.currentRunningFlow) {
		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Requote":
			data_map = common.Requote_excel_data_map;
			break;
		case "CAN":
			data_map = common.CAN_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;
		}
		double splitRate = 0.00;
		if(recipient.contains("Pen Property Consortium")){recipient = "PenPropertyConsortium";}		
		try{
			splitRate = Double.parseDouble((String)data_map.get("TS_"+recipient+"SplitRate"))/100;	
		}catch(NullPointerException npe){
			splitRate = 0;
		}
		return new double[] {splitRate};
	}catch(Throwable t){
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		TestUtil.reportFunctionFailed("Failed in "+methodName+" function");    
		t.printStackTrace();
		k.reportErr("Failed in "+methodName+" function", t);
		Assert.fail("Failed in Get Split Rate.  \n", t);
		return new double[] {0};
	}
}
public double OtherCalculationTransSummary(Map<Object, Object> map_data, int rownum, int columnum){

	String tabelXpath= "//*[@id='table0']/tbody";

	double generalAmount = 0.0;
	try{
		String recipient = driver.findElement(By.xpath(tabelXpath+"/tr["+rownum+"]/td["+(columnum-5)+"]")).getText();
		String account = driver.findElement(By.xpath(tabelXpath+"/tr["+rownum+"]/td["+(columnum-4)+"]")).getText();			

		if(account.equalsIgnoreCase("L040")||account.equalsIgnoreCase("AL946") && TestBase.product.equalsIgnoreCase("POI")){
		generalAmount = calculateCarrierComm(recipient,rownum,columnum);
		}
	 if(account.equalsIgnoreCase("PPC_Z001") && TestBase.product.equalsIgnoreCase("POI")){	
		 generalAmount = totalPenPropertyConsortiumCalculation(map_data,recipient,rownum,columnum);
		}else if((recipient.equalsIgnoreCase("Brokerage Account")) && account.equalsIgnoreCase("Z001")){	
			generalAmount = calculateBrokeageAmount(map_data,recipient,rownum,columnum);
		}else if((recipient.equalsIgnoreCase("PENFEE")) && account.equalsIgnoreCase("Z001")){	
			generalAmount = calculatePenFeeAmount(map_data,recipient,rownum,columnum);
		}
		return generalAmount;
	}catch(Throwable t) {
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		TestUtil.reportFunctionFailed("Failed in "+methodName+" function");     k.reportErr("Failed in "+methodName+" function", t);
		Assert.fail("Failed in Other premiums calculation on Transaction summary page. \n", t);
		return 0;
	}

}
public double calculateCarrierComm(String recipient,int rowNumber, int columnNumber){
	try{
		String tableXpath= "//*[@id='table0']/tbody";
		double expectedDue = 0.00;
		double PPC = 0.00;
		double[] actualvalues = getActualValues(tableXpath, rowNumber, columnNumber);
		double[] calculationValues = getCalculationValues(recipient);
		double generalNetNetPremium = calculationValues[0];
		double generalIPT = calculationValues[1];
		double generalGrossPremium = calculationValues[2];
		
		String Account = driver.findElement(By.xpath(tableXpath+"/tr["+rowNumber+"]/td["+(columnNumber-4)+"]")).getText();
		if(Account.equalsIgnoreCase("Z906")){
			double[] splitRate = getSplitandCommissionRates(recipient);
			expectedDue = generalGrossPremium * (splitRate[1]/100);
			CommonFunction.compareValues(expectedDue, actualvalues[0], recipient+" Premium");
			CommonFunction.compareValues(expectedDue, actualvalues[2], recipient+" Due Amount");
		}else{
			if(TestBase.businessEvent.equalsIgnoreCase("Renewal")){
				  PPC = PenPropertyConsortiumCalculation(common.Renewal_excel_data_map, generalGrossPremium);	
		   }else{
			     PPC = PenPropertyConsortiumCalculation(common.NB_excel_data_map, generalGrossPremium);
		   }
			double[] expectedvalues = OtherCalculation(recipient,generalNetNetPremium, generalIPT, PPC);
			CommonFunction.compareValues(expectedvalues[0], actualvalues[0], recipient+" Premium");
			CommonFunction.compareValues(expectedvalues[1], actualvalues[1], recipient+" IPT");
			expectedDue = expectedvalues[0] + expectedvalues[1];
			CommonFunction.compareValues(expectedDue ,actualvalues[2], recipient+" Due Amount");
		}
		return expectedDue;

	}catch(Throwable t) {
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		TestUtil.reportFunctionFailed("Failed in "+methodName+" function");
		t.printStackTrace();
		k.reportErr("Failed in "+methodName+" function", t);
		Assert.fail("Failed in Calculate General preimum for genral covers \n", t);
		return 0;
	}
}
public double[] getCalculationValues(String recipient){
	try{
		Map<Object,Object> data_map = null;

		switch (common.currentRunningFlow) {
		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Requote":
			data_map = common.Requote_excel_data_map;
			break;
		case "CAN":
			data_map = common.CAN_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;
		}
		
		double MDPremium =0.00, MDIPT = 0.00, MDNetNetPremium = 0.00;
		double FP_Premium =0.00, FP_IPT = 0.00, FP_NetNetPremium = 0.00;
		double LRIPremium =0.00, LRIIPT = 0.00, LRINetNetPremium = 0.00;
		double ELPremium =  0.00,ELIPT =0.00, ELNetNetPremium = 0.00;
		double POLPremium = 0.00, POLIPT = 0.00, POLNetNetPremium = 0.00;
		double totalGrossPremium =0.00, totalGrossIPT =0.00, totalNetNetPremium = 0.00, PPC = 0.0;
		
	double [] calculationvalues =null;
		
		if(TestBase.businessEvent.equalsIgnoreCase("Rewind") || common.currentRunningFlow.equals("NB") || common.currentRunningFlow.equals("Renewal") || (TestUtil.businessEvent.equalsIgnoreCase("Renewal") && common.currentRunningFlow.equalsIgnoreCase("Rewind"))){
			//Iterator CoverName = (Iterator) common.CoversDetails_data_list.iterator();
			common.CoversDetails_data_list.get(0);
			for (String SectionCover : common.CoversDetails_data_list){

				if(SectionCover.equalsIgnoreCase("MaterialDamage")){
					if(!((String)data_map.get("PS_Duration")).equalsIgnoreCase("365")) {
						MDPremium =  common_VELA.transaction_Premium_Values.get("Material Damage").get("Gross Premium");
						MDIPT = common_VELA.transaction_Premium_Values.get("Material Damage").get("Insurance Tax");
						MDNetNetPremium = common_VELA.transaction_Premium_Values.get("Material Damage").get("Net Net Premium");
			       }else {
						MDPremium =  Double.parseDouble((String)data_map.get("PS_MaterialDamage_GP"));
						MDIPT = Double.parseDouble((String)data_map.get("PS_MaterialDamage_GT"));
						MDNetNetPremium = Double.parseDouble((String)data_map.get("PS_MaterialDamage_NetNetPremium"));
					}
					
				}
				if(SectionCover.equalsIgnoreCase("LossOfRentalIncome")){
					if(!((String)data_map.get("PS_Duration")).equalsIgnoreCase("365")) {
						LRIPremium =  common_VELA.transaction_Premium_Values.get("Loss Of Rental Income").get("Gross Premium");
						LRIIPT = common_VELA.transaction_Premium_Values.get("Loss Of Rental Income").get("Insurance Tax");
						LRINetNetPremium = common_VELA.transaction_Premium_Values.get("Loss Of Rental Income").get("Net Net Premium");
					}else {
						LRIPremium =  Double.parseDouble((String)data_map.get("PS_LossOfRentalIncome_GP"));
						LRIIPT = Double.parseDouble((String)data_map.get("PS_LossOfRentalIncome_GT"));
						LRINetNetPremium = Double.parseDouble((String)data_map.get("PS_LossOfRentalIncome_NetNetPremium"));	
					}
					}
				switch(SectionCover){
				case "Liability":
					calculationvalues = getPremiumsAndIPT("EmployersLiability");
					ELPremium = calculationvalues[0];
					ELNetNetPremium = calculationvalues[1];
					ELIPT = calculationvalues[2];
		

						calculationvalues = getPremiumsAndIPT("PropertyOwnersLiability");
						POLPremium = calculationvalues[0];
						POLNetNetPremium = calculationvalues[1];
						POLIPT = calculationvalues[2];	
			}
			if(!((String)data_map.get("PS_Duration")).equalsIgnoreCase("365")) {
				totalGrossPremium =  common_VELA.transaction_Premium_Values.get("Totals").get("Gross Premium");
				totalGrossIPT = common_VELA.transaction_Premium_Values.get("Totals").get("Insurance Tax");
				totalNetNetPremium = common_VELA.transaction_Premium_Values.get("Totals").get("Net Net Premium");
			}else {
				totalGrossPremium = Double.parseDouble((String)data_map.get("PS_Total_GP"));
				totalGrossIPT = Double.parseDouble((String)data_map.get("PS_Total_GT"));
				totalNetNetPremium = Double.parseDouble((String)data_map.get("PS_NetPremiumTotal"));
			}
				}
		}else if((TestBase.businessEvent.equals("MTA") && common.currentRunningFlow.equals("Rewind")) || common.currentRunningFlow.equalsIgnoreCase("MTA")){
			//Iterator collectiveDataIT = 
			
			java.util.Iterator collectiveDataIT = common.transaction_Details_Premium_Values.entrySet().iterator();
			while(collectiveDataIT.hasNext()){
				Map.Entry collectiveDataMapValue = (Map.Entry)collectiveDataIT.next();
				String SectionCover = collectiveDataMapValue.getKey().toString();
			
			if(SectionCover.equalsIgnoreCase("Material Damage")){
					MDPremium =  common.transaction_Details_Premium_Values.get("Material Damage").get("Gross Premium");
					MDIPT = common.transaction_Details_Premium_Values.get("Material Damage").get("Insurance Tax");
					MDNetNetPremium = common.transaction_Details_Premium_Values.get("Material Damage").get("Net Net Premium");
		       }
			try{
			if(SectionCover.contains("_FP")){
				FP_Premium =  common.transaction_Details_Premium_Values.get(SectionCover).get("Gross Premium");
				FP_IPT = common.transaction_Details_Premium_Values.get(SectionCover).get("Insurance Tax");
				FP_NetNetPremium = common.transaction_Details_Premium_Values.get(SectionCover).get("Net Net Premium");
	         }
		  }catch(NullPointerException npe){
				return new double[] {0, 0, 0};
			}
			if(SectionCover.equalsIgnoreCase("Loss Of Rental Income")){
					LRIPremium =  common.transaction_Details_Premium_Values.get("Loss Of Rental Income").get("Gross Premium");
					LRIIPT = common.transaction_Details_Premium_Values.get("Loss Of Rental Income").get("Insurance Tax");
					LRINetNetPremium = common.transaction_Details_Premium_Values.get("Loss Of Rental Income").get("Net Net Premium");
			}
			if(SectionCover.equalsIgnoreCase("Employers Liability")){
				ELPremium =  common.transaction_Details_Premium_Values.get("Employers Liability").get("Gross Premium");
				ELIPT = common.transaction_Details_Premium_Values.get("Employers Liability").get("Insurance Tax");
				ELNetNetPremium = common.transaction_Details_Premium_Values.get("Employers Liability").get("Net Net Premium");
			}
			if(SectionCover.equalsIgnoreCase("Property Owners Liability")){
				POLPremium =  common.transaction_Details_Premium_Values.get("Property Owners Liability").get("Gross Premium");
				POLIPT = common.transaction_Details_Premium_Values.get("Property Owners Liability").get("Insurance Tax");
				POLNetNetPremium = common.transaction_Details_Premium_Values.get("Property Owners Liability").get("Net Net Premium");
			}
			
			totalGrossPremium =  common.transaction_Details_Premium_Values.get("Totals").get("Gross Premium");
			totalGrossIPT = common.transaction_Details_Premium_Values.get("Totals").get("Insurance Tax");
			totalNetNetPremium = common.transaction_Details_Premium_Values.get("Totals").get("Net Net Premium");
	       }
           }else if(common.currentRunningFlow.equals("CAN")){
				Iterator collectiveDataIT = common_COB.CAN_COB_ReturnP_Values_Map.entrySet().iterator();
				while(collectiveDataIT.hasNext()){
					Map.Entry collectiveDataMapValue = (Map.Entry)collectiveDataIT.next();
					String SectionCover = collectiveDataMapValue.getKey().toString();
					if(SectionCover.equalsIgnoreCase("Material Damage")){
						MDPremium =  -common_COB.CAN_COB_ReturnP_Values_Map.get("Material Damage").get("Gross Premium");
						MDIPT = -common_COB.CAN_COB_ReturnP_Values_Map.get("Material Damage").get("Insurance Tax");
						MDNetNetPremium = -common_COB.CAN_COB_ReturnP_Values_Map.get("Material Damage").get("Net Net Premium");
			       }
					if(SectionCover.equalsIgnoreCase("Loss Of Rental Income")){
						LRIPremium =  -common_COB.CAN_COB_ReturnP_Values_Map.get("Loss Of Rental Income").get("Gross Premium");
						LRIIPT = -common_COB.CAN_COB_ReturnP_Values_Map.get("Loss Of Rental Income").get("Insurance Tax");
						LRINetNetPremium = -common_COB.CAN_COB_ReturnP_Values_Map.get("Loss Of Rental Income").get("Net Net Premium");
				   }
					if(SectionCover.equalsIgnoreCase("Employers Liability")){
						ELPremium =  -common_COB.CAN_COB_ReturnP_Values_Map.get("Employers Liability").get("Gross Premium");
						ELIPT = -common_COB.CAN_COB_ReturnP_Values_Map.get("Employers Liability").get("Insurance Tax");
						ELNetNetPremium = -common_COB.CAN_COB_ReturnP_Values_Map.get("Employers Liability").get("Net Net Premium");
					}
					if(SectionCover.equalsIgnoreCase("Property Owners Liability")){
						POLPremium =  -common_COB.CAN_COB_ReturnP_Values_Map.get("Property Owners Liability").get("Gross Premium");
						POLIPT = -common_COB.CAN_COB_ReturnP_Values_Map.get("Property Owners Liability").get("Insurance Tax");
						POLNetNetPremium = -common_COB.CAN_COB_ReturnP_Values_Map.get("Property Owners Liability").get("Net Net Premium");
					}
				
				totalGrossPremium = -common_COB.CAN_COB_ReturnP_Values_Map.get("Totals").get("Gross Premium");
				totalGrossIPT = -common_COB.CAN_COB_ReturnP_Values_Map.get("Totals").get("Insurance Tax");
				totalNetNetPremium = -common_COB.CAN_COB_ReturnP_Values_Map.get("Totals").get("Net Net Premium");
              }
              }
				
		
		double generalNetNetPremium = (MDNetNetPremium) + (LRINetNetPremium) + (ELNetNetPremium) + (POLNetNetPremium) + (FP_NetNetPremium) ;
		double generalIPT = (MDIPT) + (LRIIPT) + (ELIPT) + (POLIPT) + (FP_IPT);
		double generalGrossPremium = (MDPremium) + (LRIPremium) + (ELPremium) + (POLPremium) + (FP_Premium);
		
        return new double[] {generalNetNetPremium, generalIPT, generalGrossPremium};
		
	}catch(Throwable t) {
String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
TestUtil.reportFunctionFailed("Failed in "+methodName+" function");   
t.printStackTrace();
k.reportErr("Failed in "+methodName+" function", t);
Assert.fail("Failed in Getting Calculation value of Transaction Summary for "+common.currentRunningFlow+" Flow.  \n", t);
return new double[] {0, 0, 0};

}
}
public double[] OtherCalculation(String recipient,double premiumAmt, double ipt, double PPCAmt){
	try{

		double[] splitandCommissionRates= getSplitandCommissionRates(recipient);
		//double commRate = (100 - splitandCommissionRates[1])/100;
		double Premium =  (premiumAmt - PPCAmt) * splitandCommissionRates[0];
		double IPT =  ipt * splitandCommissionRates[0];

		return new double[] {Premium, IPT, PPCAmt};
	}catch(Throwable t) {
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		TestUtil.reportFunctionFailed("Failed in "+methodName+" function"); 
		t.printStackTrace();
		k.reportErr("Failed in "+methodName+" function", t);
		Assert.fail("Failed in Calculate Other ammount according "+recipient+" to Split.  \n", t);
		return new double[] {0, 0};
	}
}

public double totalPenPropertyConsortiumCalculation(Map<Object, Object> map_data, String recipient,int rowNumber, int columnNumber)
{
	try{
		Map<Object,Object> data_map = null;
		switch (common.currentRunningFlow) {
		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Requote":
			data_map = common.Requote_excel_data_map;
			break;
		case "CAN":
			data_map = common.NB_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;
		}
		
	String tableXpath= "//*[@id='table0']/tbody";
	double expectedDue = 0.00, grossPremiumAmt = 0.00;
	double[] actualvalues = getActualValues(tableXpath, rowNumber, columnNumber);
	if(TestBase.businessEvent.equalsIgnoreCase("Rewind") || common.currentRunningFlow.equals("NB") || common.currentRunningFlow.equals("Renewal") || (TestUtil.businessEvent.equalsIgnoreCase("Renewal") && common.currentRunningFlow.equalsIgnoreCase("Rewind"))){
	  if(!((String)data_map.get("PS_Duration")).equalsIgnoreCase("365")){
	 grossPremiumAmt = common_VELA.transaction_Premium_Values.get("Totals").get("Gross Premium");
     }else{
	 grossPremiumAmt = Double.parseDouble(((String)map_data.get("PS_Total_GP")));
	 }
	}else if((TestBase.businessEvent.equals("MTA") && common.currentRunningFlow.equals("Rewind")) || common.currentRunningFlow.equalsIgnoreCase("MTA")){
		grossPremiumAmt = common.transaction_Details_Premium_Values.get("Totals").get("Gross Premium");
	}else if((TestBase.businessEvent.equals("CAN"))){
		grossPremiumAmt = -common_COB.CAN_COB_ReturnP_Values_Map.get("Totals").get("Gross Premium");
	}
	
	double PPCsplitrate = Double.parseDouble(((String)map_data.get("TS_PenPropertyConsortiumSplitRate")))/100;
 
	double PPC = grossPremiumAmt * PPCsplitrate;
	expectedDue = PPC;
	CommonFunction.compareValues(PPC ,actualvalues[0], recipient+" Amount");
	CommonFunction.compareValues(expectedDue ,actualvalues[2], recipient+" Due Amount");
	return PPC;
	}catch(Throwable t) {
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
		TestUtil.reportFunctionFailed("Failed in "+methodName+" function"); 
		t.printStackTrace();
		k.reportErr("Failed in "+methodName+" function", t);
		Assert.fail("Failed in Calculate Pen Property Consortium method.  \n", t);
	}
	return 0.00;
	}

public double calculateBrokeageAmount(Map<Object, Object> map_data, String recipient,int rowNumber, int columnNumber){
	try{
		Map<Object,Object> data_map = null;
		switch (common.currentRunningFlow) {
		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Requote":
			data_map = common.Requote_excel_data_map;
			break;
		case "CAN":
			data_map = common.NB_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;
		}
		double BrokerCommission = 0;
		
		String tableXpath= "//*[@id='table0']/tbody";
		double expectedDue = 0.00;
		
		double[] actualvalues = getActualValues(tableXpath, rowNumber, columnNumber);
	
		if(TestBase.businessEvent.equalsIgnoreCase("Rewind") || common.currentRunningFlow.equals("NB") || common.currentRunningFlow.equals("Renewal") || (TestUtil.businessEvent.equalsIgnoreCase("Renewal") && common.currentRunningFlow.equalsIgnoreCase("Rewind"))){	
		  if(!((String)data_map.get("PS_Duration")).equalsIgnoreCase("365")){
			BrokerCommission = common_VELA.transaction_Premium_Values.get("Totals").get("Pen Comm");
		 }else{
		BrokerCommission = Double.parseDouble(((String)map_data.get("PS_PenCommTotal")));
		 }
	   }else if((TestBase.businessEvent.equals("MTA") && common.currentRunningFlow.equals("Rewind")) || common.currentRunningFlow.equalsIgnoreCase("MTA")){
		   BrokerCommission = common.transaction_Details_Premium_Values.get("Totals").get("Pen Comm");
	   }else if((TestBase.businessEvent.equals("CAN"))){
		   BrokerCommission = -common_COB.CAN_COB_ReturnP_Values_Map.get("Totals").get("Pen Comm");
	   }
		expectedDue = BrokerCommission;
		
		CommonFunction.compareValues(BrokerCommission ,actualvalues[0], recipient+" Amount");
		CommonFunction.compareValues(expectedDue ,actualvalues[2], recipient+" Due Amount");
	return BrokerCommission;
	
}catch(Throwable t) {
	String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
	TestUtil.reportFunctionFailed("Failed in "+methodName+" function");     k.reportErr("Failed in "+methodName+" function", t);
	Assert.fail("Failed in Calculate Brokerage ammout. \n", t);
	return 0;
}

}
public double calculatePenFeeAmount(Map<Object, Object> map_data, String recipient,int rowNumber, int columnNumber){
	try{
		Map<Object,Object> data_map = null;
		switch (common.currentRunningFlow) {
		case "NB":
			data_map = common.NB_excel_data_map;
			break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
			break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
			break;
		case "Requote":
			data_map = common.Requote_excel_data_map;
			break;
		case "CAN":
			data_map = common.NB_excel_data_map;
			break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
			break;
		}
		double PenFee = 0;
		
		String tableXpath= "//*[@id='table0']/tbody";
		double expectedDue = 0.00;
		
		double[] actualvalues = getActualValues(tableXpath, rowNumber, columnNumber);
		PenFee = Double.parseDouble(((String)map_data.get("PS_TotalAdminFee")));
		expectedDue = PenFee;
		
		CommonFunction.compareValues(PenFee ,actualvalues[0], recipient+" Amount");
		CommonFunction.compareValues(expectedDue ,actualvalues[2], recipient+" Due Amount");
	return PenFee;
	
}catch(Throwable t) {
	String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
	TestUtil.reportFunctionFailed("Failed in "+methodName+" function");     k.reportErr("Failed in "+methodName+" function", t);
	Assert.fail("Failed in Calculate Brokerage ammout. \n", t);
	return 0;
}

}

public boolean Verify_premiumSummaryTable_MTA(){
	err_count = 0;
	Map<Object,Object> data_map = null;
	
	switch(common.currentRunningFlow){
		case "NB":
			data_map = common.NB_excel_data_map;
		break;
		case "Renewal":
			data_map = common.Renewal_excel_data_map;
		break;
		case "MTA":
			data_map = common.MTA_excel_data_map;
		break;
		case "Rewind":
			data_map = common.Rewind_excel_data_map;
		break;
	}
	
	final Map<String,String> locator_map = new HashMap<>();
	locator_map.put("PenComm", "pencom");
	locator_map.put("NetPremium", "nprem");
	locator_map.put("BrokerComm", "comm");
	locator_map.put("GrossPremium", "gprem");
	locator_map.put("InsuranceTax", "gipt");
	
	final Map<String,String> section_map = new HashMap<>();
	
		section_map.put("MaterialDamage", "md11");	
		section_map.put("LossOfRentalIncome", "bi5");
		section_map.put("EmployersLiability","el6");
		section_map.put("PropertyOwnersLiability","pl5");
		section_map.put("Terrorism", "tr7");
		section_map.put("LegalExpenses","lg4");
	
	double exp_Premium = 0.0;
	
	try{
	
		String annualTble_xpath = "html/body/div[3]/form/div/table[2]";
		int trans_tble_Rows = driver.findElements(By.xpath(annualTble_xpath+"/tbody/tr")).size();
		String sectionName = null;
		
		boolean PremiumFlag = false;
		if(common.currentRunningFlow.equalsIgnoreCase("Rewind") && TestBase.businessEvent.equalsIgnoreCase("MTA")){
			
			if(!PremiumFlag)
			for(int i =1;i<=trans_tble_Rows-1;i++){
				String annualTblXpath2 = "/tbody/tr["+i+"]/td[1]";
				sectionName = driver.findElement(By.xpath(annualTble_xpath+annualTblXpath2)).getText().replaceAll(" ", "");
				if(sectionName.contains("Totals"))
					sectionName = "Total";
				if(((String)common.NB_excel_data_map.get("CD_"+sectionName)).equals("No") && ((String)common.MTA_excel_data_map.get("CD_"+sectionName)).equals("No") && ((String)common.Rewind_excel_data_map.get("CD_"+sectionName)).equals("Yes")){

				
					customAssert.assertTrue(common_POG.funcAddInput_PremiumSummary(sectionName,section_map.get(sectionName),data_map),"Add Premium Summary Input function having issues for "+sectionName);
					if(((String)data_map.get("PS_TaxExempt")).equalsIgnoreCase("Yes")){
						data_map.put("PS_"+sectionName+"_IPT", "0.0");
					}else{
						data_map.put("PS_"+sectionName+"_IPT", "12");
					}
				}else{
					String cover_name = section_map.get(sectionName);
					String PencCommXpath , BrokerCommXpath;
					if(cover_name.contains("md")){
						PencCommXpath = "//*[@name='md_ccc"+"_penr']";
						BrokerCommXpath ="//*[@name='md_ccc"+"_comr']" ;
					}else if(cover_name.contains("el") && !TestBase.product.equals("COB") && !TestBase.product.equals("COA")){
						PencCommXpath = "//*[@name='el_ccc"+"_penr']";
						BrokerCommXpath ="//*[@name='el_ccc"+"_comr']" ;
					}else{
						 PencCommXpath = "//*[contains(@id,'"+cover_name+"_penr')]";
						 BrokerCommXpath ="//*[contains(@id,'"+cover_name+"_comr')]";
					}
					
					String penComm = driver.findElement(By.xpath(PencCommXpath)).getAttribute("value");
					String BrokComm = driver.findElement(By.xpath(BrokerCommXpath)).getAttribute("value");
					common.Rewind_excel_data_map.put("PS_"+sectionName+"_PenComm_rate", penComm);
					common.Rewind_excel_data_map.put("PS_"+sectionName+"_BrokerComm_rate", BrokComm);
				}
			}
		}
		if(common.currentRunningFlow.equalsIgnoreCase("Rewind") && TestBase.businessEvent.equalsIgnoreCase("Rewind")){
			
			if(!PremiumFlag)
				for(int i =1;i<=trans_tble_Rows-1;i++){
					String annualTblXpath2 = "/tbody/tr["+i+"]/td[1]";
					sectionName = driver.findElement(By.xpath(annualTble_xpath+annualTblXpath2)).getText().replaceAll(" ", "");
					if(sectionName.contains("Totals"))
						sectionName = "Total";
				
					customAssert.assertTrue(common_POG.funcAddInput_PremiumSummary(sectionName,section_map.get(sectionName),data_map),"Add Premium Summary Input function having issues for "+sectionName);
					if(((String)data_map.get("PS_TaxExempt")).equalsIgnoreCase("Yes")){
						data_map.put("PS_"+sectionName+"_IPT", "0.0");
					}else{
						data_map.put("PS_"+sectionName+"_IPT", "12");
					}
				}
		}
		if(common.currentRunningFlow.equalsIgnoreCase("Rewind") && TestBase.businessEvent.equalsIgnoreCase("Renewal")){
			
			if(!PremiumFlag)
				for(int i =1;i<=trans_tble_Rows-1;i++){
					String annualTblXpath2 = "/tbody/tr["+i+"]/td[1]";
					sectionName = driver.findElement(By.xpath(annualTble_xpath+annualTblXpath2)).getText().replaceAll(" ", "");
					if(sectionName.contains("Totals"))
						sectionName = "Total";
				
					customAssert.assertTrue(common_POG.funcAddInput_PremiumSummary(sectionName,section_map.get(sectionName),data_map),"Add Premium Summary Input function having issues for "+sectionName);
					if(((String)data_map.get("PS_TaxExempt")).equalsIgnoreCase("Yes")){
						data_map.put("PS_"+sectionName+"_IPT", "0.0");
					}else{
						data_map.put("PS_"+sectionName+"_IPT", "12");
					}
				}
		}
		
		if(common.currentRunningFlow.equalsIgnoreCase("Requote")){
			
			if(!PremiumFlag)
			for(int i =1;i<=trans_tble_Rows-1;i++){
				String annualTblXpath2 = "/tbody/tr["+i+"]/td[1]";
				sectionName = driver.findElement(By.xpath(annualTble_xpath+annualTblXpath2)).getText().replaceAll(" ", "");
				if(sectionName.contains("Totals"))
					sectionName = "Total";
				if(sectionName.contains("BusinesssInterruption"))
					sectionName = "BusinessInterruption";
			
				customAssert.assertTrue(common_POG.funcAddInput_PremiumSummary(sectionName,section_map.get(sectionName),data_map),"Add Premium Summary Input function having issues for "+sectionName);
				if(((String)data_map.get("PS_TaxExempt")).equalsIgnoreCase("Yes")){
					data_map.put("PS_"+sectionName+"_IPT", "0.0");
				}else{
					data_map.put("PS_"+sectionName+"_IPT", "12");
				}
			}
		
		}
		
		if(common.currentRunningFlow.equalsIgnoreCase("MTA")){
			
			//if(!PremiumFlag)
			for(int i =1;i<=trans_tble_Rows-1;i++){
				String annualTblXpath2 = "/tbody/tr["+i+"]/td[1]";
				sectionName = driver.findElement(By.xpath(annualTble_xpath+annualTblXpath2)).getText().replaceAll(" ", "");
				
/*				switch(sectionName){
				
				
				}*/
				if(sectionName.contains("Totals"))
					sectionName = "Total";
				if(CommonFunction.businessEvent.equalsIgnoreCase("Renewal") && common.currentRunningFlow.equalsIgnoreCase("MTA")){
					String CoverName = null;
					if(sectionName.contains("Liability")){
						CoverName = "Liability";
				   }else{
					   CoverName = sectionName;
				   }
					if(((String)common.Renewal_excel_data_map.get("CD_"+CoverName)).equals("No") && ((String)common.MTA_excel_data_map.get("CD_"+CoverName)).equals("Yes")){

						customAssert.assertTrue(common_VELA.funcAddInput_PremiumSummary(sectionName,section_map.get(sectionName),data_map),"Add Premium Summary Input function having issues for "+sectionName);
						if(((String)data_map.get("PS_TaxExempt")).equalsIgnoreCase("Yes")){
							data_map.put("PS_"+sectionName+"_IPT", "0.0");
						}else{
							data_map.put("PS_"+sectionName+"_IPT", "12");
						}
					}else{
						String cover_name = section_map.get(sectionName);
						String PencCommXpath , BrokerCommXpath;
						
					    PencCommXpath = "//*[contains(@id,'"+cover_name+"_penr')]";
						BrokerCommXpath ="//*[contains(@id,'"+cover_name+"_comr')]";
						
						
						String penComm = driver.findElement(By.xpath(PencCommXpath)).getAttribute("value");
						String BrokComm = driver.findElement(By.xpath(BrokerCommXpath)).getAttribute("value");
						common.MTA_excel_data_map.put("PS_"+sectionName+"_PenComm_rate", penComm);
						common.MTA_excel_data_map.put("PS_"+sectionName+"_BrokerComm_rate", BrokComm);
								
						
					}
				}else{
					String coveractname = sectionName;
						if(sectionName.contains("Liability")){
							coveractname="Liability";
						}
						
			//		if(((String)common.NB_excel_data_map.get("CD_"+sectionName)).equals("No") && ((String)common.MTA_excel_data_map.get("CD_"+sectionName)).equals("Yes")){
					if(((String)common.NB_excel_data_map.get("CD_"+coveractname)).equals("No") && ((String)common.MTA_excel_data_map.get("CD_"+coveractname)).equals("Yes")){
						/*if(sectionName.contains("Liability")){
							sectionName="EmployersLiability";
						}*/
						customAssert.assertTrue(common_VELA.funcAddInput_PremiumSummary(sectionName,section_map.get(sectionName),data_map),"Add Premium Summary Input function having issues for "+sectionName);
						if(((String)data_map.get("PS_TaxExempt")).equalsIgnoreCase("Yes")){
							data_map.put("PS_"+sectionName+"_IPT", "0.0");
						}else{
							data_map.put("PS_"+sectionName+"_IPT", "12");
						}
					}else{
						String cover_name = section_map.get(sectionName);
						String PencCommXpath , BrokerCommXpath;
						PencCommXpath = "//*[contains(@id,'"+cover_name+"_penr')]";
					    BrokerCommXpath ="//*[contains(@id,'"+cover_name+"_comr')]";
					    String penComm = driver.findElement(By.xpath(PencCommXpath)).getAttribute("value");
						String BrokComm = driver.findElement(By.xpath(BrokerCommXpath)).getAttribute("value");
						common.MTA_excel_data_map.put("PS_"+sectionName+"_PenComm_rate", penComm);
						common.MTA_excel_data_map.put("PS_"+sectionName+"_BrokerComm_rate", BrokComm);
					}
				}
					}
				}
		PremiumFlag = true;		
		customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Premium Summary .");
		
			
		for(int i =1;i<=trans_tble_Rows-1;i++){
			String annualTblXpath2 = "/tbody/tr["+i+"]/td[1]";
			sectionName = driver.findElement(By.xpath(annualTble_xpath+annualTblXpath2)).getText().replaceAll(" ", "");
			
		 if(sectionName.contains("Totals")){
				sectionName = "Total";
			}
			err_count = err_count + func_PremiumSummaryCalculation_MTA(section_map.get(sectionName),sectionName,locator_map);
			exp_Premium = exp_Premium + Double.parseDouble((String)data_map.get("PS_"+sectionName+"_TotalPremium"));
		}
		
		double Total_GP = 00.00;
		double Total_GT = 00.00;
		double Total_NetNetPemium = 00.00,Total_PenComm = 00.00,Total_NetPremium=00.00 ,Total_BrokComm = 00.00;
		
		for(int i =1;i<=trans_tble_Rows-1;i++){
			String annualTblXpath2 = "/tbody/tr["+i+"]/td[1]";
			sectionName = driver.findElement(By.xpath(annualTble_xpath+annualTblXpath2)).getText().replaceAll(" ", "");
			if(sectionName.contains("AbbeyLegalExpenses")){
				sectionName = "LegalExpenses";}
			else if(sectionName.contains("Totals")){
				sectionName = "Total";}
			Total_GP = Total_GP + Double.parseDouble((String)data_map.get("PS_"+sectionName+"_GP"));
			Total_GT = Total_GT + Double.parseDouble((String)data_map.get("PS_"+sectionName+"_GT"));
			Total_NetNetPemium = Total_NetNetPemium + Double.parseDouble((String)data_map.get("PS_"+sectionName+"_NetNetPremium"));
			Total_PenComm = Total_PenComm + Double.parseDouble((String)data_map.get("PS_"+sectionName+"_PenComm"));
			Total_BrokComm = Total_BrokComm + Double.parseDouble((String)data_map.get("PS_"+sectionName+"_BrokerComm"));
			Total_NetPremium = Total_NetPremium + Double.parseDouble((String)data_map.get("PS_"+sectionName+"_NetPremium"));
		}
		
		data_map.put("PS_Total_GT", f.format(Total_GT));
		data_map.put("PS_Total_GP", f.format(Total_GP));
		data_map.put("PS_TotalPremium", f.format(exp_Premium));
		data_map.put("PS_NetNetPemiumTotal", f.format(Total_NetNetPemium));
		data_map.put("PS_BrokerCommissionTotal", f.format(Total_BrokComm));
		data_map.put("PS_PenCommTotal", f.format(Total_PenComm));
		

		String exp_Total_Premium = common.roundedOff(Double.toString(exp_Premium));
		String act_Total_Premium = k.getAttribute("SPI_Total_Premium", "value");
		act_Total_Premium = act_Total_Premium.replaceAll(",", "");
		
		TestUtil.reportStatus("---------------Total Annual Premium-----------------","Info",false);
		
		CommonFunction.compareValues(Double.parseDouble(exp_Total_Premium),Double.parseDouble(act_Total_Premium),"Total Premium.");
		
		return true;
	  
	}catch(Throwable t){
				return false;
		
	}
}
public int func_PremiumSummaryCalculation_MTA(String code,String covername,Map<String,String> premium_loc) {
	
	Map<Object,Object> map_data = null;
	Map<Object,Object> Tax_map_data = new HashMap<>();
	
	String event=null;
	
	
	switch(TestBase.businessEvent){
		case "NB":
			map_data = common.NB_excel_data_map;
			Tax_map_data = common.NB_excel_data_map;
		break;
		case "Rewind":
			if(common.currentRunningFlow.equals("NB")){
				map_data = common.NB_excel_data_map;
				Tax_map_data = common.NB_excel_data_map;
				event = "NB";
				}
			else{
				map_data = common.Rewind_excel_data_map;
				Tax_map_data = common.Rewind_excel_data_map;
				event = "Rewind";
				
			}
		break;
		case "Requote":
			if(common.currentRunningFlow.equals("NB")){
				map_data = common.NB_excel_data_map;
				Tax_map_data = common.NB_excel_data_map;
				event = "NB";
				}
			else{
				map_data = common.Requote_excel_data_map;
				Tax_map_data = common.Requote_excel_data_map;
				event = "Requote";
				
			}
		break;
		case "MTA":
			if(common.currentRunningFlow.equals("NB")){
				map_data = common.NB_excel_data_map;
				Tax_map_data = common.NB_excel_data_map;
				event = "NB";
				}
			else if(CommonFunction.businessEvent.equalsIgnoreCase("MTA") && common.currentRunningFlow.equalsIgnoreCase("Rewind")){
				map_data = common.Rewind_excel_data_map;
				Tax_map_data = common.MTA_excel_data_map;
				event = "Rewind";
			}
			else{
				map_data = common.MTA_excel_data_map;
				Tax_map_data = common.NB_excel_data_map;
				event = "MTA";
				
			}
			break;	
		case "Renewal":
			if(CommonFunction.businessEvent.equalsIgnoreCase("Renewal") && common.currentRunningFlow.equalsIgnoreCase("MTA")){
				map_data = common.MTA_excel_data_map;
				Tax_map_data = common.Renewal_excel_data_map;
				event = "MTA";
			}else if(CommonFunction.businessEvent.equalsIgnoreCase("Renewal") && common.currentRunningFlow.equalsIgnoreCase("Rewind")){
				map_data = common.Rewind_excel_data_map;
				Tax_map_data = common.Renewal_excel_data_map;
				event = "Rewind";
			}else{
				map_data = common.Renewal_excel_data_map;
				Tax_map_data = common.Renewal_excel_data_map;
				event = "Renewal";
			}
			break;
		case "CAN":
			if(common.currentRunningFlow.equals("NB")){
				map_data = common.NB_excel_data_map;
				Tax_map_data = common.NB_excel_data_map;
				event = "NB";
				}
			else{
				map_data = common.CAN_excel_data_map;
				Tax_map_data = common.CAN_excel_data_map;
				event = "CAN";				
			}
			
		break;	
		
	}
		double NetNet_Premium = Double.parseDouble((String)map_data.get("PS_"+covername+"_NetNetPremium"));
		
	try{
			
			TestUtil.reportStatus("---------------"+covername+"-----------------","Info",false);
			//SPI Pen commission Calculation : 
			double pen_comm = ((NetNet_Premium / (1-((Double.parseDouble((String)map_data.get("PS_"+covername+"_PenComm_rate")) + Double.parseDouble((String)map_data.get("PS_"+covername+"_BrokerComm_rate")))/100)))*((Double.parseDouble((String)map_data.get("PS_"+covername+"_PenComm_rate"))/100)));
			String pc_expected = common.roundedOff(Double.toString(pen_comm));
			
			code=code+"_";
			
									
			String pc_actual = k.getAttributeByXpath("//*[contains(@id,'"+code+premium_loc.get("PenComm")+"')]", "value");
			CommonFunction.compareValues(Double.parseDouble(pc_expected),Double.parseDouble(pc_actual),"Pen Commission");
			map_data.put("PS_"+covername+"_PenComm",pc_expected);
			
			// Net Premium verification : 
			double netP = Double.parseDouble(pc_expected) + NetNet_Premium;
			String netP_expected = common.roundedOff(Double.toString(netP));
			String netP_actual = k.getAttributeByXpath("//*[contains(@id,'"+code+premium_loc.get("NetPremium")+"')]", "value");
			CommonFunction.compareValues(Double.parseDouble(netP_expected),Double.parseDouble(netP_actual),"Net Premium");
			map_data.put("PS_"+covername+"_NetPremium",netP_expected);
			
			// Broker commission Calculation : 
			double broker_comm = ((NetNet_Premium / (1-((Double.parseDouble((String)map_data.get("PS_"+covername+"_PenComm_rate")) + Double.parseDouble((String)map_data.get("PS_"+covername+"_BrokerComm_rate")))/100)))*((Double.parseDouble((String)map_data.get("PS_"+covername+"_BrokerComm_rate"))/100)));
			String bc_expected = common.roundedOff(Double.toString(broker_comm));
			String bc_actual =  k.getAttributeByXpath("//*[contains(@id,'"+code+premium_loc.get("BrokerComm")+"')]", "value");
			CommonFunction.compareValues(Double.parseDouble(bc_expected),Double.parseDouble(bc_actual),"Broker Commission");
			map_data.put("PS_"+covername+"_BrokerComm",bc_expected);
			
			// GrossPremium verification :  
			double grossP = Double.parseDouble(netP_expected) + Double.parseDouble(bc_expected);
			
			if(!covername.contains("Terrorism")){
				PremiumExcTerrDocExp = PremiumExcTerrDocExp + grossP;
			}else if(!covername.contains("Terrorism") ){
				TerPremDocExp = grossP;
			}
			
			String grossP_actual = k.getAttributeByXpath("//*[contains(@id,'"+code+premium_loc.get("GrossPremium")+"')]", "value");
			
			if(!covername.contains("Terrorism")){
				PremiumExcTerrDocAct = PremiumExcTerrDocAct + Double.parseDouble(grossP_actual);
			}else if(covername.contains("Terrorism")){
				TerPremDocAct = Double.parseDouble(grossP_actual);
			}
	
			
			CommonFunction.compareValues(grossP,Double.parseDouble(grossP_actual),"Gross Premium");
			map_data.put("PS_"+covername+"_GP",(grossP_actual));
			
			if(!common_CCD.isGrossPremiumReferralCheckDone){
				//Section minimum  Premium Referral Messages - For MD, BI, El, PL, Money , GIT
				if(!covername.contains("PersonalAccident") && !covername.contains("LegalExpenses") && !covername.contains("Terrorism")){
					customAssert.assertTrue(common_CCD.func_Check_Section_Minimum_Gross_Premium(covername,grossP), "Error while verifying section minimum gross premium . ");
				}
			}
			
			if(common.currentRunningFlow.equals("MTA")){
				if(((String)map_data.get("PS_TaxExempt")).equalsIgnoreCase("Yes"))
					Tax_map_data.put("PS_"+covername+"_IPT", "0.0");
				
			}
				
			if(!common.currentRunningFlow.equalsIgnoreCase("NB")){
				String InsuranceTax = k.getAttributeByXpath("//*[contains(@id,'"+code+premium_loc.get("InsuranceTax")+"')]", "value");
				double IPT = (Double.parseDouble(InsuranceTax) / grossP) * 100.0;
				TestUtil.WriteDataToXl(TestBase.product+"_"+event, "Premium Summary",(String)map_data.get("Automation Key"), "PS_"+covername+"_IPT", common_HHAZ.roundedOff(Double.toString(IPT)), map_data);
			}
			
			double InsuranceTax = (grossP * Double.parseDouble((String)map_data.get("PS_"+covername+"_IPT")))/100.0;
			//InsuranceTax = Double.parseDouble(common.roundedOff(Double.toString(InsuranceTax)));
			String InsuranceTax_actual = k.getAttributeByXpath("//*[contains(@id,'"+code+premium_loc.get("InsuranceTax")+"')]", "value");
			
			if(covername.contains("Terrorism")){
				InsTaxTerrDoc = Double.parseDouble(InsuranceTax_actual);
			}
			if(!covername.contains("Terrorism")){
			InsTaxDocAct = InsTaxDocAct + Double.parseDouble(InsuranceTax_actual);
			InsTaxDocExp = InsTaxDocExp + InsuranceTax;
			}
			
			CommonFunction.compareValues(InsuranceTax,Double.parseDouble(InsuranceTax_actual),"Insurance Tax");
			map_data.put("PS_"+covername+"_GT",Double.toString(InsuranceTax));
			
			//SPI Total Premium verification : 
			double Premium = grossP + InsuranceTax;
			String p_expected = common.roundedOff(Double.toString(Premium));
			
			String p_actual = common.roundedOff(k.getAttributeByXpath("//*[contains(@id,'"+code+"tot')]", "value"));
			
			TotalPremiumWithAdminDocAct = TotalPremiumWithAdminDocAct + Double.parseDouble(p_actual);
			TotalPremiumWithAdminDocExp = TotalPremiumWithAdminDocExp + Double.parseDouble(p_expected);
			
			double premium_diff = Double.parseDouble(common.roundedOff(Double.toString(Double.parseDouble(p_expected) - Double.parseDouble(p_actual))));
			
			if(premium_diff<=0.20 && premium_diff>=-0.20){
				TestUtil.reportStatus("Total Premium [<b> "+p_expected+" </b>] matches with actual total premium [<b> "+p_actual+" </b>]as expected for "+covername+" on premium summary page.", "Pass", false);
				map_data.put("PS_"+covername+"_TotalPremium", p_expected);
				return 0;
				
			}else{
				TestUtil.reportStatus("<p style='color:red'> Mismatch in Expected Premium [<b> "+p_expected+"</b>] and Actual Premium [<b> "+p_actual+"</b>] for "+code+" on premium summary page. </p>", "Fail", true);
				map_data.put("PS_"+covername+"_TotalPremium", p_expected);
				return 1;
			}
				
	}catch(Throwable t) { 
        String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
        TestUtil.reportFunctionFailed("Failed in "+methodName+" function");
        Assert.fail("Insured Properties function is having issue(S). \n", t);
        return 0;
 }
	
}
public int funcTransactionDetailsTable_Verification_MTA(String sectionName,Map<String,Map<String,Double>> transactionDetails_Premium_Values){

	Map<Object,Object> map_data = common.MTA_excel_data_map;
	
	Map<Object, Object> data_map = null;
	Map<Object,Object> Tax_map_data = new HashMap<>();
	
	double NB_NNP = 0.0;
	double MTA_NNP=0.0;
	double final_trans_NNP=0.0;
	String code=null,cover_code=null;
	int p_NB_Duration = 0,p_before_MTA_days=0 , p_MTA_Duration = 0, p_new_duration=0, prev_MTA_effectiveDays;
	Map<String,Double> trans_details_values = new HashMap<>();
	
	switch (TestBase.businessEvent) {
	case "Renewal":
		if(common.currentRunningFlow.equals("MTA")){
			Tax_map_data = common.MTA_excel_data_map;
			 data_map = common.Renewal_excel_data_map;
		}
		else
			data_map = common.Renewal_excel_data_map;
		break;
	case "MTA":
		if(common.currentRunningFlow.equalsIgnoreCase("Rewind")){
			data_map = common.Rewind_excel_data_map;
			Tax_map_data = common.MTA_excel_data_map;
			
		}else if(((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes"))
		{
				data_map = common.NB_excel_data_map;
				Tax_map_data = common.MTA_excel_data_map;
			
		}
		else
		{
			data_map = common.NB_excel_data_map;
			Tax_map_data = common.MTA_excel_data_map;
		}
		break;
	case "Rewind":
		if(common.currentRunningFlow.equalsIgnoreCase("Rewind")){
			data_map = common.Rewind_excel_data_map;
			Tax_map_data = common.MTA_excel_data_map;
			
		}else{
			data_map = common.NB_excel_data_map;
			Tax_map_data = common.NB_excel_data_map;
		}
		break;
	default:
		break;
	}
	
	
	if(Integer.parseInt((String)data_map.get("PS_Duration"))!=365)
		p_NB_Duration = 365;
	else
		p_NB_Duration = Integer.parseInt((String)data_map.get("PS_Duration"));
	
	
	p_NB_Duration = Integer.parseInt((String)data_map.get("PS_Duration"));
	
	if(TestBase.businessEvent.equals("Rewind") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes"))
	{
		p_before_MTA_days = Integer.parseInt((String)data_map.get("PS_Duration")) - Integer.parseInt(((String)Tax_map_data.get("MTA_EffectiveDays")).trim());
		p_MTA_Duration = Integer.parseInt((String)common.Rewind_excel_data_map.get("PS_Duration")) - p_before_MTA_days;
	}
	else if(TestBase.businessEvent.equals("MTA") || TestBase.businessEvent.equals("Renewal"))
	{ 
		p_before_MTA_days = Integer.parseInt((String)common.MTA_excel_data_map.get("MTA_EndorsementPeriod"));
		
		if(((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes") && !(((String)map_data.get("MTA_ExistingPolicy_Type")).trim().equalsIgnoreCase("New Business")))
		{
			p_new_duration = Integer.parseInt((String)Tax_map_data.get("PS_Duration"));
		
			prev_MTA_effectiveDays = Integer.parseInt(((String)map_data.get("MTA_EffectiveDays")).trim());
		
			p_MTA_Duration = prev_MTA_effectiveDays - p_before_MTA_days + p_new_duration - p_NB_Duration;
			
			p_before_MTA_days = p_new_duration -p_MTA_Duration;
		}
		else
		{
			p_MTA_Duration = Integer.parseInt((String)common.MTA_excel_data_map.get("PS_Duration")) - p_before_MTA_days;
			
			if(common.currentRunningFlow.equalsIgnoreCase("Rewind"))
			{
				p_NB_Duration = Integer.parseInt((String)common.NB_excel_data_map.get("PS_Duration"));
				p_MTA_Duration = Integer.parseInt((String)common.Rewind_excel_data_map.get("PS_Duration")) - p_before_MTA_days;	
			}
		}
	}
	
	
	switch(sectionName){
	
	case "Employers Liability":
		code = "EmployersLiability";
		cover_code = "Liability";
		break;
	case "Property Owners Liability":
		code = "PropertyOwnersLiability";
		cover_code = "Liability";
		break;
	case "Loss Of Rental Income":
		code = "LossOfRentalIncome";
		cover_code = "LossOfRentalIncome";
		break;
	case "Terrorism":
		code = "Terrorism";
		cover_code = "Terrorism";
		break;
		
	case "Material Damage":
		code ="MaterialDamage";
		cover_code="MaterialDamage";
		break;
			
	
	default:
			System.out.println("**Cover Name is not in Scope for POF**");
		break;
	
	}
	
	try{

		TestUtil.reportStatus("---------------"+sectionName+"-----------------","Info",false);

		if(common_CCD.isMTARewindFlow)
		{ // MTA Rewind Flow
			if(((String)common.NB_excel_data_map.get("CD_"+cover_code)).equals("Yes") && (((String)common.Rewind_excel_data_map.get("CD_"+cover_code)).equals("No")))
			{
				NB_NNP = Double.parseDouble((String)common.NB_excel_data_map.get("PS_"+code+"_NetNetPremium"));
				MTA_NNP = 0.0;
				map_data = common.NB_excel_data_map;				
			}else if(((String)common.NB_excel_data_map.get("CD_"+cover_code)).equals("No") && ((String)common.Rewind_excel_data_map.get("CD_"+cover_code)).equals("Yes"))
			{
				NB_NNP = 0.0;
				MTA_NNP = Double.parseDouble((String)common.Rewind_excel_data_map.get("PS_"+code+"_NetNetPremium"));
				map_data = common.Rewind_excel_data_map;
			}else
			{
				NB_NNP = Double.parseDouble((String)common.NB_excel_data_map.get("PS_"+code+"_NetNetPremium"));
				MTA_NNP = Double.parseDouble((String)common.Rewind_excel_data_map.get("PS_"+code+"_NetNetPremium"));
				map_data = common.Rewind_excel_data_map;
			}
			final_trans_NNP = ((NB_NNP* p_before_MTA_days / 365) + (MTA_NNP * (p_MTA_Duration) / 365) - (NB_NNP + ((NB_NNP / 365) * (p_NB_Duration - 365))));
		}	
		else
		{
			if(((String)data_map.get("CD_"+cover_code)).equals("Yes") && ((String)common.MTA_excel_data_map.get("CD_"+cover_code)).equals("No"))
			{
				NB_NNP = Double.parseDouble(((String)data_map.get("PS_"+code+"_NetNetPremium")).replaceAll(",", ""));
				MTA_NNP = 0.0;
				map_data = data_map;

			}else if(((String)data_map.get("CD_"+cover_code)).equals("No") && ((String)common.MTA_excel_data_map.get("CD_"+cover_code)).equals("Yes"))
			{
				NB_NNP = 0.0;
				MTA_NNP = Double.parseDouble(((String)common.MTA_excel_data_map.get("PS_"+code+"_NetNetPremium")).replaceAll(",", ""));
				map_data = common.MTA_excel_data_map;
			}else{
				NB_NNP = Double.parseDouble((String)data_map.get("PS_"+code+"_NetNetPremium"));
				MTA_NNP = Double.parseDouble(((String)common.MTA_excel_data_map.get("PS_"+code+"_NetNetPremium")).replaceAll(",", ""));

				map_data = common.MTA_excel_data_map;	
			}
			final_trans_NNP = ((NB_NNP* p_before_MTA_days / 365) + (MTA_NNP * (p_MTA_Duration) / 365) - (NB_NNP + ((NB_NNP / 365) * (p_NB_Duration - 365))));
		}


		if(TestBase.businessEvent.equalsIgnoreCase("Renewal")){

			if(((String)data_map.get("CD_"+cover_code)).equals("No") && ((String)common.MTA_excel_data_map.get("CD_"+cover_code)).equals("Yes"))
			{
				data_map = common.MTA_excel_data_map;
			}


			String t_NetNetP_expected = common.roundedOff(Double.toString(final_trans_NNP));
			String t_NetNetP_actual = Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Net Net Premium"));
			CommonFunction.compareValues(Double.parseDouble(t_NetNetP_expected),Double.parseDouble(t_NetNetP_actual)," Net Net Premium");
			trans_details_values.put("Net Net Premium", Double.parseDouble(t_NetNetP_expected));

			double t_pen_comm = (( Double.parseDouble(t_NetNetP_expected) / (1-((Double.parseDouble((String)data_map.get("PS_"+code+"_PenComm_rate")) + Double.parseDouble((String)data_map.get("PS_"+code+"_BrokerComm_rate")))/100)))*((Double.parseDouble((String)data_map.get("PS_"+code+"_PenComm_rate"))/100)));
			String t_pc_expected = common.roundedOff(Double.toString(t_pen_comm));
			String t_pc_actual = Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Pen Comm"));
			CommonFunction.compareValues(Double.parseDouble(t_pc_expected),Double.parseDouble(t_pc_actual)," Pen Commission");
			trans_details_values.put("Pen Comm", Double.parseDouble(t_pc_expected));


			double t_netP = Double.parseDouble(t_pc_expected) + Double.parseDouble(t_NetNetP_expected);
			String t_netP_expected = common.roundedOff(Double.toString(t_netP));
			String t_netP_actual = Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Net Premium"));
			CommonFunction.compareValues(Double.parseDouble(t_netP_expected),Double.parseDouble(t_netP_actual),"Net Premium");
			trans_details_values.put("Net Premium", Double.parseDouble(t_netP_expected));


			double t_broker_comm = ((Double.parseDouble(t_NetNetP_expected) / (1-((Double.parseDouble((String)data_map.get("PS_"+code+"_PenComm_rate")) + Double.parseDouble((String)data_map.get("PS_"+code+"_BrokerComm_rate")))/100)))*((Double.parseDouble((String)data_map.get("PS_"+code+"_BrokerComm_rate"))/100)));
			String t_bc_expected = common.roundedOff(Double.toString(t_broker_comm));
			String t_bc_actual =  Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Broker Commission"));
			CommonFunction.compareValues(Double.parseDouble(t_bc_expected),Double.parseDouble(t_bc_actual),"Broker Commission");
			trans_details_values.put("Broker Commission", Double.parseDouble(t_bc_expected));


			double t_grossP = Double.parseDouble(t_netP_expected) + Double.parseDouble(t_bc_expected);
			String t_grossP_actual = Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Gross Premium"));
			CommonFunction.compareValues(t_grossP,Double.parseDouble(t_grossP_actual)," Gross Premium");
			trans_details_values.put("Gross Premium", t_grossP);

			String nbStatus = (String)map_data.get("MTA_Status");

			if(!sectionName.contains("Terrorism")){
				AdditionalExcTerrDocAct = AdditionalExcTerrDocAct + Double.parseDouble(t_grossP_actual);
			}else{
				AdditionalTerPDocAct = Double.parseDouble(t_grossP_actual);
			}

			map_data = common.MTA_excel_data_map;

			double t_InsuranceTax = 0.0;
			if(((String)map_data.get("PS_TaxExempt")).equalsIgnoreCase("Yes") || ((String)map_data.get("PS_InsuranceTaxButton")).equalsIgnoreCase("Yes")){
				t_InsuranceTax = (t_grossP * 0.0)/100.0;
			}else{
				t_InsuranceTax = (t_grossP * Double.parseDouble((String)map_data.get("PS_"+code+"_IPT")))/100.0;
			}

			t_InsuranceTax = Double.parseDouble(common.roundedOff(Double.toString(t_InsuranceTax)));
			String t_InsuranceTax_actual = Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Insurance Tax"));
			CommonFunction.compareValues(t_InsuranceTax,Double.parseDouble(t_InsuranceTax_actual),"Insurance Tax");
			trans_details_values.put("Insurance Tax", t_InsuranceTax);



			if(sectionName.contains("Terrorism")){
				AddTaxTerrDoc = Double.parseDouble(t_InsuranceTax_actual);
				if(common_POF.isMTARewindStarted){
					rewindMTADoc_AddTaxTer = Double.parseDouble(t_InsuranceTax_actual);	
				}			
			}

			String Nb_Status = (String)map_data.get("MTA_Status");
			if(common_POF.isMTARewindStarted){

				String newCover = "";
				if(sectionName.contains("PropertyOwnersLiabilities")){
					newCover = "Liabilities-POL";
				}else if(sectionName.contains("EmployersLiability")) {
					newCover = "Liabilities-EL";
				}else{
					newCover = sectionName;
				}

				String sCover = (String)map_data.get("CD_Add_"+newCover);

				if(sCover.contains("Yes")){
					rewindDoc_InsPTax  = rewindDoc_InsPTax + Double.parseDouble(t_InsuranceTax_actual);
					if(!sectionName.contains("Terrorism")){
						rewindMTADoc_Premium   = rewindMTADoc_Premium  + Double.parseDouble(t_grossP_actual);
					}else{
						rewindMTADoc_TerP   = Double.parseDouble(t_grossP_actual);
					}
				}

			}

			AdditionalInsTaxDocAct = AdditionalInsTaxDocAct + Double.parseDouble(t_InsuranceTax_actual);

			//SPI  Transaction Total Premium verification : 
			double t_Premium = t_grossP + t_InsuranceTax;
			String t_p_expected = common.roundedOff(Double.toString(t_Premium));
			trans_details_values.put("Total Premium", Double.parseDouble(t_p_expected));

			String t_p_actual = Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Total Premium"));

			common.transaction_Details_Premium_Values.put(sectionName, trans_details_values);

			double premium_diff = Double.parseDouble(t_p_expected) - Double.parseDouble(t_p_actual);

			if(premium_diff<0.05 && premium_diff>-0.05){
				TestUtil.reportStatus("Total Premium [<b> "+t_p_expected+" </b>] matches with actual total premium [<b> "+t_p_actual+" </b>]as expected for "+sectionName+" in Transaction Details table .", "Pass", false);
				//customAssert.assertTrue(WriteDataToXl(TestBase.product+"_"+TestBase.businessEvent, "Premium Summary", testName, "PS_"+code+"_TotalPremium", p_expected,common.NB_excel_data_map),"Error while writing Total Premium for cover "+code+" to excel .");
				return 0;
			}else{
				TestUtil.reportStatus("<p style='color:red'> Mismatch in Expected Premium [<b> "+t_p_expected+"</b>] and Actual Premium [<b> "+t_p_actual+"</b>] for "+sectionName+" in Transaction Details table . </p>", "Fail", true);
				//customAssert.assertTrue(WriteDataToXl(TestBase.product+"_"+TestBase.businessEvent, "Premium Summary", testName, "PS_"+code+"_TotalPremium", p_expected,common.NB_excel_data_map),"Error while writing Total Premium for cover "+code+" to excel .");
				return 1;
			}

		}else{
			String t_NetNetP_expected = common.roundedOff(Double.toString(final_trans_NNP));
			String t_NetNetP_actual = Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Net Net Premium"));
			CommonFunction.compareValues(Double.parseDouble(t_NetNetP_expected),Double.parseDouble(t_NetNetP_actual)," Net Net Premium");
			trans_details_values.put("Net Net Premium", Double.parseDouble(t_NetNetP_expected));

			double t_pen_comm = (( Double.parseDouble(t_NetNetP_expected) / (1-((Double.parseDouble((String)map_data.get("PS_"+code+"_PenComm_rate")) + Double.parseDouble((String)map_data.get("PS_"+code+"_BrokerComm_rate")))/100)))*((Double.parseDouble((String)map_data.get("PS_"+code+"_PenComm_rate"))/100)));
			String t_pc_expected = common.roundedOff(Double.toString(t_pen_comm));
			String t_pc_actual = Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Pen Comm"));
			CommonFunction.compareValues(Double.parseDouble(t_pc_expected),Double.parseDouble(t_pc_actual)," Pen Commission");
			trans_details_values.put("Pen Comm", Double.parseDouble(t_pc_expected));


			double t_netP = Double.parseDouble(t_pc_expected) + Double.parseDouble(t_NetNetP_expected);
			String t_netP_expected = common.roundedOff(Double.toString(t_netP));
			String t_netP_actual = Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Net Premium"));
			CommonFunction.compareValues(Double.parseDouble(t_netP_expected),Double.parseDouble(t_netP_actual),"Net Premium");
			trans_details_values.put("Net Premium", Double.parseDouble(t_netP_expected));


			double t_broker_comm = ((Double.parseDouble(t_NetNetP_expected) / (1-((Double.parseDouble((String)map_data.get("PS_"+code+"_PenComm_rate")) + Double.parseDouble((String)map_data.get("PS_"+code+"_BrokerComm_rate")))/100)))*((Double.parseDouble((String)map_data.get("PS_"+code+"_BrokerComm_rate"))/100)));
			String t_bc_expected = common.roundedOff(Double.toString(t_broker_comm));
			String t_bc_actual =  Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Broker Commission"));
			CommonFunction.compareValues(Double.parseDouble(t_bc_expected),Double.parseDouble(t_bc_actual),"Broker Commission");
			trans_details_values.put("Broker Commission", Double.parseDouble(t_bc_expected));


			double t_grossP = Double.parseDouble(t_netP_expected) + Double.parseDouble(t_bc_expected);
			String t_grossP_actual = Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Gross Premium"));
			CommonFunction.compareValues(t_grossP,Double.parseDouble(t_grossP_actual)," Gross Premium");
			trans_details_values.put("Gross Premium", t_grossP);

			String Nb_Status = (String)common.MTA_excel_data_map.get("MTA_Status");
			if(Nb_Status.contains("Endorsement Rewind")){
				if(!sectionName.contains("Terrorism")){
					rewindMTADoc_Premium  =  Double.parseDouble(t_grossP_actual);
				}else{
					rewindMTADoc_TerP  = Double.parseDouble(t_grossP_actual);
				}
			}

			if(!sectionName.contains("Terrorism")){
				AdditionalExcTerrDocAct = AdditionalExcTerrDocAct + Double.parseDouble(t_grossP_actual);
			}else{
				AdditionalTerPDocAct = Double.parseDouble(t_grossP_actual);
			}
			
			Map<Object, Object> data_map_tax = null;
			//For Tax Map Logic
			switch (common.currentRunningFlow) {
			case "MTA":
				data_map_tax = common.MTA_excel_data_map;
				break;
			case "Rewind":
				data_map_tax = common.Rewind_excel_data_map;
				break;
			default:
				break;
			}
			
			String insurance_Tax_Rate = null;
				
			double t_InsuranceTax = 0.0;
				
			// Below code will decide tax rate based on policy start date
			// for 10 cent rate
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date policy_Start_date = sdf.parse((String) map_data.get(("PS_PolicyStartDate")));
			Date tax_rate_change_date = sdf.parse("01/06/2017");

			if(((String) data_map_tax.get("PS_TaxExempt")).equalsIgnoreCase("Yes")	|| ((String) data_map_tax.get("PS_InsuranceTaxButton")).equalsIgnoreCase("Yes")) {
				insurance_Tax_Rate = "0";
			}else if (policy_Start_date.before(tax_rate_change_date)) {
				if (((String) data_map.get("CD_" + cover_code)).equals("Yes")	&& ((String) common.MTA_excel_data_map.get("CD_" + cover_code)).equals("No")) {
					insurance_Tax_Rate = (String) data_map.get("PS_" + code + "_IPT");
				}else{
					insurance_Tax_Rate = (String) map_data.get("PS_" + code + "_IPT");
				}
			}else{
				insurance_Tax_Rate = (String) map_data.get("PS_" + code + "_IPT");
			}
		
			t_InsuranceTax = (t_grossP * Double.parseDouble(insurance_Tax_Rate))/100.0;
	
			t_InsuranceTax = Double.parseDouble(common.roundedOff(Double.toString(t_InsuranceTax)));
			String t_InsuranceTax_actual = Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Insurance Tax"));
			CommonFunction.compareValues(t_InsuranceTax,Double.parseDouble(t_InsuranceTax_actual),"Insurance Tax");
			trans_details_values.put("Insurance Tax", t_InsuranceTax);

			if(sectionName.contains("Terrorism")){
				if(Nb_Status.contains("Endorsement Rewind")){
					rewindMTADoc_AddTaxTer  = Double.parseDouble(t_InsuranceTax_actual);
				}

				AddTaxTerrDoc = Double.parseDouble(t_InsuranceTax_actual);

			}

			AdditionalInsTaxDocAct = AdditionalInsTaxDocAct + Double.parseDouble(t_InsuranceTax_actual);

			//SPI  Transaction Total Premium verification : 
			double t_Premium = t_grossP + t_InsuranceTax;
			String t_p_expected = common.roundedOff(Double.toString(t_Premium));
			trans_details_values.put("Total Premium", Double.parseDouble(t_p_expected));

			String t_p_actual = Double.toString(transactionDetails_Premium_Values.get(sectionName).get("Total Premium"));

			common.transaction_Details_Premium_Values.put(sectionName, trans_details_values);

			double premium_diff = Double.parseDouble(t_p_expected) - Double.parseDouble(t_p_actual);

			if(premium_diff<0.05 && premium_diff>-0.05){
				TestUtil.reportStatus("Total Premium [<b> "+t_p_expected+" </b>] matches with actual total premium [<b> "+t_p_actual+" </b>]as expected for "+sectionName+" in Transaction Details table .", "Pass", false);
				return 0;
			}else{
				TestUtil.reportStatus("<p style='color:red'> Mismatch in Expected Premium [<b> "+t_p_expected+"</b>] and Actual Premium [<b> "+t_p_actual+"</b>] for "+sectionName+" in Transaction Details table . </p>", "Fail", true);
				return 1;
			}

		}


}catch(Throwable t) {
    String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
    TestUtil.reportFunctionFailed("Failed in "+methodName+" function");
    Assert.fail("Transaction Details table verification issue.  \n", t);
    return 1;
}

	
}

public int func_FP_Entries_Transaction_Details_Verification_MTA(String sectionName,Map<String, List<Map<String, String>>> internal_data_map){

	Map<Object,Object> map_data = common.MTA_excel_data_map;
	Map<Object,Object> NB_map_data = common.NB_excel_data_map;
	Map<Object, Object> data_map = null;
	
	
	double final_fp_NNP=0.0;
	String code=null,cover_code=null;
	String flat_section=null;
	
	Map<String,Double> fp_details_values = new HashMap<>();
	
	switch (TestBase.businessEvent) {
	case "Renewal":
		data_map = common.Renewal_excel_data_map;
		break;
	case "MTA":
		data_map = common.NB_excel_data_map;
		break;
	default:
		break;
	}
	
	switch(sectionName){
	
	case "Employers Liability":
		code = "EmployersLiability";
		cover_code = "Liability";
		flat_section = sectionName;
		break;
	case "Property Owners Liability":
		code = "PropertyOwnersLiability";
		cover_code = "Liability";
		flat_section = sectionName;
		break;
	case "Loss Of Rental Income":
		code = "LossOfRentalIncome";
		cover_code = "LossOfRentalIncome";
		flat_section = sectionName;
		break;
	case "Terrorism":
		code = "Terrorism";
		cover_code = "Terrorism";
		flat_section = sectionName;
		break;		
	case "Material Damage":
		code = "MaterialDamage";
		cover_code = "MaterialDamage";
		flat_section = sectionName;
		break;	
		
		
	default:
			System.out.println("**Cover Name is not in Scope for POF**");
		break;
	
	}
	
try{
		
			TestUtil.reportStatus("---------------"+sectionName+" in Flat Premium Section-----------------","Info",false);
		
			//final_fp_NNP = Double.parseDouble(internal_data_map.get("Flat-Premiums").get(count-1).get("FP_Premium"));
		
			//final_fp_NNP = common.transaction_Details_Premium_Values.get(sectionName+"_FP").get("Net Net Premium");
			final_fp_NNP = (Double)map_data.get(flat_section+"_FP");
		
			String t_NetNetP_expected = common.roundedOff(Double.toString(final_fp_NNP));
			String t_NetNetP_actual = Double.toString(common.transaction_Details_Premium_Values.get(flat_section+"_FP").get("Net Net Premium"));
			CommonFunction.compareValues(Double.parseDouble(t_NetNetP_expected),Double.parseDouble(t_NetNetP_actual)," Net Net Premium");
			//fp_details_values.put("Net Net Premium", Double.parseDouble(t_NetNetP_expected));
			
			double t_pen_comm = (( Double.parseDouble(t_NetNetP_expected) / (1-((Double.parseDouble((String)map_data.get("PS_"+code+"_PenComm_rate")) + Double.parseDouble((String)map_data.get("PS_"+code+"_BrokerComm_rate")))/100)))*((Double.parseDouble((String)map_data.get("PS_"+code+"_PenComm_rate"))/100)));
			String t_pc_expected = common.roundedOff(Double.toString(t_pen_comm));
			String t_pc_actual = Double.toString(common.transaction_Details_Premium_Values.get(flat_section+"_FP").get("Pen Comm"));
			CommonFunction.compareValues(Double.parseDouble(t_pc_expected),Double.parseDouble(t_pc_actual)," Pen Commission");
			//fp_details_values.put("Pen Comm", Double.parseDouble(t_pc_expected));
			
			
			double t_netP = Double.parseDouble(t_pc_expected) + Double.parseDouble(t_NetNetP_expected);
			String t_netP_expected = common.roundedOff(Double.toString(t_netP));
			String t_netP_actual = Double.toString(common.transaction_Details_Premium_Values.get(flat_section+"_FP").get("Net Premium"));
			CommonFunction.compareValues(Double.parseDouble(t_netP_expected),Double.parseDouble(t_netP_actual),"Net Premium");
			//fp_details_values.put("Net Premium", Double.parseDouble(t_netP_expected));
			
			
			double t_broker_comm = ((Double.parseDouble(t_NetNetP_expected) / (1-((Double.parseDouble((String)map_data.get("PS_"+code+"_PenComm_rate")) + Double.parseDouble((String)map_data.get("PS_"+code+"_BrokerComm_rate")))/100)))*((Double.parseDouble((String)map_data.get("PS_"+code+"_BrokerComm_rate"))/100)));
			String t_bc_expected = common.roundedOff(Double.toString(t_broker_comm));
			String t_bc_actual =  Double.toString(common.transaction_Details_Premium_Values.get(flat_section+"_FP").get("Broker Commission"));
			CommonFunction.compareValues(Double.parseDouble(t_bc_expected),Double.parseDouble(t_bc_actual),"Broker Commission");
			//fp_details_values.put("Broker Commission", Double.parseDouble(t_bc_expected));
			
			
			double t_grossP = Double.parseDouble(t_netP_expected) + Double.parseDouble(t_bc_expected);
			String t_grossP_actual = Double.toString(common.transaction_Details_Premium_Values.get(flat_section+"_FP").get("Gross Premium"));
			CommonFunction.compareValues(t_grossP,Double.parseDouble(t_grossP_actual)," Gross Premium");
			//fp_details_values.put("Gross Premium", t_grossP);
			
			
			double t_InsuranceTax = (t_grossP * common.transaction_Details_Premium_Values.get(flat_section+"_FP").get("Insurance Tax Rate"))/100.0;
			
			t_InsuranceTax = Double.parseDouble(common.roundedOff(Double.toString(t_InsuranceTax)));
			String t_InsuranceTax_actual = Double.toString(common.transaction_Details_Premium_Values.get(flat_section+"_FP").get("Insurance Tax"));
			//fp_details_values.put("Insurance Tax", t_InsuranceTax);
			
			//SPI  Transaction Total Premium verification : 
			double t_Premium = t_grossP + t_InsuranceTax;
			String t_p_expected = common.roundedOff(Double.toString(t_Premium));
			//fp_details_values.put("Total Premium", Double.parseDouble(t_p_expected));
			
			String t_p_actual = Double.toString(common.transaction_Details_Premium_Values.get(flat_section+"_FP").get("Total Premium"));
			
			//common.transaction_Details_Premium_Values.put(sectionName+"_FP", fp_details_values);
			
			// Add Flat premium values  to the MTA premium values to verify on documents :
			
			String sCover = null;
			
		//	if(sectionName.contains("Property Owners Liabilities")){
		//		sCover = "Liabilities - POL";
		//	}else if(sectionName.contains("Employers Liabilities")){
		//		sCover = "Liabilities - EL";
		//	}else{
				sCover = sectionName;
		//	}
						
			if(common_VELA.FP_Covers.contains(sCover) ){
				if(!sectionName.contains("Terrorism")){
					AdditionalExcTerrDocAct  = AdditionalExcTerrDocAct  + Double.parseDouble(t_grossP_actual);
				}else{
					AdditionalTerPDocAct  = AdditionalTerPDocAct  + Double.parseDouble(t_grossP_actual);
				}

				if(sectionName.contains("Terrorism")){
					AddTaxTerrDoc  = AddTaxTerrDoc  + Double.parseDouble(t_InsuranceTax_actual);						
				}
				
				String Nb_Status = (String)map_data.get("MTA_Status");
				if(common_POF.isMTARewindStarted){
					
					String newCover = "";
					if(sectionName.contains("PropertyOwnersLiabilities")){
						newCover = "Liabilities-POL";
					}else if(sectionName.contains("EmployersLiability")) {
						newCover = "Liabilities-EL";
					}else{
						newCover = sectionName;
					}
					
					String s_Cover = (String)map_data.get("CD_Add_"+newCover);
					
					if(sCover.contains("Yes")){
						rewindDoc_InsPTax  = rewindDoc_InsPTax + Double.parseDouble(t_InsuranceTax_actual);
						if(!sectionName.contains("Terrorism")){
							rewindMTADoc_Premium   = rewindMTADoc_Premium  + Double.parseDouble(t_grossP_actual);
						}else{
							rewindMTADoc_TerP   = Double.parseDouble(t_grossP_actual);
						}
					}
					
					
					
				}				
			}
			AdditionalInsTaxDocAct = AdditionalInsTaxDocAct + Double.parseDouble(t_InsuranceTax_actual);
			
			double premium_diff = Double.parseDouble(t_p_expected) - Double.parseDouble(t_p_actual);
			
			if(premium_diff<0.05 && premium_diff>-0.05){
				TestUtil.reportStatus("Total Premium [<b> "+t_p_expected+" </b>] matches with actual total premium [<b> "+t_p_actual+" </b>]as expected for "+sectionName+" in Flat Premium table .", "Pass", false);
				//customAssert.assertTrue(WriteDataToXl(TestBase.product+"_"+TestBase.businessEvent, "Premium Summary", testName, "PS_"+code+"_TotalPremium", p_expected,common.NB_excel_data_map),"Error while writing Total Premium for cover "+code+" to excel .");
				return 0;
			}else{
				TestUtil.reportStatus("<p style='color:red'> Mismatch in Expected Premium [<b> "+t_p_expected+"</b>] and Actual Premium [<b> "+t_p_actual+"</b>] for "+sectionName+" in Flat Premium table . </p>", "Fail", true);
				//customAssert.assertTrue(WriteDataToXl(TestBase.product+"_"+TestBase.businessEvent, "Premium Summary", testName, "PS_"+code+"_TotalPremium", p_expected,common.NB_excel_data_map),"Error while writing Total Premium for cover "+code+" to excel .");
				return 1;
			}
				
			
}catch(Throwable t) {
    String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
    TestUtil.reportFunctionFailed("Failed in "+methodName+" function");
    Assert.fail("Transaction Premium verification issue.  \n", t);
    return 1;
}

}	

public int funcTransactionDetailsTable_Verification_Total_MTA(List<String> sectionNames,Map<String,Map<String,Double>> transaction_Premium_Values){

try{
	
Map<String,Double> trans_details_values = new HashMap<>();
boolean Start_Fp = false;

TestUtil.reportStatus("---------------Totals In Transaction Details Table-----------------","Info",false);
double exp_value = 0.0;
outer:
for(String section : sectionNames){
	
	if(section.contains("Flat")){
		Start_Fp = true;
		continue;
	}
	
	if(!section.contains("Total") && !section.contains("Flat") && !Start_Fp){
		try{
			exp_value = exp_value + transaction_Premium_Values.get(section).get("Net Net Premium");
		}catch(Throwable t){
			continue;
		}
	}else if(Start_Fp && !section.contains("Total")){
	//for(String _section : sectionNames){
		if(common_VELA.isFPEntries && !section.contains("Flat")){
			try{
			exp_value = exp_value + common.transaction_Details_Premium_Values.get(section+"_FP").get("Net Net Premium");
		}catch(Throwable t){
			continue;
		}
		}
		//}
	//break outer;
	}
}

String t_NetNetP_actual = Double.toString(transaction_Premium_Values.get("Totals").get("Net Net Premium"));
CommonFunction.compareValues(exp_value,Double.parseDouble(t_NetNetP_actual)," Net Net Premium");
trans_details_values.put("Net Net Premium",exp_value);

exp_value = 0.0;Start_Fp = false;
for(String section : sectionNames){
	
	if(section.contains("Flat")){
		Start_Fp = true;
		continue;
	}
	
	if(!section.contains("Total") && !section.contains("Flat") && !Start_Fp){
		try{
			exp_value = exp_value + transaction_Premium_Values.get(section).get("Pen Comm");
		}catch(Throwable t){
			continue;
		}
	}else if(Start_Fp && !section.contains("Total")){
	//for(String _section : sectionNames){
		if(common_VELA.isFPEntries && !section.contains("Flat")){
			try{
			exp_value = exp_value + common.transaction_Details_Premium_Values.get(section+"_FP").get("Pen Comm");
		}catch(Throwable t){
			continue;
		}
		}
}
}
String t_pc_actual = Double.toString(transaction_Premium_Values.get("Totals").get("Pen Comm"));
CommonFunction.compareValues(exp_value,Double.parseDouble(t_pc_actual)," Pen Commission");
trans_details_values.put("Pen Comm",exp_value);

exp_value = 0.0;Start_Fp = false;
for(String section : sectionNames){

	if(section.contains("Flat")){
		Start_Fp = true;
		continue;
	}
	
	if(!section.contains("Total") && !section.contains("Flat") && !Start_Fp){
		try{
		exp_value = exp_value + transaction_Premium_Values.get(section).get("Net Premium");
		}catch(Throwable t){
			continue;
		}
	}else if(Start_Fp && !section.contains("Total")){
	//for(String _section : sectionNames){
		if(common_VELA.isFPEntries && !section.contains("Flat")){
			try{
			exp_value = exp_value + common.transaction_Details_Premium_Values.get(section+"_FP").get("Net Premium");
		}catch(Throwable t){
			continue;
		}
		}
}
}
String t_netP_actual = Double.toString(transaction_Premium_Values.get("Totals").get("Net Premium"));
CommonFunction.compareValues(exp_value,Double.parseDouble(t_netP_actual),"Net Premium");
trans_details_values.put("Net Premium",exp_value);

exp_value = 0.0;Start_Fp = false;
for(String section : sectionNames){
	
	if(section.contains("Flat")){
		Start_Fp = true;
		continue;
	}
	
	if(!section.contains("Total") && !section.contains("Flat") && !Start_Fp){
		try{
		exp_value = exp_value + transaction_Premium_Values.get(section).get("Broker Commission");
		}catch(Throwable t){
			continue;
		}
	}else if(Start_Fp && !section.contains("Total")){
	//for(String _section : sectionNames){
		if(common_VELA.isFPEntries && !section.contains("Flat")){
			try{
			exp_value = exp_value + common.transaction_Details_Premium_Values.get(section+"_FP").get("Broker Commission");
		}catch(Throwable t){
			continue;
		}
		}
}
}
String t_bc_actual =  Double.toString(transaction_Premium_Values.get("Totals").get("Broker Commission"));
CommonFunction.compareValues(exp_value,Double.parseDouble(t_bc_actual),"Broker Commission");
trans_details_values.put("Broker Commission",exp_value);

exp_value = 0.0;Start_Fp = false;
for(String section : sectionNames){
	
	if(section.contains("Flat")){
		Start_Fp = true;
		continue;
	}
	
	if(!section.contains("Total") && !section.contains("Flat") && !Start_Fp){
		try{
		exp_value = exp_value + transaction_Premium_Values.get(section).get("Gross Premium");
		}catch(Throwable t){
			continue;
		}
	}else if(Start_Fp && !section.contains("Total")){
	//for(String _section : sectionNames){
		if(common_VELA.isFPEntries && !section.contains("Flat")){
			try{
			exp_value = exp_value + common.transaction_Details_Premium_Values.get(section+"_FP").get("Gross Premium");
		}catch(Throwable t){
			continue;
		}
		}
}
}
String t_grossP_actual = Double.toString(transaction_Premium_Values.get("Totals").get("Gross Premium"));
CommonFunction.compareValues(exp_value,Double.parseDouble(t_grossP_actual)," Gross Premium");
trans_details_values.put("Gross Premium",exp_value);

exp_value = 0.0;Start_Fp = false;
for(String section : sectionNames){
	
	if(section.contains("Flat")){
		Start_Fp = true;
		continue;
	}
	
	if(!section.contains("Total") && !section.contains("Flat") && !Start_Fp){
		try{
		exp_value = exp_value + transaction_Premium_Values.get(section).get("Insurance Tax");
		}catch(Throwable t){
			continue;
		}
	}else if(Start_Fp && !section.contains("Total")){
	//for(String _section : sectionNames){
		if(common_VELA.isFPEntries && !section.contains("Flat")){
			try{
			exp_value = exp_value + common.transaction_Details_Premium_Values.get(section+"_FP").get("Insurance Tax");
		}catch(Throwable t){
			continue;
		}
		}
}
	
}
String t_InsuranceTax_actual = Double.toString(transaction_Premium_Values.get("Totals").get("Insurance Tax"));
CommonFunction.compareValues(exp_value,Double.parseDouble(t_InsuranceTax_actual),"Insurance Tax");
trans_details_values.put("Insurance Tax",exp_value);

exp_value = 0.0;Start_Fp = false;
for(String section : sectionNames){
	
	if(section.contains("Flat")){
		Start_Fp = true;
		continue;
	}
	
	if(!section.contains("Total") && !section.contains("Flat") && !Start_Fp){
		try{
		exp_value = exp_value + transaction_Premium_Values.get(section).get("Total Premium");
		}catch(Throwable t){
			continue;
		}
	}else if(Start_Fp && !section.contains("Total")){
	//for(String _section : sectionNames){
		if(common_VELA.isFPEntries && !section.contains("Flat")){
			try{
			exp_value = exp_value + common.transaction_Details_Premium_Values.get(section+"_FP").get("Total Premium");
		}catch(Throwable t){
			continue;
		}
		}
}
	
}
String t_p_actual = Double.toString(transaction_Premium_Values.get("Totals").get("Total Premium"));

trans_details_values.put("Total Premium",exp_value);

common.transaction_Details_Premium_Values.put("Totals", trans_details_values);

double premium_diff = exp_value - Double.parseDouble(t_p_actual);

if(premium_diff<0.06 && premium_diff>-0.06){
	TestUtil.reportStatus("Total Premium [<b> "+exp_value+" </b>] matches with actual total premium [<b> "+t_p_actual+" </b>]as expected for Totals in Transaction Details table .", "Pass", false);
	return 0;
	
}else{
	TestUtil.reportStatus("<p style='color:red'> Mismatch in Expected Premium [<b> "+exp_value+"</b>] and Actual Premium [<b> "+t_p_actual+"</b>] for Totals in Transaction Details table . </p>", "Fail", true);
	return 1;
}

}catch(Throwable t) {
String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
TestUtil.reportFunctionFailed("Failed in "+methodName+" function");
Assert.fail("Transaction Details Premium total Section verification issue.  \n", t);
return 1;
}
}


}




	


