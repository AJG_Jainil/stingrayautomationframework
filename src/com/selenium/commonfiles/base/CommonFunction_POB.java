package com.selenium.commonfiles.base;

import static com.selenium.commonfiles.util.TestUtil.WriteDataToXl;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.selenium.commonfiles.util.ErrorInTestMethod;
import com.selenium.commonfiles.util.ObjectMap;
import com.selenium.commonfiles.util.TestUtil;

public class CommonFunction_POB extends TestBase{

	SimpleDateFormat df = new SimpleDateFormat();
	public double MD_TotalPremium= 0.0 , LOI_TotalPremium = 0.0, PremiumExcTerrDocAct = 0, InsTaxDocAct = 0,InsTaxDocExp = 0;
	public int err_count = 0 , trans_error_val = 0;
	public static DecimalFormat f = new DecimalFormat("00.00");
	public boolean PremiumFlag = false;
	public static int errorVal=0,counter = 0;
	public boolean isInsuranceTaxDone = false;
	public static double totalGrossTax = 0.0;
	public double totalGrossTaxMTA = 0.0;
	public static double totalGrossPremium = 0.0;
	public double totalGrossPremiumMTA=0.0;
	public double totalNetPremiumTax=0.0;
	public double totalNetPremiumTaxMTA=0.0;
	public static WebElement taxTable_tBody;
	public static WebElement objTable;
	public static WebElement taxTable_tHead;
	public static int countOfCovers,countOfTableRows;
	public static Map<Object, Integer> variableTaxAdjustmentIDs = null;
	public static Map<Object, Integer> variableTaxAdjustmentIDsMTA = null;
	public static Map<Object, Double> grossTaxValues_Map = null;
	public static Map<Object, Map<Object, Object>> variableTaxAdjustmentVerificationMaps = null;
	public static Map<Object, Object> variableTaxAdjustmentDataMaps = null;
	public static Map<Object, Object> variableTaxAdjustmentDataMapsMTA = null;
	public static List<Object> headerNameStorage = null;
	public static List<Object> headerNameStorageMTA = null;
	public Map<String,Map<String,Double>> transaction_Premium_Values = new HashMap<>();
	public static ArrayList<Object> inputarraylist = null;
	public static ArrayList<Object> inputarraylistMTA = null;
	public static Map<String , String> AdjustedTaxDetails = new LinkedHashMap<String, String>();
	public static Map<String , String> AdjustedTaxCollection = new LinkedHashMap<String, String>();
	public static Map<String, Double> Adjusted_Premium_map = null;
	public static double adjustedPremium = 0.0,adjustedTotalPremium=0.0,adjustedTotalPremiumMTA=0.0,adjustedTotalTax=0.0,adjustedTotalTaxMTA=0.0,unAdjustedTotalTax=0.0,unAdjustedTotalTaxMTA=0.0;
	public static double PD_TotalRate = 0.0, PD_AdjustedRate = 0.0, PD_MD_Premium=0.0, PD_BI_Premium=0.0, PD_MD_TotalPremium = 0.00, PD_BI_TotalPremium = 0.00, finalMDPremium = 0.00, finalBIPremium= 0.00;
	public boolean isMTARewindFlow=false,isFPEntries=false,isMTARewindStarted=false, isNBRewindStarted = false, isNBRquoteStarted = false;
	public String FP_Covers = null;
	public double rewindMTADoc_AddTaxTer = 0.00;
	public double rewindMTADoc_Premium = 0.00, rewindMTADoc_TerP = 0.00, rewindMTADoc_InsPTax = 0.00, rewindMTADoc_TotalP = 0.00;
	public double rewindDoc_Premium = 0.00, rewindDoc_TerP = 0.00, rewindDoc_InsPTax = 0.00, rewindDoc_TotalP = 0.00, rewindDoc_InsTaxTer = 0.00;
	public double AdditionalPWithAdminDocAct = 0.00, AdditionalExcTerrDocAct = 0.00,  AdditionalTerPDocAct = 0.00, AdditionalInsTaxDocAct = 0.00;
	public double InsTaxTerrDoc = 0.00, tpTotal = 0.00, AddTaxTerrDoc = 0.00;
	
public void NewBusinessFlow(String code,String event){
	String testName = (String)common.NB_excel_data_map.get("Automation Key");
	try{
		
		customAssert.assertTrue(common.StingrayLogin("PEN"),"Unable to login.");
		customAssert.assertTrue(common.checkClient(common.NB_excel_data_map,code,event),"Unable to check Client.");
		customAssert.assertTrue(common.createNewQuote(common.NB_excel_data_map,code,event), "Unable to create new quote.");
		customAssert.assertTrue(common.selectLatestQuote(common.NB_excel_data_map,code,event), "Unable to select quote from table.");
		customAssert.assertTrue(funcPolicyDetails(common.NB_excel_data_map), "Policy Details function having issue .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Previous Claims"),"Issue while Navigating to Previous Claims  . ");
		customAssert.assertTrue(common_CCF.funcPreviousClaims(common.NB_excel_data_map), "Previous claim function is having issue(S) .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(common.funcCovers(common.NB_excel_data_map), "Select covers function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Specified Perils"),"Issue while Navigating to Specified Perils  . ");
		customAssert.assertTrue(common.funcSpecifiedPerils(common.NB_excel_data_map), "Select covers function is having issue(S) . ");
		
		if(((String)common.NB_excel_data_map.get("CD_MaterialDamage")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Insured Properties"),"Issue while Navigating to Insured Properties  . ");
			customAssert.assertTrue(funcInsuredProperties(common.NB_excel_data_map), "Insured Property function is having issue(S) . ");
		}
		
		if(((String)common.NB_excel_data_map.get("CD_Liability")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Employers Liability"),"Issue while Navigating to Employers Liability  . ");
			customAssert.assertTrue(common_CCF.funcEmployersLiability(common.NB_excel_data_map), "Employers Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","ELD Information"),"Issue while Navigating to ELD Information  . ");
			customAssert.assertTrue(common_CCF.funcELDInformation(common.NB_excel_data_map), "ELD Information function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability"),"Issue while Navigating to Property Owners Liability  . ");
			customAssert.assertTrue(funcPropertyOwnersLiability(common.NB_excel_data_map), "Public Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability Information"),"Issue while Navigating to Products Liability  . ");
			customAssert.assertTrue(funcLiabilityInformation(common.NB_excel_data_map), "Liability Information function is having issue(S) . ");
			}
		if(((String)common.NB_excel_data_map.get("CD_CyberandDataSecurity")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Cyber and Data Security"),"Issue while Navigating to Cyber and Data Security screen . ");
			customAssert.assertTrue(common_CCF.funcCyberAndDataSecurity(common.NB_excel_data_map), "Cyber and Data Security function is having issue(S) . ");
			}
		if(((String)common.NB_excel_data_map.get("CD_Terrorism")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Terrorism"),"Issue while Navigating to Terrorism screen . ");
			customAssert.assertTrue(common_CCF.funcTerrorism(common.NB_excel_data_map), "Terrorism function is having issue(S) . ");
			}
		
		if(((String)common.NB_excel_data_map.get("CD_LegalExpenses")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Legal Expenses"),"Issue while Navigating to Legal Expenses screen . ");
			customAssert.assertTrue(common_CCF.funcLegalExpenses(common.NB_excel_data_map,code,event), "Legal Expenses function is having issue(S) . ");
			}
		
		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
			Assert.assertTrue(common_PEN.funcPremiumSummary(common.NB_excel_data_map,code,event));
			Assert.assertTrue(common_PEN.funcStatusHandling(common.NB_excel_data_map,code,event));
		
			if(TestBase.businessEvent.equalsIgnoreCase("NB")){
				customAssert.assertEquals(common.final_err_pdf_count,0,"Varification Errors in PDF Documents . ");
				customAssert.assertTrue(common.StingrayLogout(), "Unable to Logout.");
				TestUtil.reportTestCasePassed(testName);
			}
		
	
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
	}
	
}
public void MTAFlow(String code,String event) throws ErrorInTestMethod{
	
	String testName = (String)common.MTA_excel_data_map.get("Automation Key");
	String NavigationBy = CONFIG.getProperty("NavigationBy");
	CommonFunction_PEN.AdjustedTaxDetails.clear();

	if(((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes")) {
		customAssert.assertTrue(common_EP.ExistingPolicyAlgorithm_PEN(common.MTA_excel_data_map , (String)common.MTA_excel_data_map.get("MTA_ExistingPolicy_Type"), (String)common.MTA_excel_data_map.get("MTA_ExistingPolicy_Status")), "Existing Policy Algorithm function is having issues. ");
	}else {
		if(!common.currentRunningFlow.equalsIgnoreCase("Renewal")){
			NewBusinessFlow(code,"NB");
		}
		common_HHAZ.CoversDetails_data_list.clear();
		common_PEN.PremiumFlag = false;
	}
	
	try {
	
	common.currentRunningFlow = "MTA";	
	
	customAssert.assertTrue(common.funcNextNavigateDecesionMaker(NavigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen.");
	customAssert.assertTrue(common_POB.funcCreateEndorsement(),"Error in Create Endorsement function .");
	
    customAssert.assertTrue(funcPolicyDetails(common.MTA_excel_data_map),"Policy Details function having issue");
    customAssert.assertTrue(common.funcMenuSelection("Navigate","Previous Claims"), "Issue while Navigating to Previous Claims");
    customAssert.assertTrue(common_CCF.funcPreviousClaims(common.MTA_excel_data_map), "Previous claim function is having issue(S) .");
	customAssert.assertTrue(common.funcNextNavigateDecesionMaker(NavigationBy,"Covers"),"Issue while Navigating to Covers screen.");
	customAssert.assertTrue(common.funcCovers(common.MTA_excel_data_map), "Select covers function is having issue(S) . ");
	customAssert.assertTrue(common.funcMenuSelection("Navigate","Specified Perils"),"Issue while Navigating to Specified Perils  . ");
	customAssert.assertTrue(common.funcSpecifiedPerils(common.MTA_excel_data_map), "Select covers function is having issue(S) . ");
	
	if(((String)common.MTA_excel_data_map.get("CD_MaterialDamage")).equals("Yes")){		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Insured Properties"),"Issue while Navigating to Insured Properties  . ");
		customAssert.assertTrue(common_POB.funcInsuredProperties(common.MTA_excel_data_map), "Insured Property function is having issue(S) . ");
	}
	
	if(((String)common.MTA_excel_data_map.get("CD_Liability")).equals("Yes")){
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Employers Liability"),"Issue while Navigating to Employers Liability  . ");
		customAssert.assertTrue(common_CCF.funcEmployersLiability(common.MTA_excel_data_map), "Employers Liability function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability"),"Issue while Navigating to Property Owners Liability  . ");
		customAssert.assertTrue(funcPropertyOwnersLiability(common.MTA_excel_data_map), "Public Liability function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability Information"),"Issue while Navigating to Products Liability  . ");
		customAssert.assertTrue(funcLiabilityInformation(common.MTA_excel_data_map), "Liability Information function is having issue(S) . ");
	}
	
	
	if(((String)common.MTA_excel_data_map.get("CD_CyberandDataSecurity")).equals("Yes")){
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Cyber and Data Security"),"Issue while Navigating to Cyber and Data Security screen . ");
		customAssert.assertTrue(common_CCF.funcCyberAndDataSecurity(common.MTA_excel_data_map), "Cyber and Data Security function is having issue(S) . ");
	}
	
	if(((String)common.MTA_excel_data_map.get("CD_Terrorism")).equals("Yes")){		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Terrorism"),"Issue while Navigating to Terrorism screen . ");
		customAssert.assertTrue(common_CCF.funcTerrorism(common.MTA_excel_data_map), "Terrorism function is having issue(S) . ");
		}
	
	if(((String)common.MTA_excel_data_map.get("CD_LegalExpenses")).equals("Yes")){		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Legal Expenses"),"Issue while Navigating to Legal Expenses screen . ");
		customAssert.assertTrue(common_CCF.funcLegalExpenses(common.MTA_excel_data_map,code,event), "Legal Expenses function is having issue(S) . ");
		}
	
	customAssert.assertTrue(common.funcNextNavigateDecesionMaker(NavigationBy, "Premium Summary"), "Issue while Navigating to Premium Summary screen . ");
	Assert.assertTrue(common_PEN.funcPremiumSummary_MTA(common.MTA_excel_data_map, code, event));
	
	
	if(!TestBase.businessEvent.equalsIgnoreCase("Renewal")){
		Assert.assertTrue(common_PEN.funcStatusHandling(common.MTA_excel_data_map, code, event));
		customAssert.assertEquals(err_count,0,"Errors in premium calculations . ");
		customAssert.assertEquals(trans_error_val,0,"Errors in Transaction premium calculations . ");
		customAssert.assertEquals(common.final_err_pdf_count,0,"Varification Errors in PDF Documents . ");
		
		customAssert.assertTrue(common.StingrayLogout(), "Unable to Logout.");
	
		TestUtil.reportTestCasePassed(testName);
	}
	
	}catch(Throwable t){
		
 		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
}
public void RewindFlow(String code,String event) throws ErrorInTestMethod{
	String testName = (String)common.Rewind_excel_data_map.get("Automation Key");
	try{
		
		if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes")){
			customAssert.assertTrue(common_EP.ExistingPolicyAlgorithm_PEN(common.Rewind_excel_data_map,(String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type"), (String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Status")), "Existing Policy Algorithm function is having issues. ");
		}else{
			CommonFunction_HHAZ.AdjustedTaxDetails.clear();
			if(!common.currentRunningFlow.equalsIgnoreCase("Renewal") && !common.currentRunningFlow.equalsIgnoreCase("MTA")){
				NewBusinessFlow(code,"NB");
			}
			common_PEN.PremiumFlag = false;
		}
		
	
		common.currentRunningFlow="Rewind";
		String navigationBy = CONFIG.getProperty("NavigationBy");
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common.funcRewind());
		
		TestUtil.reportStatus("<b> -----------------------Rewind flow started---------------------- </b>", "Info", false);
		
		if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes"))
		{
			customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
			customAssert.assertTrue(common.funcSearchPolicy(common.MTA_excel_data_map), "Policy Search function is having issue(S) . ");
			customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Endorsement Submitted (Rewind)"), "Verify Policy Status (Endorsement Submitted (Rewind)) function is having issue(s).");
		}
		else
		{
			customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
			customAssert.assertTrue(common.funcSearchPolicy(common.NB_excel_data_map), "Policy Search function is having issue(S) . ");
			if(TestBase.businessEvent.equalsIgnoreCase("MTA"))
			{
				customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Endorsement Submitted (Rewind)"), "Verify Policy Status (Endorsement Submitted (Rewind)) function is having issue(s).");
			}
			else
			{
				customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Submitted (Rewind)"), "Verify Policy Status (Submitted (Rewind)) function is having issue(s).");
			}
		}
		
		customAssert.assertTrue(funcPolicyDetails(common.Rewind_excel_data_map), "Policy Details function having issue .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Previous Claims"),"Issue while Navigating to Previous Claims  . ");
		customAssert.assertTrue(common_CCF.funcPreviousClaims(common.Rewind_excel_data_map), "Previous claim function is having issue(S) .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(common.funcCovers(common.Rewind_excel_data_map), "Select covers function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Specified Perils"),"Issue while Navigating to Specified Perils  . ");
		customAssert.assertTrue(common.funcSpecifiedPerils(common.Rewind_excel_data_map), "Select covers function is having issue(S) . ");
		if(((String)common.Rewind_excel_data_map.get("CD_MaterialDamage")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Insured Properties"),"Issue while Navigating to Insured Properties  . ");
			customAssert.assertTrue(funcInsuredProperties(common.Rewind_excel_data_map), "Insured Property function is having issue(S) . ");
		}
		
		if(((String)common.Rewind_excel_data_map.get("CD_Liability")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Employers Liability"),"Issue while Navigating to Employers Liability  . ");
			customAssert.assertTrue(common_CCF.funcEmployersLiability(common.Rewind_excel_data_map), "Employers Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","ELD Information"),"Issue while Navigating to ELD Information  . ");
			customAssert.assertTrue(common_CCF.funcELDInformation(common.Rewind_excel_data_map), "ELD Information function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability"),"Issue while Navigating to Property Owners Liability  . ");
			customAssert.assertTrue(funcPropertyOwnersLiability(common.Rewind_excel_data_map), "Public Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability Information"),"Issue while Navigating to Products Liability  . ");
			customAssert.assertTrue(funcLiabilityInformation(common.Rewind_excel_data_map), "Liability Information function is having issue(S) . ");
			}
		if(((String)common.Rewind_excel_data_map.get("CD_CyberandDataSecurity")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Cyber and Data Security"),"Issue while Navigating to Cyber and Data Security screen . ");
			customAssert.assertTrue(common_CCF.funcCyberAndDataSecurity(common.Rewind_excel_data_map), "Cyber and Data Security function is having issue(S) . ");
			}
		if(((String)common.Rewind_excel_data_map.get("CD_Terrorism")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Terrorism"),"Issue while Navigating to Terrorism screen . ");
			customAssert.assertTrue(common_CCF.funcTerrorism(common.Rewind_excel_data_map), "Terrorism function is having issue(S) . ");
			}
		
		if(((String)common.Rewind_excel_data_map.get("CD_LegalExpenses")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Legal Expenses"),"Issue while Navigating to Legal Expenses screen . ");
			customAssert.assertTrue(common_CCF.funcLegalExpenses(common.Rewind_excel_data_map,code,event), "Legal Expenses function is having issue(S) . ");
			}
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		
		if(TestBase.businessEvent.equalsIgnoreCase("MTA")){
			customAssert.assertTrue(common_PEN.funcPremiumSummary_MTA(common.Rewind_excel_data_map, code, event), "Rewind MTA Premium Summary in function is having issue(S) . ");
		}else if(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes")){
			customAssert.assertTrue(common_PEN.funcPremiumSummary_MTA(common.Rewind_excel_data_map, code, event), "Rewind MTA Premium Summary in function is having issue(S) . ");
		}else{
			customAssert.assertTrue(common_PEN.funcPremiumSummary(common.Rewind_excel_data_map,code,event), "Premium Summary function is having issue(S) . ");
		}
		if(!TestBase.businessEvent.equalsIgnoreCase("MTA")){ 
			Assert.assertTrue(common_PEN.funcStatusHandling(common.Rewind_excel_data_map,code,event));
		}
			
		if(TestBase.businessEvent.equals("Rewind")){
			customAssert.assertEquals(err_count,0,"Errors in premium calculations . ");
			customAssert.assertEquals(trans_error_val,0,"Errors in Transaction premium calculations . ");
			customAssert.assertEquals(common.final_err_pdf_count,0,"Varification Errors in PDF Documents . ");
			customAssert.assertTrue(common.StingrayLogout(), "Unable to Logout.");
			TestUtil.reportTestCasePassed(testName);
			
		} 
		
		
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
	
	
	
}


public void RenewalFlow(String code,String event){
	String testName = (String)common.Renewal_excel_data_map.get("Automation Key");
	common.currentRunningFlow = "Renewal";
	common_CCD.isMTARewindStarted = true;
	try{
		
		customAssert.assertTrue(common.StingrayLogin("PEN"),"Unable to login.");
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""),"");
		customAssert.assertTrue(common_CCJ.renewalSearchPolicyNEW(common.Renewal_excel_data_map), "Policy Search function is having issue(S) . ");
		
		if(!common_HHAZ.isAssignedToUW){ // This variable is initialized in common_CCJ.renewalSearchPolicyNEW function
			customAssert.assertTrue(common.funcButtonSelection("Assign Underwriter"));
			customAssert.assertTrue(common_SPI.funcAssignPolicyToUW());
		}
		
		customAssert.assertTrue(common.funcButtonSelection("Send to Underwriter"));
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""),"");
		customAssert.assertTrue(common.funcSearchPolicy_Renewal(common.Renewal_excel_data_map), "Policy Search function is having issue(S) . ");
		customAssert.assertTrue(common.funcVerifyPolicyStatus_Renewal(common.Renewal_excel_data_map,CommonFunction.product,CommonFunction.businessEvent,"Renewal Submitted"), "Verify Policy Status (Renewal Submitted) function is having issue(S) . ");
		
		customAssert.assertTrue(funcPolicyDetails(common.Renewal_excel_data_map), "Policy Details function having issue .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Previous Claims"),"Issue while Navigating to Previous Claims  . ");
		customAssert.assertTrue(common_CCF.funcPreviousClaims(common.Renewal_excel_data_map), "Previous claim function is having issue(S) .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(common.funcCovers(common.Renewal_excel_data_map), "Select covers function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Specified Perils"),"Issue while Navigating to Specified Perils  . ");
		customAssert.assertTrue(common.funcSpecifiedPerils(common.Renewal_excel_data_map), "Select covers function is having issue(S) . ");
		
		if(((String)common.Renewal_excel_data_map.get("CD_MaterialDamage")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Insured Properties"),"Issue while Navigating to Insured Properties  . ");
			customAssert.assertTrue(funcInsuredProperties(common.Renewal_excel_data_map), "Insured Property function is having issue(S) . ");
		}
		
		if(((String)common.Renewal_excel_data_map.get("CD_Liability")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Employers Liability"),"Issue while Navigating to Employers Liability  . ");
			customAssert.assertTrue(common_CCF.funcEmployersLiability(common.Renewal_excel_data_map), "Employers Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","ELD Information"),"Issue while Navigating to ELD Information  . ");
			customAssert.assertTrue(common_CCF.funcELDInformation(common.Renewal_excel_data_map), "ELD Information function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability"),"Issue while Navigating to Property Owners Liability  . ");
			customAssert.assertTrue(funcPropertyOwnersLiability(common.Renewal_excel_data_map), "Public Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability Information"),"Issue while Navigating to Products Liability  . ");
			customAssert.assertTrue(funcLiabilityInformation(common.Renewal_excel_data_map), "Liability Information function is having issue(S) . ");
			}
		if(((String)common.Renewal_excel_data_map.get("CD_CyberandDataSecurity")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Cyber and Data Security"),"Issue while Navigating to Cyber and Data Security screen . ");
			customAssert.assertTrue(common_CCF.funcCyberAndDataSecurity(common.Renewal_excel_data_map), "Cyber and Data Security function is having issue(S) . ");
			}
		if(((String)common.Renewal_excel_data_map.get("CD_Terrorism")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Terrorism"),"Issue while Navigating to Terrorism screen . ");
			customAssert.assertTrue(common_CCF.funcTerrorism(common.Renewal_excel_data_map), "Terrorism function is having issue(S) . ");
			}
		
		if(((String)common.Renewal_excel_data_map.get("CD_LegalExpenses")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Legal Expenses"),"Issue while Navigating to Legal Expenses screen . ");
			customAssert.assertTrue(common_CCF.funcLegalExpenses(common.Renewal_excel_data_map,code,event), "Legal Expenses function is having issue(S) . ");
		}
		
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		Assert.assertTrue(common_PEN.funcPremiumSummary(common.Renewal_excel_data_map,code,event));
		Assert.assertTrue(common_PEN.funcStatusHandling(common.Renewal_excel_data_map,code,event));
		
		
		customAssert.assertEquals(common.final_err_pdf_count,0,"Varification Errors in PDF Documents . ");
		customAssert.assertTrue(common.StingrayLogout(), "Unable to Logout.");
		
		TestUtil.reportTestCasePassed(testName);
	
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
	}
	
}

public void RenewalRewindFlow(String code,String event) throws ErrorInTestMethod{
	String testName = (String)common.Rewind_excel_data_map.get("Automation Key");
	try{
		
		common.currentRunningFlow="Rewind";
		common.CoversDetails_data_list.clear();
		String navigationBy = CONFIG.getProperty("NavigationBy");
		common_HHAZ.PremiumFlag = false;
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common.funcRewind());
		
		TestUtil.reportStatus("<b> -----------------------Renewal Rewind flow is started---------------------- </b>", "Info", false);
		
		customAssert.assertTrue(common.funcMenuSelection("Policies", ""));
		customAssert.assertTrue(common.funcSearchPolicy(common.Renewal_excel_data_map), "Policy Search function is having issue(S) . ");
		customAssert.assertTrue(common.funcVerifyPolicyStatus(common.Rewind_excel_data_map,code,event,"Renewal Submitted (Rewind)"), "Verify Policy Status (Submitted (Rewind)) function is having issue(S) . ");
		
		customAssert.assertTrue(funcPolicyDetails(common.Rewind_excel_data_map), "Policy Details function having issue .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Previous Claims"),"Issue while Navigating to Previous Claims  . ");
		customAssert.assertTrue(common_CCF.funcPreviousClaims(common.Rewind_excel_data_map), "Previous claim function is having issue(S) .");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
		customAssert.assertTrue(common.funcCovers(common.Rewind_excel_data_map), "Select covers function is having issue(S) . ");
		customAssert.assertTrue(common.funcMenuSelection("Navigate","Specified Perils"),"Issue while Navigating to Specified Perils  . ");
		customAssert.assertTrue(common.funcSpecifiedPerils(common.Rewind_excel_data_map), "Select covers function is having issue(S) . ");
		
		if(((String)common.Rewind_excel_data_map.get("CD_MaterialDamage")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Insured Properties"),"Issue while Navigating to Insured Properties  . ");
			customAssert.assertTrue(funcInsuredProperties(common.Rewind_excel_data_map), "Insured Property function is having issue(S) . ");
		}
		
		if(((String)common.Rewind_excel_data_map.get("CD_Liability")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Employers Liability"),"Issue while Navigating to Employers Liability  . ");
			customAssert.assertTrue(common_CCF.funcEmployersLiability(common.Rewind_excel_data_map), "Employers Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","ELD Information"),"Issue while Navigating to ELD Information  . ");
			customAssert.assertTrue(common_CCF.funcELDInformation(common.Rewind_excel_data_map), "ELD Information function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability"),"Issue while Navigating to Property Owners Liability  . ");
			customAssert.assertTrue(funcPropertyOwnersLiability(common.Rewind_excel_data_map), "Public Liability function is having issue(S) . ");
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability Information"),"Issue while Navigating to Products Liability  . ");
			customAssert.assertTrue(funcLiabilityInformation(common.Rewind_excel_data_map), "Liability Information function is having issue(S) . ");
			}
		if(((String)common.Rewind_excel_data_map.get("CD_CyberandDataSecurity")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Cyber and Data Security"),"Issue while Navigating to Cyber and Data Security screen . ");
			customAssert.assertTrue(common_CCF.funcCyberAndDataSecurity(common.Rewind_excel_data_map), "Cyber and Data Security function is having issue(S) . ");
			}
		if(((String)common.Rewind_excel_data_map.get("CD_Terrorism")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Terrorism"),"Issue while Navigating to Terrorism screen . ");
			customAssert.assertTrue(common_CCF.funcTerrorism(common.Rewind_excel_data_map), "Terrorism function is having issue(S) . ");
			}
		
		if(((String)common.Rewind_excel_data_map.get("CD_LegalExpenses")).equals("Yes")){		
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Legal Expenses"),"Issue while Navigating to Legal Expenses screen . ");
			customAssert.assertTrue(common_CCF.funcLegalExpenses(common.Rewind_excel_data_map,code,event), "Legal Expenses function is having issue(S) . ");
		}
		
		//customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Endorsements"),"Issue while Navigating to Property Owners Liability screen.");
		//customAssert.assertTrue(common_PEN.funcEndorsementOperations(common.Rewind_excel_data_map),"Insurance tax adjustment is having issue(S).");
		customAssert.assertTrue(common.funcNextNavigateDecesionMaker(navigationBy,"Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
		customAssert.assertTrue(common_PEN.funcPremiumSummary(common.Rewind_excel_data_map,code,event), "Premium Summary function is having issue(S) . ");
		
		
		
	}catch(Throwable t){
		TestUtil.reportTestCaseFailed(testName, t);
		throw new ErrorInTestMethod(t.getMessage());
	}
	
	
	
}

/**
 * 
 * This method handles POB Policy Details screens scripting.
 * 
 */
@SuppressWarnings("static-access")
public boolean funcPolicyDetails(Map<Object, Object> map_data){
	
	boolean retvalue = true;
	try{
		customAssert.assertTrue(common.funcPageNavigation("Policy Details", ""), "Navigation problem to Policy Details page .");
		customAssert.assertTrue(k.Input("CCF_PD_ProposerName", (String)map_data.get("PD_ProposerName")),	"Unable to enter value in Proposer Name  field .");
		
		if(common.currentRunningFlow.equalsIgnoreCase("NB") || (TestBase.businessEvent.equals("Rewind") && !(((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy")).equalsIgnoreCase("Yes") && ((String)common.Rewind_excel_data_map.get("Rewind_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement")))){
			customAssert.assertTrue(k.SelectRadioBtn("CCF_PD_NewVenture", (String)map_data.get("PD_NewVenture")), "Unable to Select New Venture radio button on Policy Details Page.");
			customAssert.assertTrue(k.SelectRadioBtn("CCF_PD_Prospect", (String)map_data.get("PD_Prospect")), "Unable to Select Prospect radio button on Policy Details Page.");
			customAssert.assertTrue(k.SelectRadioBtn("CCF_PD_CrossSell", (String)map_data.get("PD_CrossSell")), "Unable to Select CrossSell radio button on Policy Details Page.");
			/*customAssert.assertTrue(k.Click("inception_date"), "Unable to Click inception date.");
			customAssert.assertTrue(k.Input("inception_date", (String)common.NB_excel_data_map.get("QC_InceptionDate")),"Unable to Enter inception date.");
			customAssert.assertTrue(k.Click("calander_btn"), "Unable to click on done button in calander.");*/
			customAssert.assertTrue(!k.getAttributeIsEmpty("inception_date", "value"),"Inception Date Field Should Contain Valid value  .");
			/*customAssert.assertTrue(k.Click("deadline_date"), "Unable to Click deadline date.");
			customAssert.assertTrue(k.Input("deadline_date", (String)common.NB_excel_data_map.get("QC_DeadlineDate")),"Unable to Enter deadline date.");
			customAssert.assertTrue(k.Click("calander_btn"), "Unable to click on done button in calander.");*/
			customAssert.assertTrue(!k.getAttributeIsEmpty("deadline_date", "value"),"Deadline date Field Should Contain Valid value  .");
			customAssert.assertTrue(k.Input("CCF_QC_TargetPemium", (String) map_data.get("QC_TargetPemium")),"Unable to enter value in Target Pemium field. ");
			
			k.waitTwoSeconds();
			customAssert.assertTrue(k.SelectRadioBtn("CCF_PD_HoldingBroker", (String)map_data.get("PD_HoldingBroker")), "Unable to Select Holding Broker radio button on Policy Details Page.");
			if(map_data.get("PD_HoldingBroker").equals("No")){
				customAssert.assertTrue(k.Input("CCF_PD_HoldingBrokerInfo", (String) map_data.get("PD_HoldingBrokerInfo")),"Unable to enter value in HoldingBrokerInfo field. ");
			}
		}
		customAssert.assertTrue(!k.getAttributeIsEmpty("CCF_PD_ProposerName", "value"),"Proposer Name Field Should Contain Valid Name  .");
		customAssert.assertTrue(k.Input("CCF_CC_TradingName", (String)map_data.get("PD_TradingName")),	"Unable to enter value in Trading Name  field .");
		customAssert.assertTrue(k.Input("CCF_PD_BusinessDesc", (String)map_data.get("PD_BusinessDesc")),	"Unable to enter value in Business Desc  field .");
		if(!TestBase.businessEvent.equals("Renewal") && !common.currentRunningFlow.equals("MTA") && !common.currentRunningFlow.equals("Rewind")){
			customAssert.assertTrue(k.SelectRadioBtn("CCF_PD_1QS", (String)map_data.get("PD_1QS")), "Unable to Select 1QS radio button on Policy Details Page.");
		}
		customAssert.assertTrue(k.Input("CCF_PD_DateEstablishment", (String)map_data.get("PD_DateEstablishment")),	"Unable to enter value in Date Establishment  field .");
		customAssert.assertTrue(k.Input("CCF_Address_CC_Address", (String) map_data.get("PD_Address")),"Unable to enter value in Address field. ");
		customAssert.assertTrue(!k.getAttributeIsEmpty("CCF_Address_CC_Address", "value"),"Address Field Should Contain Valid Address  .");
		customAssert.assertTrue(k.Input("CCF_Address_CC_line2", (String) map_data.get("PD_Line1")),"Unable to enter value in Address field line 1 . ");
		customAssert.assertTrue(k.Input("CCF_Address_CC_line3", (String) map_data.get("PD_Line2")),"Unable to enter value in Address field line 2 . ");
		customAssert.assertTrue(k.Input("CCF_Address_CC_Town", (String) map_data.get("PD_Town")),"Unable to enter value in Town field . ");
		customAssert.assertTrue(k.Input("CCF_Address_CC_County", (String) map_data.get("PD_County")),"Unable to enter value in County  . ");
		customAssert.assertTrue(k.Input("CCF_Address_CC_Postcode", (String)map_data.get("PD_Postcode")),"Unable to enter value in PostCode");
		customAssert.assertTrue(!k.getAttributeIsEmpty("CCF_Address_CC_Postcode", "value"),"PostCode Field Should Contain Valid Postcode  .");
		customAssert.assertTrue(common.validatePostCode((String)map_data.get("PD_Postcode")),"Post Code is not in Correct format .");
		
		customAssert.assertTrue(k.SelectRadioBtn("CCF_PD_TaxExempt", (String)map_data.get("PD_TaxExempt")), "Unable to Select TaxExempt radio button on Policy Details Page.");
		
		if(TestBase.product.equalsIgnoreCase("POE")){
		//	customAssert.assertTrue(k.SelectRadioBtn("POE_businessEP", (String)map_data.get("PD_businessEP")), "Unable to Select Is this Business a Micro Business Enterprise ? on Policy Details Page.");
		}else{
			customAssert.assertTrue(k.SelectRadioBtn("CCF_PD_CarrierOverride", (String)map_data.get("PD_CarrierOverride")), "Unable to Select Carrier Override radio button on Policy Details Page.");
		}
		
		if(!TestBase.businessEvent.equalsIgnoreCase("Renewal") && !common.currentRunningFlow.equals("MTA")&& !common.currentRunningFlow.equals("Rewind")){
			customAssert.assertTrue(k.Input("CCF_PD_PreviousPremium", (String) map_data.get("PD_PreviousPremium")),"Unable to enter value in Previous Premium field. ");
			if(map_data.get("PD_CarrierOverride").equals("Yes")){
				customAssert.assertTrue(k.SelectRadioBtn("CCF_PD_CO_RefferedToHead", (String)map_data.get("PD_CO_RefferedToHead")), "Unable to Select Reffered To Head radio button on Policy Details Page.");
			}
			k.waitTwoSeconds();
		}
		
	
		//TradeCode Selection & Verification
		if(((String)map_data.get("PD_TCS_TradeCode_Button")).equalsIgnoreCase("Yes")){
			customAssert.assertTrue(common.tradeCodeSelection((String)map_data.get("PD_TCS_TradeCode") ,"Policy Details" , 0),"Trade code selection function is having issue(S).");
		
		}
		
		switch (common.product) {
		case "POC":
			customAssert.assertTrue(k.SelectRadioBtn("POC_PD_HazardGroup", (String)map_data.get("PD_HazardGroup")), "Unable to Select  Hazard Group radio button on Policy Details Page.");
			switch ((String)map_data.get("PD_HazardGroup")) {
			case "Yes":
				customAssert.assertTrue(k.Input("POC_PD_NewHazardGroupValue", (String) map_data.get("PD_NewHazardGroupValue")),"Unable to enter value in  Hazard Group value field. ");
				customAssert.assertTrue(k.Input("POC_PD_HazardGroupOverrideReason", (String) map_data.get("PD_HazardGroupOverrideReason")),"Unable to enter value in Hazard Group Override Reason. ");
				break;
			}
		}
		
		TestUtil.reportStatus("Entered all the details on Policy Details page .", "Info", true);
		
		return retvalue;
		
	}catch(Throwable t) {
        String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
        TestUtil.reportFunctionFailed("Failed in "+methodName+" function");
        Assert.fail("Unable to to do operation on policy details page. \n", t);
        return false;
 }
}

public static boolean verification(String actualValue,String expectedValue,String sectionName,String description){
	
	if(description.equalsIgnoreCase("Gross Tax") || description.equalsIgnoreCase("Gross Premium") ||description.equalsIgnoreCase("Un Adjusted Premium") ){
		double actVal = Double.parseDouble(actualValue);
		double expVal = Double.parseDouble(expectedValue);
		double diffrence = actVal - expVal;
		
		if(diffrence<=0.05 && diffrence>=-0.05){
			return true;
		}else{
			TestUtil.reportStatus("Mistmatch in "+description+" for [<b>  "+sectionName+"  </b>]  cover ---  Expected "+description+" is :  <b>[ "+expectedValue+" ]</b> and Actual "+description+" on Stingray application is : <b>[ "+actualValue+" ]</b>", "Fail", false);
			return false;
		}
	}else{
		if(actualValue.equalsIgnoreCase(expectedValue)){
			return true;
		}else{
			TestUtil.reportStatus("Mistmatch in "+description+" for [<b>  "+sectionName+"  </b>]  cover ---  Expected "+description+" is :  <b>[ "+expectedValue+" ]</b> and Actual "+description+" on Stingray application is : <b>[ "+actualValue+" ]</b>", "Fail", false);
			return false;
		}
	}
	
	
}


public boolean deleteItems(){
	
	boolean isNotStale=true;
	k.ImplicitWaitOff();
	while(isNotStale){
		try{
			
			
			List<WebElement> delete_Btns = driver.findElements(By.xpath("//*[text()='Delete']"));
			
			for(WebElement element: delete_Btns){
				if(element.isDisplayed())
					element = driver.findElement(By.xpath("//*[text()='Delete']"));
					JavascriptExecutor j_exe = (JavascriptExecutor) driver;
					j_exe.executeScript("arguments[0].scrollIntoView(true);", element);
					element.click();
					WebDriverWait wait = new WebDriverWait(driver, 3);
					if(wait.until(ExpectedConditions.alertIsPresent())!=null){
						k.AcceptPopup();
					}
				else
					continue;
			}
			isNotStale=false;
		}catch(Throwable t){
			continue;
		}
	}
	k.ImplicitWaitOn();
	return true;
	
}
@SuppressWarnings({ "rawtypes", "static-access" })
public boolean verifyAdjustedTaxOnBusinessEvent(Map<Object, Object> map_data) throws Exception {
	
	taxTable_tBody = k.getObject("inssuranceTaxMainTableBODY"); 
	List<WebElement> list2 = taxTable_tBody.findElements(By.tagName("tr"));
	countOfCovers = list2.size();
	String sectionName;
	
	double AP,AT,UP,UGT,GT = 0.0,IPT = 0;
	for(int j=0;j<countOfCovers-1;j++){
		
		taxTable_tBody = k.getObject("inssuranceTaxMainTableBODY");
		sectionName = taxTable_tBody.findElement(By.xpath("tr["+(j+1)+"]/td[1]")).getText();
		
		if(sectionName.equalsIgnoreCase("") || sectionName==null || sectionName.equalsIgnoreCase("Totals")){
			continue;
		}else{
			
			if(sectionName.contains("Personal Accident Standard")){
				sectionName = "Personal Accident";
			}
			if(sectionName.contains("Goods In Transit")){
				sectionName = "Goods in Transit";
			}
			String expectedGP = (String)map_data.get("PS_"+sectionName.replaceAll(" ", "")+"_GP");
			String actualTotalGP = taxTable_tBody.findElement(By.xpath("tr["+(j+1)+"]/td[2]")).getText();
			GT =  Double.parseDouble((String)map_data.get("PS_"+sectionName.replaceAll(" ", "")+"_GT"));
			double expectedTotalGT = 0.0;
			
			if(((String)map_data.get("PD_TaxExempt")).equalsIgnoreCase("Yes")){
				expectedTotalGT =  0.0;
			}else{
				Iterator collectiveDataIT = AdjustedTaxDetails.entrySet().iterator();
				while(collectiveDataIT.hasNext()){
					Map.Entry collectiveAdjustedDetails = (Map.Entry)collectiveDataIT.next();
					String sectionNameofAjustedTax = collectiveAdjustedDetails.getKey().toString();
					
					if(sectionNameofAjustedTax.contains(sectionName.replaceAll(" ", ""))){
						AP =  Double.parseDouble(AdjustedTaxDetails.get(sectionName.replaceAll(" ", "")+"_AP"));
						AT =  Double.parseDouble(AdjustedTaxDetails.get(sectionName.replaceAll(" ", "")+"_AT"));
						UP = Double.parseDouble(expectedGP) - AP;
						UGT = UP * Double.parseDouble((String)map_data.get("PS_IPTRate"))/100.0;
						GT = UGT + AT;
						IPT = GT / Double.parseDouble(expectedGP) * 100.0;
						break;
					}
				
				}
				expectedTotalGT =  GT;
				IPT = GT / Double.parseDouble(expectedGP) * 100.0;
			}
			
			String actualTotalGT = taxTable_tBody.findElement(By.xpath("tr["+(j+1)+"]/td[5]")).getText();
			
			customAssert.assertTrue(CommonFunction.compareValues(Double.parseDouble(expectedGP), Double.parseDouble(actualTotalGP), "Gross Premium for "+sectionName+" - <b> [ New business to "+TestBase.businessEvent+" ] </b> flow."), "Unable to compare gross premium on Tax adjustment screen.");
			customAssert.assertTrue(CommonFunction.compareValues(expectedTotalGT, Double.parseDouble(actualTotalGT), "Gross Tax for "+sectionName+" - <b> [ New business to "+TestBase.businessEvent+" ] </b> flow."), "Unable to compare gross tax on Tax adjustment screen.");
			customAssert.assertTrue(WriteDataToXl(TestBase.product+"_"+common.currentRunningFlow, "Premium Summary", (String)map_data.get("Automation Key"), "PS_"+sectionName.replaceAll(" ", "")+"_IPT", common_HHAZ.roundedOff(Double.toString(IPT)),map_data),"Error while writing Policy Duration data to excel .");
			
		}
			
		taxTable_tBody = k.getObject("inssuranceTaxMainTableBODY");
		List<WebElement> list3 = taxTable_tBody.findElements(By.tagName("tr"));
		countOfCovers = list3.size();
	}
	
	return true;
	
}
@SuppressWarnings("static-access")
public static boolean verifyCoverDetails(){
	
	try {
		
		int count = 0 , count_datasheet = 0;
		String coverName = null , coverName_datasheet = null;
		try{
			k.ImplicitWaitOff();
			k.Click("insuranceTaxAddAdjustmentButton");
		}catch(Exception e){
			k.Click("insuranceTaxAddAdjustmentButton_1");
		}finally {
			k.ImplicitWaitOn();
		}
		
		List<WebElement> names = driver.findElements(By.tagName("option"));
		List<String> coversNameList = new ArrayList<>();
		String policy_status_actual = k.getText("Policy_status_header");
		String coverName_withoutSpace =null;
		
		for(int i=0;i<names.size();i++){
			coverName = names.get(i).getText();
			
			coverName_withoutSpace = coverName.replaceAll(" ", "");
			if(coverName_withoutSpace.equalsIgnoreCase("EmployersLiability") || coverName_withoutSpace.equalsIgnoreCase("PropertyOwnersLiability"))
			{coverName_withoutSpace = "Liability";}
			
			coversNameList.add(coverName_withoutSpace);
			if(common.currentRunningFlow.equalsIgnoreCase("NB")){
			
			String key = "CD_"+coverName_withoutSpace;
			if((policy_status_actual).contains("Rewind")){
				key = "CD_Add_"+coverName_withoutSpace;
			}
			
			if(common.NB_excel_data_map.get(key).toString().equalsIgnoreCase("Yes")){
				continue;
			}else{
				if(common.NB_excel_data_map.get("CD_"+coverName_withoutSpace.replaceAll(" ", "")).toString().equalsIgnoreCase("Yes")){
					
				}else{
					TestUtil.reportStatus("Cover Name <b>  ["+coverName+"]  </b> should not present in the dropdown list as This cover is not selected on Cover Details page.", "FAIL", false);
					count++;
				}
				
			}
			}else if(common.currentRunningFlow.equalsIgnoreCase("MTA")){
			String key = "CD_Add_"+coverName_withoutSpace;
							
			if(common.MTA_excel_data_map.get(key).toString().equalsIgnoreCase("Yes")){
				continue;
			}else{
				if(common.MTA_excel_data_map.get("CD_Add_"+coverName_withoutSpace.replaceAll(" ", "")).toString().equalsIgnoreCase("Yes")){
					
				}else{
					TestUtil.reportStatus("Cover Name <b>  ["+coverName+"]  </b> should not present in the dropdown list as This cover is not selected on Cover Details page.", "FAIL", false);
					count++;
				}
				
			}
		}
		else if(common.currentRunningFlow.equalsIgnoreCase("Renewal")){
			String key = "CD_"+coverName_withoutSpace;
							
			if(common.Renewal_excel_data_map.get(key).toString().equalsIgnoreCase("Yes")){
				continue;
			}else{
				if(common.Renewal_excel_data_map.get("CD_"+coverName_withoutSpace.replaceAll(" ", "")).toString().equalsIgnoreCase("Yes")){
					
				}else{
					TestUtil.reportStatus("Cover Name <b>  ["+coverName+"]  </b> should not present in the dropdown list as This cover is not selected on Cover Details page.", "FAIL", false);
					count++;
				}
				
			}
		}
		else if(common.currentRunningFlow.equalsIgnoreCase("Rewind")){
			String key = "CD_"+coverName_withoutSpace;
			
							
			if(common.Rewind_excel_data_map.get(key).toString().equalsIgnoreCase("Yes")){
				continue;
			}else{
				if(common.Rewind_excel_data_map.get("CD_"+coverName_withoutSpace.replaceAll(" ", "")).toString().equalsIgnoreCase("Yes")){
					
				}else{
					TestUtil.reportStatus("Cover Name <b>  ["+coverName+"]  </b> should not present in the dropdown list as This cover is not selected on Cover Details page.", "FAIL", false);
					count++;
				}
				
			}
		}
		else if(common.currentRunningFlow.equalsIgnoreCase("Requote")){
			String key = "CD_"+coverName_withoutSpace;
							
			if(common.Requote_excel_data_map.get(key).toString().equalsIgnoreCase("Yes")){
				continue;
			}else{
				if(common.Requote_excel_data_map.get("CD_"+coverName_withoutSpace.replaceAll(" ", "")).toString().equalsIgnoreCase("Yes")){
					
				}else{
					TestUtil.reportStatus("Cover Name <b>  ["+coverName+"]  </b> should not present in the dropdown list as This cover is not selected on Cover Details page.", "FAIL", false);
					count++;
				}
				
			}
		}
	}
		for(int p=0;p<common.CoversDetails_data_list.size();p++){
			coverName_datasheet = common.CoversDetails_data_list.get(p);
			
			if(coversNameList.contains(coverName_datasheet)){
				continue;
			}else{
				TestUtil.reportStatus("Cover Name <b>  ["+coverName_datasheet+"]  </b> is selected as 'NO' in datasheet but still listed in the dropdown list.", "FAIL", false);
				count_datasheet++;
			}
		}
		
		WebElement adjustmentTax = k.getObject("insuranceTaxAddAdjustmentTable");
		customAssert.assertTrue(k.SelectBtnWebElement(adjustmentTax, "insuranceTaxAddAdjustmentSaveCancleButton", "Cancel"), "Unable to select Cancel button.");
		
		if(count==0 && count_datasheet==0){
			TestUtil.reportStatus("<b> Verified covers present in dropdown list of Adjustment Tax table. </b>", "Info", false);
		}
		
		return true;
	
	}catch (Throwable t) {
		
		return false;
	}
}

public boolean funcInsuredProperties(Map<Object, Object> map_data){
	Map<String, List<Map<String, String>>> internal_data_map = new HashMap<>();
	//Map<Object, Object> event_data_map = 
	
	switch(common.currentRunningFlow){
	
	case "NB":
		internal_data_map=common.NB_Structure_of_InnerPagesMaps;
		break;
	case "MTA":
		internal_data_map= common.MTA_Structure_of_InnerPagesMaps;
		break;
	case "Renewal":
		internal_data_map = common.Renewal_Structure_of_InnerPagesMaps;
		break;
	case "Rewind":
		internal_data_map = common.Rewind_Structure_of_InnerPagesMaps;
		break;
	case "Requote":
		internal_data_map = common.Requote_Structure_of_InnerPagesMaps;
		break;
	
	}
	MD_TotalPremium = 0.0;
	LOI_TotalPremium = 0.0;
	boolean r_Value = true;
	try{
		customAssert.assertTrue(common.funcPageNavigation("Insured Properties", ""),"Insured Properties page is having issue(S)");
		if(!common.currentRunningFlow.equalsIgnoreCase("NB")){
			
			if(TestBase.businessEvent.equalsIgnoreCase("MTA") && ((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes")){
			
			try {	
				switch((String)map_data.get("MTA_Operation")) {
    	  	
				case "AP":
				case "RP":
	    		  
					String _cover = common_EP.AP_RP_Cover_Key.split("-")[1];
	    		  
					if(!_cover.contains("MaterialDamage")) {
						common.MTA_excel_data_map.put("PS_MaterialDamage_NP", common_PEN.MD_Premium);
						TestUtil.reportStatus("Material Damage Net Premium captured successfully . ", "Info", true);
						return true;
					}
					break;
				case "Non-Financial":
	    		 
	    			  	common.MTA_excel_data_map.put("PS_MaterialDamage_NP", common_PEN.MD_Premium);
	    			  	common.MTA_excel_data_map.put("PS_LossOfRentalIncome_NP", common_PEN.LRI_Premium);
	    			  	TestUtil.reportStatus("Due to Non-Financial Flow, Only Material Damage Net Premium captured  . ", "Info", true);
	    			  	TestUtil.reportStatus("Due to Non-Financial Flow, Only LossOfRentalIncome Net Premium captured  . ", "Info", true);
	    			  	return true;
	    		}
			}catch(NullPointerException npe) {
				
			}
			}
			
			  customAssert.assertTrue(common_HHAZ.deleteItems(),"Delete Items function is having issues.");
	    }
		customAssert.assertTrue(k.Input("CCF_IP_AnyOneEvent", (String)map_data.get("IP_AnyOneEvent")),	"Unable to enter value in any one Event field .");
		customAssert.assertTrue(k.Input("IP_Landslip", (String)map_data.get("IP_Landslip")),"Unable to enter value in Subsidence Ground Heave or Landslip field .");
		
		customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Insured Properties .");
		
		int count = 0;
		int noOfProperties = 0;
		if(common.no_of_inner_data_sets.get("Property Details")==null){
			noOfProperties = 0;
		}else{
			noOfProperties = common.no_of_inner_data_sets.get("Property Details");
		}
		while(count < noOfProperties ){
			
			customAssert.assertTrue(k.Click("CCF_Btn_AddProperty"), "Unable to click Add Property Button on Insured Properties .");
			customAssert.assertTrue(addProperty(map_data,count),"Error while adding insured proprty  .");
			TestUtil.reportStatus("Insured Property  <b>[  "+internal_data_map.get("Property Details").get(count).get("Automation Key")+"  ]</b>  added successfully . ", "Info", true);
			customAssert.assertTrue(k.Click("CCF_Btn_Back"), "Unable to click on Back Button on Property Details .");
			count++;
		}
		
		TestUtil.reportStatus("All the specified Insured properties added and verified successfully . ", "Info", true);
		
		return r_Value;
		
		
	}catch(Throwable t) {
        String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
        TestUtil.reportFunctionFailed("Failed in "+methodName+" function");
        Assert.fail("Insured Properties function is having issue(S). \n", t);
        return false;
 }
}

public boolean addProperty(Map<Object, Object> map_data,int count){
	
	boolean r_value=true;
	
	Map<String, List<Map<String, String>>> internal_data_map = new HashMap<>();
	//Map<Object, Object> event_data_map = 
	
	switch(common.currentRunningFlow){
	
	case "NB":
		internal_data_map=common.NB_Structure_of_InnerPagesMaps;
		break;
	case "MTA":
		internal_data_map= common.MTA_Structure_of_InnerPagesMaps;
		break;
	case "Renewal":
		internal_data_map = common.Renewal_Structure_of_InnerPagesMaps;
		break;
	case "Rewind":
		internal_data_map = common.Rewind_Structure_of_InnerPagesMaps;
		break;
	case "Requote":
		internal_data_map = common.Requote_Structure_of_InnerPagesMaps;
		break;
	
	}
	
	try{
	
		customAssert.assertTrue(common.funcPageNavigation("Property Details", ""),"Property Details page navigation issue(S)");
		if(!(internal_data_map.get("Property Details").get(count).get("PoD_CopyAddress")).equalsIgnoreCase("Yes")){
			customAssert.assertTrue(k.Input("CCF_Address_CC_Address", internal_data_map.get("Property Details").get(count).get("PoD_Address")),"Unable to enter value in Address field. ");
			customAssert.assertTrue(!k.getAttributeIsEmpty("CCF_Address_CC_Address", "value"),"Address Field Should Contain Valid Value on Client Details .");
			customAssert.assertTrue(k.Input("CCF_Address_CC_line2", internal_data_map.get("Property Details").get(count).get("PoD_AddressL2")),"Unable to enter value in Address field line 2 . ");
			customAssert.assertTrue(k.Input("CCF_Address_CC_line3", internal_data_map.get("Property Details").get(count).get("PoD_AddressL3")),"Unable to enter value in Address field line 3 . ");
			customAssert.assertTrue(k.Input("CCF_Address_CC_Town", internal_data_map.get("Property Details").get(count).get("PoD_Town")),"Unable to enter value in Town field . ");
			customAssert.assertTrue(k.Input("CCF_Address_CC_County", internal_data_map.get("Property Details").get(count).get("PoD_County")),"Unable to enter value in County  . ");
			customAssert.assertTrue(k.Input("CCF_Address_CC_Postcode", internal_data_map.get("Property Details").get(count).get("PoD_Postcode")),"Unable to enter value in PostCode field .");
			customAssert.assertTrue(!k.getAttributeIsEmpty("CCF_Address_CC_Postcode", "value"),"Postcode Field Should Contain Valid Value on Client Details .");
			customAssert.assertTrue(common.validatePostCode(internal_data_map.get("Property Details").get(count).get("PoD_Postcode")),"Post Code is not in Correct format .");
		}else{
			customAssert.assertTrue(k.Click("CCF_Btn_CopyCorAddress"));
		}
		
		customAssert.assertTrue(k.Input("CCF_PoD_PropertyAge", internal_data_map.get("Property Details").get(count).get("PoD_PropertyAge")),"Unable to enter value in Age of Property (years) . ");
		customAssert.assertTrue(k.DropDownSelection("CCF_PoD_TerrorismZone", internal_data_map.get("Property Details").get(count).get("PoD_TerrorismZone")), "Unable to select value from Terrorism Zone dropdown .");
		
		//Statement of Fact
		customAssert.assertTrue(k.SelectRadioBtn("CCF_PoD_SOF_Q1", internal_data_map.get("Property Details").get(count).get("PoD_SOF_Q1")), "Unable to Select first SOF radio button on Policy Details Page.");
		customAssert.assertTrue(k.SelectRadioBtn("CCF_PoD_SOF_Q2", internal_data_map.get("Property Details").get(count).get("PoD_SOF_Q2")), "Unable to Select second SOF radio button on Policy Details Page.");
		customAssert.assertTrue(k.SelectRadioBtn("CCF_PoD_SOF_Q4", internal_data_map.get("Property Details").get(count).get("PoD_SOF_Q4")), "Unable to Select fourth SOF radio button on Policy Details Page.");
		
		//Property Certificate
		customAssert.assertTrue(k.SelectRadioBtn("POB_PoD_PC_Q1", internal_data_map.get("Property Details").get(count).get("PoD_SOF_Q1")), "Unable to Select first SOF radio button on Policy Details Page.");
		if((internal_data_map.get("Property Details").get(count).get("PoD_SOF_Q1")).equalsIgnoreCase("True")){
			customAssert.assertTrue(k.Input("POB_PoD_PC_Occupancy", internal_data_map.get("Property Details").get(count).get("PoD_PC_Occupancy")),"Unable to enter value in Property Certificate Occupancy  . ");
		}
		customAssert.assertTrue(k.Input("POB_PoD_PC_Premium", internal_data_map.get("Property Details").get(count).get("PoD_PC_Premium")),"Unable to enter value in Property Certificate Premium  . ");
		customAssert.assertTrue(k.Input("POB_PoD_PC_IPT", internal_data_map.get("Property Details").get(count).get("PoD_PC_IPT")),"Unable to enter value in Property Certificate IPT  . ");
		customAssert.assertTrue(k.Input("POB_PoD_PC_TotalPremium", internal_data_map.get("Property Details").get(count).get("PoD_PC_TotalPremium")),"Unable to enter value in Property Certificate Total Premium  . ");
		
		
		//Proximity
		customAssert.assertTrue(k.Input("CCF_PoD_ProximityDescription", internal_data_map.get("Property Details").get(count).get("PoD_ProximityDescription")),"Unable to enter value in Proximity description . ");
		
		//Trade Code
		if((internal_data_map.get("Property Details").get(count).get("PoD_TCS_TradeCode_Button")).equalsIgnoreCase("Yes")){
			customAssert.assertTrue(common.tradeCodeSelection((String)internal_data_map.get("Property Details").get(count).get("PoD_MD_TCS_TradeCode"),"Property Details" , count),"Trade code selection function is having issue(S).");	
		}
		
		//EML
		customAssert.assertTrue(k.Input("CCF_PoD_EmlAmount_GBP", internal_data_map.get("Property Details").get(count).get("PoD_EmlAmount_GBP")),"Unable to enter value in EmlAmount_GBP . ");
		customAssert.assertTrue(!k.getAttributeIsEmpty("CCF_PoD_EmlAmount_GBP", "value"),"Eml amount (GBP) Field Should Contain Valid Value on Property Details .");
		customAssert.assertTrue(k.Input("CCF_PoD_EmlAmount_Percent", internal_data_map.get("Property Details").get(count).get("PoD_EmlAmount_Percent")),"Unable to enter value in Eml amount (%) . ");
		
		//Inner MD-Bespoke Sum Insured
		//customAssert.assertTrue(addMD_BIBespokeSumInsured(map_data), "Error while adding Bespoke data . ");
		List<WebElement> bespoke_MD_btns = k.getWebElements("CCF_Btn_AddBespokeSumIns");
		WebElement MD_bespoke_btn = bespoke_MD_btns.get(0);
		MD_bespoke_btn.click();
		customAssert.assertTrue(k.Input("CCF_BSI_MD_Description", internal_data_map.get("Property Details").get(count).get("BSI_MD_Description")),"Unable to enter value in Bespoke Description . ");
		customAssert.assertTrue(k.Input("CCF_BSI_MD_SumInsured", internal_data_map.get("Property Details").get(count).get("BSI_MD_DeclaredValue")),"Unable to enter value in Bespoke Sum Insured . ");
		customAssert.assertTrue(k.clickInnerButton("CCF_inner_page_locator", "Save"), "Unable to click on Save Button on Bespoke Sum Insured inner page .");
		
		if(((String)map_data.get("CD_LossOfRentalIncome")).equalsIgnoreCase("Yes")){
			List<WebElement> bespoke_BI_btns = k.getWebElements("CCF_Btn_AddBespokeSumIns");
			WebElement BI_bespoke_btn = bespoke_BI_btns.get(1);
			BI_bespoke_btn.click();
			customAssert.assertTrue(k.Input("CCF_BSI_MD_Description", internal_data_map.get("Property Details").get(count).get("BSI_LOI_Description")),"Unable to enter value in BI Bespoke Description . ");
			customAssert.assertTrue(k.Input("CCF_BSI_MD_SumInsured", internal_data_map.get("Property Details").get(count).get("BSI_LOI_DeclaredValue")),"Unable to enter value in Bespoke Sum Insured . ");
			customAssert.assertTrue(k.DropDownSelection("CCF_BSI_BI_IndemnityPeriod", internal_data_map.get("Property Details").get(count).get("BSI_LOI_IndemnityPeriod")), "Unable to select value from Indemnity Period dropdown .");
			customAssert.assertTrue(k.clickInnerButton("CCF_inner_page_locator", "Save"), "Unable to click on Save Button on Bespoke Sum Insured inner page .");

		}
		int tableIndex =0; 
		
		tableIndex = k.getTableIndex("Declared Value","xpath"," html/body/div[3]/form/div/table ");
		customAssert.assertTrue(inputTableData(count, "MD", tableIndex));
		customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Insured Properties .");
		customAssert.assertTrue(calculatePremium(count, "MD", tableIndex));
		TestUtil.WriteDataToXl(CommonFunction.product+"_"+CommonFunction.businessEvent, "Premium Summary",(String)map_data.get("Automation Key"), "PS_MaterialDamage_NP", Double.toString(MD_TotalPremium), map_data);
		
		if(((String)map_data.get("CD_LossOfRentalIncome")).equalsIgnoreCase("Yes")){
			tableIndex = k.getTableIndex("Declaration Uplift (%)","xpath"," html/body/div[3]/form/div/table ");
			customAssert.assertTrue(inputTableData(count, "LOI", tableIndex));
			customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Insured Properties .");
			customAssert.assertTrue(calculatePremium(count, "LOI", tableIndex));
			TestUtil.WriteDataToXl(CommonFunction.product+"_"+CommonFunction.businessEvent, "Premium Summary",(String)map_data.get("Automation Key"), "PS_LossOfRentalIncome_NP", Double.toString(LOI_TotalPremium), map_data);
		}
		customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Insured Properties .");
		
	}catch(Throwable t){
		return false;
	}
	
	
	
	return r_value;
}


public boolean inputTableData(int count , String coverInitial , int tableIndex) {
	
	Map<String, List<Map<String, String>>> internal_data_map = new HashMap<>();
	Map<Object, Object> map_data = new HashMap<>();
	//Map<Object, Object> event_data_map = 
	
	switch(common.currentRunningFlow){
	
	case "NB":
		internal_data_map=common.NB_Structure_of_InnerPagesMaps;
		map_data = common.NB_excel_data_map;
		break;
		
	case "MTA":
		internal_data_map= common.MTA_Structure_of_InnerPagesMaps;
		map_data = common.MTA_excel_data_map;
		break;
	
	case "Renewal":
		internal_data_map = common.Renewal_Structure_of_InnerPagesMaps;
		map_data = common.Renewal_excel_data_map;
		break;
	case "Rewind":
		internal_data_map = common.Rewind_Structure_of_InnerPagesMaps;
		map_data = common.Rewind_excel_data_map;
		break;
	case "Requote":
		internal_data_map = common.Requote_Structure_of_InnerPagesMaps;
		map_data = common.Requote_excel_data_map;
		break;
	
	}
	
	String s_CoverName = coverInitial,AP_RP_Flag = "";
	 //To Handle AP RP functionality
	 String coverPremium = "0.0";
		
		switch(s_CoverName.replaceAll(" ", "")) {
			case "MD":
				coverPremium = common_PEN.MD_Premium;
				s_CoverName = "MaterialDamage";
			break;
			case "LOI":
				coverPremium = common_PEN.LRI_Premium; // Loss of Rental Income
				s_CoverName = "LossOfRentalIncome";
				break;
		}
		
		/**
		 * 
		 *
		 *  To manage AP(Additional Premium) or RP(Reduced Premium) for MTA flow.
		 * 
		 * 
		 */
		
		
		if(common.currentRunningFlow.equalsIgnoreCase("MTA")){
			
			String AP_RP_Key = (String)map_data.get("CD_AP_RP_CoverSpecific_Decision");
			
			if(!(AP_RP_Key.equalsIgnoreCase(""))){
				String[] AP_RP_Array = AP_RP_Key.split(",");
				
				for(String cover : AP_RP_Array){
					
					String[] splitCoverNameFormat = cover.split("-");
					
					if(splitCoverNameFormat[1].equalsIgnoreCase(s_CoverName.replaceAll(" ", ""))){
						
						if(splitCoverNameFormat[0].equalsIgnoreCase("AP")){
							AP_RP_Flag = "AP";
							TestUtil.reportStatus("<b>------ Additional Premium for Cover - "+s_CoverName+" -------------</b>", "Info", false);
							break;
						}else if(splitCoverNameFormat[0].equalsIgnoreCase("RP")){
							AP_RP_Flag = "RP";
							TestUtil.reportStatus("<b>------- Reduced Premium for Cover - "+s_CoverName+" -------------</b>", "Info", false);
							break;
						}
					}
					
				}
			}
		}
	
	List<WebElement> listOfRows = driver.findElements(By.xpath("html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr"));
	int innerBeSpokeCount = 1;//common.no_of_inner_data_sets("Be Spkoe column name"); ---> this can be used in case of Multiple Bespoke item added.
	for(int i=0;i<(innerBeSpokeCount+listOfRows.size()-1)-1;i++){
		String Abvr = "";
		String sectionName = driver.findElement(By.xpath("html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[1]")).getText();
		if(sectionName.equalsIgnoreCase(internal_data_map.get("Property Details").get(count).get("BSI_MD_Description"))){
			Abvr = "BSI_MD_";
		}else if(sectionName.equalsIgnoreCase(internal_data_map.get("Property Details").get(count).get("BSI_LOI_Description"))){
			Abvr = "BSI_LOI_";
		}else{
			Abvr = CommonFunction.func_GetAbrrivation(coverInitial, sectionName);
			if(!AP_RP_Flag.equals("") && Double.parseDouble(coverPremium) != 0.0){
				double DV = CommonFunction_CCF.get_MD_BI_DeclaredValue(AP_RP_Flag,s_CoverName);
				customAssert.assertTrue(k.DynamicXpathWebDriver_Inner_Value(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[2]/input", internal_data_map , "Property Details" , count , DV, Abvr ,"DeclaredValue",sectionName,"Input"),"Unable to enter DeclaredValue for : " + sectionName);
				internal_data_map.get("Property Details").get(count).put(Abvr+"DeclaredValue",Double.toString(DV));
    		}else {
    			customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[2]/input" ,internal_data_map , "Property Details", count , Abvr , "DeclaredValue", sectionName,"Input"),"BAC");
    		}
		}
			if(coverInitial.equalsIgnoreCase("MD")){
				
				customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[4]/input" , internal_data_map , "Property Details", count , Abvr , "FireRate",sectionName, "Input"),"BAC");
	    		customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[5]/input" , internal_data_map , "Property Details", count , Abvr , "PerilsRate",sectionName, "Input"),"BAC");
	    		customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[6]/input" , internal_data_map , "Property Details", count , Abvr , "SprinkRate",sectionName, "Input"),"BAC");
	    		customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[7]/input" , internal_data_map , "Property Details", count , Abvr , "ADRate", sectionName,"Input"),"BAC");
	    		customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[8]/input" , internal_data_map , "Property Details", count , Abvr , "SubsRate",sectionName, "Input"),"BAC");
	    		customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[10]/input", internal_data_map , "Property Details", count , Abvr , "TechAdjust",sectionName, "Input"),"BAC");
	    		customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[11]/input" , internal_data_map , "Property Details", count , Abvr , "CommAdjust", sectionName,"Input"),"BAC");
	    		
			}
			if(coverInitial.equalsIgnoreCase("LOI")){
				
				customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[5]/input" , internal_data_map , "Property Details", count , Abvr , "FireRate",sectionName, "Input"),"BAC");
	    		customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[6]/input" , internal_data_map , "Property Details", count , Abvr , "PerilsRate",sectionName, "Input"),"BAC");
	    		customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[7]/input" , internal_data_map , "Property Details", count , Abvr , "SprinkRate",sectionName, "Input"),"BAC");
	    		customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[8]/input" , internal_data_map , "Property Details", count , Abvr , "ADRate", sectionName,"Input"),"BAC");
	    		customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[9]/input" , internal_data_map , "Property Details", count , Abvr , "SubsRate",sectionName, "Input"),"BAC");
	    		customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[11]/input", internal_data_map , "Property Details", count , Abvr , "TechAdjust",sectionName, "Input"),"BAC");
	    		customAssert.assertTrue(k.DynamicXpathWebDriver_Inner(driver, "html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[12]/input" , internal_data_map , "Property Details", count , Abvr , "CommAdjust", sectionName,"Input"),"BAC");
			}
	}
	return true;
}

public boolean calculatePremium(int count , String coverInitial , int tableIndex) throws Throwable, Exception {
	
	Map<String, List<Map<String, String>>> internal_data_map = new HashMap<>();
	//Map<Object, Object> event_data_map = 
	
	switch(common.currentRunningFlow){
	
	case "NB":
		internal_data_map=common.NB_Structure_of_InnerPagesMaps;
		break;
	case "MTA":
		internal_data_map= common.MTA_Structure_of_InnerPagesMaps;
		break;
	case "Renewal":
		internal_data_map = common.Renewal_Structure_of_InnerPagesMaps;
		break;
	case "Rewind":
		internal_data_map = common.Rewind_Structure_of_InnerPagesMaps;
		break;
	case "Requote":
		internal_data_map = common.Requote_Structure_of_InnerPagesMaps;
		break;
	
	}
	
	List<WebElement> listOfRows = driver.findElements(By.xpath("html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr"));
	double MD_Premium = 0.0 , LOI_Premium = 0.0;
	int innerBeSpokeCount = 1;//common.no_of_inner_data_sets("Be Spkoe column name"); ---> this can be used in case of Multiple Bespoke item added.
	for(int i=0;i<(innerBeSpokeCount+listOfRows.size()-1)-1;i++){
		String Abvr = "";
		JavascriptExecutor j_exe = (JavascriptExecutor) driver;
		j_exe.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[1]")));
		String sectionName = driver.findElement(By.xpath("html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[1]")).getText();
		if(sectionName.equalsIgnoreCase(internal_data_map.get("Property Details").get(count).get("BSI_MD_Description"))){
			Abvr = "BSI_MD_";
		}else if(sectionName.equalsIgnoreCase(internal_data_map.get("Property Details").get(count).get("BSI_LOI_Description"))){
			Abvr = "BSI_LOI_";
		}else{
			Abvr = CommonFunction.func_GetAbrrivation(coverInitial, sectionName);
		}
		if(coverInitial.equalsIgnoreCase("MD")){
			
			String actPremium = driver.findElement(By.xpath("html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[13]/input")).getAttribute("value");
			double totalRate = Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"FireRate")) + 
					Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"PerilsRate")) + 
					Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"SprinkRate")) + 
					Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"ADRate")) + 
					Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"SubsRate"));
			double commAdjustment = ((totalRate * Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"TechAdjust"))) / 100.0 ) + totalRate ; 
			double adjustedRate =  (commAdjustment * Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"CommAdjust")) / 100.0 ) + commAdjustment;
			String expPremium = Double.toString(((Double.parseDouble(WholeNumberRoundedOff(internal_data_map.get("Property Details").get(count).get(Abvr+"DeclaredValue")).split("\\.")[0])) * adjustedRate ) / 100.0);
			CommonFunction.compareValues(Double.parseDouble(expPremium),Double.parseDouble(actPremium),sectionName+" - MD premium");
			MD_Premium = MD_Premium + Double.parseDouble(expPremium);
			
			if(driver.findElement(By.xpath("html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+2)+"]/td[12]")).getText().equalsIgnoreCase("Total")){
				String actTotalPremium = driver.findElement(By.xpath("html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+2)+"]/td[13]/input")).getAttribute("value");
				String expTotalPremium = Double.toString(MD_Premium);
				CommonFunction.compareValues(Double.parseDouble(expTotalPremium),Double.parseDouble(actTotalPremium),"Property-"+(count+1)+" Total MD premium");
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+CommonFunction.businessEvent, "Property Details", internal_data_map.get("Property Details").get(count).get("Automation Key"), "MD_TotalPremium", Double.toString(MD_Premium),internal_data_map.get("Property Details").get(count));
				MD_TotalPremium = MD_TotalPremium + MD_Premium;
			}
			
		}
		if(coverInitial.equalsIgnoreCase("LOI")){
			
			String actPremium = driver.findElement(By.xpath("html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+1)+"]/td[14]/input")).getAttribute("value");
			double totalRate = Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"FireRate")) + 
					Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"PerilsRate")) + 
					Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"SprinkRate")) + 
					Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"ADRate")) + 
					Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"SubsRate"));
			double commAdjustment = ((totalRate * Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"TechAdjust"))) / 100.0 ) + totalRate ; 
			double adjustedRate =  (commAdjustment * Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"CommAdjust")) / 100.0 ) + commAdjustment;
			//String expPremium = common.roundedOff(Double.toString(((Double.parseDouble(internal_data_map.get("Property Details").get(count).get(Abvr+"DeclaredValue")) * adjustedRate ) / 100.0)));
			String expPremium = Double.toString(((Double.parseDouble(WholeNumberRoundedOff(internal_data_map.get("Property Details").get(count).get(Abvr+"DeclaredValue")).split("\\.")[0])) * adjustedRate ) / 100.0);
			CommonFunction.compareValues(Double.parseDouble(expPremium),Double.parseDouble(actPremium),sectionName+" - LOI premium");
			LOI_Premium = LOI_Premium + Double.parseDouble(expPremium);
			
			if(driver.findElement(By.xpath("html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+2)+"]/td[13]")).getText().equalsIgnoreCase("Total")){
				String actTotalPremium = driver.findElement(By.xpath("html/body/div[3]/form/div/table["+tableIndex+"]/tbody/tr["+(i+2)+"]/td[14]/input")).getAttribute("value");
				String expTotalPremium = Double.toString(LOI_Premium);
				CommonFunction.compareValues(Double.parseDouble(expTotalPremium),Double.parseDouble(actTotalPremium),"Property-"+(count+1)+" Total LOI premium");
				TestUtil.WriteDataToXl_innerSheet(CommonFunction.product+"_"+CommonFunction.businessEvent, "Property Details", internal_data_map.get("Property Details").get(count).get("Automation Key"), "LOI_TotalPremium", Double.toString(LOI_Premium),internal_data_map.get("Property Details").get(count));
				LOI_TotalPremium = LOI_TotalPremium + LOI_Premium;
			}
		}
	}
	return true;
}


public boolean addMD_BIBespokeSumInsured(Map<Object, Object> map_data){
	boolean r_value=true;
	
	try{
		
		int total_count_MD_bespoke = common.no_of_inner_data_sets.get("BS Insured MD");
		int count=0;
		while(count < total_count_MD_bespoke){
			List<WebElement> bespoke_btns = k.getWebElements("CCF_Btn_AddBespokeSumIns");
			System.out.println(bespoke_btns.size());
			WebElement MD_bespoke_btn = bespoke_btns.get(0);
			MD_bespoke_btn.click();
			k.waitTwoSeconds();
			
			customAssert.assertTrue(k.Input("CCF_BSI_MD_Description", common.NB_Structure_of_InnerPagesMaps.get("BS Insured MD").get(count).get("BSI_MD_Description")),"Unable to enter value in Bespoke Description . ");
			customAssert.assertTrue(k.Input("CCF_BSI_MD_SumInsured", Keys.chord(Keys.CONTROL, "a")),"Unable to select Bespoke Sum Insured field");
			customAssert.assertTrue(k.Input("CCF_BSI_MD_SumInsured", common.NB_Structure_of_InnerPagesMaps.get("BS Insured MD").get(count).get("BSI_MD_SumInsured")),"Unable to enter value in Bespoke Sum Insured . ");
			
			customAssert.assertTrue(k.clickInnerButton("CCF_inner_page_locator", "Save"), "Unable to click on Save Button on Bespoke Sum Insured inner page .");
			count++;
		}
		
		if(((String)map_data.get("CD_LossOfRentalIncome")).equals("Yes")){
			int total_count_BI_bespoke = common.no_of_inner_data_sets.get("BS Insured LOI");
			count=0;
			while(count < total_count_BI_bespoke){
				k.Click("CCF_Btn_BI_Bespoke");
				k.waitTwoSeconds();
			
				customAssert.assertTrue(k.Input("CCF_BSI_MD_Description", common.NB_Structure_of_InnerPagesMaps.get("BS Insured LOI").get(count).get("BSI_LOI_Description")),"Unable to enter value in BI Bespoke Description . ");
				customAssert.assertTrue(k.Input("CCF_BSI_MD_SumInsured", Keys.chord(Keys.CONTROL, "a")),"Unable to select Bespoke Sum Insured field");
				customAssert.assertTrue(k.Input("CCF_BSI_MD_SumInsured", common.NB_Structure_of_InnerPagesMaps.get("BS Insured LOI").get(count).get("BSI_LOI_SumInsured")),"Unable to enter value in Bespoke Sum Insured . ");
				customAssert.assertTrue(k.DropDownSelection("CCF_BSI_BI_IndemnityPeriod", common.NB_Structure_of_InnerPagesMaps.get("BS Insured LOI").get(count).get("BSI_LOI_IndemnityPeriod")), "Unable to select value from Indemnity Period dropdown .");
				customAssert.assertTrue(k.clickInnerButton("CCF_inner_page_locator", "Save"), "Unable to click on Save Button on Bespoke Sum Insured inner page .");
				count++;
			}
		}
		
		
	}catch(Throwable t){
		
		r_value=false;
	}
	
	return r_value;
	
}


public boolean funcPropertyOwnersLiability(Map<Object, Object> map_data){
	
	boolean r_value= true;
	
	try{
		
		customAssert.assertTrue(common.funcPageNavigation("Property Owners Liability", ""),"Employers Liability page navigations issue(S)");
		if(!common.currentRunningFlow.equalsIgnoreCase("NB")){
	    	  
	    	  JavascriptExecutor j_exe = (JavascriptExecutor) driver;
			  j_exe.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[contains(@id,'_tot')]")));
			  common_PEN.POL_Premium = driver.findElement(By.xpath("//*[contains(@id,'_tot')]")).getAttribute("value");
	    	  
	    	  if(TestBase.businessEvent.equalsIgnoreCase("MTA") && ((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes")){
	   	  
	    	  try {	  
	    		  switch((String)map_data.get("MTA_Operation")) {
	    	  	
	    		  case "AP":
	    		  case "RP":
	    		  
	    			  String _cover = common_EP.AP_RP_Cover_Key.split("-")[1];
	    		  
	    		 	if(!_cover.contains("PropertyOwnersLiability")) {
	    		 		common.MTA_excel_data_map.put("PS_PropertyOwnersLiability_NP", common_PEN.POL_Premium);
	    		 		TestUtil.reportStatus("Property Owners Liability Net Premium captured successfully . ", "Info", true);
	    		 		return true;
	    		 	}
	    		 	break;
	    		 
	    		  case "Policy-level":
	    		  
	    			  break;
	    		 
	    		  case "Non-Financial":
	   		 
	    			  common.MTA_excel_data_map.put("PS_PropertyOwnersLiability_NP", common_PEN.POL_Premium);
	    			  TestUtil.reportStatus("Due to Non-Financial Flow, Only Property Owners Liability Net Premium captured  . ", "Info", true);
	    			  return true;
	    		
	    		  }
	    	  }catch(NullPointerException npe) {
					
				}
	    	  }
	    	  customAssert.assertTrue(common_HHAZ.deleteItems(),"Delete Items function is having issues.");
	      }
		
		customAssert.assertTrue(k.Input("POB_POL_LiabilityLimit", (String)map_data.get("POL_IndemnityLimit")),"Unable to enter value in Property Owners Liability Limit . ");
		customAssert.assertTrue(k.Input("POB_POL_LiabilityExcess", (String)map_data.get("POL_LiabilityExcess")),"Unable to enter value in Property Owners Liability Excess . ");
		
		//Inner BI-Specified Suppliers
		customAssert.assertTrue(addPOLItems(), "Error while adding PL Items . ");
		
		customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Public Liability .");
		
		double sPremiumm = CommonFunction.func_AllCvers_HandleTables( map_data, "POL", "Property Owners Liability", "POL AddItem");
		TestUtil.WriteDataToXl(CommonFunction.product+"_"+CommonFunction.businessEvent, "Premium Summary",(String)map_data.get("Automation Key"), "PS_PropertyOwnersLiability_NP", Double.toString(sPremiumm), map_data);
		
		
		TestUtil.reportStatus("Property Owners Liability details are filled sucessfully . ", "Info", true);
		
	}catch(Throwable t){
		return false;
		
	}
	
	return r_value;
	}

public boolean addPOLItems(){
	boolean r_value=true;
	
	try{
		Map<String, List<Map<String, String>>> data_map = null;
		switch(common.currentRunningFlow){
		case "NB":
			data_map = common.NB_Structure_of_InnerPagesMaps;
		break;
		case "CAN":
			data_map = common.CAN_Structure_of_InnerPagesMaps;
		break;
		case "MTA":
			data_map = common.MTA_Structure_of_InnerPagesMaps;
		break;
		case "Renewal":
			data_map = common.Renewal_Structure_of_InnerPagesMaps;
		break;
		case "Rewind":
			data_map = common.Rewind_Structure_of_InnerPagesMaps;
		break;
		case "Requote":
			data_map = common.Requote_Structure_of_InnerPagesMaps;
		break;
	}
	
		int total_count_PL_items = common.no_of_inner_data_sets.get("POL AddItem");
		int count=0;
		while(count < total_count_PL_items){
			
			customAssert.assertTrue(k.Click("CCF_Btn_AddItem"), "Unable to click on Add Item Button on PL page .");
			k.waitTwoSeconds();
			
			customAssert.assertTrue(k.Input("CCF_BSI_MD_Description", data_map.get("POL AddItem").get(count).get("AD_POL_ItemDesc")),"Unable to enter value in Description field. ");
			customAssert.assertTrue(k.Input("CCF_BSI_MD_SumInsured", data_map.get("POL AddItem").get(count).get("AD_POL_ItemSumIns")),"Unable to enter value in Sum Insured field. ");
			
			customAssert.assertTrue(k.clickInnerButton("CCF_inner_page_locator", "Save"), "Unable to click on Save Button on Specified Customer inner page .");
			count++;
		}
		
	}catch(Throwable t){
		
		r_value=false;
	}
	
	return r_value;
	
}

public boolean funcLiabilityInformation(Map<Object, Object> map_data){
	
	boolean r_value= true;
	
	try{
		
		customAssert.assertTrue(common.funcPageNavigation("Property Owners Liability Information", ""),"Liability Information page navigations issue(S)");
		
		//Statement of Fact
		customAssert.assertTrue(k.SelectRadioBtn("POB_POLI_SOF_Q1", (String)map_data.get("LI_SOF_Q1")), "Unable to Select Liability Information MF Q1 radio button .");
		customAssert.assertTrue(k.SelectRadioBtn("POB_POLI_SOF_Q2", (String)map_data.get("LI_SOF_Q2")), "Unable to Select Liability Information MF Q2 radio button .");
		customAssert.assertTrue(k.SelectRadioBtn("POB_POLI_SOF_Q3", (String)map_data.get("LI_SOF_Q3")), "Unable to Select Liability Information MF Q3 radio button .");
			
		customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Insured Properties .");
		
		TestUtil.reportStatus("Liability Information details are filled sucessfully . ", "Info", true);
		
	}catch(Throwable t){
		return false;
		
	}
	
	return r_value;
	}

public boolean funcRewindOperation(){
	
	boolean r_value= true;
	
	try{
		
		if(((String)common.NB_excel_data_map.get("CD_Add_Remove_Cover")).equalsIgnoreCase("Yes")){
			customAssert.assertTrue(common.funcMenuSelection("Navigate","Covers"),"Issue while Navigating to Covers  . ");
			customAssert.assertTrue(common.funcRewindCoversCheck(common.NB_excel_data_map), "Select covers function is having issue(S) . ");
			if(((String)common.NB_excel_data_map.get("CD_Add_MaterialDamage")).equals("Yes") &&
					((String)common.NB_excel_data_map.get("CD_MaterialDamage")).equals("No")){		
				customAssert.assertTrue(common.funcMenuSelection("Navigate","Insured Properties"),"Issue while Navigating to Insured Properties  . ");
				customAssert.assertTrue(funcInsuredProperties(common.NB_excel_data_map), "Insured Property function is having issue(S) . ");
			}
			
			if(((String)common.NB_excel_data_map.get("CD_Add_Liability")).equals("Yes")&&
					((String)common.NB_excel_data_map.get("CD_Liability")).equals("No")){		
				customAssert.assertTrue(common.funcMenuSelection("Navigate","Employers Liability"),"Issue while Navigating to Employers Liability  . ");
				customAssert.assertTrue(common_CCF.funcEmployersLiability(common.NB_excel_data_map), "Employers Liability function is having issue(S) . ");
				customAssert.assertTrue(common.funcMenuSelection("Navigate","ELD Information"),"Issue while Navigating to ELD Information  . ");
				customAssert.assertTrue(common_CCF.funcELDInformation(common.NB_excel_data_map), "ELD Information function is having issue(S) . ");
				customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability"),"Issue while Navigating to Property Owners Liability  . ");
				customAssert.assertTrue(funcPropertyOwnersLiability(common.NB_excel_data_map), "Public Liability function is having issue(S) . ");
				customAssert.assertTrue(common.funcMenuSelection("Navigate","Property Owners Liability Information"),"Issue while Navigating to Products Liability  . ");
				customAssert.assertTrue(funcLiabilityInformation(common.NB_excel_data_map), "Liability Information function is having issue(S) . ");
				}
			if(((String)common.NB_excel_data_map.get("CD_Add_CyberandDataSecurity")).equals("Yes")&&
					((String)common.NB_excel_data_map.get("CD_CyberandDataSecurity")).equals("No")){		
				customAssert.assertTrue(common.funcMenuSelection("Navigate","Cyber and Data Security"),"Issue while Navigating to Cyber and Data Security screen . ");
				customAssert.assertTrue(common_CCF.funcCyberAndDataSecurity(common.NB_excel_data_map), "Cyber and Data Security function is having issue(S) . ");
				}
			if(((String)common.NB_excel_data_map.get("CD_Add_Terrorism")).equals("Yes")&&
					((String)common.NB_excel_data_map.get("CD_Terrorism")).equals("No")){		
				customAssert.assertTrue(common.funcMenuSelection("Navigate","Terrorism"),"Issue while Navigating to Terrorism screen . ");
				customAssert.assertTrue(common_CCF.funcTerrorism(common.NB_excel_data_map), "Terrorism function is having issue(S) . ");
				}
			
			if(((String)common.NB_excel_data_map.get("CD_Add_LegalExpenses")).equals("Yes")&&
					((String)common.NB_excel_data_map.get("CD_LegalExpenses")).equals("No")){		
				customAssert.assertTrue(common.funcMenuSelection("Navigate","Legal Expenses"),"Issue while Navigating to Legal Expenses screen . ");
				customAssert.assertTrue(common_CCF.funcLegalExpenses(common.NB_excel_data_map,CommonFunction.product,CommonFunction.businessEvent), "Legal Expenses function is having issue(S) . ");
				}
			
				customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Issue while Navigating to Premium Summary screen . ");
				//Assert.assertTrue(common.funcPremiumSummary(common.NB_excel_data_map,CommonFunction.product,CommonFunction.businessEvent,"RewindAddCover"));
				
				customAssert.assertTrue(common.funcButtonSelection("Put Rewind On Cover"));
				customAssert.assertTrue(common.funcSearchPolicy(common.NB_excel_data_map), "Policy Search function is having issue(S) . ");
				customAssert.assertTrue(common.funcVerifyPolicyStatus(common.NB_excel_data_map,CommonFunction.product,CommonFunction.businessEvent,"On Cover"), "Verify Policy Status (Submitted (Rewind)) function is having issue(S) . ");
				customAssert.assertTrue(common.funcPDFdocumentVerification_Rewind("Documents"), "Document verification function is having issue(S) . ");
		}
		
	}catch(Throwable t){
		return false;
		
	}
	
	return r_value;
}


//---------------------------------------------ajinkya-------------------------------------------------

public boolean funcCreateEndorsement(){
	
	boolean retvalue = true;
    Object dateobj = null;
    String testName = null;
    df = new SimpleDateFormat("dd/MM/yyyy");
    
    Map<Object,Object> data_map = null;
    
    switch(common.currentRunningFlow){
    
    case "MTA":
    	data_map = common.MTA_excel_data_map;
    	break;
    
    case "Renewal":
    	 data_map = common.Renewal_excel_data_map;
    	 break;
    	 
    
    
    }
    
    
    try{
    	
    	customAssert.assertTrue(common.funcMenuSelection("Navigate","Premium Summary"),"Navigation problem to Premium Summary page .");
    	customAssert.assertTrue(common.funcPageNavigation("Premium Summary", ""),"Premium Summary page is not loaded to perfrm MTA . ");
    	   
    	customAssert.assertTrue(common.funcButtonSelection("Create Endorsement"), "Unable to click on Create Endorsement button .");
    	
    	
    	       int ammendmet_period= Integer.parseInt(data_map.get("MTA_EndorsementPeriod").toString());
    	
    	       if(CommonFunction.businessEvent.equalsIgnoreCase("Renewal")){
    	    		if(ammendmet_period > Integer.parseInt((String)common.Renewal_excel_data_map.get("PS_Duration"))){
    	        		TestUtil.reportStatus("Amendement Period Should not be greater than Policy Duration", "Fail", true);
    	        		return false;
    	        	}
    	    	}else if(((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes") && ((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement")){
    	    		if(ammendmet_period > Integer.parseInt(((String)common.MTA_excel_data_map.get("MTA_EffectiveDays")).trim())){
    	    			if(Integer.parseInt((String)common.MTA_excel_data_map.get("MTA_EffectiveDays")) >= 3)
    	    				ammendmet_period = Integer.parseInt(((String)common.MTA_excel_data_map.get("MTA_EffectiveDays")).trim()) - 2;
    	    			else
    	    				ammendmet_period = Integer.parseInt(((String)common.MTA_excel_data_map.get("MTA_EffectiveDays")).trim());
    	    			common.MTA_excel_data_map.put("MTA_EndorsementPeriod",Integer.toString(ammendmet_period));
    	         	}
    	    	}else{
    	    		if(ammendmet_period >= Integer.parseInt((String)common.NB_excel_data_map.get("PS_Duration"))){
    	    			ammendmet_period = Integer.parseInt((String)common.NB_excel_data_map.get("PS_Duration")) - 2;
    	        		TestUtil.reportStatus("Amendement Period adjusted to create endrosement . ", "Info", true);
    	        		common.MTA_excel_data_map.put("MTA_EndorsementPeriod",Integer.toString(ammendmet_period));
    	        	}
    	    	}
    	       
    	
    	TimeZone uk_Instance = TimeZone.getTimeZone("Europe/London");
    	Calendar c = Calendar.getInstance(uk_Instance);
    	
    	if(((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes") && ((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy_Type")).equalsIgnoreCase("Endorsement")){
    		//dateobj = common.addDays(df.parse((String)common.MTA_excel_data_map.get("MTA_EffectiveDate")), ammendmet_period);
			SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	  	  	String d = (String)common.MTA_excel_data_map.get("MTA_EffectiveDate")+" 00:00:00";
  			TimeZone uk_timezone1 = TimeZone.getTimeZone("Europe/London");
			df.setTimeZone(uk_timezone1);	
		  	Date date = df.parse(d);
			df1.format(date);
			//dateobj = common.addDays(df.parse((String)common.Renewal_excel_data_map.get("PS_PolicyStartDate")), ammendmet_period);
  			dateobj = common.addDays(date, ammendmet_period);
    	}else if(((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("Yes") && ((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy_Type")).equalsIgnoreCase("New Business")){
				dateobj = common.addDays(df.parse((String)common.NB_excel_data_map.get("PS_PolicyStartDate")), ammendmet_period);
    	}else if(TestBase.businessEvent.equalsIgnoreCase("Renewal")){
    		
    		SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	  	  	String d = (String)common.Renewal_excel_data_map.get("PS_PolicyStartDate")+" 00:00:00";
  			TimeZone uk_timezone1 = TimeZone.getTimeZone("Europe/London");
			df.setTimeZone(uk_timezone1);	
		  	Date date = df.parse(d);
			df1.format(date);
			//dateobj = common.addDays(df.parse((String)common.Renewal_excel_data_map.get("PS_PolicyStartDate")), ammendmet_period);
  			dateobj = common.addDays(date, ammendmet_period);
    		
    		//dateobj = common.addDays(df.parse((String)common.Renewal_excel_data_map.get("PS_PolicyStartDate")), ammendmet_period);
      	}else if(TestBase.businessEvent.equalsIgnoreCase("MTA")){
      		/*if(((String)common.MTA_excel_data_map.get("MTA_ExistingPolicy")).equalsIgnoreCase("No")) {
      			if(((String)common.NB_excel_data_map.get("QC_isDefaultQuoteDates")).equalsIgnoreCase("No")){
      				dateobj = common.addDays(df.parse((String)common.NB_excel_data_map.get("PS_PolicyStartDate")), ammendmet_period);
      			}
      		} 
      			c.add(Calendar.DATE, ammendmet_period);
         	  	dateobj = df.parse(df.format(c.getTime()));*/
      		
      		//dateobj = common.addDays(df.parse((String)common.NB_excel_data_map.get("PS_PolicyStartDate")), ammendmet_period);
	  	  	SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	  	  	TimeZone uk_timezone1 = TimeZone.getTimeZone("Europe/London");
			df.setTimeZone(uk_timezone1);	
		  	Date date = new Date();
			df1.format(date);
			dateobj = common.addDays(date, ammendmet_period);
      		
      	}else{
      		
    		c.add(Calendar.DATE, ammendmet_period);
      	  	dateobj = df.parse(df.format(c.getTime()));
      	
    	}
    	
    	customAssert.assertTrue(k.Click("POB_Endorsement_effective_date"), "Unable to enter Endorsement effective Date.");
        customAssert.assertTrue(k.Type("POB_Endorsement_effective_date", dateobj.toString()), "Unable to Enter Endorsement effective Date .");
        customAssert.assertTrue(k.Click("calander_btn"), "Unable to click on done button in calander.");
        if(TestBase.businessEvent.equalsIgnoreCase("Renewal")){
	      	WriteDataToXl(TestBase.product+"_"+common.currentRunningFlow, "MTA_Endorsement", (String)data_map.get("Automation Key"), "MTA_EffectiveDate", k.getAttribute("SPI_Endorsement_eff_date", "value"),data_map);
	    }else{
	      	WriteDataToXl(TestBase.product+"_"+TestBase.businessEvent, "MTA_Endorsement", (String)data_map.get("Automation Key"), "MTA_EffectiveDate", k.getAttribute("SPI_Endorsement_eff_date", "value"),data_map);
	    }
        customAssert.assertTrue(k.Input("POB_Reason_for_Endorsement", (String)data_map.get("MTA_Reason_for_Endorsement")),"Unable to Enter Reason for Endorsement");
    	customAssert.assertTrue(k.Click("POB_Create_Endorsement_button"), "Unable to click on create Endorsement button ");
      //Writing to MTA Excel
    	WriteDataToXl(TestBase.product+"_"+common.currentRunningFlow, "MTA_Endorsement", (String)data_map.get("Automation Key"), "MTA_PolicyNumber", k.getText("SPI_MTA_policy_number"),data_map);
    	
    	
    	if(common.currentRunningFlow.equals("MTA")){
    		if(((String)data_map.get("MTA_Status")).equals("Endorsement Rewind"))
    			WriteDataToXl(TestBase.product+"_"+common.currentRunningFlow, "MTA_Endorsement", testName, "MTA_isMTARewind", "Y",data_map);
    		else
    			WriteDataToXl(TestBase.product+"_"+common.currentRunningFlow, "MTA_Endorsement", testName, "MTA_isMTARewind", "N",data_map);
    	}
        
    	TestUtil.reportStatus("Create Endorsement Details filled successfully . ", "Info", true);
		
        return retvalue;
        
        
    }catch (ParseException e) {
		System.out.println("Unable to Parse Endorsement Dates - "+e.getMessage());
		return false;
	}
	catch(Throwable t) {
         String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
         TestUtil.reportFunctionFailed("Failed in "+methodName+" function");  
         k.reportErr("Failed in "+methodName+" function", t);
         return false;
  }  
    
    
}


public boolean funcUpdateCoverDetails_MTA(Map<Object, Object> map_data){
	   
	try {
			customAssert.assertTrue(common.funcPageNavigation("Covers", ""),"Cover page is having issue(S)");
			String coverName = null;
			String c_locator = null;
			k.pressDownKeyonPage();
			String all_cover = ObjectMap.properties.getProperty(CommonFunction.product+"_CD_AllCovers");
			String[] split_all_covers = all_cover.split(",");
			for(String coverWithLocator : split_all_covers){
				String coverWithoutLocator = coverWithLocator.split("_")[0];
				try{
					//CoversDetails_data_list.add(coverWithoutLocator);
					coverName = coverWithLocator.split("_")[0];	
					c_locator = coverWithLocator.split("_")[1];
					k.waitTwoSeconds();
					if(c_locator.equals("md")){
						
						
						if (!driver.findElement(By.xpath("//*[contains(@name,'"+c_locator+"_selected')]")).isSelected()){
							if(((String) map_data.get("CD_"+coverName)).equalsIgnoreCase("No"))
								continue;
							else
					 			customAssert.assertTrue(common.selectCover(coverWithLocator,map_data), "Select covers function is having issue(S) . ");
						}else{
							if(((String) map_data.get("CD_"+coverName)).equalsIgnoreCase("Yes"))
								continue;
							else
								customAssert.assertTrue(common.deSelectCovers(coverWithLocator,map_data), "Select covers function is having issue(S) . ");
						}
					
					}else if(c_locator.equals("PEL")){
					
					}else if(c_locator.equals("cob_hcp")){
							if(((String)map_data.get("QC_AgencyName")).equalsIgnoreCase("Arthur J. Gallagher (UK) Ltd")){
								
								if (!driver.findElement(By.xpath("//*[contains(@name,'"+c_locator+"_selected')]")).isSelected()){
									JavascriptExecutor j_exe = (JavascriptExecutor) driver;
									j_exe.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[contains(@name,'"+c_locator+"_selected')]")));
									
										if(((String) map_data.get("CD_"+coverName)).equalsIgnoreCase("No"))
											continue;
										else
								 			customAssert.assertTrue(common.selectCover(coverWithLocator,map_data), "Select covers function is having issue(S) . ");
															
									}else{
										if(((String) map_data.get("CD_"+coverName)).equalsIgnoreCase("Yes"))
											continue;
										else
											customAssert.assertTrue(common.deSelectCovers(coverWithLocator,map_data), "Select covers function is having issue(S) . ");
									 }
							
							}
							else{
								
							}
					}
					else{
				
						if (!driver.findElement(By.xpath("//*[contains(@name,'"+c_locator+"_selected')]")).isSelected()){
							JavascriptExecutor j_exe = (JavascriptExecutor) driver;
							j_exe.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[contains(@name,'"+c_locator+"_selected')]")));
							
								if(((String) map_data.get("CD_"+coverName)).equalsIgnoreCase("No"))
									continue;
								else
						 			customAssert.assertTrue(common.selectCover(coverWithLocator,map_data), "Select covers function is having issue(S) . ");
													
							}else{
								if(((String) map_data.get("CD_"+coverName)).equalsIgnoreCase("Yes"))
									continue;
								else
									customAssert.assertTrue(common.deSelectCovers(coverWithLocator,map_data), "Select covers function is having issue(S) . ");
							 }
					
					}	
					customAssert.assertTrue(k.Click("CCF_Btn_Save"), "Unable to click on Save Button on Covers .");
					
				}catch(Throwable tt){
					System.out.println("Error while Updating Cover data for MTA - "+coverWithoutLocator);
					break;
				}
	 		}
 	 
	 	  return true;
		} catch (Exception e) {
			return false;
		}
	   
   }

//----------------------------------------------MTA Specific functions------------------------------------------------------

	
// This method rounds whole number part. E.g. 1234.56 -> 1235.56
public String WholeNumberRoundedOff(String number) {
    
	try {
		number = Double.toString(Math.round(Double.valueOf(number)*100.0)/100.0);
	
	   if(number.contains(".")){
		   String replacedString = number.replace(".", ",");
		   String[] stringArray = replacedString.split(",");
		   if(stringArray[1].length()>1){
		   
              if(Integer.parseInt(stringArray[1]) > 49){
             	 double formatedNumber = Double.parseDouble(number) + 1;
                  String roundedNumber = f.format(formatedNumber);
                  return roundedNumber;
              }else{
             	 double formatedNumber = Double.parseDouble(number);
                  String roundedNumber = f.format(formatedNumber);
                  return roundedNumber;
              }
              
		   }else if(stringArray[1].length()==1){
			   if(Integer.parseInt(stringArray[1]) >= 5){
	             	 double formatedNumber = Double.parseDouble(number) + 1;
	                  String roundedNumber = f.format(formatedNumber);
	                  return roundedNumber;
	              }else{
	             	 double formatedNumber = Double.parseDouble(number);
	                  String roundedNumber = f.format(formatedNumber);
	                  return roundedNumber;
	              }
		   }
        
	   }else{
		   String formatedNumber = f.format(Double.parseDouble(number));
		   return formatedNumber;
	   }
	return number;
	}catch(Throwable t) {
		System.out.println("Number Format Exception - "+t.getMessage());
	}
	return number;
}
//End of CommonFunction_POB
}
