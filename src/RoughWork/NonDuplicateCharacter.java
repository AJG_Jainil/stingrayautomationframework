package RoughWork;

import java.util.ArrayList;

public class NonDuplicateCharacter {

	public static void main(String[] args) {
		
		
		String name = "AanshiSharvilShah";
		int count = 0;
		
		ArrayList<Character> arr = new ArrayList<>();
		
		for(int i=0;i<name.length();i++){
			
			if(!arr.contains(name.charAt(i))){
				for(int j=0;j<name.length();j++){
					
					if(name.charAt(i)==name.charAt(j)){
						
						count++;
					}
					
				}
				
				if(count>1){
					System.out.println(name.charAt(i)+" : Duplicate character");
				}else{
					System.out.println(name.charAt(i)+" : Non Duplicate character");
				}
				arr.add(name.charAt(i));
				count = 0;
			}
			
		}
		
	}

}
